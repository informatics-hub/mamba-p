<?xml version="1.0" encoding="UTF-8"?>
<!-- Created with Jaspersoft Studio version 6.6.0.final using JasperReports Library version 6.6.0  -->
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="PTracker_Visit_Count(List)" pageWidth="555" pageHeight="802" columnWidth="555" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" uuid="8609599b-7751-4156-a1dc-6d14489a7f42">
	<property name="com.jaspersoft.studio.data.sql.tables" value=""/>
	<property name="ireport.jasperserver.url" value="https://ptracker-bi.globalhealthapp.net/jasperserver/"/>
	<property name="ireport.jasperserver.user" value="ezekiel"/>
	<property name="ireport.jasperserver.report.resource" value="/reports/PTracker_Data_Quality_Reports/PTracker_Visit_Count_List__files/main_jrxml"/>
	<property name="ireport.jasperserver.reportUnit" value="/reports/PTracker_Data_Quality_Reports/PTracker_Visit_Count_List_"/>
	<property name="com.jaspersoft.studio.data.sql.SQLQueryDesigner.sash.w1" value="285"/>
	<property name="com.jaspersoft.studio.data.sql.SQLQueryDesigner.sash.w2" value="707"/>
	<property name="com.jaspersoft.studio.data.defaultdataadapter" value="ptracker_test_server"/>
	<template><![CDATA["repo:UC_Styles.jrtx"]]></template>
	<parameter name="facility" class="java.util.Collection">
		<parameterDescription><![CDATA[facility of delivery]]></parameterDescription>
	</parameter>
	<parameter name="FromDate" class="java.sql.Date">
		<parameterDescription><![CDATA[filter start date]]></parameterDescription>
	</parameter>
	<parameter name="ToDate" class="java.sql.Date">
		<parameterDescription><![CDATA[filter end date]]></parameterDescription>
	</parameter>
	<queryString>
		<![CDATA[SELECT
	region,
	district,
	facility,
	COUNT(DISTINCT(IF( visit_uuid = '2549af50-75c8-4aeb-87ca-4bb2cef6c69a' AND anc_visit_type = 'New ANC Visit', encounter_uuid, NULL ))) AS first_anc,
	COUNT(DISTINCT(IF( visit_uuid = '2549af50-75c8-4aeb-87ca-4bb2cef6c69a' AND anc_visit_type IS NULL OR anc_visit_type = 'Return ANC Visit' , encounter_uuid, NULL))) AS followup_anc,
	COUNT(DISTINCT(IF( visit_uuid = '2678423c-0523-4d76-b0da-18177b439eed', encounter_uuid, NULL ))) AS labor_and_delivery,
	COUNT(DISTINCT(IF( visit_uuid = '269bcc7f-04f8-4ddc-883d-7a3a0d569aad', encounter_uuid, NULL ))) AS mother_pnc,
	COUNT(DISTINCT(IF( visit_uuid = 'af1f1b24-d2e8-4282-b308-0bf79b365584', encounter_uuid, NULL ))) AS infant_pnc,
	COUNT(DISTINCT(encounter_uuid)) AS total_visit 
FROM mds_encounter_mch
					WHERE 
					(
			          (encounter_datetime BETWEEN $P{FromDate} AND $P{ToDate})
			          OR
			          ( ($P{FromDate} = '' OR $P{FromDate} IS NULL) or ($P{ToDate} = '' OR $P{ToDate} IS NULL) )
			       )
			       AND voided = 0
GROUP BY
	facility;]]>
	</queryString>
	<field name="region" class="java.lang.String"/>
	<field name="district" class="java.lang.String"/>
	<field name="facility" class="java.lang.String"/>
	<field name="first_anc" class="java.lang.Long"/>
	<field name="followup_anc" class="java.lang.Long"/>
	<field name="labor_and_delivery" class="java.lang.Long"/>
	<field name="mother_pnc" class="java.lang.Long"/>
	<field name="infant_pnc" class="java.lang.Long"/>
	<field name="total_visit" class="java.lang.Long"/>
	<variable name="first_anc" class="java.lang.Long" calculation="Sum">
		<variableExpression><![CDATA[$F{first_anc}]]></variableExpression>
	</variable>
	<variable name="followup_anc" class="java.lang.Long" calculation="Sum">
		<variableExpression><![CDATA[$F{followup_anc}]]></variableExpression>
	</variable>
	<variable name="labor_and_delivery" class="java.lang.Long" calculation="Sum">
		<variableExpression><![CDATA[$F{labor_and_delivery}]]></variableExpression>
	</variable>
	<variable name="mother_pnc" class="java.lang.Long" calculation="Sum">
		<variableExpression><![CDATA[$F{mother_pnc}]]></variableExpression>
	</variable>
	<variable name="infant_pnc" class="java.lang.Long" calculation="Sum">
		<variableExpression><![CDATA[$F{infant_pnc}]]></variableExpression>
	</variable>
	<variable name="total_visit" class="java.lang.Long" calculation="Sum">
		<variableExpression><![CDATA[$F{total_visit}]]></variableExpression>
	</variable>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="37">
			<staticText>
				<reportElement style="UC Report Title" x="2" y="0" width="553" height="30" uuid="150f0e8b-cb82-4b53-99c2-36593e57d0a4"/>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Total number of visit counts]]></text>
			</staticText>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="72">
			<staticText>
				<reportElement style="UC Table Header" x="2" y="52" width="98" height="20" uuid="e67e8882-9d13-4c54-9175-29621588f89f">
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
				</reportElement>
				<text><![CDATA[Facility Name]]></text>
			</staticText>
			<staticText>
				<reportElement style="UC Table Header Number" x="105" y="42" width="65" height="30" uuid="dd1b1502-267f-4384-87a9-6b205c6b5168">
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
				</reportElement>
				<textElement textAlignment="Center"/>
				<text><![CDATA[ANC First Visit]]></text>
			</staticText>
			<staticText>
				<reportElement style="UC Table Header Number" x="180" y="42" width="60" height="30" uuid="45821ebb-d2af-45df-81d4-8a586013012c">
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
				</reportElement>
				<textElement textAlignment="Center"/>
				<text><![CDATA[ANC follow-up visit]]></text>
			</staticText>
			<staticText>
				<reportElement style="UC Table Header Number" x="250" y="42" width="60" height="30" uuid="e5db70bf-c22b-4de2-801e-37c4291b3565">
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
				</reportElement>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Deliveries (L&D)]]></text>
			</staticText>
			<staticText>
				<reportElement style="UC Table Header Number" x="320" y="42" width="60" height="30" uuid="29ef9b1c-a28e-48b7-8ddb-4a8eb6df881d">
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
				</reportElement>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Mother PNC]]></text>
			</staticText>
			<staticText>
				<reportElement style="UC Table Header Number" x="460" y="42" width="81" height="30" uuid="78695b28-ab6f-467a-bdfc-0248e017318a">
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
				</reportElement>
				<textElement textAlignment="Center"/>
				<text><![CDATA[Total No. of visits]]></text>
			</staticText>
			<staticText>
				<reportElement style="UC Table Header Number" x="390" y="50" width="60" height="20" uuid="17f5ba71-a1b6-4d31-94fe-c03a47711697">
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
				</reportElement>
				<text><![CDATA[Infant PNC]]></text>
			</staticText>
			<textField pattern="MMMMM dd, yyyy">
				<reportElement x="192" y="18" width="100" height="15" uuid="60d3458f-9336-4ff2-b753-6b3213f1875e">
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
				</reportElement>
				<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="110" y="18" width="72" height="15" uuid="13b7f6fc-c62f-4297-9def-357af761c831">
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
				</reportElement>
				<textElement>
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Generated on:]]></text>
			</staticText>
			<rectangle>
				<reportElement x="0" y="37" width="555" height="2" forecolor="#FFFFFF" backcolor="#6E6E6E" uuid="4a345699-54a8-4047-b2f3-c3466f9188a4"/>
			</rectangle>
			<staticText>
				<reportElement x="110" y="0" width="75" height="15" uuid="38b253de-6b7b-4a97-8f56-0983252c287b"/>
				<textElement>
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Report period:]]></text>
			</staticText>
			<textField>
				<reportElement x="237" y="0" width="73" height="15" uuid="425a5f5a-ac85-481a-ba38-2b1a42448493">
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
				</reportElement>
				<textFieldExpression><![CDATA[new SimpleDateFormat("dd/MM/yyyy").format($P{FromDate})]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="366" y="0" width="71" height="15" uuid="00ab63f6-a8bd-44de-9232-f567eedbed09">
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
				</reportElement>
				<textFieldExpression><![CDATA[new SimpleDateFormat("dd/MM/yyyy").format($P{ToDate})]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="192" y="0" width="34" height="15" uuid="221b86df-3343-49b9-9649-9660dd3db9c8">
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
				</reportElement>
				<textElement>
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[From:]]></text>
			</staticText>
			<staticText>
				<reportElement x="328" y="0" width="24" height="15" uuid="e020a218-eb06-4c43-ad50-19c596b6ca0d"/>
				<textElement>
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[To:]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="26" splitType="Stretch">
			<textField>
				<reportElement style="UC Table Row" x="2" y="4" width="98" height="20" uuid="b64bae1b-7eb0-42f0-aa88-012facbc6d9f">
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
				</reportElement>
				<textElement>
					<font isBold="false" isItalic="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{facility}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="UC Table Row Number" x="105" y="4" width="60" height="20" uuid="1e914b7d-10e4-45cf-8345-11ca4f45734a">
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
				</reportElement>
				<textFieldExpression><![CDATA[$F{first_anc}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="UC Table Row Number" x="250" y="4" width="60" height="20" uuid="e6760cad-ceba-4260-9121-86403eb23ffa">
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
				</reportElement>
				<textFieldExpression><![CDATA[$F{labor_and_delivery}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="UC Table Row Number" x="320" y="4" width="60" height="20" uuid="7a3a4423-cf6a-4048-adc4-5500c79b88c8">
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
				</reportElement>
				<textFieldExpression><![CDATA[$F{mother_pnc}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="UC Table Row Number" x="460" y="4" width="81" height="20" uuid="bde199b4-2ab2-414c-a9ea-205bbc12722e"/>
				<textFieldExpression><![CDATA[$F{total_visit}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="UC Table Row Number" x="180" y="4" width="60" height="20" uuid="cb031858-99cc-4328-8d24-d25f332e5347">
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
				</reportElement>
				<textFieldExpression><![CDATA[$F{followup_anc}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="UC Table Row Number" x="390" y="4" width="60" height="20" uuid="e6d8727f-6e78-4cbc-ae3d-b1280e944c91">
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
				</reportElement>
				<textFieldExpression><![CDATA[$F{infant_pnc}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<summary>
		<band height="25" splitType="Stretch">
			<textField>
				<reportElement style="UC Table Header Number" x="105" y="3" width="60" height="20" uuid="78b3011a-ef31-47f6-a997-62c8f57aef10">
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
				</reportElement>
				<textFieldExpression><![CDATA[$V{first_anc}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement style="UC Table Header" x="2" y="3" width="98" height="20" uuid="4200c4d2-9008-4c68-8331-3164972fd789">
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
				</reportElement>
				<text><![CDATA[Totals]]></text>
			</staticText>
			<textField>
				<reportElement style="UC Table Header Number" x="250" y="3" width="60" height="20" uuid="bf907709-49bf-4a07-b999-b830e664706f">
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
				</reportElement>
				<textFieldExpression><![CDATA[$V{labor_and_delivery}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="UC Table Header Number" x="180" y="3" width="60" height="20" uuid="659c7012-bdb7-436d-9cea-ebd0ac72cb3c">
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
				</reportElement>
				<textFieldExpression><![CDATA[$V{followup_anc}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="UC Table Header Number" x="460" y="3" width="81" height="20" uuid="47d38555-f249-4b68-bc11-a409b3cb4733">
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
				</reportElement>
				<textFieldExpression><![CDATA[$V{total_visit}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="UC Table Header Number" x="320" y="3" width="60" height="20" uuid="92301806-9205-47b4-b9db-42ca4f4e6650">
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
				</reportElement>
				<textFieldExpression><![CDATA[$V{mother_pnc}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="UC Table Header Number" x="390" y="3" width="60" height="20" uuid="9a047364-b230-4a95-b4dc-25cbb8bd29ef">
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.y" value="pixel"/>
				</reportElement>
				<textFieldExpression><![CDATA[$V{infant_pnc}]]></textFieldExpression>
			</textField>
		</band>
	</summary>
</jasperReport>
