USE staging_ptracker_test;
GO

-- $BEGIN

DECLARE @query_drop_fk_constraint as NVARCHAR(MAX);
DECLARE @cursor_drop_fk_constraint as CURSOR;

SET @cursor_drop_fk_constraint = CURSOR FOR 
select ' alter table ' + schema_name(Schema_id)+'.'+ object_name(parent_object_id)
+ '  DROP CONSTRAINT  ' +  name  as query_drop_foreign_key  from sys.foreign_keys f1;

OPEN @cursor_drop_fk_constraint;
FETCH NEXT FROM @cursor_drop_fk_constraint INTO @query_drop_fk_constraint;

WHILE @@FETCH_STATUS = 0
BEGIN
EXECUTE sp_executesql @query_drop_fk_constraint;
FETCH NEXT FROM @cursor_drop_fk_constraint INTO @query_drop_fk_constraint;
END

CLOSE @cursor_drop_fk_constraint;
DEALLOCATE @cursor_drop_fk_constraint;

-- $END