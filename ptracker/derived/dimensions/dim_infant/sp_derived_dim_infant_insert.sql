USE [staging_ptracker_test]
GO

TRUNCATE TABLE [derived].[dim_infant];

-- $BEGIN

    WITH infant AS
    (
        SELECT DISTINCT 
            c.[client_id],
            c.[openmrs_patient_identifier],
            c.[address],
            c.[first_name],
            c.[last_name],
            c.[phone],
            c.[sex],
            c.[date_of_birth],
            c.[date_of_birth_estimated],
            c.[patient_date_created],
            c.[district],
            c.[region],
            c.[facility_id],
            mi.ptracker_identifier AS infant_number,
            'Postnatal' AS source 
        FROM
            base.dim_client c
        INNER JOIN
            base.dim_encounter e
        ON
            c.client_id=e.client_id
        INNER JOIN
            base.fact_infant_postnatal_visit mi 
        ON
            mi.encounter_id=e.encounter_id
        WHERE 
            FLOOR(DATEDIFF(DAY, c.date_of_birth, GETDATE())/ 365.25) <= 5 AND c.patient_voided=0

        UNION
        
        SELECT 
            NEWID() AS [client_id],
            NULL AS [openmrs_patient_identifier],
            NULL AS [address],
            NULL AS [first_name],
            NULL AS [last_name],
            NULL AS [phone],
            mi.[sex],
            mi.[dob],
            NULL AS [date_of_birth_estimated],
            NULL AS [patient_date_created],
            f.district_name [district],
            f.region_name [region],
            f.[facility_id] ,
            mi.infant_number,
            'Maternity' AS source 
        FROM
            base.fact_maternity_infant_visit mi 
        LEFT JOIN
            base.dim_encounter e
        ON
            mi.encounter_id=e.encounter_id
        LEFT JOIN
            base.dim_facility f
        ON
            e.facility_id=f.facility_id
        WHERE 
            NOT EXISTS (SELECT ptracker_identifier FROM base.fact_infant_postnatal_visit WHERE mi.infant_number = ptracker_identifier )
    )
    INSERT INTO [derived].[dim_infant](
        [infant_id],
        [openmrs_patient_identifier],
        [address],
        [first_name],
        [last_name],
        [phone],
        [sex],
        [date_of_birth],
        [date_of_birth_estimated],
        [patient_date_created],
        [district],
        [region],
        [facility_id],
        [pregnancy_id],
        [source],
        [ptracker_identifier]
    )
    SELECT DISTINCT
        [client_id],
        [openmrs_patient_identifier],
        [address],
        [first_name],
        [last_name],
        [phone],
        [sex],
        [date_of_birth],
        [date_of_birth_estimated],
        [patient_date_created],
        [district],
        [region],
        [facility_id],
        p.pregnancy_id,
        i.[source],
        i.infant_number
    FROM
        infant i
    LEFT JOIN
        [derived].[dim_pregnancy] p
    ON
        p.ptracker_identifier=LEFT (i.infant_number, LEN(i.infant_number)-1)

-- $END

SELECT 
    COUNT(*) as total_clients
FROM 
    [derived].[dim_infant]

SELECT 
    TOP 400 * 
FROM 
    [derived].[dim_infant] 