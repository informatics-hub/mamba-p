USE [staging_ptracker_test]
GO

-- $BEGIN

    UPDATE 
        [derived].[dim_infant]
    SET 
        [sex] = 
            CASE WHEN 
                [sex] = 'M' 
            THEN 
                'Male'
            WHEN
                [sex] = 'F' 
            THEN
                'Female'
            END
    WHERE sex IN ('M','F')
    ;
        
-- $END
SELECT 
    TOP 400 * 
FROM 
    [derived].[dim_infant]

