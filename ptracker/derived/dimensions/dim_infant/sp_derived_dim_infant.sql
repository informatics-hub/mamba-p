USE [staging_ptracker_test]
GO
-- $BEGIN

    EXEC sp_derived_dim_infant_create;
    EXEC sp_derived_dim_infant_insert;
    EXEC sp_derived_dim_infant_update;

-- $END