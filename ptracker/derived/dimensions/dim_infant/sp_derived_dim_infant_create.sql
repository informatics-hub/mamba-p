USE [staging_ptracker_test]
GO

DROP TABLE IF exists [derived].[dim_infant];

-- $BEGIN

    CREATE TABLE [derived].[dim_infant](
        [infant_id] [UNIQUEIDENTIFIER] NOT NULL,
        [openmrs_patient_identifier] [int] NULL,
        [address] [nvarchar] (255) NULL,
        [first_name] [nvarchar] (255) NULL,
        [last_name] [nvarchar] (255) NULL,
        [phone] [nvarchar] (255) NULL,
        [sex] [nvarchar] (255) NULL,
        [date_of_birth] [date] NULL,
        [date_of_birth_estimated] [tinyint] NULL,
        [patient_date_created] [date] NULL,
        [district] [nvarchar] (255) NULL,
        [region] [nvarchar] (255) NULL,
        [facility_id] [UNIQUEIDENTIFIER] NULL,
        [pregnancy_id] [UNIQUEIDENTIFIER] NULL,
        [source] [NVARCHAR] (255) NULL,
        [ptracker_identifier] [NVARCHAR] (255) NULL

    );

    ALTER TABLE [derived].[dim_infant] ADD CONSTRAINT [PK_dim_infant_derived] PRIMARY KEY ([infant_id]);

    ALTER TABLE [derived].[dim_infant] ADD CONSTRAINT FK_dim_infant_facility_derived FOREIGN KEY ([facility_id]) REFERENCES [derived].[dim_facility]([facility_id]);
    
    ALTER TABLE [derived].[dim_infant] ADD CONSTRAINT FK_dim_infant_pregnancy_derived FOREIGN KEY([pregnancy_id]) REFERENCES [derived].[dim_pregnancy]([pregnancy_id]);

-- $END