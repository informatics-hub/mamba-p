USE [staging_ptracker_test]
GO
-- $BEGIN

    EXEC sp_derived_dim_pregnancy_create;
    EXEC sp_derived_dim_pregnancy_insert;
    EXEC sp_derived_dim_pregnancy_update;
-- $END