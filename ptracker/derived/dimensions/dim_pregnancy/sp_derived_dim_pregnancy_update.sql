USE [staging_ptracker_test]
GO

-- $BEGIN
    UPDATE [derived].[dim_pregnancy]
    SET
        [is_currently_pregnant] = 0
    WHERE [ptracker_identifier] IN (SELECT [ptracker_identifier]
                                    FROM [base].[fact_mother_postnatal_visit]
                                    UNION
                                    SELECT [ptracker_identifier]
                                    FROM [base].[fact_maternity_visit]);
-- $END

SELECT TOP 400
    *
FROM [derived].[dim_pregnancy];