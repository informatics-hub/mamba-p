USE [staging_ptracker_test]
GO
TRUNCATE TABLE [derived].[dim_pregnancy];
-- $BEGIN
    --Insert Pregnencies recorded at Antenatal clinic
    WITH pregnancies AS
    (
        SELECT 
            mother_id,
            ptracker_identifier,
            source,
            is_currently_pregnant
        FROM
            (
                SELECT DISTINCT
                    mo.mother_id mother_id,
                    anc.ptracker_identifier ptracker_identifier,
                    'Antenatal clinic' source,
                    1 is_currently_pregnant
                FROM [derived].[dim_mother] mo
                INNER JOIN [base].[dim_encounter] e
                ON mo.[mother_id] = e.[client_id]
                INNER JOIN [base].[fact_antenatal_visit] anc
                ON anc.[encounter_id] = e.[encounter_id]
            ) prg
        WHERE ptracker_identifier is not null
        UNION ALL
        --Insert Pregnencies recorded at Maternity clinic that do not exist in fact_antenatal_visit
        SELECT 
            mother_id,
            ptracker_identifier,
            source,
            NULL
        FROM
            (
                SELECT DISTINCT
                    mo.mother_id,
                    ma.ptracker_identifier,
                    'Maternity clinic' source
                FROM [derived].[dim_mother] mo
                INNER JOIN [base].[dim_encounter] e
                ON mo.[mother_id] = e.[client_id]
                INNER JOIN [base].[fact_maternity_visit] ma
                ON ma.[encounter_id] = e.[encounter_id]
            ) prg
        WHERE ptracker_identifier IS NOT NULL
            AND NOT EXISTS (SELECT DISTINCT 1
                                            FROM [base].[fact_antenatal_visit] WHERE ptracker_identifier = prg.ptracker_identifier)
        UNION ALL
        --Insert Pregnencies recorded at Postnatal clinic that do not exist in fact_antenatal_visit & fact_maternity_visit
        SELECT 
            mother_id,
            ptracker_identifier,
            source,
            NULL
        FROM
            (
                SELECT DISTINCT
                    mo.mother_id,
                    pnc.ptracker_identifier,
                    'Postnatal clinic' source
                FROM [derived].[dim_mother] mo
                INNER JOIN [base].[dim_encounter] e
                ON mo.[mother_id] = e.[client_id]
                INNER JOIN [base].[fact_mother_postnatal_visit] pnc
                ON pnc.[encounter_id] = e.[encounter_id]
            ) prg
        WHERE ptracker_identifier IS NOT NULL
        AND NOT EXISTS (SELECT 1 FROM (SELECT DISTINCT ptracker_identifier
                                            FROM [base].[fact_antenatal_visit]
                                            UNION
                                            SELECT DISTINCT ptracker_identifier
                                            FROM [base].[fact_maternity_visit])a  WHERE a.ptracker_identifier=prg.ptracker_identifier)
    )
    INSERT INTO [derived].[dim_pregnancy]
    (
        [pregnancy_id],
        [mother_id],
        [ptracker_identifier],
        [source],
        [is_currently_pregnant]
    )
    SELECT 
        NEWID(), 
        * 
    FROM pregnancies;
-- $END

SELECT TOP 400
    *
FROM [derived].[dim_pregnancy];