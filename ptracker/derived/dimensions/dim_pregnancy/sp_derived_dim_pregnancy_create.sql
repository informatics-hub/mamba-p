USE [staging_ptracker_test]
GO

DROP TABLE IF EXISTS [derived].[dim_pregnancy];

-- $BEGIN

    CREATE TABLE [derived].[dim_pregnancy](
        [pregnancy_id] [UNIQUEIDENTIFIER] NOT NULL,
        [mother_id] [UNIQUEIDENTIFIER] NOT NULL,
        [ptracker_identifier] [NVARCHAR] (255) NOT NULL,
        [source] [NVARCHAR] (255) NOT NULL,
        [is_currently_pregnant] [TINYINT] NULL
    );

    ALTER TABLE [derived].[dim_pregnancy] ADD CONSTRAINT [PK_dim_pregnancy_derived] PRIMARY KEY ([pregnancy_id]);
    ALTER TABLE [derived].[dim_pregnancy] ADD CONSTRAINT [FK_dim_pregnancy_mother_derived] FOREIGN KEY ([mother_id]) REFERENCES [derived].[dim_mother]([mother_id]);
-- $END