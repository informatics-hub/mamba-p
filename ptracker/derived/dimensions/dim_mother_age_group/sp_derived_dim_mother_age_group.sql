USE [staging_ptracker_test]
GO
-- $BEGIN

    EXEC sp_derived_dim_mother_age_group_create;
    EXEC sp_derived_dim_mother_age_group_insert;
    EXEC sp_derived_dim_mother_age_group_create_related_dimension;
    
-- $END