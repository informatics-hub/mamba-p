USE [staging_ptracker_test]
GO

DROP TABLE IF exists [derived].[dim_mother_age_group];

-- $BEGIN

    CREATE TABLE [derived].[dim_mother_age_group](
        [mother_age_group_id] UNIQUEIDENTIFIER NOT NULL,
        [age] [int] NOT NULL,
        [age_p_tracker_monthly_interval] [nvarchar] (255) NULL,
        [age_five_year_interval] [nvarchar] (255) NULL
    );

    ALTER TABLE [derived].[dim_mother_age_group] ADD CONSTRAINT [PK_dim_mother_age_group_derived] PRIMARY KEY ([mother_age_group_id]);

-- $END