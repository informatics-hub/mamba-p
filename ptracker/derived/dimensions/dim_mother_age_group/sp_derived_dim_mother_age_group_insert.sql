USE [staging_ptracker_test]
GO

TRUNCATE TABLE [derived].[dim_mother_age_group]

-- $BEGIN

    DECLARE @MaxAge INT = 150
	DECLARE @RowNum INT = 1
	DECLARE @Age INT = 1;
	
	DECLARE @monthly_interval_group CHAR(30)
	DECLARE @five_year_interval_group CHAR(30)

	WHILE @Age <= @MaxAge
		BEGIN
			SET @monthly_interval_group = 
				CASE 
					WHEN @Age >= 15 AND @Age <= 24 THEN '15 - 24 years'
					WHEN @Age >= 25 AND @Age <= 49 THEN '25 - 49 years'
				END
			SET @five_year_interval_group = 
                CASE 
                    WHEN @Age < 10 THEN '<10'
                    WHEN @Age >= 10 AND @Age <= 14 THEN '10 - 14'
                    WHEN @Age >= 15 AND @Age <= 19 THEN '15 - 19'
                    WHEN @Age >= 20 AND @Age <= 24 THEN '20 - 24'
                    WHEN @Age >= 25 AND @Age <= 29 THEN '25 - 29'
                    WHEN @Age >= 30 AND @Age <= 34 THEN '30 - 34'
                    WHEN @Age >= 35 AND @Age <= 39 THEN '35 - 39'
                    WHEN @Age >= 40 AND @Age <= 44 THEN '40 - 44'
                    WHEN @Age >= 45 AND @Age <= 49 THEN '45 - 49'
                    WHEN @Age >= 50 THEN '50+'

                END

			INSERT INTO [derived].[dim_mother_age_group](
				[mother_age_group_id],
				[age],
        		[age_p_tracker_monthly_interval],
				[age_five_year_interval]
			)
			VALUES( 
				NEWID(),
				@Age,
				@monthly_interval_group,
				@five_year_interval_group
			);

			SET @RowNum = @RowNum + 1
			SET @Age = @Age + 1;

	END
	        
-- $END
SELECT 
    COUNT(*) as total_ages
FROM 
    [derived].[dim_mother_age_group]

SELECT 
    TOP 400 * 
FROM 
    [derived].[dim_mother_age_group]
ORDER BY age

