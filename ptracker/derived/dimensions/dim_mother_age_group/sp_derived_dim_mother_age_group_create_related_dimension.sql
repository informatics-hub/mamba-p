USE [staging_ptracker_test]
GO
-- $BEGIN

    DROP TABLE IF EXISTS [derived].[dim_mother_age_group_anc];
    DROP TABLE IF EXISTS [derived].[dim_mother_age_group_maternity];
    DROP TABLE IF EXISTS [derived].[dim_mother_age_group_pnc];
    
    SELECT * INTO derived.dim_mother_age_group_anc FROM derived.dim_mother_age_group
    SELECT * INTO derived.dim_mother_age_group_maternity FROM derived.dim_mother_age_group
    SELECT * INTO derived.dim_mother_age_group_pnc FROM derived.dim_mother_age_group
    
    ALTER TABLE [derived].[dim_mother_age_group_anc] ADD CONSTRAINT PK_dim_mother_age_group_anc_derived_final PRIMARY KEY (mother_age_group_id);
    ALTER TABLE [derived].[dim_mother_age_group_maternity] ADD CONSTRAINT PK_dim_mother_age_group_maternity_derived_final PRIMARY KEY (mother_age_group_id);
    ALTER TABLE [derived].[dim_mother_age_group_pnc] ADD CONSTRAINT PK_dim_mother_age_group_pnc_derived_final PRIMARY KEY (mother_age_group_id);
    
    
-- $END