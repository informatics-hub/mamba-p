USE [staging_ptracker_test]
GO

DROP TABLE IF exists [derived].[dim_mother];

-- $BEGIN

    CREATE TABLE [derived].[dim_mother](
        [mother_id] UNIQUEIDENTIFIER NOT NULL,
        [openmrs_patient_identifier] [int] NOT NULL,
        [address] [nvarchar] (255) NULL,
        [first_name] [nvarchar] (255) NULL,
        [last_name] [nvarchar] (255) NULL,
        [phone] [nvarchar] (255) NULL,
        [date_of_birth] [date] NULL,
        [date_of_birth_estimated] [tinyint] NULL,
        [patient_date_created] [date] NOT NULL,
        [district] [varchar] (255) NULL,
        [region] [varchar] (255) NULL,
        [facility_id] [UNIQUEIDENTIFIER] NULL,
        [art_number] [nvarchar] (255) NULL,
        [has_multiple_art_numbers] [tinyint] NOT NULL,
    );

    ALTER TABLE [derived].[dim_mother] ADD CONSTRAINT [PK_dim_mother_derived] PRIMARY KEY ([mother_id]);

    ALTER TABLE [derived].[dim_mother] ADD CONSTRAINT FK_dim_mother_facility_derived FOREIGN KEY ([facility_id]) REFERENCES [derived].[dim_facility]([facility_id]);

-- $END