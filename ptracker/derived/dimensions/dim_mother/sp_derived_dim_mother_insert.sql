USE [staging_ptracker_test]
GO

TRUNCATE TABLE [derived].[dim_mother]

-- $BEGIN

    INSERT INTO [derived].[dim_mother](
        [mother_id],
        [openmrs_patient_identifier],
        [address],
        [first_name],
        [last_name],
        [phone],
        [date_of_birth],
        [date_of_birth_estimated],
        [patient_date_created],
        [district],
        [region],
        [facility_id],
        [art_number],
        [has_multiple_art_numbers]
    )
    SELECT
        [client_id],
        [openmrs_patient_identifier],
        [address],
        [first_name],
        [last_name],
        [phone],
        [date_of_birth],
        [date_of_birth_estimated],
        [patient_date_created],
        [district],
        [region],
        [facility_id],
        [art_number],
        [has_multiple_art_numbers]   
    FROM
        base.dim_client dc
    WHERE 
        sex='F' 
    AND
        FLOOR(DATEDIFF(DAY, date_of_birth, GETDATE())/ 365.25) >= 9
    AND 
        dc.patient_voided=0
        
-- $END
SELECT 
    COUNT(*) as total_mother
FROM 
    [derived].[dim_mother]

SELECT 
    TOP 400 * 
FROM 
    [derived].[dim_mother]