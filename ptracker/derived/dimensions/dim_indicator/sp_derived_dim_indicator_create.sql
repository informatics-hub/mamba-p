USE [staging_ptracker_test]
GO

DROP TABLE IF EXISTS [derived].[dim_indicator];

-- $BEGIN

    CREATE TABLE [derived].[dim_indicator](
        [indicator_id] UNIQUEIDENTIFIER NOT NULL,
        [indicator_identifier] [nvarchar](255) NULL,
        [indicator_name] [nvarchar](255) NULL,
        [report_type] [nvarchar](255) NULL,
        [dhis2_identifier] [nvarchar](255) NULL,
        [code] [nvarchar](255) NULL,
        [description] [nvarchar](max) NULL
    );

    ALTER TABLE [derived].[dim_indicator] ADD CONSTRAINT PK_dim_indicator_derived PRIMARY KEY (indicator_id);

-- $END