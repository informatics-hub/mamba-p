USE [staging_ptracker_test]
GO

TRUNCATE TABLE [derived].[dim_indicator];

-- $BEGIN

    INSERT INTO [derived].[dim_indicator]
    (
        [indicator_id],
        [indicator_identifier],
        [indicator_name],
        [report_type],
        [dhis2_identifier],
        [code],
        [description]
    )

    SELECT 
        [indicator_id],
        [indicator_identifier],
        [indicator_name],
        [report_type],
        [dhis2_identifier],
        [code],
        [description]
    FROM 
        [base].[dim_indicator]

    ;

-- $END


SELECT 
    TOP 400 * 
FROM 
    [derived].[dim_indicator]
