USE [staging_ptracker_test]
GO

-- $BEGIN

    EXEC sp_derived_dim_indicator_create;
    EXEC sp_derived_dim_indicator_insert;

-- $END