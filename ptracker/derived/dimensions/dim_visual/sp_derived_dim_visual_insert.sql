USE [staging_ptracker_test]
GO

TRUNCATE TABLE [derived].[dim_visual];

-- $BEGIN

    INSERT INTO [derived].[dim_visual]
    (
        [visual_id],
        [visual_name]
    )

    SELECT 
        [visual_id],
        [visual_name]
    FROM
        [base].[dim_visual]
    ;

-- $END


SELECT 
    TOP 400 * 
FROM 
    [derived].[dim_visual]
