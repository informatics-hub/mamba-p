USE [staging_ptracker_test]
GO

DROP TABLE IF EXISTS [derived].[dim_visual];

-- $BEGIN

    CREATE TABLE [derived].[dim_visual](
        [visual_id] UNIQUEIDENTIFIER NOT NULL, 
        [visual_name] [nvarchar](255) NULL
    );

    ALTER TABLE [derived].[dim_visual] ADD CONSTRAINT PK_dim_visual_derived PRIMARY KEY (visual_id);

-- $END