USE [staging_ptracker_test]
GO

-- $BEGIN

    UPDATE [derived].[dim_facility]
    SET
        [ptracker_facility] = 1
    WHERE [facility_id] IN 
        (
            SELECT DISTINCT [facility_id]
            FROM [base].[dim_encounter]
        );

-- $END