USE [staging_ptracker_test]
GO

TRUNCATE TABLE [derived].[dim_facility]

-- $BEGIN

    INSERT INTO [derived].[dim_facility]
    (
        [facility_id],
        [openmrs_location_identifier],
        [facility_name],
        [district_name],
        [region_name],
        [latitude],
        [longitude],
        [country],
        [mfl_code],
        [dhis_facility_orgunit_id],
        [dhis_maternity_orgunit_id],
        [ptracker_facility]
    )

    SELECT 
        [facility_id],
        [openmrs_location_identifier],
        [facility_name],
        [district_name],
        [region_name],
        [latitude],
        [longitude],
        [country],
        [mfl_code],
        [dhis_facility_orgunit_id],
        [dhis_maternity_orgunit_id],
        0
    FROM 
       [base].[dim_facility]
    ;

-- $END

SELECT 
    COUNT(*) as total_facilities
FROM 
    [derived].[dim_facility]

SELECT 
    TOP 400 * 
FROM 
    [derived].[dim_facility]