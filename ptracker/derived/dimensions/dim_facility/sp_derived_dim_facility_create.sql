USE [staging_ptracker_test]
GO

DROP TABLE IF EXISTS [derived].[dim_facility];

-- $BEGIN

    CREATE TABLE [derived].[dim_facility](
        [facility_id] [UNIQUEIDENTIFIER] NOT NULL,
        [openmrs_location_identifier] [int] NULL,
        [facility_name] [nvarchar](255) NOT NULL,
        [district_name] [nvarchar](255) NULL,
        [region_name] [nvarchar](255) NULL,
        [latitude] [float] NULL,
        [longitude] [float] NULL,
        [country] [nvarchar](255) NULL,
        [mfl_code] [int] NULL,
        [ptracker_facility] [int] NULL,
        [dhis_facility_orgunit_id] [nvarchar](255) NULL,
        [dhis_maternity_orgunit_id] [nvarchar] (255) NULL
    );

    ALTER TABLE [derived].[dim_facility] ADD CONSTRAINT PK_dim_facility_derived PRIMARY KEY (facility_id);

-- $END