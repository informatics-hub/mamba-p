USE [staging_ptracker_test]
GO

-- $BEGIN

    EXEC sp_derived_dim_facility_create;
    EXEC sp_derived_dim_facility_insert;
    EXEC sp_derived_dim_facility_update;
    EXEC sp_derived_dim_facility_create_related_dimension;

-- $END