USE [staging_ptracker_test]
GO
-- $BEGIN

    DROP TABLE IF EXISTS [derived].[dim_anc_facility];
    DROP TABLE IF EXISTS [derived].[dim_maternity_facility];
    DROP TABLE IF EXISTS [derived].[dim_pnc_facility];
    DROP TABLE IF EXISTS [derived].[dim_mother_pnc_facility];
    DROP TABLE IF EXISTS [derived].[dim_infant_pnc_facility];
    DROP TABLE IF EXISTS [derived].[dim_indicator_facility];
    DROP TABLE IF EXISTS [derived].[dim_pcr_result_facility];
    
    SELECT * INTO derived.dim_anc_facility FROM derived.dim_facility
    SELECT * INTO derived.dim_maternity_facility FROM derived.dim_facility
    SELECT * INTO derived.dim_pnc_facility FROM derived.dim_facility
    SELECT * INTO derived.dim_mother_pnc_facility FROM derived.dim_facility
    SELECT * INTO derived.dim_infant_pnc_facility FROM derived.dim_facility
    SELECT * INTO derived.dim_indicator_facility FROM derived.dim_facility
    SELECT * INTO derived.dim_pcr_result_facility FROM derived.dim_facility
    
    ALTER TABLE [derived].[dim_anc_facility] ADD CONSTRAINT PK_dim_anc_facility_derived_final PRIMARY KEY (facility_id);
    ALTER TABLE [derived].[dim_maternity_facility] ADD CONSTRAINT PK_dim_maternity_facility_derived_final PRIMARY KEY (facility_id);
    ALTER TABLE [derived].[dim_pnc_facility] ADD CONSTRAINT PK_dim_pnc_facility_derived_final PRIMARY KEY (facility_id);
    ALTER TABLE [derived].[dim_mother_pnc_facility] ADD CONSTRAINT PK_dim_mother_pnc_facility_derived_final PRIMARY KEY (facility_id);
    ALTER TABLE [derived].[dim_infant_pnc_facility] ADD CONSTRAINT PK_dim_infant_pnc_facility_derived_final PRIMARY KEY (facility_id);
    ALTER TABLE [derived].[dim_indicator_facility] ADD CONSTRAINT PK_dim_indicator_facility_derived_final PRIMARY KEY (facility_id);
    ALTER TABLE [derived].[dim_pcr_result_facility] ADD CONSTRAINT PK_dim_pcr_result_facility_derived_final PRIMARY KEY (facility_id);
    
    
-- $END