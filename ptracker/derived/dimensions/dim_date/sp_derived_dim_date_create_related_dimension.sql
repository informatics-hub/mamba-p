USE [staging_ptracker_test]
GO
-- $BEGIN

    DROP TABLE IF EXISTS [derived].[dim_anc_date];
    DROP TABLE IF EXISTS [derived].[dim_maternity_date];
    DROP TABLE IF EXISTS [derived].[dim_pnc_date];
    DROP TABLE IF EXISTS [derived].[dim_indicator_report_date];
    DROP TABLE IF EXISTS [derived].[dim_report_access_date];
    
    SELECT * INTO derived.dim_anc_date FROM derived.dim_date
    SELECT * INTO derived.dim_maternity_date FROM derived.dim_date
    SELECT * INTO derived.dim_pnc_date FROM derived.dim_date
    SELECT * INTO derived.dim_indicator_report_date FROM derived.dim_date
    SELECT * INTO derived.dim_visual_access_date FROM derived.dim_date
    
    ALTER TABLE [derived].[dim_anc_date] ADD CONSTRAINT PK_dim_anc_date_derived_final PRIMARY KEY (date_id);
    ALTER TABLE [derived].[dim_maternity_date] ADD CONSTRAINT PK_dim_maternity_date_derived_final PRIMARY KEY (date_id);
    ALTER TABLE [derived].[dim_pnc_date] ADD CONSTRAINT PK_dim_pnc_date_derived_final PRIMARY KEY (date_id);
    ALTER TABLE [derived].[dim_indicator_report_date] ADD CONSTRAINT PK_dim_indicator_report_date_derived_final PRIMARY KEY (date_id);
    ALTER TABLE [derived].[dim_visual_access_date] ADD CONSTRAINT PK_dim_visual_access_date_derived_final PRIMARY KEY (date_id);
    
    
-- $END