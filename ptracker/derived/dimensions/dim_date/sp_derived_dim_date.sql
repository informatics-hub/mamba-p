USE [staging_ptracker_test]
GO
-- $BEGIN

    EXEC sp_derived_dim_date_create;
    EXEC sp_derived_dim_date_insert;
    EXEC sp_derived_dim_date_update;
    EXEC sp_derived_dim_date_create_related_dimension;

-- $END