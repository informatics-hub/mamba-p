USE [staging_ptracker_test]
GO

TRUNCATE TABLE [derived].[dim_date]

-- $BEGIN
    
    INSERT INTO [derived].[dim_date]
    (
        [date_id],
        [date_yyyymmdd],
        [date_slash_yyyymd],
        [date_slash_mdyyyy],
        [date_slash_dmyyyy],
        [day_number_of_week],
        [day_name_of_week],
        [day_of_month],
        [day_of_year],
        [weekday_weekend],
        [week_number_of_year],
        [month_name],
        [month_number_of_year],
        [year],
        [last_day_of_month],
        [datim_annual_period],
        [datim_semiannual_period],
        [datim_quarter_period],
        [quarter_period],
        [semiannual_period],
        [pepfar_annual_period],
        [pepfar_quarter_period] 
    )
    SELECT
        [date_id],
        [date_yyyymmdd],
        [date_slash_yyyymd],
        [date_slash_mdyyyy],
        [date_slash_dmyyyy],
        [day_number_of_week],
        [day_name_of_week],
        [day_of_month],
        [day_of_year],
        [weekday_weekend],
        [week_number_of_year],
        [month_name],
        [month_number_of_year],
        [year],
        [last_day_of_month],
        [datim_annual_period],
        [datim_semiannual_period],
        [datim_quarter_period],
        [quarter_period],
        [semiannual_period],
        [pepfar_annual_period],
        [pepfar_quarter_period] 
    FROM 
        base.dim_date
    
-- $END

SELECT 
    COUNT(*) as total_dates
FROM 
    [derived].[dim_date]

SELECT 
    TOP 400 * 
FROM 
    [derived].[dim_date]
