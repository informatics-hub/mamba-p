USE [staging_ptracker_test]
GO

-- $BEGIN

    UPDATE [derived].[dim_date]
    SET
        [month_year] = CONCAT(month_name, ' ', [year]) 
    
    UPDATE [derived].[dim_date]
    SET
        [three_month_period] = CONCAT('Oct - Dec ', [year])
    WHERE [pepfar_quarter_period] LIKE '%Q1%'

    UPDATE [derived].[dim_date]
    SET
        [three_month_period] = CONCAT('Jan - Mar ', [year])
    WHERE [pepfar_quarter_period] LIKE '%Q2%'

    UPDATE [derived].[dim_date]
    SET
        [three_month_period] = CONCAT('Apr - Jun ', [year])
    WHERE [pepfar_quarter_period] LIKE '%Q3%'

    UPDATE [derived].[dim_date]
    SET
        [three_month_period] = CONCAT('Jul - Sep ', [year])
    WHERE [pepfar_quarter_period] LIKE '%Q4%'

-- $END

SELECT TOP 400
    *
FROM [derived].[dim_date];