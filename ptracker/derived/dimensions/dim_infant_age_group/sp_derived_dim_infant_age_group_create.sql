USE [staging_ptracker_test]
GO

DROP TABLE IF exists [derived].[dim_infant_age_group];

-- $BEGIN

    CREATE TABLE [derived].[dim_infant_age_group](
        [infant_age_group_id] [UNIQUEIDENTIFIER] NOT NULL,
        [age_in_weeks] [int] NOT NULL,
        [age_in_months] [float] NOT NULL,
        [age_p_tracker_monthly_interval_1st_test] [nvarchar] (255) NULL,
        [age_p_tracker_monthly_interval_2nd_test] [nvarchar] (255) NULL,
        [age_p_tracker_monthly_interval_3rd_test] [nvarchar] (255) NULL,
        [age_p_tracker_monthly_interval_confirmed_positive] [nvarchar] (255) NULL,
        [age_p_tracker_monthly_interval] [nvarchar] (255) NULL,
        [age_p_tracker_monthly_interval_val] [nvarchar] (255) NULL
    );

    ALTER TABLE [derived].[dim_infant_age_group] ADD CONSTRAINT [PK_dim_infant_age_group_derived] PRIMARY KEY ([infant_age_group_id]);

-- $END