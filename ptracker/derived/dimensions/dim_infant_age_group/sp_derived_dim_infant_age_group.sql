USE [staging_ptracker_test]
GO
-- $BEGIN

    EXEC sp_derived_dim_infant_age_group_create;
    EXEC sp_derived_dim_infant_age_group_insert;
    
-- $END