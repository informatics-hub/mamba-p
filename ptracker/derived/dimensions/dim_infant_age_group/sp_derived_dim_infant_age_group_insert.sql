USE [staging_ptracker_test]
GO

TRUNCATE TABLE [derived].[dim_infant_age_group]

-- $BEGIN

    DECLARE @MaxAge INT = 96
	DECLARE @RowNum INT = 1
	DECLARE @Age INT = 1
    DECLARE @InMonths INT = 4
	DECLARE @month_age_group_val INT
	
	DECLARE @first_test_group CHAR(30)
	DECLARE @second_test_group CHAR(30)
	DECLARE @third_test_group CHAR(30)
	DECLARE @confirmation_test_group CHAR(30)
	DECLARE @month_age_group CHAR(30)

	WHILE @Age <= @MaxAge
		BEGIN
			SET @first_test_group = 
				CASE 
					WHEN @Age >= 6 AND @Age <= 8 THEN '6 - 8 weeks'
					WHEN @Age >= 9 AND CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT) < 9 THEN '9 weeks - 8 months'
					WHEN CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT) >= 9 AND CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT) <= 12 THEN '9 - 12 months'
					WHEN CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT) > 12 THEN '> 12 Months'
				END

			SET @second_test_group = 
				CASE 
					WHEN CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT) < 9 THEN '< 9 months'
					WHEN CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT) >= 9 AND CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT) <= 12 THEN '9 - 12 months'
					WHEN CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT) > 12 THEN '> 12 Months'
				END

			SET @third_test_group = 
				CASE
					WHEN CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT) >= 9 AND CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT) <= 17 THEN '9 - 17 months'
					WHEN CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT) > 18 THEN '> 18 months'
				END

			SET @confirmation_test_group = 
				CASE 
					WHEN @Age >= 9 AND CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT) <= 8 THEN '9 weeks - 8 months'
					WHEN CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT) >= 9 AND CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT) <= 12 THEN '9 - 12 months'
					WHEN CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT) > 12 THEN '> 12 Months'
				END

			SET @month_age_group = 
				CASE 
					WHEN CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT) >= 0 AND CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT) <= 2 THEN '0 - 2 months'
					WHEN CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT) > 2 AND CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT) <= 12 THEN '> 2 - 12 months'
					WHEN CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT) > 12 THEN '> 12 Months'
				END
			
			SET @month_age_group_val = 
				CASE 
					WHEN CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT) >= 0 AND CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT) <= 2 THEN 1
					WHEN CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT) > 2 AND CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT) <= 12 THEN 2
					WHEN CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT) > 12 THEN 3
				END

			INSERT INTO [derived].[dim_infant_age_group](
				[infant_age_group_id],
				[age_in_weeks],
                [age_in_months],
                [age_p_tracker_monthly_interval_1st_test],
                [age_p_tracker_monthly_interval_2nd_test],
                [age_p_tracker_monthly_interval_3rd_test],
                [age_p_tracker_monthly_interval_confirmed_positive],
				[age_p_tracker_monthly_interval],
				[age_p_tracker_monthly_interval_val]
			)
			VALUES(
				NEWID(), 
				@Age,
				CAST(@Age AS FLOAT) / CAST(@InMonths AS FLOAT),
				@first_test_group,
				@second_test_group,
				@third_test_group,
				@confirmation_test_group,
				@month_age_group,
				@month_age_group_val
			);

			SET @RowNum = @RowNum + 1
			SET @Age = @Age + 1;

	END

	

	        
-- $END
SELECT 
    COUNT(*) as total_age_groups
FROM 
    [derived].[dim_infant_age_group]

SELECT 
    TOP 400 * 
FROM 
    [derived].[dim_infant_age_group]
ORDER BY age_in_weeks
