USE staging_ptracker_test;
GO

-- $BEGIN
    
    DROP TABLE IF EXISTS derived.fact_pregnancy_outcome;
    DROP TABLE IF EXISTS derived.fact_infant_outcome;
    DROP TABLE IF EXISTS derived.fact_antenatal_visit;
    DROP TABLE IF EXISTS derived.fact_infant_postnatal_visit;
    DROP TABLE IF EXISTS derived.fact_maternity_infant_visit;
    DROP TABLE IF EXISTS derived.fact_maternity_visit;
    DROP TABLE IF EXISTS derived.fact_mother_postnatal_visit;
    DROP TABLE IF EXISTS derived.fact_viral_load_result;
    DROP TABLE IF EXISTS derived.fact_pcr_result;
    
    DROP TABLE IF EXISTS derived.dim_pregnancy;
    DROP TABLE IF EXISTS derived.dim_infant;
    DROP TABLE IF EXISTS derived.dim_mother;
    DROP TABLE IF EXISTS derived.dim_mother_age_group_anc;
    DROP TABLE IF EXISTS derived.dim_mother_age_group_maternity;
    DROP TABLE IF EXISTS derived.dim_mother_age_group_pnc;
    DROP TABLE IF EXISTS derived.dim_infant_age_group;
    DROP TABLE IF EXISTS derived.dim_facility;
    DROP TABLE IF EXISTS derived.dim_anc_facility;
    DROP TABLE IF EXISTS derived.dim_maternity_facility;
    DROP TABLE IF EXISTS derived.dim_pnc_facility;
    DROP TABLE IF EXISTS derived.dim_date;
    DROP TABLE IF EXISTS derived.dim_anc_date;
    DROP TABLE IF EXISTS derived.dim_maternity_date;
    DROP TABLE IF EXISTS derived.dim_pnc_date;

-- $END