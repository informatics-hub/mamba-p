USE [staging_ptracker_test]
GO

-- $BEGIN

    PRINT 'Starting execution of ptracker derived objects'

    --PRINT ' Dropping tables '
    PRINT 'dbo.sp_xf_system_drop_all_foreign_keys in derived'
    EXEC dbo.sp_xf_system_drop_all_foreign_keys_in_schema 'derived';
    
    PRINT 'dbo.sp_xf_system_drop_all_tables in derived'
    EXEC dbo.sp_xf_system_drop_all_tables_in_schema 'derived';

    -- dimensions
    EXEC derived.sp_dim_date;
    EXEC derived.sp_dim_facility;
    EXEC derived.sp_dim_mother_age_group;
    EXEC derived.sp_dim_infant_age_group;
    EXEC derived.sp_dim_indicator;
    EXEC derived.sp_dim_mother;
    EXEC derived.sp_dim_pregnancy;
    EXEC derived.sp_dim_infant;
    EXEC derived.sp_dim_visual;

    -- facts
    EXEC derived.sp_fact_antenatal_visit;
    EXEC derived.sp_fact_infant_postnatal_visit;
    EXEC derived.sp_fact_maternity_infant_visit;
    EXEC derived.sp_fact_maternity_visit;
    EXEC derived.sp_fact_mother_postnatal_visit;
    EXEC derived.sp_fact_viral_load_result;
    EXEC derived.sp_fact_pcr_result;
    EXEC derived.sp_fact_pregnancy_outcome;
    EXEC derived.sp_fact_infant_outcome;
    EXEC derived.sp_fact_facility_indicator;
    EXEC derived.sp_fact_anc_dhis2_export;
    EXEC derived.sp_fact_maternity_dhis2_export;
    EXEC derived.sp_fact_mbfu_dhis2_export;
    EXEC derived.sp_dashboard_refresh;
    EXEC derived.sp_fact_visual_access;

    PRINT 'Completed execution of ptracker derived objects'

-- $END