USE [staging_ptracker_test]
GO

DROP TABLE IF EXISTS [derived].[dashboard_refresh];

-- $BEGIN

CREATE TABLE [derived].[dashboard_refresh](
	[refresh_id] [int] IDENTITY(1,1) NOT NULL,
	[refresh_date] [date] NULL
);

ALTER TABLE [derived].[dashboard_refresh] ADD CONSTRAINT PK_dashboard_refresh_derived PRIMARY KEY ([refresh_id]);

-- $END
