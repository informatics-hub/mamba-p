USE [staging_ptracker_test]
GO

TRUNCATE TABLE [derived].[dashboard_refresh]

-- $BEGIN

    INSERT INTO [derived].[dashboard_refresh](
        refresh_date
    )

    SELECT 
        MAX(encounter_date)
    FROM 
        base.dim_encounter
    ;

-- $END
SELECT TOP 400 *
FROM [derived].[dashboard_refresh]