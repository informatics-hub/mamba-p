USE [staging_ptracker_test]
GO
-- $BEGIN

    EXEC sp_derived_fact_maternity_visit_create;
    EXEC sp_derived_fact_maternity_visit_insert;
    EXEC sp_derived_fact_maternity_visit_update;

-- $END