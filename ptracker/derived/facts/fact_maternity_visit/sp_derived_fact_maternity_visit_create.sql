USE [staging_ptracker_test]
GO

DROP TABLE IF exists [derived].[fact_maternity_visit];

-- $BEGIN

    CREATE TABLE [derived].[fact_maternity_visit](
        [maternity_visit_id] [uniqueidentifier] NOT NULL,
        [pregnancy_id] [uniqueidentifier] NULL,
        [mother_id] [uniqueidentifier] NULL, 
        [facility_id] [uniqueidentifier] NULL,
        [creator] [nvarchar](255) NOT NULL,
        [provider] [nvarchar](255) NOT NULL,
        [encounter_date] [date],
        [encounter_date_id] [uniqueidentifier] NULL,
        [date_created] [date] NULL, 
        [date_created_id] [uniqueidentifier] NULL,
        age [int] NULL,
        age_group_id [uniqueidentifier],
        [first_anc_date] [date] NULL,
        [first_anc_date_id] [uniqueidentifier] NULL,
        first_anc_facility_id [uniqueidentifier],
        [anc_booked] [int] NULL,
        [anc_hiv_test_status] [nvarchar] (255) NULL,
        [is_anc_hiv_test_status_known] [int] NULL,
        [is_anc_hiv_result_positive] [int] NULL,
        [is_art_initiated_during_anc_4_weeks_before_delivery] [int] NULL,
        [is_art_initiated_during_anc_less_than_4_weeks_before_delivery] [int] NULL,
        [hiv_test_status] [nvarchar] (255) NULL,
        [is_hiv_test_status_tested] [int] NULL,
        [is_hiv_test_status_previously_known] [int] NULL,
        [is_hiv_test_status_not_tested] [int] NULL,
        [hiv_test_result] [nvarchar] (255) NULL,
        [is_hiv_test_results_positive] [int] NULL,
        [is_hiv_test_results_negative] [int] NULL,
        [is_hiv_test_results_unknown] [int] NULL,
        [is_hiv_positive] [int] NULL, 
        [hiv_re_test_at_36_weeks] [nvarchar] (255) NULL,
        [is_positive_at_36_weeks_hiv_re_test] [int] NULL,
        [is_negative_at_36_weeks_hiv_re_test] [int] NULL, 
        [recent_viral_load_test_done] [nvarchar] (255) NULL,
        [is_recent_viral_load_test_done] [int] NULL,
        [viral_load_copies] [float] NULL,
        [viral_load_test_result] [nvarchar] (255) NULL,
        [viral_load_test_date] [date] NULL,
        [viral_load_test_date_id] [uniqueidentifier] NULL,
        [is_viral_load_test_result_undetectable_or_less_than_40] [int] NULL,
        [is_viral_load_test_result_not_suppressed] [int] NULL,
        [art_initiation] [nvarchar] (255) NULL,
        [art_start_date] [date] NULL,
        [art_start_date_id] [uniqueidentifier] NULL,
        [is_art_initiated_already] [int] NULL,
        [is_art_initiated_during_anc] [int] NULL,
        [is_art_initiated_during_lnd] [int] NULL,
        [is_art_not_initiated] [int] NULL,
        [is_refused_art] [int] NULL,
        [is_stock_out] [int] NULL,
        [reason_for_refusing_art] [nvarchar] (max) NULL,
        [mother_status] [nvarchar] (255) NULL
    );

    ALTER TABLE [derived].[fact_maternity_visit] ADD CONSTRAINT [PK_fact_maternity_visit] PRIMARY KEY ([maternity_visit_id]);

    ALTER TABLE [derived].[fact_maternity_visit] ADD CONSTRAINT FK_fact_maternity_dim_pregnancy FOREIGN KEY ([pregnancy_id]) REFERENCES [derived].[dim_pregnancy]([pregnancy_id]);

    ALTER TABLE [derived].[fact_maternity_visit] ADD CONSTRAINT FK_fact_maternity_dim_maternity_anc_facility_derived FOREIGN KEY ([facility_id]) REFERENCES [derived].[dim_facility]([facility_id]);

    ALTER TABLE [derived].[fact_maternity_visit] ADD CONSTRAINT FK_fact_maternity_encounter_date_id FOREIGN KEY ([encounter_date_id]) REFERENCES [derived].[dim_maternity_date]([date_id]);

    ALTER TABLE [derived].[fact_maternity_visit] ADD CONSTRAINT FK_fact_maternity_created_date_id FOREIGN KEY ([date_created_id]) REFERENCES [derived].[dim_maternity_date]([date_id]);

    ALTER TABLE [derived].[fact_maternity_visit] ADD CONSTRAINT FK_fact_maternity_age_group_id FOREIGN KEY ([age_group_id]) REFERENCES [derived].[dim_mother_age_group_maternity]([mother_age_group_id]);

    ALTER TABLE [derived].[fact_maternity_visit] ADD CONSTRAINT FK_fact_maternity_first_anc_date_id FOREIGN KEY ([first_anc_date_id]) REFERENCES [derived].[dim_maternity_date]([date_id]);

    ALTER TABLE [derived].[fact_maternity_visit] ADD CONSTRAINT FK_fact_maternity_first_anc_facility_id FOREIGN KEY ([first_anc_facility_id]) REFERENCES [derived].[dim_facility]([facility_id]);

    ALTER TABLE [derived].[fact_maternity_visit] ADD CONSTRAINT FK_fact_maternity_viral_load_test_date_id FOREIGN KEY ([viral_load_test_date_id]) REFERENCES [derived].[dim_maternity_date]([date_id]);

    ALTER TABLE [derived].[fact_maternity_visit] ADD CONSTRAINT FK_fact_maternity_art_start_date_id FOREIGN KEY ([art_start_date_id]) REFERENCES [derived].[dim_maternity_date]([date_id]);
 
    ALTER TABLE [derived].[fact_maternity_visit] ADD CONSTRAINT FK_fact_maternity_visit_dim_mother FOREIGN KEY ([mother_id]) REFERENCES [derived].[dim_mother]([mother_id]);

-- $END