USE [staging_ptracker_test]
GO

TRUNCATE TABLE [derived].[fact_maternity_visit]

-- $BEGIN

    INSERT INTO [derived].[fact_maternity_visit](
        [maternity_visit_id], 
        [pregnancy_id],
        [mother_id],
        [facility_id],
        [creator],
        [provider],
        [encounter_date],
        [encounter_date_id],
        [date_created], 
        [date_created_id],
        [age],
        [age_group_id],
        [first_anc_date],
        [anc_booked],
        [anc_hiv_test_status],
        [hiv_test_status],
        [hiv_test_result],
        [hiv_re_test_at_36_weeks],
        [recent_viral_load_test_done],
        [viral_load_copies],
        [viral_load_test_result],
        [viral_load_test_date],
        [viral_load_test_date_id],
        [art_initiation],
        [art_start_date],
        [art_start_date_id],
        [reason_for_refusing_art],
        [mother_status]
    )
    
    SELECT DISTINCT
        NEWID(),
        prg.pregnancy_id,
        prg.mother_id,
        e.facility_id,
        e.creator,
        e.provider,
        e.encounter_date,
        e.encounter_date_id,
        e.date_created,
        e.date_created_id,
        FLOOR(DATEDIFF(DAY,mo.[date_of_birth],e.[encounter_date])/365.25),
        mage.mother_age_group_id,
        anc.enc_date,
        CASE WHEN 
            ma.anc_booked='Yes'
        THEN
            1
        WHEN
            ma.anc_booked='No'
        THEN
            0
        ELSE
            NULL
        END,
        ma.anc_hiv_test_status,
        ma.hiv_test_status,
        ma.hiv_test_result,
        ma.hiv_re_test_at_36_weeks,
        ma.recent_viral_load_test_done,
        ma.viral_load_copies,
        ma.viral_load_test_result,
        ma.viral_load_test_date,
        viral_load_dt.date_id,
        ma.art_initiation,
        ma.art_start_date,
        art_dt.date_id,
        ma.reason_for_refusing_art,
        ma.mother_status
    FROM 
        [base].[fact_maternity_visit] ma
    LEFT JOIN
        [derived].[dim_pregnancy] prg
    ON [prg].[ptracker_identifier] = [ma].[ptracker_identifier]
    INNER JOIN
        [base].[dim_encounter] e ON ma.encounter_id=e.encounter_id
    INNER JOIN
        [derived].[dim_mother] mo ON mo.mother_id = prg.mother_id
    LEFT JOIN
        derived.dim_date art_dt ON art_start_date = art_dt.date_slash_yyyymd
    LEFT JOIN
        derived.dim_date viral_load_dt ON art_start_date = viral_load_dt.date_slash_yyyymd
    LEFT JOIN
        [derived].[dim_mother_age_group_maternity] mage ON FLOOR(DATEDIFF(DAY,mo.[date_of_birth],e.[encounter_date])/365.25) = mage.age
    LEFT JOIN
        (
            SELECT 
                min_anc.mother_id, min_anc.pregnancy_id, enc_date
            FROM
            (
                SELECT 
                    mother_id, pregnancy_id, MIN(encounter_date) AS enc_date
                FROM 
                    [derived].[fact_antenatal_visit] av
                GROUP BY
                    mother_id, pregnancy_id
            ) min_anc
        ) anc ON mo.mother_id = anc.mother_id AND prg.pregnancy_id = anc.pregnancy_id
-- $END;
SELECT TOP 400 *
FROM [derived].[fact_maternity_visit]