USE [staging_ptracker_test]
GO

-- $BEGIN

    UPDATE 
        derived.fact_maternity_visit
    SET
        [first_anc_date_id] = e.[encounter_date_id],
        [first_anc_facility_id] = e.[facility_id]
    FROM
        [derived].[fact_maternity_visit] mat
    INNER JOIN
        [derived].[dim_pregnancy] prg
    ON 
        prg.[pregnancy_id] = mat.[pregnancy_id]
    INNER JOIN
        [base].[dim_encounter] e
    ON
        e.[client_id] = prg.[mother_id] AND e.[encounter_date] = mat.[first_anc_date]
    ;


    UPDATE
        [derived].[fact_maternity_visit]
    SET
        [derived].[fact_maternity_visit].is_anc_hiv_test_status_known = 
            CASE WHEN 
                anc_known_status.pregnancy_id IS NOT NULL OR anc_hiv_test_status in ('Previously known positive','NEGATIVE','POSITIVE')
            THEN 
                1
            ELSE
                0
            END,
        [derived].[fact_maternity_visit].is_anc_hiv_result_positive = 
            CASE WHEN 
                anc_known_pos_result.pregnancy_id IS NOT NULL OR anc_hiv_test_status in ('Previously known positive','POSITIVE')
            THEN 
                1
            ELSE
                0
            END
    FROM
        [derived].[fact_maternity_visit] mat
    LEFT JOIN
        (
            SELECT DISTINCT mother_id, pregnancy_id
            FROM derived.fact_antenatal_visit a
            WHERE is_hiv_test_status_previously_known=1 OR (is_hiv_test_status_tested = 1 AND (is_hiv_test_results_negative=1 OR is_hiv_test_results_positive=1))
        ) anc_known_status
    ON 
        anc_known_status.mother_id = mat.mother_id AND anc_known_status.pregnancy_id = mat.pregnancy_id
    LEFT JOIN
        (
            SELECT DISTINCT mother_id, pregnancy_id
            FROM derived.fact_antenatal_visit a
            WHERE is_hiv_test_status_previously_known = 1 OR (is_hiv_test_status_tested = 1 AND is_hiv_test_results_positive=1)
        ) anc_known_pos_result
    ON 
        anc_known_pos_result.mother_id = mat.mother_id AND anc_known_pos_result.pregnancy_id = mat.pregnancy_id;
    ;

    UPDATE
        [derived].[fact_maternity_visit]
    SET
        [is_art_initiated_already] = 
            CASE WHEN 
                art_initiation = 'Already on ART before current pregnancy' OR anc.mother_id IS NOT NULL
            THEN 
                1
            END
    FROM
        [derived].[fact_maternity_visit] mat
    LEFT JOIN
        (
            SELECT  
                a.mother_id,
                a.pregnancy_id,
                art_start_date
            FROM 
                derived.fact_antenatal_visit a
            WHERE
                is_art_initiated_before = 1
        ) anc
    ON 
        anc.mother_id = mat.mother_id
    ;

    UPDATE
        [derived].[fact_maternity_visit]
    SET
        [derived].[fact_maternity_visit].is_art_initiated_during_anc_4_weeks_before_delivery = 
            CASE WHEN 
                anc.art_start_date IS NOT NULL AND anc.art_start_date <= mat.encounter_date AND (DATEDIFF(WEEK,mat.encounter_date, anc.art_start_date) >= 4 OR DATEDIFF(WEEK,mat.encounter_date, mat.art_start_date) >= 4)
            THEN 
                1
            ELSE
                0
            END,
        [derived].[fact_maternity_visit].[is_art_initiated_during_anc_less_than_4_weeks_before_delivery] = 
            CASE WHEN 
                anc.art_start_date IS NOT NULL AND anc.art_start_date <= mat.encounter_date AND (DATEDIFF(WEEK,mat.encounter_date, anc.art_start_date) < 4 OR DATEDIFF(WEEK,mat.encounter_date, mat.art_start_date) < 4)
            THEN 
                1
            ELSE
                0
            END,
        [derived].[fact_maternity_visit].[is_art_initiated_during_anc] =
            CASE WHEN 
                anc.mother_id IS NOT NULL
            THEN 
                1
            END
    FROM
        [derived].[fact_maternity_visit] mat
    LEFT JOIN
        (
            SELECT  
                a.mother_id,
                a.pregnancy_id,
                art_start_date
            FROM 
                derived.fact_antenatal_visit a
            WHERE
                is_art_initiated = 1
        ) anc
    ON 
        anc.mother_id = mat.mother_id
   


    UPDATE 
        [derived].[fact_maternity_visit]
    SET 
        [is_hiv_test_status_tested] = 
            CASE WHEN 
                hiv_test_status = 'Tested for HIV during this visit' AND is_anc_hiv_test_status_known=0
            THEN 
                1
            ELSE
                0
            END,

        [is_hiv_test_status_previously_known] = 
            CASE WHEN 
                hiv_test_status = 'Previously known positive' AND is_anc_hiv_test_status_known=0
            THEN 
                1
            ELSE
                0
            END,
        
        [is_hiv_test_status_not_tested] = 
            CASE WHEN 
                (hiv_test_status = 'Not tested for HIV during this visit' OR hiv_re_test_at_36_weeks='Not tested for HIV during this visit') AND is_anc_hiv_test_status_known=0
            THEN 
                1
            ELSE
                0
            END,
        
        [is_hiv_test_results_positive] = 
            CASE WHEN 
                hiv_test_result = 'Positive' AND is_anc_hiv_test_status_known=0
            THEN 
                1
            ELSE
                0
            END,
        
        
        [is_hiv_test_results_negative] = 
            CASE WHEN 
                hiv_test_result = 'Negative' AND is_anc_hiv_test_status_known=0
            THEN 
                1
            ELSE
                0
            END,

        [is_hiv_test_results_unknown] = 
            CASE WHEN 
                (hiv_test_result = 'Unknown' OR hiv_re_test_at_36_weeks='Unknown') AND is_anc_hiv_test_status_known=0
            THEN 
                1
            ELSE 
                0
            END,

        [is_hiv_positive] = 
            CASE WHEN 
                hiv_test_result = 'Positive' OR hiv_test_status = 'Previously known positive' OR is_anc_hiv_result_positive = 1 OR hiv_re_test_at_36_weeks = 'Positive'
            THEN 
                1
            ELSE
                0
            END,
        
        [is_positive_at_36_weeks_hiv_re_test] = 
            CASE WHEN 
                hiv_re_test_at_36_weeks = 'Positive'
            THEN 
                1
            ELSE 
                0
            END,

        [is_negative_at_36_weeks_hiv_re_test] = 
            CASE WHEN 
                hiv_re_test_at_36_weeks = 'Negative'
            THEN 
                1
            ELSE 
                0
            END,

        [is_recent_viral_load_test_done] = 
            CASE WHEN 
                recent_viral_load_test_done = 'Yes'
            THEN 
                1
            WHEN 
                recent_viral_load_test_done = 'No'
            THEN
                0
            ELSE
                NULL
            END,

        [is_viral_load_test_result_undetectable_or_less_than_40] = 
            CASE WHEN 
                    viral_load_test_result = 'Not Detected' 
                OR 
                (
                    viral_load_test_result = 'Target Detected' AND viral_load_copies < 40
                )
            THEN 
                1
            ELSE
                0
            END,

        [is_viral_load_test_result_not_suppressed] = 
            CASE WHEN 
                viral_load_test_result = 'Target Detected' AND viral_load_copies >= 40
            THEN 
                1
            ELSE
                0
            END,
        [is_art_initiated_during_lnd] = 
            CASE WHEN 
                art_initiation = 'Started on ART in ANC current pregnancy' AND (is_art_initiated_already IS NULL OR  is_art_initiated_during_anc IS NULL)
            THEN 
                1 
            END,
        [is_refused_art] = 
            CASE WHEN 
                art_initiation = 'Refused ART'
            THEN 
                1
            ELSE
                0
            END,

        [is_stock_out] = 
            CASE WHEN 
                art_initiation = 'Not started due to stockout of ART'
            THEN 
                1
            ELSE
                0
            END,

        [is_art_not_initiated] = 
            CASE WHEN 
                art_initiation = 'Not started due to stockout of ART'
            OR
                art_initiation = 'Refused ART' 
            THEN 
                1
            ELSE
                0
            END
    
        
    ;
 
-- $END
SELECT TOP 400 *
FROM [derived].[fact_maternity_visit]
