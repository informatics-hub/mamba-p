USE [staging_ptracker_test]
GO

-- $BEGIN

    UPDATE 
        mo
    SET 
        mother_first_anc_age = FLOOR(DATEDIFF(DAY,mo.[date_of_birth],mo.first_anc_date)/365.25),
        mother_first_anc_age_group_id = m_age.mother_age_group_id
    FROM 
        [derived].[fact_pregnancy_outcome] mo
    LEFT JOIN 
        [derived].[dim_mother_age_group_anc] m_age
    ON
        FLOOR(DATEDIFF(DAY,mo.[date_of_birth],mo.first_anc_date)/365.25) = m_age.age

    UPDATE 
        mo
    SET 
        mother_lnd_age = FLOOR(DATEDIFF(DAY,mo.[date_of_birth],mo.maternity_date)/365.25),
        mother_lnd_age_group_id = m_age.mother_age_group_id
    FROM 
        [derived].[fact_pregnancy_outcome] mo
    LEFT JOIN 
        [derived].[dim_mother_age_group_maternity] m_age
    ON
        FLOOR(DATEDIFF(DAY,mo.[date_of_birth],mo.maternity_date)/365.25) = m_age.age
    
    
    UPDATE 
        mo
    SET
        is_new_tested_hiv_at_first_anc = a.is_hiv_test_status_tested,
        is_new_tested_hiv_positive_at_first_anc = a.is_hiv_test_results_positive,
        is_new_tested_hiv_negative_at_first_anc = a.is_hiv_test_results_negative,
        is_new_art_started_at_first_anc = 
            CASE WHEN
                a.is_art_initiated = 1 OR a.is_art_initiated_before = 1
            THEN
                1
            ELSE
                0
            END,
        is_new_known_positive_at_first_anc = is_hiv_test_status_previously_known,
        is_new_all_positive_at_first_anc = 
            CASE WHEN
                a.is_hiv_test_results_positive = 1 OR a.is_hiv_test_status_previously_known = 1
            THEN
                1
            ELSE
                0
            END,
        is_new_hiv_status_known_at_first_anc = 
            CASE WHEN
                (a.is_hiv_test_results_positive = 1 OR a.is_hiv_test_results_negative = 1) OR a.is_hiv_test_status_previously_known = 1
            THEN
                1
            ELSE
                0
            END,
        is_new_first_visit_at_anc = a.is_first_visit
    FROM 
        [derived].[fact_pregnancy_outcome] mo
    LEFT JOIN 
        [derived].[fact_antenatal_visit] a
    ON
        a.antenatal_visit_id = mo._first_anc_visit_id AND a.is_first_visit=1
    ;

    UPDATE 
        p_outcome
    SET 
        [is_hiv_positive] =
        CASE WHEN 
            av.is_hiv_positive = 1 OR mv.is_hiv_positive = 1 OR mpv.is_hiv_positive = 1
        THEN
            1
        ELSE
            0
        END
    FROM 
        [derived].[fact_pregnancy_outcome] p_outcome 
    LEFT JOIN
        (
            SELECT DISTINCT
                mother_id, is_hiv_positive
            FROM
                [derived].[fact_antenatal_visit]
            WHERE
                is_hiv_positive=1 
        ) av
    ON 
        p_outcome.mother_id=av.mother_id
    LEFT JOIN
        (
            SELECT DISTINCT
                mother_id, is_hiv_positive
            FROM
                [derived].[fact_maternity_visit]
            WHERE
                is_hiv_positive=1 
        ) mv
    ON 
        p_outcome.mother_id=mv.mother_id
    LEFT JOIN
        (
            SELECT DISTINCT
                mother_id, is_hiv_positive
            FROM
                [derived].[fact_mother_postnatal_visit]
            WHERE
                is_hiv_positive=1 
        ) mpv
    ON 
        p_outcome.mother_id=mpv.mother_id

    UPDATE 
        mo
    SET 
        is_art_initiated_in_anc = ini_anc.is_art_initiated,
        is_art_initiated_before_anc = ini_before_anc.is_art_initiated_before,
        is_art_initiated_before_or_in_anc =
            CASE WHEN 
                ini_anc.is_art_initiated = 1 OR ini_before_anc.is_art_initiated_before = 1
            THEN
                1
            ELSE
                0
            END
    FROM 
        [derived].[fact_pregnancy_outcome] mo
    LEFT JOIN
        (
            SELECT DISTINCT mother_id, pregnancy_id, is_art_initiated
            FROM [derived].[fact_antenatal_visit]
            WHERE is_art_initiated=1
        ) ini_anc
    ON mo.mother_id=ini_anc.mother_id AND mo.pregnancy_id=ini_anc.pregnancy_id
    LEFT JOIN
        (
            SELECT DISTINCT mother_id, pregnancy_id,  is_art_initiated_before
            FROM [derived].[fact_antenatal_visit]
            WHERE is_art_initiated_before=1
        ) ini_before_anc
    ON mo.mother_id=ini_before_anc.mother_id AND mo.pregnancy_id=ini_before_anc.pregnancy_id
    ;

    UPDATE mo
    SET
        is_new_hiv_positive_at_first_anc_refused = 1
    FROM 
        [derived].[fact_pregnancy_outcome] mo
    INNER JOIN
        (
            SELECT
                DISTINCT mother_id
            FROM
                derived.fact_antenatal_visit
            WHERE
                art_initiation = 'Refused ART'
        ) art_ref
    ON
        art_ref.mother_id=mo.mother_id;

    WITH anc_art_start AS 
    (
        SELECT
            *
        FROM
        (
            SELECT 
                mother_id,
                art_start_date,
                art_initiation,
                ROW_NUMBER() OVER (PARTITION BY mother_id ORDER BY art_start_date ASC) AS row_number
            FROM
                derived.fact_antenatal_visit
            WHERE
                art_initiation = 'Started on ART in ANC current pregnancy'
        ) a 
        WHERE a.row_number=1
    )

    UPDATE mo
    SET 
        new_hiv_positive_at_first_anc_start_time = DATEDIFF(DAY, st.art_start_date, first_anc_date),
        new_hiv_positive_at_first_anc_status = 
            CASE WHEN
                mo.is_art_initiated_before_anc = 1
            THEN
                'Already on ART before current pregnancy' 
            WHEN 
                st.art_start_date IS NOT NULL AND DATEDIFF(DAY, st.art_start_date, first_anc_date ) = 0
            THEN
                'Same day initiation'
            WHEN 
                st.art_start_date IS NOT NULL AND DATEDIFF(DAY, st.art_start_date, first_anc_date ) <= 7
            THEN
                'Within a week'
            WHEN 
                st.art_start_date IS NOT NULL AND DATEDIFF(DAY, st.art_start_date, first_anc_date ) > 7
            THEN
                '> 7 days'
            WHEN 
                is_new_hiv_positive_at_first_anc_refused = 1
            THEN
                'Refused'
            WHEN
                st.art_start_date IS NULL
            THEN
                'Missing'
            END
    FROM 
        [derived].[fact_pregnancy_outcome] mo
    LEFT JOIN
        anc_art_start st 
    ON
        st.mother_id=mo.mother_id;

    
    UPDATE 
        mo
    SET
        [is_art_initiated_before_or_in_anc_with_vl_result] = 
            CASE WHEN
                vl.pregnancy_id IS NOT NULL
            THEN
                1
            ELSE
                0
            END
    FROM 
        [derived].[fact_pregnancy_outcome] mo
    LEFT JOIN
        derived.fact_viral_load_result vl
    ON
        mo.pregnancy_id = vl.pregnancy_id
    WHERE 
        is_new_art_started_at_first_anc = 1
    AND
        vl_result!='Results Pending'
    ;


    WITH anc_most_recent_vls AS 
    (
        SELECT
            *
        FROM
        (
            SELECT 
                vl.pregnancy_id,
                vl.result_date,
                CASE WHEN
                    vl_result = 'Not Detected'
                THEN
                    1
                ELSE
                    0
                END AS is_vl_result_undetectable,
                CASE WHEN
                    vl_result = 'Target Detected' AND vl_copies < 40
                THEN
                    1
                ELSE
                    0
                END AS is_vl_result_less_than_40,
                CASE WHEN
                    vl_result = 'Target Detected' AND vl_copies >= 40 AND vl_copies <= 1000
                THEN
                    1
                ELSE
                    0
                END AS is_vl_result_40_to_1000_copies,
                CASE WHEN
                    vl_result = 'Target Detected' AND vl_copies > 1000
                THEN
                    1
                ELSE
                    0
                END AS is_vl_result_greater_than_1000_copies,
                ROW_NUMBER() OVER (PARTITION BY pregnancy_id ORDER BY result_date DESC) AS row_number
            FROM
                derived.fact_viral_load_result vl
            WHERE
                vl.result_source='Antenatal' AND vl.vl_result!='Results Pending'
        ) a 
        WHERE a.row_number=1
        
    )

    UPDATE 
        mo
    SET
        [is_art_initiated_before_or_in_anc_with_valid_vl_result] = 
            CASE WHEN
                a_vls.pregnancy_id IS NOT NULL
            THEN
                1
            ELSE
                0
            END,
        [is_art_initiated_before_or_in_anc_with_valid_undetectable_vl_result] = a_vls.is_vl_result_undetectable,
        [is_art_initiated_before_or_in_anc_with_valid_less_than_40_vl_result] = a_vls.is_vl_result_less_than_40,
        [is_art_initiated_before_or_in_anc_with_valid_40_to_1000_copies_vl_result] = a_vls.is_vl_result_40_to_1000_copies,
        [is_art_initiated_before_or_in_anc_with_valid_greater_than_1000_vl_result] = a_vls.is_vl_result_greater_than_1000_copies
    FROM 
        [derived].[fact_pregnancy_outcome] mo
    LEFT JOIN
        anc_most_recent_vls a_vls
    ON
        mo.pregnancy_id = a_vls.pregnancy_id
    WHERE 
        is_new_art_started_at_first_anc = 1 AND DATEDIFF(MONTH, a_vls.result_date, mo.last_anc_date ) <= 3
    ;

    UPDATE 
        mo
    SET 
        maternity_hiv_test_status = m.hiv_test_status,
        maternity_hiv_test_result = m.hiv_test_result,
        is_hiv_status_unknown_at_maternity = m.is_hiv_test_results_unknown,
        is_not_tested_at_maternity = is_hiv_test_status_not_tested,
        is_new_hiv_negative_at_maternity = m.is_hiv_test_results_negative,
        is_hiv_positive_at_36_weeks_retest_maternity = m.is_positive_at_36_weeks_hiv_re_test,
        is_art_initiated_during_anc_4_weeks_before_delivery = m.is_art_initiated_during_anc_4_weeks_before_delivery,
        is_art_initiated_during_anc_less_than_4_weeks_before_delivery = m.is_art_initiated_during_anc_less_than_4_weeks_before_delivery,
        is_known_hiv_positive_at_maternity = 
            CASE WHEN 
                m.is_hiv_test_status_previously_known = 1 OR m.is_anc_hiv_result_positive = 1
            THEN
                1
            ELSE
                0
            END,
        is_hiv_status_known_at_maternity = 
            CASE WHEN
                m.is_hiv_test_status_previously_known = 1 OR m.is_anc_hiv_test_status_known = 1
            THEN
                1
            ELSE
                0
            END,
        is_art_initiated_during_maternity = 
            CASE WHEN   
                (m.is_anc_hiv_result_positive = 1 OR m.is_hiv_test_results_positive = 1 OR m.is_positive_at_36_weeks_hiv_re_test = 1 ) AND m.is_art_initiated_during_lnd = 1
            THEN
                1
            ELSE
                0
            END
        ,
        is_art_initiated_already_at_maternity = 
            CASE WHEN   
                (m.is_anc_hiv_result_positive = 1 OR m.is_hiv_test_results_positive = 1 OR m.is_positive_at_36_weeks_hiv_re_test = 1 ) AND m.is_art_initiated_already = 1
            THEN
                1
            ELSE
                0
            END
        ,
        is_art_initiated_during_anc = 
            CASE WHEN   
                (m.is_anc_hiv_result_positive = 1 OR m.is_hiv_test_results_positive = 1 OR m.is_positive_at_36_weeks_hiv_re_test = 1 ) AND m.is_art_initiated_during_anc = 1
            THEN
                1
            ELSE
                0
            END
        ,
        is_art_refused_during_maternity = 
            CASE WHEN   
                (m.is_anc_hiv_result_positive = 1 OR m.is_hiv_test_results_positive = 1 OR m.is_positive_at_36_weeks_hiv_re_test = 1 ) AND m.is_refused_art = 1
            THEN
                1
            ELSE
                0
            END
        ,
        is_on_art_at_maternity = 
            CASE WHEN 
                (m.is_anc_hiv_result_positive = 1 OR m.is_hiv_test_results_positive = 1 OR m.is_positive_at_36_weeks_hiv_re_test = 1) 
                and 
                (m.is_art_initiated_during_lnd = 1 OR m.is_art_initiated_already = 1 OR m.is_art_initiated_during_anc = 1)
            THEN
                1
            ELSE
                0
            END,
        is_hiv_positive_at_maternity = 
            CASE WHEN
                m.is_anc_hiv_result_positive = 1 OR m.is_hiv_test_results_positive = 1 OR m.is_positive_at_36_weeks_hiv_re_test = 1
            THEN
                1
            ELSE
                0
            END,
        is_new_hiv_positive_at_maternity = 
            CASE WHEN 
                m.is_hiv_test_results_positive = 1
            THEN 
                1 
            ELSE 
                0 
            END,
        maternity_recent_viral_load_test_done = m.recent_viral_load_test_done,
        is_vl_result_undetectable_at_maternity = 
            CASE WHEN 
                m.viral_load_test_result = 'Not Detected' 
            THEN
                1
            ELSE
                0
            END,
        is_vl_result_less_than_40_at_maternity = 
            CASE WHEN
                m.viral_load_test_result = 'Target Detected' AND m.viral_load_copies < 40
            THEN
                1
            ELSE
                0
            END,
        is_vl_result_40_to_1000_copies_at_maternity =
            CASE WHEN
                m.viral_load_test_result = 'Target Detected' AND (m.viral_load_copies >= 40 AND m.viral_load_copies <= 1000)
            THEN
                1
            ELSE
                0
            END,
        is_vl_result_greater_than_1000_at_maternity = 
            CASE WHEN
                m.viral_load_test_result = 'Target Detected' AND (m.viral_load_copies > 1000)
            THEN
                1
            ELSE
                0
            END,
        is_vl_result_pending_at_maternity = 
            CASE WHEN
                m.viral_load_test_result = 'Results Pending'
            THEN
                1
            ELSE
                0
            END,
        is_virally_suppressed_at_maternity = m.is_viral_load_test_result_undetectable_or_less_than_40,
        is_not_virally_suppressed_at_maternity = m.is_viral_load_test_result_not_suppressed,
        _maternity_id = m.maternity_visit_id
    FROM 
        [derived].[fact_pregnancy_outcome] mo
    LEFT JOIN
        [derived].[fact_maternity_visit] m
    ON
        mo.pregnancy_id = m.pregnancy_id
    WHERE
        is_maternity_service_received = 1
    ;

    UPDATE 
        mo
    SET 
        is_received_complete_cascade =
            CASE WHEN
                is_pnc_service_received = 1 AND is_maternity_service_received = 1 AND is_anc_service_received = 1
            THEN
                1
            ELSE
                0
            END
    FROM 
        [derived].[fact_pregnancy_outcome] mo
    
    UPDATE [derived].[fact_pregnancy_outcome]
    SET 
        [is_new_hiv_positive_at_first_anc_status_already_on_art] = 
            CASE WHEN 
                new_hiv_positive_at_first_anc_status = 'Already on ART before current pregnancy'
            THEN
                1
            ELSE
                0
            END,
        [is_new_hiv_positive_at_first_anc_status_same_day] = 
            CASE WHEN 
                new_hiv_positive_at_first_anc_status = 'Same day initiation'
            THEN
                1
            ELSE
                0
            END,
        [is_new_hiv_positive_at_first_anc_status_within_7_days] = 
            CASE WHEN 
                new_hiv_positive_at_first_anc_status = 'Within a week'
            THEN
                1
            ELSE
                0
            END,
        [is_new_hiv_positive_at_first_anc_status_more_than_7_days] = 
            CASE WHEN 
                new_hiv_positive_at_first_anc_status = '> 7 days'
            THEN
                1
            ELSE
                0
            END,
        [is_new_hiv_positive_at_first_anc_status_refused] = 
            CASE WHEN 
                new_hiv_positive_at_first_anc_status = 'Refused'
            THEN
                1
            ELSE
                0
            END,
        [is_new_hiv_positive_at_first_anc_status_missing] = 
            CASE WHEN 
                new_hiv_positive_at_first_anc_status IS NULL
            THEN
                1
            ELSE
                0
            END
-- $END
SELECT TOP 400 *
FROM [derived].[fact_pregnancy_outcome]