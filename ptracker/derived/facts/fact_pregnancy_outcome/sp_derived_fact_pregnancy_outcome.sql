USE [staging_ptracker_test]
GO
-- $BEGIN

    EXEC sp_derived_fact_pregnancy_outcome_create;
    EXEC sp_derived_fact_pregnancy_outcome_insert;
    EXEC sp_derived_fact_pregnancy_outcome_update;
    EXEC sp_derived_fact_pregnancy_outcome_trim_facility_dimensions;

-- $END