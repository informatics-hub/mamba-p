USE [staging_ptracker_test]
GO

DROP TABLE IF exists [derived].[fact_pregnancy_outcome];

-- $BEGIN

    CREATE TABLE [derived].[fact_pregnancy_outcome](
        [pregnancy_outcome_id] [uniqueidentifier] NOT NULL,
        [mother_id] [uniqueidentifier] NULL,
        [pregnancy_id] [uniqueidentifier] NULL,
        [ptracker_identifier] [nvarchar] (255) NULL, 
        [openmrs_patient_identifier] [int] NULL,
        [date_of_birth] [date] NULL,
        [date_of_birth_estimated] [int] NULL,
        [mother_first_anc_age] [nvarchar] (255) NULL,
        [mother_first_anc_age_group_id] [uniqueidentifier] NULL,
        [mother_lnd_age] [nvarchar] (255) NULL,
        [mother_lnd_age_group_id] [uniqueidentifier] NULL,
        [district] [nvarchar] (255) NULL,
        [region] [nvarchar] (255) NULL,
        [facility_id] [uniqueidentifier] NULL,
        [date_created] [date] NULL,
        [date_created_id] [uniqueidentifier] NULL,
        [first_anc_date] [date] NULL,
        [first_anc_date_id] [uniqueidentifier] NULL,
        [last_anc_date] [date] NULL,
        [last_anc_date_id] [uniqueidentifier] NULL,
        [maternity_date] [date] NULL,
        [maternity_date_id] [uniqueidentifier] NULL,
        [first_pnc_date] [date] NULL,
        [first_pnc_date_id] [uniqueidentifier] NULL,
        [first_anc_facility_id] [uniqueidentifier] NULL,
        [maternity_facility_id] [uniqueidentifier] NULL,
        [first_pnc_facility_id] [uniqueidentifier]  NULL,
        [is_anc_service_received] [tinyint] NULL,
        [is_maternity_service_received] [tinyint] NULL,
        [is_pnc_service_received] [tinyint] NULL,
        [is_received_complete_cascade] [tinyint] NULL,
        [is_new_first_visit_at_anc] [tinyint] NULL,
        [is_new_tested_hiv_at_first_anc] [tinyint] NULL,
        [is_new_tested_hiv_positive_at_first_anc] [tinyint] NULL,
        [is_new_tested_hiv_negative_at_first_anc] [tinyint] NULL,
        [is_new_known_positive_at_first_anc] [tinyint] NULL,
        [is_new_all_positive_at_first_anc] [tinyint] NULL,
        [is_new_art_started_at_first_anc] [tinyint] NULL,
        [is_new_hiv_status_known_at_first_anc] [tinyint] NULL,
        [new_hiv_positive_at_first_anc_start_time] [int] NULL,
        [is_new_hiv_positive_at_first_anc_refused] [tinyint] NULL,
        [is_hiv_positive] [tinyint] NULL,
        [is_art_initiated_in_anc] [tinyint] NULL,
        [is_art_initiated_before_anc] [tinyint] NULL,
        [is_art_initiated_before_or_in_anc] [tinyint] NULL,
        [is_art_initiated_before_or_in_anc_with_vl_result] [tinyint] NULL,
        [is_art_initiated_before_or_in_anc_with_valid_vl_result] [tinyint] NULL,
        [is_art_initiated_before_or_in_anc_with_valid_undetectable_vl_result] [tinyint] NULL,
        [is_art_initiated_before_or_in_anc_with_valid_less_than_40_vl_result] [tinyint] NULL,
        [is_art_initiated_before_or_in_anc_with_valid_40_to_1000_copies_vl_result] [tinyint] NULL,
        [is_art_initiated_before_or_in_anc_with_valid_greater_than_1000_vl_result] [tinyint] NULL,
        [new_hiv_positive_at_first_anc_status] [nvarchar] (255) NULL,
        [is_new_hiv_positive_at_first_anc_status_already_on_art] [tinyint] NULL,
        [is_new_hiv_positive_at_first_anc_status_same_day] [tinyint] NULL,
        [is_new_hiv_positive_at_first_anc_status_within_7_days] [tinyint] NULL,
        [is_new_hiv_positive_at_first_anc_status_more_than_7_days] [tinyint] NULL,
        [is_new_hiv_positive_at_first_anc_status_refused] [tinyint] NULL,
        [is_new_hiv_positive_at_first_anc_status_missing] [tinyint] NULL,
        [maternity_hiv_test_status] [nvarchar] (255) NULL,
        [maternity_hiv_test_result] [nvarchar] (255) NULL,
        [is_new_hiv_positive_at_maternity] [tinyint] NULL,
        [is_new_hiv_negative_at_maternity] [tinyint] NULL,
        [is_hiv_positive_at_36_weeks_retest_maternity] [tinyint] NULL,
        [is_known_hiv_positive_at_maternity] [tinyint] NULL,
        [is_hiv_status_known_at_maternity] [tinyint] NULL,
        [is_hiv_status_unknown_at_maternity] [tinyint] NULL,
        [is_not_tested_at_maternity] [tinyint] NULL,
        [is_on_art_at_maternity] [tinyint] NULL,
        [is_art_initiated_during_maternity] [tinyint] NULL,
        [is_art_refused_during_maternity] [tinyint] NULL,
        [is_art_initiated_already_at_maternity] [tinyint] NULL,
        [is_art_initiated_during_anc] [tinyint] NULL,
        [is_hiv_positive_at_maternity] [tinyint] NULL,
        [is_art_initiated_during_anc_4_weeks_before_delivery] [tinyint] NULL,
        [is_art_initiated_during_anc_less_than_4_weeks_before_delivery] [tinyint] NULL,
        [maternity_recent_viral_load_test_done] [nvarchar] (255) NULL,
        [is_virally_suppressed_at_maternity] [tinyint] NULL,
        [is_vl_result_undetectable_at_maternity] [tinyint] NULL,
        [is_vl_result_less_than_40_at_maternity] [tinyint] NULL,
        [is_vl_result_40_to_1000_copies_at_maternity] [tinyint] NULL,
        [is_vl_result_greater_than_1000_at_maternity] [tinyint] NULL,
        [is_vl_result_pending_at_maternity] [tinyint] NULL,
        [is_not_virally_suppressed_at_maternity] [tinyint] NULL,
        [_first_anc_visit_id] [uniqueidentifier] NULL,
        [_maternity_id] [uniqueidentifier] NULL
    );

    ALTER TABLE [derived].[fact_pregnancy_outcome] ADD CONSTRAINT [PK_fact_pregnancy_outcome_derived] PRIMARY KEY ([pregnancy_outcome_id]);

    ALTER TABLE [derived].[fact_pregnancy_outcome] ADD CONSTRAINT FK_fact_pregnancy_outcome_dim_mother_derived FOREIGN KEY ([mother_id]) REFERENCES [derived].[dim_mother]([mother_id]);

    ALTER TABLE [derived].[fact_pregnancy_outcome] ADD CONSTRAINT FK_fact_pregnancy_outcome_dim_pregnancy_derived FOREIGN KEY ([pregnancy_id]) REFERENCES [derived].[dim_pregnancy]([pregnancy_id]);

    ALTER TABLE [derived].[fact_pregnancy_outcome] ADD CONSTRAINT FK_fact_pregnancy_outcome_dim_age_group_anc_derived FOREIGN KEY ([mother_first_anc_age_group_id]) REFERENCES [derived].[dim_mother_age_group_anc]([mother_age_group_id]);

    ALTER TABLE [derived].[fact_pregnancy_outcome] ADD CONSTRAINT FK_fact_pregnancy_outcome_dim_age_group_lnd_derived FOREIGN KEY ([mother_lnd_age_group_id]) REFERENCES [derived].[dim_mother_age_group_maternity]([mother_age_group_id]);

    ALTER TABLE [derived].[fact_pregnancy_outcome] ADD CONSTRAINT FK_fact_pregnancy_outcome_dim_facility_derived FOREIGN KEY ([facility_id]) REFERENCES [derived].[dim_facility]([facility_id]);

    ALTER TABLE [derived].[fact_pregnancy_outcome] ADD CONSTRAINT FK_fact_pregnancy_outcome_dim_anc_facility_derived FOREIGN KEY ([first_anc_facility_id]) REFERENCES [derived].[dim_anc_facility]([facility_id]);

    ALTER TABLE [derived].[fact_pregnancy_outcome] ADD CONSTRAINT FK_fact_pregnancy_outcome_dim_maternity_facility_derived FOREIGN KEY ([maternity_facility_id]) REFERENCES [derived].[dim_maternity_facility]([facility_id]);

    ALTER TABLE [derived].[fact_pregnancy_outcome] ADD CONSTRAINT FK_fact_pregnancy_outcome_dim_mother_pnc_facility_derived FOREIGN KEY ([first_pnc_facility_id]) REFERENCES [derived].[dim_mother_pnc_facility]([facility_id]);

    ALTER TABLE [derived].[fact_pregnancy_outcome] ADD CONSTRAINT FK_fact_pregnancy_outcome_dim_date_derived FOREIGN KEY ([date_created_id]) REFERENCES [derived].[dim_anc_date]([date_id]);

    ALTER TABLE [derived].[fact_pregnancy_outcome] ADD CONSTRAINT FK_fact_pregnancy_outcome_dim_first_anc_date_derived FOREIGN KEY ([first_anc_date_id]) REFERENCES [derived].[dim_anc_date]([date_id]);

    ALTER TABLE [derived].[fact_pregnancy_outcome] ADD CONSTRAINT FK_fact_pregnancy_outcome_dim_last_anc_date_derived FOREIGN KEY ([last_anc_date_id]) REFERENCES [derived].[dim_anc_date]([date_id]);

    ALTER TABLE [derived].[fact_pregnancy_outcome] ADD CONSTRAINT FK_fact_pregnancy_outcome_dim_maternity_date_derived FOREIGN KEY ([maternity_date_id]) REFERENCES [derived].[dim_maternity_date]([date_id]);

    ALTER TABLE [derived].[fact_pregnancy_outcome] ADD CONSTRAINT FK_fact_pregnancy_outcome_dim_first_pnc_date_derived FOREIGN KEY ([first_pnc_date_id]) REFERENCES [derived].[dim_pnc_date]([date_id]);
    
-- $END