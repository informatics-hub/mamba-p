USE [staging_ptracker_test]
GO

TRUNCATE TABLE [derived].[fact_pregnancy_outcome];

-- $BEGIN

    WITH min_anc AS 
    (
        SELECT 
            encounter_date, 
            a.antenatal_visit_id,
            a.mother_id, 
            a.pregnancy_id, 
            facility_id, 
            encounter_date_id, 
            1 AS anc_service_received,
            ROW_NUMBER() OVER (PARTITION BY mother_id,pregnancy_id ORDER BY encounter_date ASC) AS row_number
        FROM 
            [derived].[fact_antenatal_visit] a
    ),
    max_anc AS 
    (
        SELECT 
            encounter_date, 
            a.mother_id, 
            a.pregnancy_id, 
            encounter_date_id,
            ROW_NUMBER() OVER (PARTITION BY mother_id,pregnancy_id ORDER BY encounter_date DESC) AS row_number
        FROM 
            [derived].[fact_antenatal_visit] a
    ),
    min_mat AS 
    (
        SELECT 
            encounter_date, 
            a.mother_id, 
            a.pregnancy_id, 
            facility_id, 
            encounter_date_id, 
            1 AS mat_service_received,
            ROW_NUMBER() OVER (PARTITION BY mother_id,pregnancy_id ORDER BY encounter_date ASC) AS row_number
        FROM 
            [derived].[fact_maternity_visit] a
    ),
    min_pnc AS 
    (
        SELECT 
            encounter_date, 
            a.mother_id,
            a.pregnancy_id, 
            facility_id, 
            encounter_date_id, 
            1 AS pnc_service_received,
            ROW_NUMBER() OVER (PARTITION BY mother_id,pregnancy_id ORDER BY encounter_date ASC) AS row_number
        FROM 
            [derived].[fact_mother_postnatal_visit] a
    )

    INSERT INTO [derived].[fact_pregnancy_outcome](
        [pregnancy_outcome_id],
        [mother_id],
        [pregnancy_id],
        [ptracker_identifier], 
        [openmrs_patient_identifier],
        [date_of_birth],
        [date_of_birth_estimated],
        [district],
        [region],
        [facility_id],
        [date_created],
        [date_created_id],
        [first_anc_date],
        [maternity_date],
        [first_pnc_date],
        [first_anc_facility_id],
        [maternity_facility_id],
        [first_pnc_facility_id],
        [first_anc_date_id],
        [maternity_date_id],
        [first_pnc_date_id],
        [last_anc_date],
        [last_anc_date_id],
        [is_anc_service_received],
        [is_maternity_service_received],
        [is_pnc_service_received],
        [_first_anc_visit_id]
    )
    
    SELECT DISTINCT
        NEWID(),
        m.mother_id,
        p.pregnancy_id,
        p.ptracker_identifier,
        m.openmrs_patient_identifier,
        m.date_of_birth,
        m.date_of_birth_estimated,
        m.district,
        m.region,
        m.facility_id,
        m.patient_date_created,
        d_create.date_id,
        min_anc.encounter_date,
        min_mat.encounter_date,
        min_pnc.encounter_date,
        min_anc.facility_id,
        min_mat.facility_id,
        min_pnc.facility_id,
        min_anc.encounter_date_id,
        min_mat.encounter_date_id,
        min_pnc.encounter_date_id,
        max_anc.encounter_date,
        max_anc.encounter_date_id,
        min_anc.anc_service_received,
        min_mat.mat_service_received,
        min_pnc.pnc_service_received,
        min_anc.antenatal_visit_id
    FROM 
        [derived].[dim_mother] m
    LEFT JOIN
        [derived].[dim_pregnancy] p ON m.mother_id=p.mother_id
    LEFT JOIN
        [derived].[dim_date] d_create ON d_create.date_yyyymmdd=m.patient_date_created
    LEFT JOIN
        min_anc ON min_anc.mother_id=m.mother_id AND min_anc.pregnancy_id=p.pregnancy_id AND min_anc.row_number=1
    LEFT JOIN
        max_anc ON max_anc.mother_id=m.mother_id AND max_anc.pregnancy_id=p.pregnancy_id AND max_anc.row_number=1
    LEFT JOIN
        min_mat ON min_mat.mother_id=m.mother_id AND min_mat.pregnancy_id=p.pregnancy_id AND min_mat.row_number=1
    LEFT JOIN
        min_pnc ON min_pnc.mother_id=m.mother_id AND min_pnc.pregnancy_id=p.pregnancy_id AND min_pnc.row_number=1
    
-- $END

SELECT TOP 400 *
FROM [derived].[fact_pregnancy_outcome]


