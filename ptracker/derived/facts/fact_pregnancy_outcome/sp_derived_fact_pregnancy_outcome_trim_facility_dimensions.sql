USE staging_ptracker_test;
GO

--Count facilities before trimming

    SELECT COUNT(*) count_dim_anc_facility_before_trim
    FROM derived.dim_anc_facility;
    
    SELECT COUNT(*) count_dim_maternity_facility_before_trim
    FROM derived.dim_maternity_facility;

    SELECT COUNT(*) count_dim_mother_pnc_facility_before_trim
    FROM derived.dim_mother_pnc_facility;

-- $BEGIN

    DELETE a
    FROM derived.dim_anc_facility a
    WHERE 
        NOT EXISTS
            (
                SELECT 1
                FROM derived.fact_pregnancy_outcome b
                WHERE a.facility_id=b.first_anc_facility_id
            )

    DELETE a
    FROM derived.dim_maternity_facility a
    WHERE 
        NOT EXISTS
            (
                SELECT 1
                FROM derived.fact_pregnancy_outcome b
                WHERE a.facility_id=b.maternity_facility_id
            )
    
    DELETE a
    FROM derived.dim_mother_pnc_facility a
    WHERE 
        NOT EXISTS
            (
                SELECT 1
                FROM derived.fact_pregnancy_outcome b
                WHERE a.facility_id=b.first_pnc_facility_id
            )


-- $END

--Count facilities after trimming

    SELECT COUNT(*) count_dim_anc_facility_after_trim
    FROM derived.dim_anc_facility;
    
    SELECT COUNT(*) count_dim_maternity_facility_after_trim
    FROM derived.dim_maternity_facility;

    SELECT COUNT(*) count_dim_mother_pnc_facility_after_trim
    FROM derived.dim_mother_pnc_facility;

