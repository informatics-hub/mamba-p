USE [staging_ptracker_test]
GO

-- $BEGIN

    UPDATE 
        [derived].[fact_antenatal_visit]
    SET 
        [is_first_visit] = 
            CASE WHEN 
                [visit_type] = 'New ANC Visit' 
            THEN 
                1
            ELSE
                0
            END,
        [is_hiv_test_status_previously_known] = 
            CASE WHEN 
                hiv_test_status = 'Previously known positive'
            THEN 
                1
            ELSE
                0
            END,
        [is_hiv_test_status_tested] = 
            CASE WHEN 
                hiv_test_status = 'Tested for HIV during this visit'
            THEN 
                1
           ELSE
                0
            END,
        [is_hiv_test_status_not_tested] = 
            CASE WHEN 
                hiv_test_status = 'Not tested for HIV during this visit'
            THEN 
                1
            ELSE
                0
            END,
        [is_hiv_test_results_positive] = 
            CASE WHEN 
                hiv_test_results = 'Positive'
            THEN 
                1
            ELSE
                0
            END,
        [is_hiv_test_results_negative] = 
            CASE WHEN 
                hiv_test_results = 'Negative'
            THEN 
                1
            ELSE
                0
            END,
        [is_hiv_test_results_unknown] = 
            CASE WHEN 
                hiv_test_results = 'Unknown'
            THEN
                1
            ELSE
                0
            END,
        [is_hiv_positive] = 
            CASE WHEN 
                hiv_test_results = 'Positive' OR hiv_test_status = 'Previously known positive'
            THEN 
                1
            ELSE
                0
            END,
        [is_partner_hiv_test_done] = 
            CASE WHEN 
                partner_hiv_test_done = 'Yes'
            THEN 
                1
            ELSE
                0
            END,
        [is_partner_known_positive] = 
            CASE WHEN 
                partner_hiv_test_done = 'Previously known positive'
            THEN 
                1
            ELSE
                0
            END,
        [is_partner_hiv_positive] = 
            CASE WHEN 
                partner_hiv_test_status = 'Positive'    
            OR
                partner_hiv_test_done = 'Previously known positive'
            THEN 
                1
            ELSE
                0
            END,
        [is_art_initiated] = 
            CASE WHEN 
                art_initiation = 'Started on ART in ANC current pregnancy'
            THEN 
                1
            ELSE
                0
            END,
        [is_art_initiated_before] = 
            CASE WHEN 
                art_initiation = 'Already on ART before current pregnancy'
            THEN 
                1
            ELSE
                0
            END,
        [is_art_not_initiated] = 
            CASE WHEN 
                art_initiation IN 
                (
                    'Refused ART','Not started due to stockout of ART'
                ) 
            THEN 
                1
            ELSE
                0
            END,
        [is_refused_art] = 
            CASE WHEN 
                art_initiation = 'Refused ART'
            THEN 
                1
            ELSE
                0
            END,
        [is_stock_out] = 
            CASE WHEN 
                art_initiation = 'Not started due to stockout of ART'
            THEN 
                1
            ELSE
                0
            END,
        [is_viral_load_test_result_target_detected] = 
            CASE WHEN 
                viral_load_test_result = 'Target Detected'
            THEN 
                1
            ELSE
                0
            END,
        [is_viral_load_test_result_not_detected] = 
            CASE WHEN 
                viral_load_test_result = 'Not Detected'
            THEN 
                1
            ELSE
                0
            END,
        [is_viral_load_test_result_sample_rejected] = 
            CASE WHEN 
                viral_load_test_result = 'Sample Rejected'
            THEN 
                1
            ELSE
                0
            END,
        [is_viral_load_test_result_results_pending] = 
            CASE WHEN 
                viral_load_test_result = 'Results Pending'
            THEN 
                1
            ELSE
                0
            END,
        [is_viral_load_test_result_undetectable_or_less_than_40] = 
            CASE WHEN 
                viral_load_test_result = 'Not Detected' 
                OR 
                (
                    viral_load_test_result = 'Target Detected' AND viral_load_copies < 40
                )
            THEN
                1
            ELSE
                0
            END,
        [is_viral_load_test_result_not_suppressed] = 
            CASE WHEN 
                viral_load_test_result = 'Target Detected' AND viral_load_copies >= 40
            THEN 
                1
            ELSE
                0
            END,
        [is_recent_viral_load_test_done] = 
            CASE WHEN 
                recent_viral_load_test_done = 'Yes'
            THEN 
                1
            ELSE
                0
            END
    ;

    WITH first_anc AS 
    (
        SELECT 
            antenatal_visit_id,
            pregnancy_id,
            lnmp
        FROM 
            [derived].[fact_antenatal_visit]
        WHERE 
            is_first_visit = 1
    )

    UPDATE 
        [derived].[fact_antenatal_visit]
    SET 
        [gestation_period] = DATEDIFF(WEEK, fa.lnmp , a.encounter_date)
    FROM 
        [derived].[fact_antenatal_visit] a
    INNER JOIN
        first_anc fa 
    ON
        fa.pregnancy_id= a.pregnancy_id
    WHERE 
        a.is_first_visit = 0


-- $END
SELECT TOP 400 *
FROM [derived].[fact_antenatal_visit]