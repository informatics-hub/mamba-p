USE [staging_ptracker_test]
GO

DROP TABLE IF exists [derived].[fact_antenatal_visit];

-- $BEGIN

    CREATE TABLE [derived].[fact_antenatal_visit](
        [antenatal_visit_id] UNIQUEIDENTIFIER NOT NULL,
        [pregnancy_id] [uniqueidentifier] NULL,
        [mother_id] [uniqueidentifier] NULL,
        [facility_id] [uniqueidentifier] NULL,
        [date_created] [date] NULL, 
        [date_created_id] [uniqueidentifier] NULL,
        [creator] [nvarchar](255) NOT NULL,
        [provider] [nvarchar](255) NOT NULL,
        [encounter_date] [date] NULL, 
        [encounter_date_id] [uniqueidentifier] NULL,
        [age_group_id] [uniqueidentifier] NULL,
        [age] [int] NULL,
        [visit_type] [nvarchar] (255) NULL,
        [is_first_visit] [int] NULL,
        [para] [int] NULL,
        [lnmp] [date] NULL,
        [lnmp_id] [uniqueidentifier] NULL,
        [gravida] [int] NULL,
        [edd] [date] NULL,
        [edd_id] [uniqueidentifier] NULL,
        [hiv_test_status] [nvarchar] (255) NULL,
        [is_hiv_test_status_previously_known] [int] NULL,
        [is_hiv_test_status_tested] [int] NULL,
        [is_hiv_test_status_not_tested] [int] NULL,
        [hiv_test_results] [nvarchar] (255) NULL,
        [is_hiv_test_results_negative] [int] NULL,
        [is_hiv_test_results_positive] [int] NULL,
        [is_hiv_test_results_unknown] [int] NULL, 
        [is_hiv_positive] [int] NULL,      
        [partner_hiv_test_status] [nvarchar] (255) NULL,
        [partner_hiv_test_done] [nvarchar] (255) NULL,
        [is_partner_hiv_test_done] [int] NULL,
        [is_partner_known_positive] [int] NULL,
        [is_partner_hiv_positive] [int] NULL,
        [partner_hiv_test_date] [date] NULL,
        [partner_hiv_test_date_id] [uniqueidentifier] NULL,
        [art_initiation] [nvarchar] (255) NULL,
        [is_art_initiated] [int] NULL,
        [is_art_initiated_before] [int] NULL,
        [is_art_not_initiated] [int] NULL,
        [is_refused_art] [int] NULL,
        [is_stock_out] [int] NULL,
        [art_start_date] [date] NULL,
        [art_start_date_id] [uniqueidentifier] NULL,
        [reason_for_refusing_art] [nvarchar] (255) NULL,
        [viral_load_test_result] [nvarchar] (255) NULL,
        [is_viral_load_test_result_target_detected] [int] NULL,
        [is_viral_load_test_result_not_detected] [int] NULL,
        [is_viral_load_test_result_sample_rejected] [int] NULL,
        [is_viral_load_test_result_results_pending] [int] NULL,
        [viral_load_test_date] [date] NULL,
        [viral_load_test_date_id] [uniqueidentifier] NULL,
        [viral_load_copies] [float] NULL,
        [is_viral_load_test_result_undetectable_or_less_than_40] [int] NULL,
        [is_viral_load_test_result_not_suppressed] [int] NULL,
        [recent_viral_load_test_done] [nvarchar] (255) NULL,
        [is_recent_viral_load_test_done] [int] NULL,
        [next_visit_date] [date] NULL,
        [next_visit_date_id] [uniqueidentifier] NULL,
        [facility_of_next_appointment] [nvarchar] (255) NULL,
        [gestation_period] [int] NULL
    );

    ALTER TABLE [derived].[fact_antenatal_visit] ADD CONSTRAINT [PK_fact_antenatal_visit_derived] PRIMARY KEY ([antenatal_visit_id]);

    ALTER TABLE [derived].[fact_antenatal_visit] ADD CONSTRAINT FK_fact_antenatal_visit_dim_date_encounter_derived FOREIGN KEY ([encounter_date_id]) REFERENCES [derived].[dim_anc_date]([date_id]);

    ALTER TABLE [derived].[fact_antenatal_visit] ADD CONSTRAINT FK_fact_antenatal_visit_dim_date_created_derived FOREIGN KEY ([date_created_id]) REFERENCES [derived].[dim_anc_date]([date_id]);

    ALTER TABLE [derived].[fact_antenatal_visit] ADD CONSTRAINT FK_fact_antenatal_visit_dim_facility_derived FOREIGN KEY ([facility_id]) REFERENCES [derived].[dim_facility]([facility_id]);

    ALTER TABLE [derived].[fact_antenatal_visit] ADD CONSTRAINT FK_fact_antenatal_visit_dim_age_group_derived FOREIGN KEY ([age_group_id]) REFERENCES [derived].[dim_mother_age_group_anc]([mother_age_group_id]);

    ALTER TABLE [derived].[fact_antenatal_visit] ADD CONSTRAINT FK_fact_antenatal_visit_dim_antenatal_lnmp_derived FOREIGN KEY ([lnmp_id]) REFERENCES [derived].[dim_anc_date]([date_id]);

    ALTER TABLE [derived].[fact_antenatal_visit] ADD CONSTRAINT FK_fact_antenatal_visit_dim_antenatal_edd_derived FOREIGN KEY ([edd_id]) REFERENCES [derived].[dim_anc_date]([date_id]);

    ALTER TABLE [derived].[fact_antenatal_visit] ADD CONSTRAINT FK_fact_antenatal_visit_dim_antenatal_partner_test_derived FOREIGN KEY ([partner_hiv_test_date_id]) REFERENCES [derived].[dim_anc_date]([date_id]);

    ALTER TABLE [derived].[fact_antenatal_visit] ADD CONSTRAINT FK_fact_antenatal_visit_dim_date_art_start_derived FOREIGN KEY ([art_start_date_id]) REFERENCES [derived].[dim_anc_date]([date_id]);

    ALTER TABLE [derived].[fact_antenatal_visit] ADD CONSTRAINT FK_fact_antenatal_visit_dim_date_vl_test_derived FOREIGN KEY ([viral_load_test_date_id]) REFERENCES [derived].[dim_anc_date]([date_id]);

    ALTER TABLE [derived].[fact_antenatal_visit] ADD CONSTRAINT FK_fact_antenatal_visit_dim_date_next_visit_derived FOREIGN KEY ([next_visit_date_id]) REFERENCES [derived].[dim_anc_date]([date_id]);    
    
    ALTER TABLE [derived].[fact_antenatal_visit] ADD CONSTRAINT FK_fact_antenatal_visit_dim_pregnancy FOREIGN KEY ([pregnancy_id]) REFERENCES [derived].[dim_pregnancy]([pregnancy_id]);

    ALTER TABLE [derived].[fact_antenatal_visit] ADD CONSTRAINT FK_fact_antenatal_visit_dim_mother FOREIGN KEY ([mother_id]) REFERENCES [derived].[dim_mother]([mother_id]);
-- $END