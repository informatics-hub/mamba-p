USE [staging_ptracker_test]
GO
-- $BEGIN

    EXEC sp_derived_fact_antenatal_visit_create;
    EXEC sp_derived_fact_antenatal_visit_insert;
    EXEC sp_derived_fact_antenatal_visit_update;

-- $END