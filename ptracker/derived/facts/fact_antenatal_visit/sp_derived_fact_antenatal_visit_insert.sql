USE [staging_ptracker_test]
GO

TRUNCATE TABLE [derived].[fact_antenatal_visit]

-- $BEGIN

    INSERT INTO [derived].[fact_antenatal_visit](
        [antenatal_visit_id],
        [pregnancy_id],
        [mother_id],
        [facility_id],
        [creator],
        [provider],
        [encounter_date],
        [encounter_date_id],
        [date_created],
        [date_created_id],
        [age],
        [age_group_id],
        [visit_type],
        [para],
        [lnmp],
        [lnmp_id],
        [gravida],
        [edd],
        [edd_id],
        [hiv_test_status],
        [hiv_test_results],
        [partner_hiv_test_status],
        [partner_hiv_test_done],
        [partner_hiv_test_date],
        [partner_hiv_test_date_id],
        [reason_for_refusing_art],
        [art_start_date],
        [art_start_date_id],
        [art_initiation],
        [viral_load_test_result],
        [viral_load_test_date],
        [viral_load_test_date_id],
        [viral_load_copies],
        [recent_viral_load_test_done],
        [next_visit_date],
        [next_visit_date_id],
        [facility_of_next_appointment]
    )
    SELECT DISTINCT
        NEWID(),
        prg.pregnancy_id,
        prg.mother_id,
        e.[facility_id],
        e.[creator],
        e.[provider],
        e.[encounter_date],
        e.[encounter_date_id],
        e.[date_created],
        e.[date_created_id],
        FLOOR(DATEDIFF(DAY,mo.[date_of_birth],e.[encounter_date])/365.25),
        mage.mother_age_group_id,
        fa.visit_type,
        fa.para,
        fa.lnmp,
        lnmp.date_id,
        fa.gravida,
        fa.edd,
        edd.date_id,
        fa.hiv_test_status,
        fa.hiv_test_results,
        fa.partner_hiv_test_status,
        fa.partner_hiv_test_done,
        fa.partner_hiv_test_date,
        partner.date_id,
        fa.reason_for_refusing_art,
        fa.art_start_date,
        start_art.date_id,
        fa.art_initiation,
        fa.viral_load_test_result,
        fa.viral_load_test_date,
        vl_date.date_id,
        fa.viral_load_copies,
        fa.recent_viral_load_test_done,
        fa.next_visit_date,
        next_visit.date_id,
        fa.facility_of_next_appointment
    FROM 
        [base].[fact_antenatal_visit] fa
    LEFT JOIN
        [derived].[dim_pregnancy] prg ON fa.ptracker_identifier = prg.ptracker_identifier
    INNER JOIN
        [base].[dim_encounter] e ON fa.encounter_id=e.encounter_id
    INNER JOIN
        [derived].[dim_mother] mo ON mo.[mother_id] = prg.[mother_id]
    LEFT JOIN
        [derived].[dim_anc_date] lnmp ON fa.lnmp=lnmp.date_slash_yyyymd
    LEFT JOIN
        [derived].[dim_anc_date] next_visit ON fa.next_visit_date=next_visit.date_slash_yyyymd
    LEFT JOIN
        [derived].[dim_anc_date] vl_date ON fa.viral_load_test_date=vl_date.date_slash_yyyymd
    LEFT JOIN
        [derived].[dim_anc_date] edd ON fa.edd=edd.date_slash_yyyymd
    LEFT JOIN
        [derived].[dim_anc_date] start_art ON fa.art_start_date=start_art.date_slash_yyyymd
    LEFT JOIN
        [derived].[dim_anc_date] partner ON fa.partner_hiv_test_date=partner.date_slash_yyyymd
    LEFT JOIN
        [derived].[dim_mother_age_group_anc] mage ON mage.age = FLOOR(DATEDIFF(DAY,mo.[date_of_birth],e.[encounter_date])/365.25)

-- $END
SELECT TOP 400 *
FROM [derived].[fact_antenatal_visit]