USE [staging_ptracker_test]
GO

DROP TABLE IF exists [derived].[fact_infant_postnatal_visit];

-- $BEGIN

    CREATE TABLE [derived].[fact_infant_postnatal_visit](
        [infant_postnatal_visit_id] [uniqueidentifier] NOT NULL,
        [infant_id] [uniqueidentifier] NULL,
        [pregnancy_id] [uniqueidentifier] NULL,
        [facility_id] [uniqueidentifier] NULL,
        [creator] [nvarchar](255) NOT NULL,
        [provider] [nvarchar](255) NOT NULL,
        [encounter_date] [date] NULL,
        [encounter_date_id] [uniqueidentifier] NULL,
        [date_created] [date] NULL, 
        [date_created_id] [uniqueidentifier] NULL,
        [age_in_months] [int] NULL,
        [age_in_weeks] [int] NULL,
        [infant_age_group_id] [uniqueidentifier] NULL,
        [is_first_visit] [tinyint] NULL,
        [is_repeat_visit] [tinyint] NULL,
        [hiv_exposure] [nvarchar] (255) NULL,
        [is_hiv_exposed] [tinyint] NULL,
        [is_hiv_unexposed] [tinyint] NULL,
        [arv_prophylaxis_status] [nvarchar] (255) NULL,
        [is_prophylaxis_arv] [tinyint] NULL,
        [arv_prophylaxis_adherence] [nvarchar] (255) NULL,
        [ctx_prophylaxis_status] [nvarchar] (255) NULL,
        [ctx_prophylaxis_adherence] [nvarchar] (255) NULL,
        [is_prophylaxis_started_ctx] [tinyint] NULL,
        [hiv_test_status] [nvarchar] (255) NULL,
        [hiv_test_type] [nvarchar] (255) NULL,
        [is_hiv_test_type_pcr] [tinyint] NULL,
        [is_hiv_test_type_rapid] [tinyint] NULL,
        [dna_pcr_test_result] [nvarchar] (255) NULL,
        [rapid_test_result] [nvarchar] (255) NULL,
        [confirmatory_test_done] [nvarchar] (255) NULL,
        [is_confirmatory_test_done] [tinyint] NULL,
        [final_test_result] [nvarchar] (255) NULL,
        [is_positive_confirmatory_test] [tinyint] NULL,
        [is_negative_confirmatory_test] [tinyint] NULL,
        [linked_to_art] [nvarchar] (255) NULL,
        [is_linked_to_art] [tinyint] NULL,
        [breastfeeding_status] [nvarchar] (255) NULL,
        [is_exclusive_breast_feeding] [tinyint] NULL,
        [other_feeding_method] [nvarchar] (255) NULL,
        [outcome_status] [nvarchar] (255) NULL,
        [is_outcome_status_in_care] [tinyint] NULL,
        [is_outcome_status_transfer_in] [tinyint] NULL,
        [is_outcome_status_negative] [tinyint] NULL,
        [is_outcome_status_transfer_out] [tinyint] NULL,
        [is_outcome_status_lost] [tinyint] NULL,
        [is_outcome_status_dead] [tinyint] NULL,
        [transfer_in_from] [nvarchar] (max) NULL,
        [transfer_in_date] [date] NULL,
        [transfer_in_date_id] [uniqueidentifier] NULL, 
        [transfer_out_to] [nvarchar] (max) NULL,
        [transfer_out_to_date] [date] NULL,
        [transfer_out_to_date_id] [uniqueidentifier] NULL,
        [death_date] [date] NULL,
        [death_date_id] [uniqueidentifier] NULL, 
        [next_visit_date] [date] NULL,
        [next_visit_date_id] [uniqueidentifier] NULL
    );

    ALTER TABLE [derived].[fact_infant_postnatal_visit] ADD CONSTRAINT [PK_fact_infant_postnatal_visit_derived] PRIMARY KEY ([infant_postnatal_visit_id]);

    ALTER TABLE [derived].[fact_infant_postnatal_visit] ADD CONSTRAINT FK_fact_infant_postnatal_visit_dim_infant_derived FOREIGN KEY ([infant_id]) REFERENCES [derived].[dim_infant]([infant_id]);

    ALTER TABLE [derived].[fact_infant_postnatal_visit] ADD CONSTRAINT FK_fact_infant_postnatal_visit_dim_date_created_derived FOREIGN KEY ([date_created_id]) REFERENCES [derived].[dim_pnc_date]([date_id]);

    ALTER TABLE [derived].[fact_infant_postnatal_visit] ADD CONSTRAINT FK_fact_infant_postnatal_visit_dim_pregnancy_derived FOREIGN KEY ([pregnancy_id]) REFERENCES [derived].[dim_pregnancy]([pregnancy_id]);

    ALTER TABLE [derived].[fact_infant_postnatal_visit] ADD CONSTRAINT FK_fact_infant_postnatal_visit_dim_pnc_date_encounter_derived FOREIGN KEY ([encounter_date_id]) REFERENCES [derived].[dim_pnc_date]([date_id]);

    ALTER TABLE [derived].[fact_infant_postnatal_visit] ADD CONSTRAINT FK_fact_infant_postnatal_visit_dim_infant_age_group_derived FOREIGN KEY ([infant_age_group_id]) REFERENCES [derived].[dim_infant_age_group]([infant_age_group_id]);
    
    ALTER TABLE [derived].[fact_infant_postnatal_visit] ADD CONSTRAINT FK_fact_infant_postnatal_visit_dim_pnc_facility_derived FOREIGN KEY ([facility_id]) REFERENCES [derived].[dim_pnc_facility]([facility_id]);
    
    ALTER TABLE [derived].[fact_infant_postnatal_visit] ADD CONSTRAINT FK_fact_infant_postnatal_visit_dim_pnc_date_transfer_in_derived FOREIGN KEY ([transfer_in_date_id]) REFERENCES [derived].[dim_pnc_date]([date_id]);
    
    ALTER TABLE [derived].[fact_infant_postnatal_visit] ADD CONSTRAINT FK_fact_infant_postnatal_visit_dim_pnc_date_transfer_out_derived FOREIGN KEY ([transfer_out_to_date_id]) REFERENCES [derived].[dim_pnc_date]([date_id]);

    ALTER TABLE [derived].[fact_infant_postnatal_visit] ADD CONSTRAINT FK_fact_infant_postnatal_visit_dim_pnc_date_death_derived FOREIGN KEY ([death_date_id]) REFERENCES [derived].[dim_pnc_date]([date_id]);

    ALTER TABLE [derived].[fact_infant_postnatal_visit] ADD CONSTRAINT FK_fact_infant_postnatal_visit_dim_pnc_date_next_derived FOREIGN KEY ([next_visit_date_id]) REFERENCES [derived].[dim_pnc_date]([date_id]);

-- $END