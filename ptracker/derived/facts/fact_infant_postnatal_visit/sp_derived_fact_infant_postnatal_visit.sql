USE [staging_ptracker_test]
GO
-- $BEGIN

    EXEC sp_derived_fact_infant_postnatal_visit_create;
    EXEC sp_derived_fact_infant_postnatal_visit_insert;
    EXEC sp_derived_fact_infant_postnatal_visit_update;

-- $END