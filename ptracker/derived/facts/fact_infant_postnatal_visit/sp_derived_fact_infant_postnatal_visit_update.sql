USE [staging_ptracker_test]
GO

-- $BEGIN

    WITH first_visit AS 
    (
        SELECT 
            *
        FROM
        (
            SELECT 
                infant_postnatal_visit_id,
                pregnancy_id, 
                infant_id,
                encounter_date,
                ROW_NUMBER() OVER (PARTITION BY infant_id ORDER BY encounter_date ASC) AS row_number
            FROM
            derived.fact_infant_postnatal_visit
        ) f
        WHERE row_number  = 1
    )

    UPDATE
        [derived].[fact_infant_postnatal_visit]
    SET
        [derived].[fact_infant_postnatal_visit].is_first_visit = 
            CASE WHEN 
                first_visit.infant_postnatal_visit_id IS NOT NULL
            THEN 
                1
            ELSE
                0
            END,
        [derived].[fact_infant_postnatal_visit].is_repeat_visit = 
            CASE WHEN 
                first_visit.infant_postnatal_visit_id IS NULL
            THEN 
                1
            ELSE
                0
            END
    FROM
        [derived].[fact_infant_postnatal_visit] ipv
    LEFT JOIN
        first_visit
    ON
        ipv.infant_postnatal_visit_id = first_visit.infant_postnatal_visit_id


    UPDATE 
        [derived].[fact_infant_postnatal_visit]
    SET 
        [is_hiv_exposed] = 
            CASE WHEN 
                hiv_exposure = 'Currently Exposed'
            THEN 
                1
            END,
        [is_hiv_unexposed] =
            CASE WHEN 
                hiv_exposure = 'Currently Unexposed'
            THEN 
                1
            END,
        [is_prophylaxis_arv] = 
            CASE WHEN 
                arv_prophylaxis_status = 'Received ARV prophylaxis'
            THEN 
                1
            END,
        [is_prophylaxis_started_ctx] = 
            CASE WHEN 
                ctx_prophylaxis_status = 'Received CTX prophylaxis'
            THEN 
                1
            END,
        [is_hiv_test_type_pcr] =
            CASE WHEN 
                hiv_test_type = 'DNA PCR'
            THEN 
                1
            END,
        [is_hiv_test_type_rapid] =
            CASE WHEN 
                hiv_test_type = 'Rapid Test'
            THEN 
                1
            END,
        [is_confirmatory_test_done] =
            CASE WHEN 
                confirmatory_test_done = 'Yes'
            THEN 
                1
            END,
        [is_positive_confirmatory_test] = 
            CASE WHEN 
                final_test_result = 'Positive'
            THEN 
                1
            END,
        [is_negative_confirmatory_test] = 
            CASE WHEN 
                final_test_result = 'Negative'
            THEN 
                1
            END,
        [is_linked_to_art] = 
            CASE WHEN 
                linked_to_art = 'Yes'
            THEN 
                1
            END,
        [is_exclusive_breast_feeding] = 
            CASE WHEN 
                breastfeeding_status = 'BREASTFED EXCLUSIVELY'
            THEN 
                1
            END,
        [is_outcome_status_in_care] =
            CASE WHEN 
                outcome_status = 'Still in Care'
            THEN 
                1
            END,
        [is_outcome_status_transfer_in] =
            CASE WHEN 
                outcome_status = 'Transfer in'
            THEN 
                1
            END,
        [is_outcome_status_negative] = 
            CASE WHEN 
                outcome_status = 'HIV negative infant discharged from PMTCT'
            THEN 
                1
            END,
        [is_outcome_status_transfer_out] = 
            CASE WHEN 
                outcome_status = 'Transferred Out'
            THEN 
                1
            END,
        [is_outcome_status_lost] = 
            CASE WHEN 
                outcome_status = 'Lost to followup'
            THEN 
                1
            END,
        [is_outcome_status_dead] = 
            CASE WHEN 
                outcome_status = 'Dead'
            THEN 
                1
            END
    ;

-- $END
SELECT TOP 400
    *
FROM 
    [derived].[fact_infant_postnatal_visit]

