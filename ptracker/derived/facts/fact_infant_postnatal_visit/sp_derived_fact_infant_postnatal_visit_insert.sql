USE [staging_ptracker_test]
GO

TRUNCATE TABLE [derived].[fact_infant_postnatal_visit]

-- $BEGIN

    INSERT INTO [derived].[fact_infant_postnatal_visit](
        [infant_postnatal_visit_id],
        [infant_id],
        [pregnancy_id],
        [creator],
        [provider],
        [facility_id],
        [encounter_date],
        [encounter_date_id],
        [date_created],
        [date_created_id],
        [age_in_months],
        [age_in_weeks],
        [infant_age_group_id],
        [hiv_exposure],
        [arv_prophylaxis_status],
        [arv_prophylaxis_adherence],
        [ctx_prophylaxis_status],
        [ctx_prophylaxis_adherence],
        [hiv_test_status],
        [hiv_test_type],
        [dna_pcr_test_result],
        [rapid_test_result],
        [confirmatory_test_done],
        [final_test_result],
        [linked_to_art],
        [breastfeeding_status],
        [other_feeding_method],
        [outcome_status],
        [transfer_in_from],
        [transfer_in_date],
        [transfer_in_date_id], 
        [transfer_out_to],
        [transfer_out_to_date],
        [transfer_out_to_date_id],
        [death_date],
        [death_date_id], 
        [next_visit_date],
        [next_visit_date_id]
    )
    
    SELECT DISTINCT
        [infant_postnatal_visit_id],
        inf.infant_id,
        [pregnancy_id],
        e.[creator],
        e.[provider],
        e.facility_id,
        [encounter_date],
        pdate_enc.date_id,
        e.[date_created],
        e.[date_created_id],
        [age_in_months],
        [age_in_weeks],
        [infant_age_group_id],
        [hiv_exposure],
        [arv_prophylaxis_status],
        [arv_prophylaxis_adherence],
        [ctx_prophylaxis_status],
        [ctx_prophylaxis_adherence],
        [hiv_test_status],
        [hiv_test_type],
        [dna_pcr_test_result],
        [rapid_test_result],
        [confirmatory_test_done],
        [final_test_result],
        [linked_to_art],
        [breastfeeding_status],
        [other_feeding_method],
        [outcome_status],
        [transfer_in_from],
        [transfer_in_date],
        pdate_transfer_in.date_id, 
        [transfer_out_to],
        [transfer_out_to_date],
        pdate_transfer_out.date_id,
        [death_date],
        pdate_death.date_id, 
        [next_visit_date],
        pdate_next.date_id
    FROM 
        [base].[fact_infant_postnatal_visit] ip
    INNER JOIN
        [base].[dim_encounter] e 
    ON 
        ip.encounter_id = e.encounter_id
    INNER JOIN
        [derived].[dim_infant] inf 
    ON 
        e.client_id =inf.infant_id
    LEFT JOIN
        [derived].[dim_infant_age_group] inf_age 
    ON 
        inf_age.age_in_weeks = DATEDIFF(WEEK, inf.date_of_birth, e.encounter_date)
    LEFT JOIN
        [derived].[dim_pnc_date] pdate_enc
    ON
        pdate_enc.date_yyyymmdd = e.encounter_date
    LEFT JOIN
        [derived].[dim_pnc_date] pdate_transfer_out
    ON
        pdate_transfer_out.date_yyyymmdd = ip.transfer_out_to_date
    LEFT JOIN
        [derived].[dim_pnc_date] pdate_transfer_in
    ON
        pdate_transfer_in.date_yyyymmdd = ip.transfer_in_date
    LEFT JOIN
        [derived].[dim_pnc_date] pdate_death
    ON
        pdate_death.date_yyyymmdd = ip.death_date
    LEFT JOIN
        [derived].[dim_pnc_date] pdate_next
    ON
        pdate_next.date_yyyymmdd = ip.next_visit_date
    
-- $END
SELECT TOP 400 
    *
FROM 
    [derived].[fact_infant_postnatal_visit]
