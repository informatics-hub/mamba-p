USE [staging_ptracker_test]
GO

TRUNCATE TABLE [derived].[fact_mother_postnatal_visit]

-- $BEGIN

    INSERT INTO [derived].[fact_mother_postnatal_visit](
        [mother_postnatal_visit_id],
        [pregnancy_id],
        [mother_id],
        [facility_id],
        [creator],
        [provider],
        [encounter_date],
        [encounter_date_id],
        [date_created],
        [date_created_id],
        [mother_age_group_id],
        [age],
        [hiv_test_status],
        [hiv_test_results],
        [recent_viral_load_test_done],
        [viral_load_test_date],
        [viral_load_test_date_id],
        [viral_load_test_result],
        [viral_load_copies],
        [art_initiation],
        [art_start_date],
        [art_start_date_id],
        [reason_for_refusing_art],
        [next_visit_date],
        [next_visit_date_id],
        [next_appointment_facility],
        [transfer_out_to],
        [transfer_out_date],
        [transfer_out_date_id]
    )
    
    SELECT
        mp.mother_postnatal_visit_id,
        prg.pregnancy_id,
        prg.mother_id,
        en.facility_id,
        en.creator,
        en.provider,
        en.encounter_date,
        pdate_enc.date_id,
        en.[date_created],
        en.[date_created_id],
        mage.mother_age_group_id,
        FLOOR(DATEDIFF(DAY,mo.date_of_birth,en.encounter_date)/365.25) AS age,
        mp.hiv_test_status,
        mp.hiv_test_results,
        mp.recent_viral_load_test_done,
        mp.viral_load_test_date,
        pdate_vl.date_id,
        mp.viral_load_test_result,
        mp.viral_load_copies,
        mp.art_initiation,
        mp.art_start_date,
        pdate_art.date_id,
        mp.reason_for_refusing_art,
        mp.next_visit_date,
        pdate_next.date_id,
        mp.next_appointment_facility,
        mp.transfer_out_to,
        mp.transfer_out_date,
        pdate_transfer.date_id
    FROM 
        base.fact_mother_postnatal_visit mp
    INNER JOIN
        [base].[dim_encounter] en 
    ON 
        mp.encounter_id=en.encounter_id
    LEFT JOIN
        [derived].[dim_pregnancy] prg 
    ON 
        mp.ptracker_identifier = prg.ptracker_identifier
    INNER JOIN
        [derived].[dim_mother] mo 
    ON 
        prg.mother_id = mo.mother_id
    LEFT JOIN
        [derived].[dim_mother_age_group_pnc] mage 
    ON 
        mage.age = FLOOR(DATEDIFF(DAY,mo.date_of_birth,en.encounter_date)/365.25)
    LEFT JOIN
        [derived].[dim_pnc_date] pdate_vl
    ON
        pdate_vl.date_yyyymmdd = mp.viral_load_test_date
    LEFT JOIN
        [derived].[dim_pnc_date] pdate_enc
    ON
        pdate_enc.date_yyyymmdd = en.encounter_date
    LEFT JOIN
        [derived].[dim_pnc_date] pdate_art
    ON
        pdate_art.date_yyyymmdd = mp.art_start_date
    LEFT JOIN
        [derived].[dim_pnc_date] pdate_next
    ON
        pdate_next.date_yyyymmdd = mp.next_visit_date
    LEFT JOIN
        [derived].[dim_pnc_date] pdate_transfer
    ON
        pdate_transfer.date_yyyymmdd = mp.transfer_out_date
        
-- $END

SELECT TOP 400 *
FROM [derived].[fact_mother_postnatal_visit]


