USE [staging_ptracker_test]
GO

-- $BEGIN

    UPDATE 
        [derived].[fact_mother_postnatal_visit]
    SET 
        [is_hiv_test_status_previously_known] = 
            CASE WHEN 
                hiv_test_status = 'Previously known positive'
            THEN 
                1
            ELSE
                0
            END,
        [is_hiv_test_results_positive] = 
            CASE WHEN 
                hiv_test_results = 'Positive'
            THEN 
                1
            ELSE
                0
            END,
        [is_hiv_test_results_negative] = 
            CASE WHEN 
                hiv_test_results = 'Negative'
            THEN 
                1
            ELSE
                0
            END,
        [is_hiv_positive] = 
            CASE WHEN 
                hiv_test_results = 'Positive' OR hiv_test_status = 'Previously known positive'
            THEN 
                1
            ELSE
                0
            END,
        [is_recent_viral_load_test_done] = 
            CASE WHEN 
                recent_viral_load_test_done = 'Yes'
            THEN 
                1
            WHEN
                recent_viral_load_test_done = 'No'
            THEN
                0
            ELSE
                NULL
            END,
        [is_viral_load_test_result_undetectable_or_less_than_40] = 
            CASE WHEN 
                viral_load_test_result = 'Not Detected' OR (viral_load_test_result = 'Target Detected' AND viral_load_copies < 40) 
            THEN 
                1
            END,
        [is_viral_load_test_result_not_suppressed] = 
            CASE WHEN 
                viral_load_test_result = 'Target Detected' AND viral_load_copies >= 40
            THEN 
                1
            END
        
-- $END

SELECT TOP 400 *
FROM [derived].[fact_mother_postnatal_visit]


