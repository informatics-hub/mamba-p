USE [staging_ptracker_test]
GO

DROP TABLE IF exists [derived].[fact_mother_postnatal_visit];

-- $BEGIN

    CREATE TABLE [derived].[fact_mother_postnatal_visit](
        [mother_postnatal_visit_id] [uniqueidentifier] NOT NULL,
        [pregnancy_id] [uniqueidentifier] NULL,
        [mother_id] [uniqueidentifier] NULL,
        [creator] [nvarchar](255) NOT NULL,
        [provider] [nvarchar](255) NOT NULL,
        [facility_id] [uniqueidentifier] NULL,
        [encounter_date] [date] NULL,
        [encounter_date_id] [uniqueidentifier] NULL,
        [date_created] [date] NULL, 
        [date_created_id] [uniqueidentifier] NULL,
        [mother_age_group_id] [uniqueidentifier] NULL,
        [age] [int] NULL,
        [hiv_test_status] [nvarchar] (255) NULL,
        [is_hiv_test_status_previously_known] [int] NULL,
        [hiv_test_results] [nvarchar] (255) NULL,
        [is_hiv_test_results_negative] [int] NULL,
        [is_hiv_test_results_positive] [int] NULL,
        [is_hiv_positive] [int] NULL,
        [recent_viral_load_test_done] [nvarchar] (255) NULL,
        [is_recent_viral_load_test_done] [int] NULL,
        [viral_load_test_date] [date] NULL,
        [viral_load_test_date_id] [uniqueidentifier] NULL,
        [viral_load_test_result] [nvarchar] (255) NULL,
        [viral_load_copies] [float] NULL,
        [is_viral_load_test_result_undetectable_or_less_than_40] [int] NULL,
        [is_viral_load_test_result_not_suppressed] [int] NULL,
        [art_initiation] [nvarchar] (255) NULL,
        [art_start_date] [date] NULL,
        [art_start_date_id] [uniqueidentifier] NULL,
        [reason_for_refusing_art] [nvarchar] (max) NULL,
        [next_visit_date] [date]  NULL,
        [next_visit_date_id] [uniqueidentifier] NULL,
        [next_appointment_facility] [nvarchar] (255) NULL,
        [transfer_out_to] [nvarchar] (max) NULL,
        [transfer_out_date] [date] NULL,
        [transfer_out_date_id] [uniqueidentifier] NULL
    );

    ALTER TABLE [derived].[fact_mother_postnatal_visit] ADD CONSTRAINT PK_fact_mother_postnatal_visit_derived PRIMARY KEY ([mother_postnatal_visit_id]);

    ALTER TABLE [derived].[fact_mother_postnatal_visit] ADD CONSTRAINT FK_fact_mother_postnatal_visit_dim_pregnancy_derived FOREIGN KEY ([pregnancy_id]) REFERENCES [derived].[dim_pregnancy]([pregnancy_id]);

    ALTER TABLE [derived].[fact_mother_postnatal_visit] ADD CONSTRAINT FK_fact_mother_postnatal_visit_dim_date_created_derived FOREIGN KEY ([date_created_id]) REFERENCES [derived].[dim_pnc_date]([date_id]);

    ALTER TABLE [derived].[fact_mother_postnatal_visit] ADD CONSTRAINT FK_fact_mother_postnatal_visit_dim_mother_derived FOREIGN KEY ([mother_id]) REFERENCES [derived].[dim_mother]([mother_id]);

    ALTER TABLE [derived].[fact_mother_postnatal_visit] ADD CONSTRAINT FK_fact_mother_postnatal_visit_dim_pnc_facility_derived FOREIGN KEY ([facility_id]) REFERENCES [derived].[dim_pnc_facility]([facility_id]);

    ALTER TABLE [derived].[fact_mother_postnatal_visit] ADD CONSTRAINT FK_fact_mother_postnatal_visit_dim_pnc_date_encounter_derived FOREIGN KEY ([encounter_date_id]) REFERENCES [derived].[dim_pnc_date]([date_id]);

    ALTER TABLE [derived].[fact_mother_postnatal_visit] ADD CONSTRAINT FK_fact_mother_postnatal_visit_dim_age_group_derived FOREIGN KEY ([mother_age_group_id]) REFERENCES [derived].[dim_mother_age_group_pnc]([mother_age_group_id]);
    
    ALTER TABLE [derived].[fact_mother_postnatal_visit] ADD CONSTRAINT FK_fact_mother_postnatal_visit_dim_pnc_date_art_start_derived FOREIGN KEY ([art_start_date_id]) REFERENCES [derived].[dim_pnc_date]([date_id]);

    ALTER TABLE [derived].[fact_mother_postnatal_visit] ADD CONSTRAINT FK_fact_mother_postnatal_visit_dim_pnc_date_vl_test_derived FOREIGN KEY ([viral_load_test_date_id]) REFERENCES [derived].[dim_pnc_date]([date_id]);
    
    ALTER TABLE [derived].[fact_mother_postnatal_visit] ADD CONSTRAINT FK_fact_mother_postnatal_visit_dim_pnc_date_next_visit_derived FOREIGN KEY ([next_visit_date_id]) REFERENCES [derived].[dim_pnc_date]([date_id]);
    
    ALTER TABLE [derived].[fact_mother_postnatal_visit] ADD CONSTRAINT FK_fact_mother_postnatal_visit_dim_pnc_date_transfer_out_derived FOREIGN KEY ([transfer_out_date_id]) REFERENCES [derived].[dim_pnc_date]([date_id]);

-- $END