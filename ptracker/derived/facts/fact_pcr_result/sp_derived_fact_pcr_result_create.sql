USE [staging_ptracker_test]
GO

DROP TABLE IF exists [derived].[fact_pcr_result];

-- $BEGIN

    CREATE TABLE [derived].[fact_pcr_result](
        [pcr_result_id] [uniqueidentifier] NOT NULL,
        [facility_id] [uniqueidentifier] NULL,
        [pregnancy_id] [uniqueidentifier] NULL,
        [infant_id] [uniqueidentifier] NOT NULL,
        [pcr_result] [nvarchar] (255) NULL,
        [pcr_result_order] [int] NULL,
        [result_type] [nvarchar] (255) NULL,
        [result_period] [nvarchar] (255) NULL,
        [result_date] [date] NULL,
        [result_date_id] [uniqueidentifier] NULL,
        [is_positive] [tinyint] NULL,
        [is_negative] [tinyint] NULL,
        [is_pending] [tinyint] NULL
    );

    ALTER TABLE [derived].[fact_pcr_result] ADD CONSTRAINT [PK_fact_pcr_result_derived] PRIMARY KEY ([pcr_result_id]);

    ALTER TABLE [derived].[fact_pcr_result] ADD CONSTRAINT FK_fact_pcr_result_dim_infant_derived FOREIGN KEY ([infant_id]) REFERENCES [derived].[dim_infant]([infant_id]);

    ALTER TABLE [derived].[fact_pcr_result] ADD CONSTRAINT FK_fact_pcr_result_dim_pcr_result_facility_derived FOREIGN KEY ([facility_id]) REFERENCES [derived].[dim_pcr_result_facility]([facility_id]);

    ALTER TABLE [derived].[fact_pcr_result] ADD CONSTRAINT FK_fact_pcr_result_dim_pnc_date_result_derived FOREIGN KEY ([result_date_id]) REFERENCES [derived].[dim_pnc_date]([date_id]);

    ALTER TABLE [derived].[fact_pcr_result] ADD CONSTRAINT FK_fact_pcr_result_dim_pregnancy_derived FOREIGN KEY ([pregnancy_id]) REFERENCES [derived].[dim_pregnancy]([pregnancy_id]);

-- $END