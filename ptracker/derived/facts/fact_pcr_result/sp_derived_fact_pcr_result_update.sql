USE [staging_ptracker_test]
GO

-- $BEGIN

    UPDATE 
        [derived].[fact_pcr_result]
    SET
        pcr_result = 'Missing'
    WHERE
        pcr_result IS NULL
    ;

    UPDATE 
        [derived].[fact_pcr_result]
    SET
        pcr_result_order = 
            CASE WHEN 
                pcr_result = 'Negative'
            THEN
                1
            WHEN 
                pcr_result = 'Positive'
            THEN
                2
            WHEN 
                pcr_result = 'Results Pending'
            THEN
                3
            WHEN 
                pcr_result = 'Missing'
            THEN
                4
            END
        
    ;

    WITH birth_result 
    AS 
    (
        SELECT  *
        FROM
        (
        SELECT  p.facility_id
            ,pcr_result_id
            ,p.pregnancy_id
            ,p.infant_id
            ,result_date
            ,pcr_result
            ,result_type
            ,ROW_NUMBER() OVER (PARTITION BY p.infant_id ORDER BY result_date ASC) AS row_number
        FROM derived.fact_pcr_result p
        INNER JOIN derived.dim_infant i ON i.infant_id = p.infant_id
        WHERE DATEDIFF(DAY, date_of_birth, result_date) <= 2 
        ) f
        WHERE row_number = 1
    ), 

    first_result AS 
    (
        SELECT  *
        FROM
        (
        SELECT  p.facility_id
            ,pcr_result_id
            ,p.pregnancy_id
            ,p.infant_id
            ,result_date
            ,pcr_result
            ,result_type
            ,ROW_NUMBER() OVER (PARTITION BY p.infant_id ORDER BY result_date ASC) AS row_number
        FROM derived.fact_pcr_result p
        INNER JOIN derived.dim_infant i ON i.infant_id = p.infant_id
        WHERE DATEDIFF(DAY, date_of_birth, result_date) > 2 
        ) f
        WHERE row_number = 1
    ), 

    second_result AS 
    (
        SELECT  *
        FROM
        (
        SELECT  facility_id
            ,pcr_result_id
            ,pregnancy_id
            ,infant_id
            ,result_date
            ,pcr_result
            ,result_type
            ,ROW_NUMBER() OVER (PARTITION BY infant_id ORDER BY result_date ASC) AS row_number
        FROM derived.fact_pcr_result
        ) f2
        WHERE row_number = 2
    ), 

    third_result AS 
    (
        SELECT  *
        FROM
        (
        SELECT  p.facility_id
            ,pcr_result_id
            ,p.pregnancy_id
            ,p.infant_id
            ,result_date
            ,pcr_result
            ,result_type
            ,ROW_NUMBER() OVER (PARTITION BY p.infant_id ORDER BY result_date ASC) AS row_number
        FROM derived.fact_pcr_result p
        INNER JOIN derived.dim_infant i ON i.infant_id = p.infant_id
        WHERE DATEDIFF(MONTH, date_of_birth, result_date) >= 9
        ) f3
        WHERE row_number = 3 
    )
    
    UPDATE 
        [derived].[fact_pcr_result]
    SET
        result_period = 
            CASE WHEN 
                b.pcr_result_id IS NOT NULL
            THEN
                'Birth Test'
            WHEN 
                f.pcr_result_id IS NOT NULL
            THEN
                'First Test'
            WHEN 
                s.pcr_result_id IS NOT NULL
            THEN
                'Second Test'
            WHEN 
                t.pcr_result_id IS NOT NULL
            THEN
                'Third Test'
            END
    FROM 
        [derived].[fact_pcr_result] p
    LEFT JOIN
        birth_result b
    ON
        p.pcr_result_id = b.pcr_result_id
    LEFT JOIN
        first_result f
    ON
        p.pcr_result_id = f.pcr_result_id
    LEFT JOIN
        second_result s
    ON
        p.pcr_result_id = s.pcr_result_id
    LEFT JOIN
        third_result t
    ON
        p.pcr_result_id = t.pcr_result_id
    ;
    
-- $END
SELECT TOP 400 
    *
FROM 
    [derived].[fact_pcr_result]
