USE [staging_ptracker_test]
GO

TRUNCATE TABLE [derived].[fact_pcr_result];

-- $BEGIN
    WITH pcr_result AS
    (
        SELECT DISTINCT
            infant_id,
            dna_pcr_test_result AS test_result,
            pregnancy_id,
            facility_id,
            encounter_date,
            encounter_date_id,
            'DNA PCR' AS result_type
        FROM 
            [derived].[fact_infant_postnatal_visit]
        WHERE 
            hiv_test_status = 'Tested for HIV during this visit' AND hiv_test_type='DNA PCR'
        
        UNION

        SELECT DISTINCT
            infant_id,
            rapid_test_result AS test_result,
            pregnancy_id,
            facility_id,
            encounter_date,
            encounter_date_id,
            'Rapid' AS result_type
        FROM 
            [derived].[fact_infant_postnatal_visit]
        WHERE 
            hiv_test_status = 'Tested for HIV during this visit' AND hiv_test_type='Rapid Test'
    )

    INSERT INTO [derived].[fact_pcr_result](
        [pcr_result_id],
        [infant_id],
        [facility_id],
        [pregnancy_id],
        [pcr_result],
        [result_date],
        [result_date_id],
        [result_type],
        [is_positive],
        [is_negative],
        [is_pending]
    )
    SELECT
        NEWID(),
        infant_id,
        facility_id,
        pregnancy_id,
        test_result,
        encounter_date,
        encounter_date_id,
        result_type,
        CASE WHEN
            test_result = 'Positive'
        THEN
            1
        ELSE 
            0
        END AS is_positive,
        CASE WHEN
            test_result = 'Negative'
        THEN
            1
        ELSE 
            0
        END AS is_negative,
        CASE WHEN
            test_result = 'Results Pending'
        THEN
            1
        ELSE 
            0
        END AS is_pending
    FROM
        pcr_result
    
    
    
-- $END
SELECT TOP 400 
    *
FROM 
    [derived].[fact_pcr_result]
