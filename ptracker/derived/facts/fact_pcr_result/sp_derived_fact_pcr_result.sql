USE [staging_ptracker_test]
GO
-- $BEGIN

    EXEC sp_derived_fact_pcr_result_create;
    EXEC sp_derived_fact_pcr_result_insert;
    EXEC sp_derived_fact_pcr_result_update;

-- $END