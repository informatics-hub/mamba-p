USE [staging_ptracker_test]
GO

TRUNCATE TABLE [derived].[fact_viral_load_result];

-- $BEGIN
    WITH viral_load_result AS
    (
        SELECT DISTINCT
            a.facility_id,
            a.pregnancy_id,
            a.mother_id,
            a.viral_load_test_result AS vl_result,
            'Antenatal' AS result_source,
            a.viral_load_test_date AS result_date,
            d.date_id AS result_date_id,
            a.viral_load_copies AS vl_copies,
            a.is_viral_load_test_result_undetectable_or_less_than_40 AS is_vl_result_undetectable_or_less_than_40,
            a.is_viral_load_test_result_not_suppressed AS is_vl_result_not_suppressed,
            CASE WHEN 
                a.viral_load_test_result = 'Sample Rejected'
            THEN
                1
            ELSE
                0
            END AS is_vl_result_rejected,
            CASE WHEN 
                a.viral_load_test_result = 'Results Pending'
            THEN
                1
            ELSE
                0
            END AS is_vl_result_pending
        FROM 
            [derived].[fact_antenatal_visit] a
        INNER JOIN
            [derived].[dim_pregnancy] p 
        ON
            a.pregnancy_id=p.pregnancy_id
        LEFT JOIN
            [derived].[dim_date] d 
        ON 
            a.viral_load_test_date = d.date_yyyymmdd
        WHERE 
            a.viral_load_test_result IS NOT NULL
        
        UNION

        SELECT DISTINCT
            m.facility_id,
            m.pregnancy_id,
            m.mother_id,
            m.viral_load_test_result AS vl_result,
            'Maternity' AS result_source,
            m.viral_load_test_date AS result_date,
            d.date_id AS result_date_id,
            m.viral_load_copies AS vl_copies,
            m.is_viral_load_test_result_undetectable_or_less_than_40 AS is_vl_result_undetectable_or_less_than_40,
            m.is_viral_load_test_result_not_suppressed AS is_vl_result_not_suppressed,
            CASE WHEN 
                m.viral_load_test_result = 'Sample Rejected'
            THEN
                1
            ELSE
                0
            END AS is_vl_result_rejected,
            CASE WHEN 
                m.viral_load_test_result = 'Results Pending'
            THEN
                1
            ELSE
                0
            END AS is_vl_result_pending
        FROM 
            [derived].[fact_maternity_visit] m
        LEFT JOIN
            [derived].[dim_pregnancy] p 
        ON
            m.pregnancy_id=p.pregnancy_id
        LEFT JOIN
            [derived].[dim_date] d 
        ON 
            m.viral_load_test_date = d.date_yyyymmdd
        WHERE 
            m.viral_load_test_result IS NOT NULL
        
        UNION

        SELECT DISTINCT
            mp.facility_id,
            mp.pregnancy_id,
            mp.mother_id,
            mp.viral_load_test_result AS vl_result,
            'Postnatal' AS result_source,
            mp.viral_load_test_date AS result_date,
            d.date_id AS result_date_id,
            mp.viral_load_copies AS vl_copies,
            CASE WHEN 
                mp.viral_load_test_result = 'Not Detected'
            THEN
                1
            ELSE
                0
            END AS is_vl_result_undetectable_or_less_than_40,
            CASE WHEN 
                mp.viral_load_test_result = 'Target Detected'
            THEN
                1
            ELSE
                0
            END AS is_vl_result_not_suppressed,
            CASE WHEN 
                mp.viral_load_test_result = 'Sample Rejected'
            THEN
                1
            ELSE
                0
            END AS is_vl_result_rejected,
            CASE WHEN 
                mp.viral_load_test_result = 'Results Pending'
            THEN
                1
            ELSE
                0
            END AS is_vl_result_pending
        FROM 
            [derived].[fact_mother_postnatal_visit] mp
        LEFT JOIN
            [derived].[dim_pregnancy] p 
        ON
            mp.pregnancy_id=p.pregnancy_id
        LEFT JOIN
            [derived].[dim_date] d 
        ON 
            mp.viral_load_test_date = d.date_yyyymmdd
        WHERE 
            mp.viral_load_test_result IS NOT NULL
        

    )

    INSERT INTO [derived].[fact_viral_load_result](
        [viral_load_result_id],
        [facility_id],
        [pregnancy_id],
        [mother_id],
        [vl_result],
        [result_source],
        [result_date],
        [result_date_id],
        [vl_copies],
        [is_vl_result_undetectable_or_less_than_40],
        [is_vl_result_not_suppressed],
        [is_vl_result_rejected],
        [is_vl_result_pending]
    )
    SELECT
        NEWID(),
        facility_id,
        pregnancy_id,
        mother_id,
        vl_result,
        result_source,
        result_date,
        result_date_id,
        vl_copies,
        is_vl_result_undetectable_or_less_than_40,
        is_vl_result_not_suppressed,
        is_vl_result_rejected,
        is_vl_result_pending
    FROM
        viral_load_result a
    
    
    
-- $END
SELECT TOP 400 
    *
FROM 
    [derived].[fact_viral_load_result]

