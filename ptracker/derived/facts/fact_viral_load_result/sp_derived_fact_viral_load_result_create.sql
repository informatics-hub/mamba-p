USE [staging_ptracker_test]
GO

DROP TABLE IF exists [derived].[fact_viral_load_result];

-- $BEGIN

    CREATE TABLE [derived].[fact_viral_load_result](
        [viral_load_result_id] [uniqueidentifier] NOT NULL,
        [facility_id] [uniqueidentifier] NULL,
        [pregnancy_id] [uniqueidentifier] NULL,
        [mother_id] [uniqueidentifier] NULL,
        [vl_result] [nvarchar] (255) NULL,
        [result_source] [nvarchar] (255) NULL,
        [result_date] [date] NULL,
        [result_date_id] [uniqueidentifier] NULL,
        [vl_copies] [float] NULL,
        [is_vl_result_undetectable_or_less_than_40] [tinyint] NULL,
        [is_vl_result_not_suppressed] [tinyint] NULL,
        [is_vl_result_rejected] [tinyint] NULL,
        [is_vl_result_pending] [tinyint] NULL
    );

    ALTER TABLE [derived].[fact_viral_load_result] ADD CONSTRAINT [PK_fact_viral_load_result_derived] PRIMARY KEY ([viral_load_result_id]);

    ALTER TABLE [derived].[fact_viral_load_result] ADD CONSTRAINT FK_fact_viral_load_result_dim_faciltiy_derived FOREIGN KEY ([facility_id]) REFERENCES [derived].[dim_facility]([facility_id]);

    ALTER TABLE [derived].[fact_viral_load_result] ADD CONSTRAINT FK_fact_viral_load_result_dim_mother_derived FOREIGN KEY ([mother_id]) REFERENCES [derived].[dim_mother]([mother_id]);

    ALTER TABLE [derived].[fact_viral_load_result] ADD CONSTRAINT FK_fact_viral_load_result_dim_pnc_date_result_derived FOREIGN KEY ([result_date_id]) REFERENCES [derived].[dim_date]([date_id]);

    ALTER TABLE [derived].[fact_viral_load_result] ADD CONSTRAINT FK_fact_viral_load_result_dim_pregnancy_derived FOREIGN KEY ([pregnancy_id]) REFERENCES [derived].[dim_pregnancy]([pregnancy_id]);

-- $END