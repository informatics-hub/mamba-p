USE [staging_ptracker_test]
GO
-- $BEGIN

    EXEC sp_derived_fact_viral_load_result_create;
    EXEC sp_derived_fact_viral_load_result_insert;

-- $END