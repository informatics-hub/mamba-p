USE [staging_ptracker_test]
GO

DROP TABLE IF exists [derived].[fact_maternity_dhis2_export];

-- $BEGIN

    CREATE TABLE [derived].[fact_maternity_dhis2_export](
        [data_element] [nvarchar] (255) NOT NULL,
        [period] [nvarchar] (255) NOT NULL,
        [org_unit_uid] [nvarchar] (255) NOT NULL, 
        [COCUID] [nvarchar] (255) NULL,
        [AOCUID] [nvarchar] (255) NULL,
        [datavalue] [int] NULL
    );
    
-- $END