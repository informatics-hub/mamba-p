USE [staging_ptracker_test]
GO
-- $BEGIN

    EXEC sp_derived_fact_facility_indicator_create;
    EXEC sp_derived_fact_facility_indicator_insert;
    EXEC sp_derived_fact_facility_indicator_update;
    EXEC sp_derived_fact_facility_indicator_trim_date_dimensions;

-- $END