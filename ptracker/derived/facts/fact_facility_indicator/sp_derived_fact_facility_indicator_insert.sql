USE [staging_ptracker_test]
GO

TRUNCATE TABLE [derived].[fact_facility_indicator];

-- $BEGIN

    WITH first_test AS 
    (
        SELECT DISTINCT 
            infant_id,facility_id, age_in_weeks,result_date,pcr_result,sex
        FROM
        (
            SELECT  
                p.pregnancy_id
                ,p.infant_id
                ,result_date
                ,p.facility_id
                ,pcr_result
                ,i.sex
                ,DATEDIFF(WEEK, date_of_birth, p.result_date) AS age_in_weeks
                ,ROW_NUMBER() OVER (PARTITION BY p.infant_id ORDER BY result_date ASC) AS row_number
            FROM derived.fact_pcr_result p
            INNER JOIN derived.dim_infant i ON p.infant_id = i.infant_id
        ) f
        WHERE row_number = 1 
    )

    INSERT INTO [derived].[fact_facility_indicator](
        [facility_indicator_id],
        [indicator_id],
        [facility_id],
        [gender],
        [age_category],
        [art_initiation],
        [anc_visit_type],
        [hiv_test_result],
        [hiv_test_status],
        [infant_outcome],
        [indicator_date],
        [indicator_date_id],
        [reported_value]
    )
    
    SELECT
        NEWID(),
        i.indicator_id,
        af.facility_id,
        NULL AS [gender],
        NULL AS [age_category],
        NULL AS [art_initiation],
        NULL AS [anc_visit_type],
        NULL AS [hiv_test_result],
        NULL AS [hiv_test_status],
        NULL AS [infant_outcome],
        ad.eom AS [indicator_date],
        NULL AS [indicator_date_id],
        NULL AS indicator_value
    FROM 
        [derived].[dim_indicator] i
    CROSS JOIN
        derived.dim_indicator_facility af
    CROSS JOIN 
    (
        SELECT DISTINCT EOMONTH(date_yyyymmdd) AS eom
        FROM derived.dim_anc_date
        WHERE date_yyyymmdd BETWEEN '2015-01-01' AND EOMONTH(GETDATE())
    ) ad
    WHERE
        i.report_type = 'ANC Monthly Report' AND af.region_name IS NOT NULL

    UNION

    SELECT
        NEWID(),
        i.indicator_id,
        mf.facility_id,
        NULL AS [gender],
        NULL AS [age_category],
        NULL AS [art_initiation],
        NULL AS [anc_visit_type],
        NULL AS [hiv_test_result],
        NULL AS [hiv_test_status],
        NULL AS [infant_outcome],
        md.eom,
        NULL AS [indicator_date_id],
        NULL AS indicator_value
    FROM 
        [derived].[dim_indicator] i
    CROSS JOIN
        derived.dim_indicator_facility mf
    CROSS JOIN 
    (
        SELECT DISTINCT EOMONTH(date_yyyymmdd) AS eom
        FROM derived.dim_maternity_date
        WHERE date_yyyymmdd BETWEEN '2015-01-01' AND EOMONTH(GETDATE())
    ) md
    WHERE
        i.report_type = 'L&D Monthly Report' AND mf.region_name IS NOT NULL

    UNION

    SELECT
        NEWID(),
        i.indicator_id,
        pf.facility_id,
        NULL AS [gender],
        NULL AS [age_category],
        NULL AS [art_initiation],
        NULL AS [anc_visit_type],
        NULL AS [hiv_test_result],
        NULL AS [hiv_test_status],
        NULL AS [infant_outcome],
        pd.eom,
        NULL AS [indicator_date_id],
        NULL AS indicator_value
    FROM 
        [derived].[dim_indicator] i
    CROSS JOIN
        derived.dim_indicator_facility pf
    CROSS JOIN 
    (
        SELECT DISTINCT EOMONTH(date_yyyymmdd) AS eom
        FROM derived.dim_pnc_date
        WHERE date_yyyymmdd BETWEEN '2015-01-01' AND EOMONTH(GETDATE())
    ) pd
    WHERE
        i.report_type = 'MBFU Monthly Report' AND pf.region_name IS NOT NULL

    UNION

    SELECT 
        NEWID(),
        i.indicator_id,
        ind.facility_id,
        ind.[gender],
        ind.[age_category],
        ind.[art_initiation],
        ind.[anc_visit_type],
        ind.[hiv_test_result],
        ind.[hiv_test_status],
        ind.[infant_outcome],
        qtr.date_yyyymmdd,
        qtr.date_id,
        ind.indicator_value
    FROM 
        [derived].[dim_indicator] i
    LEFT JOIN 
    (
        
        SELECT
            'PMTCT_STAT_Numerator' as indicator,
            af.facility_id,
            'F' AS gender, 
            anc_ag.age_five_year_interval AS age_category, 
            adt.pepfar_quarter_period,
            COUNT(DISTINCT mother_id) AS indicator_value,
            NULL AS art_initiation,
            a.visit_type AS anc_visit_type,
            CASE WHEN
                is_hiv_test_status_previously_known = 1
            THEN
                'Known positives'
            END AS hiv_test_status,
            CASE WHEN
                is_hiv_test_results_positive = 1
            THEN
                'Newly tested positives'
            WHEN
                is_hiv_test_results_negative = 1
            THEN
                'New Negatives' 
            END AS hiv_test_result,
            NULL AS infant_outcome
        FROM derived.fact_antenatal_visit a
        INNER JOIN derived.dim_anc_facility af ON af.facility_id = a.facility_id
        INNER JOIN derived.dim_anc_date adt ON adt.date_yyyymmdd = a.encounter_date
        INNER JOIN derived.dim_mother_age_group_anc anc_ag ON a.age_group_id=anc_ag.mother_age_group_id
        WHERE is_first_visit = 1 AND (is_hiv_test_status_previously_known=1 OR (is_hiv_test_results_negative = 1 OR is_hiv_test_results_positive = 1))
        GROUP BY af.region_name, af.district_name, af.facility_id, facility_name, mfl_code, adt.pepfar_quarter_period, anc_ag.age_five_year_interval,a.visit_type, is_hiv_test_status_previously_known, is_hiv_test_results_positive,is_hiv_test_results_negative

        UNION

        SELECT 
            'PMTCT_STAT_Denominator' AS indicator,
            af.facility_id,
            'F' AS gender, 
            anc_ag.age_five_year_interval AS age_category, 
            adt.pepfar_quarter_period,
            COUNT(DISTINCT mother_id) AS indicator_value,
            NULL AS art_initiation,
            a.visit_type AS anc_visit_type,
            NULL AS hiv_test_status,
            NULL AS hiv_test_result,
            NULL AS infant_outcome
        FROM derived.fact_antenatal_visit a
        INNER JOIN derived.dim_anc_facility af ON af.facility_id = a.facility_id
        INNER JOIN derived.dim_anc_date adt ON adt.date_yyyymmdd = a.encounter_date
        INNER JOIN derived.dim_mother_age_group_anc anc_ag ON a.age_group_id=anc_ag.mother_age_group_id
        WHERE is_first_visit = 1
        GROUP BY af.region_name, af.district_name, af.facility_id, facility_name, mfl_code, anc_ag.age_five_year_interval, adt.pepfar_quarter_period, a.visit_type

        UNION

        SELECT
            'PMTCT_ART_Numerator' AS indicator,
            af.facility_id,
            'F' AS gender, 
            anc_ag.age_five_year_interval AS age_category, 
            adt.pepfar_quarter_period,
            COUNT(DISTINCT mother_id) AS indicator_value,
            CASE WHEN
                is_art_initiated_before = 1
            THEN
                'Already on ART before current pregnancy'
            WHEN
                is_art_initiated = 1
            THEN
                'New on ART '
            END AS art_initiation,
            a.visit_type AS anc_visit_type,
            NULL AS hiv_test_status,
            NULL AS hiv_test_result,
            NULL AS infant_outcome
        FROM derived.fact_antenatal_visit a
        INNER JOIN derived.dim_anc_facility af ON af.facility_id = a.facility_id
        INNER JOIN derived.dim_anc_date adt ON adt.date_yyyymmdd = a.encounter_date
        INNER JOIN derived.dim_mother_age_group_anc anc_ag ON a.age_group_id=anc_ag.mother_age_group_id
        WHERE is_first_visit = 1 AND (is_art_initiated_before = 1 OR is_art_initiated = 1)
        GROUP BY af.region_name, af.district_name, af.facility_id, facility_name, mfl_code, anc_ag.age_five_year_interval,adt.pepfar_quarter_period,is_art_initiated,a.visit_type,is_art_initiated_before

        UNION
        
        SELECT
            'PMTCT_STAT_POS' AS indicator,
            af.facility_id,
            'F' AS gender, 
            anc_ag.age_five_year_interval AS age_category, 
            adt.pepfar_quarter_period,
            COUNT(DISTINCT mother_id) AS indicator_value,
            NULL AS art_initiation,
            a.visit_type AS anc_visit_type,
            CASE WHEN 
                is_art_initiated_before = 1
            THEN
                'Previously known positive'
            WHEN 
                is_art_initiated = 1
            THEN
                'Newly tested positive'
            END AS hiv_test_status,
            NULL AS hiv_test_result,
            NULL AS infant_outcome
        FROM derived.fact_antenatal_visit a
        INNER JOIN derived.dim_anc_facility af ON af.facility_id = a.facility_id
        INNER JOIN derived.dim_anc_date adt ON adt.date_yyyymmdd = a.encounter_date
        INNER JOIN derived.dim_mother_age_group_anc anc_ag ON a.age_group_id=anc_ag.mother_age_group_id
        WHERE is_first_visit = 1 AND (is_art_initiated_before = 1 OR is_art_initiated = 1 )
        GROUP BY af.region_name, af.district_name, af.facility_id,facility_name, mfl_code, anc_ag.age_five_year_interval, adt.pepfar_quarter_period, a.visit_type, a.is_art_initiated_before, a.is_art_initiated
       
        UNION
        
        SELECT
            'PMTCT_EID_Numerator' AS indicator,
            pf.facility_id,
            LEFT(ft.sex,1) AS gender, 
            ag.age_p_tracker_monthly_interval AS age_category, 
            pepfar_quarter_period,
            COUNT(DISTINCT ft.infant_id) AS [count],
            CASE WHEN 
                is_outcome_status_transfer_in = 1
            THEN 
                'New on ART'
            END AS art_initiation,
            NULL AS anc_visit_type,
            NULL AS hiv_test_status,
            CASE WHEN
                ft.pcr_result = 'Positive'
            THEN
                'Positive'
            WHEN
                ft.pcr_result = 'Negative'
            THEN
                'Negative'
            WHEN
                ft.pcr_result = 'Missing'
            THEN
                'Missing'
            WHEN
                ft.pcr_result = 'Results Pending'
            THEN
                'Results Pending'
            END AS hiv_test_result,
            NULL AS infant_outcome
        FROM derived.fact_infant_postnatal_visit a 
        LEFT JOIN first_test ft ON a.infant_id = ft.infant_id
        LEFT JOIN derived.dim_pnc_facility pf ON pf.facility_id = ft.facility_id
        LEFT JOIN derived.dim_infant_age_group ag ON ft.age_in_weeks=ag.age_in_weeks
        LEFT JOIN derived.dim_pnc_date pdt ON pdt.date_yyyymmdd = encounter_date 
        GROUP BY pf.facility_id, ft.sex, ag.age_p_tracker_monthly_interval,pepfar_quarter_period, pcr_result, is_outcome_status_transfer_in


        UNION
        
        SELECT
            'PMTCT_HEI_POS' AS indicator,
            pf.facility_id,
            LEFT(a.sex,1) AS gender, 
            ag.age_p_tracker_monthly_interval AS age_category, 
            pdt.pepfar_quarter_period,
            COUNT(DISTINCT infant_id) AS indicator_value,
            NULL AS art_initiation,
            NULL AS anc_visit_type,
            NULL AS hiv_test_status,
            'Positive' AS hiv_test_result,
            NULL AS infant_outcome
        FROM derived.fact_infant_outcome a
        INNER JOIN derived.dim_indicator_facility pf ON pf.facility_id = a.facility_id
        INNER JOIN derived.dim_pnc_date pdt ON pdt.date_yyyymmdd = a.last_visit_date
        INNER JOIN derived.dim_infant_age_group ag ON a.last_visit_age_group_id=ag.infant_age_group_id
        WHERE is_ever_tested_by_12_months = 1 AND is_ever_hiv_test_result_positive = 1
        GROUP BY pf.region_name, pf.district_name, pf.facility_id, facility_name, mfl_code, a.sex, ag.age_p_tracker_monthly_interval,pdt.pepfar_quarter_period
        
        UNION
        
        SELECT
            'PMTCT_FO_numerator' AS indicator,
            pf.facility_id,
            LEFT(a.sex,1) AS gender, 
            ag.age_p_tracker_monthly_interval AS age_category, 
            pdt.pepfar_quarter_period,
            COUNT(DISTINCT infant_id) AS indicator_value,
            NULL AS art_initiation,
            NULL AS anc_visit_type,
            NULL AS hiv_test_status,
            CASE WHEN
                is_final_status_confirmed_negative = 1
            THEN
                'Negative'
            WHEN
                is_final_status_transfer_in_to_art_clinic = 1
            THEN
                'Positive'
            END AS hiv_test_result,
            CASE WHEN 
                is_final_status_confirmed_negative = 1
            THEN
                'HIV-Negative'
            WHEN 
                is_final_status_transfer_in_to_art_clinic = 1
            THEN
                'HIV-Positive'
            WHEN 
                is_final_status_dead = 1
            THEN
                'Died'
            END AS infant_outcome
        FROM derived.fact_infant_outcome a
        INNER JOIN derived.dim_indicator_facility pf ON pf.facility_id = a.facility_id
        INNER JOIN derived.dim_pnc_date pdt ON pdt.date_yyyymmdd = a.last_visit_date
        INNER JOIN derived.dim_infant_age_group ag ON a.last_visit_age_group_id=ag.infant_age_group_id
        WHERE (is_final_status_transfer_in_to_art_clinic = 1 OR is_final_status_confirmed_negative = 1 OR is_final_status_dead = 1)
        GROUP BY pf.region_name, pf.district_name, pf.facility_id, facility_name, mfl_code, a.sex, ag.age_p_tracker_monthly_interval, pdt.pepfar_quarter_period, is_final_status_transfer_in_to_art_clinic, is_final_status_confirmed_negative, is_final_status_dead
        
        UNION
        
        SELECT
            'PMTCT_FO_denominator' AS indicator,
            pf.facility_id,
            LEFT(a.sex,1) AS gender, 
            ag.age_p_tracker_monthly_interval AS age_category, 
            pdt.pepfar_quarter_period,
            COUNT(DISTINCT infant_id) AS indicator_value,
            NULL AS art_initiation,
            NULL AS anc_visit_type,
            NULL AS hiv_test_status,
            NULL AS hiv_test_result,
            NULL AS infant_outcome
        FROM derived.fact_infant_outcome a
        INNER JOIN derived.dim_indicator_facility pf ON pf.facility_id = a.facility_id
        INNER JOIN derived.dim_pnc_date pdt ON pdt.date_yyyymmdd = a.last_visit_date
        INNER JOIN derived.dim_infant_age_group ag ON a.last_visit_age_group_id=ag.infant_age_group_id
        WHERE DATEDIFF(MONTH, a.date_of_birth, a.last_visit_date) <= 24
        GROUP BY pf.region_name, pf.district_name, pf.facility_id, facility_name, mfl_code, a.sex, ag.age_p_tracker_monthly_interval,pdt.pepfar_quarter_period
    ) ind ON ind.indicator = i.code
    LEFT JOIN
    (
        SELECT
            *
        FROM 
        (
            SELECT 
                date_id,
                pepfar_quarter_period,
                date_yyyymmdd,
                ROW_NUMBER() OVER (PARTITION BY pepfar_quarter_period ORDER BY date_yyyymmdd DESC) AS row_number
            FROM derived.dim_pnc_date
        ) a
        WHERE a.row_number=1
    ) qtr ON qtr.pepfar_quarter_period = ind.pepfar_quarter_period
    WHERE
        i.report_type = 'PEPFAR Indicators' 

-- $END

SELECT TOP 400 *
FROM [derived].[fact_facility_indicator]