USE [staging_ptracker_test]
GO

DROP TABLE IF exists [derived].[fact_facility_indicator];

-- $BEGIN

    CREATE TABLE [derived].[fact_facility_indicator](
        [facility_indicator_id] UNIQUEIDENTIFIER NOT NULL,
        [indicator_id] UNIQUEIDENTIFIER NULL,
        [facility_id] [uniqueidentifier] NULL,
        [gender] [nvarchar](255) NULL,
        [age_category] [nvarchar](255) NULL,
        [art_initiation] [nvarchar](255) NULL,
        [anc_visit_type] [nvarchar](255) NULL,
        [hiv_test_result] [nvarchar](255) NULL,
        [hiv_test_status] [nvarchar](255) NULL,
        [infant_outcome] [nvarchar](255) NULL,
        [indicator_date] [date] NULL,
        [indicator_date_id] [uniqueidentifier] NULL,
        [reported_value] [int] NULL,
        [dhis2_value] [int] NULL,
        [dhis2_reported_value_difference] [int] NULL,
        [dhis2_reported_value_percent_difference] [decimal](10,2) NULL,

    );

    ALTER TABLE [derived].[fact_facility_indicator] ADD CONSTRAINT [PK_fact_facility_indicator_derived] PRIMARY KEY ([facility_indicator_id])

    ALTER TABLE [derived].[fact_facility_indicator] ADD CONSTRAINT FK_fact_facility_indicator_dim_indicator_report_date_derived FOREIGN KEY ([indicator_date_id]) REFERENCES [derived].[dim_indicator_report_date]([date_id]);

    ALTER TABLE [derived].[fact_facility_indicator] ADD CONSTRAINT FK_fact_facility_indicator_dim_facility_derived FOREIGN KEY ([facility_id]) REFERENCES [derived].[dim_indicator_facility]([facility_id]);

    ALTER TABLE [derived].[fact_facility_indicator] ADD CONSTRAINT FK_fact_facility_indicator_dim_indicator_derived FOREIGN KEY ([indicator_id]) REFERENCES [derived].[dim_indicator]([indicator_id]);
    
    CREATE NONCLUSTERED INDEX IDX_fact_facility_indicator_date_id ON [derived].[fact_facility_indicator](indicator_date_id) INCLUDE (indicator_date)

-- $END