USE [staging_ptracker_test]
GO

-- $BEGIN

    UPDATE 
        fi
    SET
        fi.reported_value = anc_val.indicator_value
    FROM 
        [derived].[fact_facility_indicator] fi 
    INNER JOIN
        [derived].[dim_indicator] i 
    ON
        i.indicator_id = fi.indicator_id
    INNER JOIN 
    (
        SELECT 
            * 
        FROM 
        (
            SELECT  
                a.facility_id,
                EOMONTH(encounter_date) AS eom,
                COUNT(DISTINCT CASE WHEN is_first_visit = 1 THEN a.mother_id END) as 'ANC 01',
                COUNT(DISTINCT CASE WHEN is_first_visit = 1 AND is_hiv_test_status_previously_known = 1 THEN a.mother_id END) as 'ANC 02',
                COUNT(DISTINCT CASE WHEN is_first_visit = 1 AND is_hiv_test_status_tested = 1 THEN a.mother_id END) as 'ANC 03',
                COUNT(DISTINCT CASE WHEN is_first_visit = 1 AND is_hiv_test_status_tested = 1 THEN a.mother_id END) as 'ANC 03.1',
                COUNT(DISTINCT CASE WHEN is_first_visit = 1 AND is_hiv_test_status_not_tested = 1 THEN a.mother_id END) as 'ANC 03.2',
                COUNT(DISTINCT CASE WHEN is_first_visit = 1 AND is_hiv_test_status_tested =  1 AND is_hiv_test_results_positive = 1 THEN a.mother_id END) as 'ANC 04',
                COUNT(DISTINCT CASE WHEN is_first_visit = 1 AND is_hiv_test_status_tested =  1 AND is_hiv_test_results_positive = 1 THEN a.mother_id END) as 'ANC 04.1',
                COUNT(DISTINCT CASE WHEN is_first_visit = 1 AND  is_hiv_test_status_tested =  1 AND is_hiv_test_results_negative = 1 THEN a.mother_id END) as 'ANC 05',
                COUNT(DISTINCT CASE WHEN is_first_visit = 1 AND is_hiv_test_status_tested =  1 AND is_hiv_test_results_negative = 1 THEN a.mother_id END) as 'ANC 05.1',
                COUNT(DISTINCT CASE WHEN is_first_visit = 1 AND is_hiv_test_status_tested =  1 AND is_hiv_test_results_unknown = 1 THEN a.mother_id END) as 'ANC 06',
                COUNT(DISTINCT CASE WHEN is_partner_known_positive =  1 THEN a.mother_id END) as 'ANC 07',
                COUNT(DISTINCT CASE WHEN is_partner_hiv_test_done =  1 THEN a.mother_id END) as 'ANC 08',
                COUNT(DISTINCT CASE WHEN is_partner_hiv_test_done =  1 AND is_partner_hiv_positive =  1 THEN a.mother_id END) as 'ANC 08.1',
                COUNT(DISTINCT CASE WHEN is_recent_viral_load_test_done =  1 THEN a.mother_id END) as 'ANC 09',
                COUNT(DISTINCT CASE WHEN is_recent_viral_load_test_done =  1 AND is_viral_load_test_result_undetectable_or_less_than_40 = 1 THEN a.mother_id END) as 'ANC 09.1',
                COUNT(DISTINCT CASE WHEN is_hiv_test_status_previously_known = 1 OR (is_hiv_test_status_tested =  1 AND is_hiv_test_results_positive = 1) THEN a.mother_id END) as 'ANC 10',
                COUNT(DISTINCT CASE WHEN is_art_initiated =  1 THEN a.mother_id END) as 'ANC 11',
                COUNT(DISTINCT CASE WHEN is_art_initiated_before =  1 THEN a.mother_id END) as 'ANC 12',
                COUNT(DISTINCT CASE WHEN is_art_not_initiated =  1 THEN a.mother_id END) as 'ANC 13',
                COUNT(DISTINCT CASE WHEN is_refused_art =  1 THEN a.mother_id END) as 'ANC 13.1',
                COUNT(DISTINCT CASE WHEN is_stock_out =  1 THEN a.mother_id END) as 'ANC 13.2'
            FROM derived.fact_antenatal_visit a
            group by facility_id,EOMONTH(encounter_date)
        ) vls
        unpivot
        (
        indicator_value
        for indicator_name in ([ANC 01], [ANC 02], [ANC 03], [ANC 03.1], [ANC 03.2],[ANC 04],[ANC 04.1],[ANC 05],[ANC 05.1],[ANC 06],[ANC 07],[ANC 08],[ANC 08.1],[ANC 09],[ANC 09.1],[ANC 10],[ANC 11],[ANC 12],[ANC 13],[ANC 13.1],[ANC 13.2])
        ) unpiv
    ) anc_val ON anc_val.indicator_name = i.code AND anc_val.facility_id=fi.facility_id AND anc_val.eom = fi.indicator_date
    WHERE 
        i.report_type = 'ANC Monthly Report' 

    UPDATE 
        fi
    SET
        fi.dhis2_value = fd.data_value
    FROM 
        [derived].[fact_facility_indicator] fi 
    INNER JOIN 
        [derived].[dim_indicator] i
    ON
        fi.indicator_id = i.indicator_id
    INNER JOIN
        [base].[fact_facility_dhis_data] fd
    ON 
        fi.indicator_id = fd.indicator_id AND fi.facility_id = fd.facility_id AND EOMONTH(CAST(fd.data_period AS DATE)) = fi.indicator_date
    WHERE 
        i.report_type = 'ANC Monthly Report'
    ;

    WITH anc_tested_after_32_weeks AS 
    (
        SELECT  
            a.mother_id,
            a.pregnancy_id,
            a.hiv_test_results,
            a.[is_hiv_test_results_negative],
            a.[is_hiv_test_results_positive],
            a.[is_hiv_test_results_unknown]
        FROM 
            derived.fact_antenatal_visit a
        WHERE
            a.gestation_period >= 32 AND a.is_hiv_test_status_tested = 1
    ),
    anc_art_start AS 
    (
        SELECT  
            a.mother_id,
            a.pregnancy_id,
            1 as art_initiated
        FROM 
            derived.fact_antenatal_visit a
        WHERE
            is_art_initiated = 1 or  is_art_initiated_before = 1
    )
    
    UPDATE 
        fi
    SET
        fi.reported_value = mat_val.indicator_value
    FROM 
        [derived].[fact_facility_indicator] fi 
    INNER JOIN
        [derived].[dim_indicator] i 
    ON
        i.indicator_id = fi.indicator_id
    INNER JOIN 
    (
        SELECT 
            * 
        FROM 
        (
            SELECT  
                m.facility_id,
                EOMONTH(m.encounter_date) AS eom,
                COUNT(DISTINCT m.mother_id ) as 'MAT 01',
                COUNT(DISTINCT CASE WHEN is_anc_hiv_test_status_known = 1 THEN m.mother_id END) as 'MAT 02',
                COUNT(DISTINCT CASE WHEN is_anc_hiv_result_positive = 1 THEN m.mother_id END) as 'MAT 02.1',
                COUNT(DISTINCT CASE WHEN anc_tested.is_hiv_test_results_negative = 1 OR m.is_hiv_test_results_negative = 1 THEN m.mother_id END) as 'MAT 03',
                COUNT(DISTINCT CASE WHEN anc_tested.is_hiv_test_results_positive = 1 THEN m.mother_id END) as 'MAT 03.1',
                COUNT(DISTINCT CASE WHEN is_anc_hiv_test_status_known = 0  THEN m.mother_id END) as 'MAT 04',
                COUNT(DISTINCT CASE WHEN is_hiv_test_status_tested = 1 THEN m.mother_id END) as 'MAT 05',
                COUNT(DISTINCT CASE WHEN m.is_hiv_test_results_positive = 1 THEN m.mother_id END) as 'MAT 05.1',
                COUNT(DISTINCT CASE WHEN is_anc_hiv_result_positive = 1 OR is_positive_at_36_weeks_hiv_re_test = 1 OR m.is_hiv_test_results_positive = 1 THEN m.mother_id END) as 'MAT 06',
                COUNT(DISTINCT CASE WHEN (is_anc_hiv_result_positive = 1 OR anc_tested.is_hiv_test_results_positive = 1 OR m.is_hiv_test_results_positive = 1) AND is_art_initiated_already = 1  THEN m.mother_id END) as 'MAT 07',
                COUNT(DISTINCT CASE WHEN (is_anc_hiv_result_positive = 1 OR anc_tested.is_hiv_test_results_positive = 1 OR m.is_hiv_test_results_positive = 1) AND is_art_initiated_during_anc = 1 THEN m.mother_id END) as 'MAT 08',
                COUNT(DISTINCT CASE WHEN (is_anc_hiv_result_positive = 1 OR anc_tested.is_hiv_test_results_positive = 1 OR m.is_hiv_test_results_positive = 1) AND is_art_initiated_during_anc = 1 AND is_art_initiated_during_anc_4_weeks_before_delivery = 1 THEN m.mother_id END) as 'MAT 08.1',
                COUNT(DISTINCT CASE WHEN (is_anc_hiv_result_positive = 1 OR anc_tested.is_hiv_test_results_positive = 1 OR m.is_hiv_test_results_positive = 1) AND is_art_initiated_during_anc = 1 AND is_art_initiated_during_anc_less_than_4_weeks_before_delivery = 1 THEN m.mother_id END) as 'MAT 08.2',
                COUNT(DISTINCT CASE WHEN (is_anc_hiv_result_positive = 1 OR anc_tested.is_hiv_test_results_positive = 1 OR m.is_hiv_test_results_positive = 1) AND is_art_initiated_during_lnd = 1 THEN m.mother_id END) as 'MAT 09',
                COUNT(DISTINCT CASE WHEN is_refused_art =  1 THEN m.mother_id END) as 'MAT 10',
                COUNT(DISTINCT CASE WHEN mi.is_arv_type_nvp = 1 THEN m.mother_id END) as 'MAT 11',
                COUNT(DISTINCT CASE WHEN mi.is_arv_type_nvp_plus_azt = 1 THEN m.mother_id END) as 'MAT 12',
                COUNT(DISTINCT CASE WHEN mi.is_exclusive_breastfeeding = 1 THEN m.mother_id END) as 'MAT 13',
                COUNT(DISTINCT CASE WHEN mi.is_replacement_feeding = 1 THEN m.mother_id END) as 'MAT 14'
            FROM derived.fact_maternity_visit m
            LEFT JOIN derived.fact_maternity_infant_visit mi ON m.pregnancy_id=mi.pregnancy_id
            LEFT JOIN anc_tested_after_32_weeks anc_tested ON m.mother_id = anc_tested.mother_id AND anc_tested.pregnancy_id = m.pregnancy_id
            LEFT JOIN anc_art_start anc_art_start ON m.mother_id = anc_art_start.mother_id AND anc_art_start.pregnancy_id = m.pregnancy_id
            group by m.facility_id, EOMONTH(m.encounter_date)
        ) vls
        unpivot
        (
        indicator_value
        for indicator_name in ([MAT 01], [MAT 02], [MAT 02.1], [MAT 03], [MAT 03.1], [MAT 04],[MAT 05],[MAT 05.1],[MAT 06],[MAT 07],[MAT 08],[MAT 08.1], [MAT 08.2],[MAT 09],[MAT 10],[MAT 11],[MAT 12],[MAT 13],[MAT 14])
        ) unpiv
    ) mat_val ON mat_val.indicator_name = i.code AND mat_val.facility_id=fi.facility_id AND mat_val.eom = fi.indicator_date
    WHERE 
        i.report_type = 'L&D Monthly Report'
    

    UPDATE 
        fi
    SET
        fi.dhis2_value = fd.data_value
    FROM 
        [derived].[fact_facility_indicator] fi 
    INNER JOIN 
        [derived].[dim_indicator] i
    ON
        fi.indicator_id = i.indicator_id
    INNER JOIN
        [base].[fact_facility_dhis_data] fd
    ON 
        fi.indicator_id = fd.indicator_id AND fi.facility_id = fd.facility_id AND EOMONTH(CAST(fd.data_period AS DATE)) = fi.indicator_date
    WHERE 
        i.report_type = 'L&D Monthly Report';

    
    WITH first_result AS 
    (
        SELECT  
            *
        FROM
        (
            SELECT  
                p.facility_id
                ,p.pregnancy_id
                ,p.infant_id
                ,result_date
                ,pcr_result
                ,DATEDIFF(WEEK, date_of_birth, p.result_date) AS age_in_weeks
                ,DATEDIFF(MONTH, date_of_birth, p.result_date) AS age_in_months
                ,result_type
                ,ROW_NUMBER() OVER (PARTITION BY p.infant_id ORDER BY result_date ASC) AS row_number
            FROM derived.fact_pcr_result p
            INNER JOIN derived.dim_infant i ON p.infant_id = i.infant_id
        ) f
        WHERE row_number = 1 
    ), 
    second_result AS 
    (
        SELECT  
            *
        FROM
        (
            SELECT  
                p.facility_id
                ,p.pregnancy_id
                ,p.infant_id
                ,result_date
                ,pcr_result
                ,DATEDIFF(WEEK, date_of_birth, p.result_date) AS age_in_weeks
                ,DATEDIFF(MONTH, date_of_birth, p.result_date) AS age_in_months
                ,result_type
                ,ROW_NUMBER() OVER (PARTITION BY p.infant_id ORDER BY result_date ASC) AS row_number
            FROM derived.fact_pcr_result p
            INNER JOIN derived.dim_infant i ON p.infant_id = i.infant_id
        ) f2
        WHERE row_number = 2 
    ), 
    third_result AS 
    (
        SELECT  
            *
        FROM
        (
            SELECT  
                p.facility_id
                ,p.pregnancy_id
                ,p.infant_id
                ,result_date
                ,pcr_result
                ,DATEDIFF(WEEK, date_of_birth, p.result_date) AS age_in_weeks
                ,DATEDIFF(MONTH, date_of_birth, p.result_date) AS age_in_months
                ,result_type
                ,ROW_NUMBER() OVER (PARTITION BY p.infant_id ORDER BY result_date ASC) AS row_number
            FROM derived.fact_pcr_result p
            INNER JOIN derived.dim_infant i ON p.infant_id = i.infant_id
        ) f3
        WHERE row_number = 3 
    )

    UPDATE 
        fi
    SET
        fi.reported_value = mbfu_val.indicator_value
    FROM 
        [derived].[fact_facility_indicator] fi 
    INNER JOIN
        [derived].[dim_indicator] i 
    ON
        i.indicator_id = fi.indicator_id
    INNER JOIN 
    (
        SELECT 
            * 
        FROM 
        (
            SELECT  
                a.facility_id,
                EOMONTH(a.encounter_date) AS eom,
                COUNT (DISTINCT CASE WHEN a.is_first_visit = 1  THEN a.infant_id END) AS 'MBFU 01',
                COUNT (DISTINCT CASE WHEN a.is_repeat_visit = 1 THEN a.infant_id END) AS 'MBFU 02',
                COUNT(DISTINCT CASE WHEN  fr.infant_id IS NOT NULL THEN a.infant_id END) AS 'MBFU 03',
                COUNT(DISTINCT CASE WHEN  sr.infant_id IS NOT NULL THEN a.infant_id END) AS 'MBFU 04',
                COUNT(DISTINCT CASE WHEN a.is_hiv_exposed = 1 AND tr.infant_id IS NOT NULL AND tr.age_in_months >= 9 THEN a.infant_id END) AS 'MBFU 05',
                COUNT (DISTINCT CASE WHEN a.is_positive_confirmatory_test = 1 AND a.age_in_weeks >= 6 THEN a.infant_id END) AS 'MBFU 06',
                COUNT (DISTINCT CASE WHEN a.is_positive_confirmatory_test = 1 AND a.is_linked_to_art = 1 THEN a.infant_id END) AS 'MBFU 07',
                COUNT (DISTINCT CASE WHEN a.is_negative_confirmatory_test = 1 THEN a.infant_id END) AS 'MBFU 08',
                COUNT (DISTINCT CASE WHEN a.is_hiv_exposed = 1 AND a.age_in_weeks BETWEEN 6 AND 8 AND a.is_prophylaxis_arv = 1 THEN a.infant_id END) AS 'MBFU 09',
                COUNT (DISTINCT CASE WHEN a.is_hiv_exposed = 1 AND a.age_in_weeks BETWEEN 6 AND 8 AND a.is_prophylaxis_started_ctx = 1 THEN a.infant_id END) AS 'MBFU 10',
                COUNT (DISTINCT CASE WHEN a.is_hiv_exposed = 1 AND a.age_in_months = 6 AND a.is_exclusive_breast_feeding = 1 THEN a.infant_id END)   AS 'MBFU 11'
            FROM 
                derived.fact_infant_postnatal_visit a
            LEFT JOIN 
                first_result fr ON a.infant_id = fr.infant_id
            LEFT JOIN 
                second_result sr ON a.infant_id = sr.infant_id
            LEFT JOIN 
                third_result tr ON a.infant_id = tr.infant_id
            group by a.facility_id, EOMONTH(a.encounter_date)
        ) vls
        unpivot
        (
        indicator_value
        for indicator_name in ([MBFU 01], [MBFU 02], [MBFU 03], [MBFU 04],[MBFU 05], [MBFU 06],[MBFU 07],[MBFU 08],[MBFU 09],[MBFU 10],[MBFU 11])
        ) unpiv
    ) mbfu_val ON mbfu_val.indicator_name = i.code AND mbfu_val.facility_id=fi.facility_id AND mbfu_val.eom = fi.indicator_date
    WHERE 
        i.report_type = 'MBFU Monthly Report'
    

    UPDATE 
        fi
    SET
        fi.dhis2_value = fd.data_value
    FROM 
        [derived].[fact_facility_indicator] fi 
    INNER JOIN 
        [derived].[dim_indicator] i
    ON
        fi.indicator_id = i.indicator_id
    INNER JOIN
        [base].[fact_facility_dhis_data] fd
    ON 
        fi.indicator_id = fd.indicator_id AND fi.facility_id = fd.facility_id AND EOMONTH(CAST(fd.data_period AS DATE)) = fi.indicator_date
    WHERE 
        i.report_type = 'MBFU Monthly Report'
    

    UPDATE 
        fi
    SET
        fi.indicator_date_id = id.date_id
    FROM 
        [derived].[fact_facility_indicator] fi 
    INNER JOIN 
        [derived].[dim_indicator_report_date] id
    ON
        fi.indicator_date = id.date_yyyymmdd
    
    UPDATE 
        fi
    SET
        fi.[dhis2_reported_value_difference] = fi.dhis2_value - fi.reported_value,
        fi.[dhis2_reported_value_percent_difference] = 
            CASE WHEN 
                fi.dhis2_value = 0 
            THEN 
                0 
            WHEN 
                (CAST((fi.dhis2_value - fi.reported_value) AS DECIMAL) / CAST(fi.dhis2_value AS DECIMAL)) < 0 
            THEN 
                0 
            ELSE 
                (CAST((fi.dhis2_value - fi.reported_value) AS DECIMAL) / CAST(fi.dhis2_value AS DECIMAL)) 
            END
    FROM 
        [derived].[fact_facility_indicator] fi 

-- $END
SELECT TOP 400
    *
FROM 
    [derived].[fact_facility_indicator]