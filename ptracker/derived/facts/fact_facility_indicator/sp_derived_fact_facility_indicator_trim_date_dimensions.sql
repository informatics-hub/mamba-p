USE staging_ptracker_test;
GO

--Count dates before trimming

    SELECT COUNT(*) count_dim_indicator_report_date_before_trim
    FROM derived.dim_indicator_report_date;
    

-- $BEGIN

    DELETE a
    FROM derived.dim_indicator_report_date a
    WHERE 
        NOT EXISTS
            (
                SELECT 1
                FROM derived.fact_facility_indicator b
                WHERE a.date_id=b.indicator_date_id
            )

-- $END

--Count dates after trimming

    SELECT COUNT(*) count_dim_indicator_report_date_after_trim
    FROM derived.dim_indicator_report_date;
