USE [staging_ptracker_test]
GO

TRUNCATE TABLE [derived].[fact_maternity_infant_visit]

-- $BEGIN

    INSERT INTO [derived].[fact_maternity_infant_visit](
        [maternity_infant_visit_id], 
        [infant_id],
        [creator],
        [provider],
        [pregnancy_id],
        [facility_id],
        [encounter_date],
        [encounter_date_id],
        [date_created], 
        [date_created_id],
        [status],
        [discharge_feeding],
        [date_of_death],
        [death_date_id],
        [still_birth],
        [received_arv],
        [reason_refused_arv]
    )
    SELECT 
        NEWID(), 
        a.infant_id,
        a.creator,
        a.provider,
        a.pregnancy_id,
        a.facility_id,
        a.encounter_date,
        a.encounter_date_id,
        a.date_created,
        a.date_created_id,
        a.status,
        a.breastfeeding,
        a.date_of_death,
        a.date_id,
        a.still_birth,
        a.received_arv,
        a.reason_refused_arv
    FROM 
    ( 
        SELECT DISTINCT
            ma.maternity_infant_visit_id,
            cl_infant.infant_id,
            p.pregnancy_id,
            e.facility_id,
            e.creator,
            e.provider,
            encounter_date,
            encounter_date_id,
            date_created,
            date_created_id,
            ma.status,
            ma.breastfeeding,
            ma.date_of_death,
            md.date_id,
            ma.still_birth,
            ma.received_arv,
            ma.reason_refused_arv
        FROM
            [base].[fact_maternity_infant_visit] ma
        INNER JOIN
            [base].[dim_encounter] e ON ma.encounter_id=e.encounter_id
        LEFT JOIN
            derived.dim_pregnancy p ON p.ptracker_identifier = LEFT(ma.infant_number, LEN(ma.infant_number)-1) 
        INNER JOIN
            [derived].[dim_infant] cl_infant ON cl_infant.pregnancy_id=p.pregnancy_id AND cl_infant.ptracker_identifier=ma.infant_number
        LEFT JOIN
            [derived].[dim_maternity_date] md ON ma.date_of_death = md.date_yyyymmdd
    ) a;
-- $END