USE [staging_ptracker_test]
GO

-- $BEGIN

    UPDATE 
        [derived].[fact_maternity_infant_visit]
    SET 
        [is_exclusive_breastfeeding] = 
            CASE WHEN 
                discharge_feeding = 'BREASTFED EXCLUSIVELY'
            THEN 
                1
            ELSE
                0
            END,
        [is_replacement_feeding] = 
            CASE WHEN 
                discharge_feeding = 'Replacement feeding'
            THEN 
                1
            ELSE
                0
            END,
        [is_arv_type_nvp] = 
            CASE WHEN 
                received_arv = 'NVP Prophylaxis daily up to 6 weeks'
            THEN 
                1
            ELSE
                0
            END,
        [is_arv_type_nvp_plus_azt] = 
            CASE WHEN 
                received_arv = 'Infant received NVP + AZT prophylaxis up to 6 weeks'
            THEN 
                1
            ELSE
                0
            END
    ;

-- $END
SELECT TOP 400 *
FROM [derived].[fact_maternity_infant_visit]