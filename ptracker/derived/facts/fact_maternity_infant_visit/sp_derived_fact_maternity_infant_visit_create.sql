USE [staging_ptracker_test]
GO

DROP TABLE IF exists [derived].[fact_maternity_infant_visit];

-- $BEGIN

    CREATE TABLE [derived].[fact_maternity_infant_visit](
        [maternity_infant_visit_id] [UNIQUEIDENTIFIER] NOT NULL,
        [infant_id] [UNIQUEIDENTIFIER] NULL,
        [pregnancy_id] [UNIQUEIDENTIFIER] NULL,
        [creator] [nvarchar](255) NOT NULL,
        [provider] [nvarchar](255) NOT NULL,
        [facility_id] [UNIQUEIDENTIFIER] NULL,
        [date_created] [date] NULL, 
        [date_created_id] [uniqueidentifier] NULL,
        [encounter_date] [date],
        [encounter_date_id] [UNIQUEIDENTIFIER],
        [status] [nvarchar] (255) NULL,
        [discharge_feeding] [nvarchar] (255) NULL,
        [is_exclusive_breastfeeding] [int] NULL,
        [is_replacement_feeding] [int] NULL,
        [date_of_death] [date] NULL,
        [death_date_id] [UNIQUEIDENTIFIER] NULL,
        [still_birth] [nvarchar] (255) NULL,
        [received_arv] [nvarchar] (255) NULL,
        [is_arv_type_nvp] [int] NULL,
        [is_arv_type_nvp_plus_azt] [int] NULL,
        [reason_refused_arv] [nvarchar] (max) NULL
    );

    ALTER TABLE [derived].[fact_maternity_infant_visit] ADD CONSTRAINT [PK_fact_maternity_infant_visit_derived] PRIMARY KEY ([maternity_infant_visit_id]);
    
    ALTER TABLE [derived].[fact_maternity_infant_visit] ADD CONSTRAINT FK_fact_maternity_infant_dim_date_death_derived FOREIGN KEY ([death_date_id]) REFERENCES [derived].[dim_maternity_date]([date_id]);

    ALTER TABLE [derived].[fact_maternity_infant_visit] ADD CONSTRAINT FK_fact_maternity_infant_dim_date_created_derived FOREIGN KEY ([date_created_id]) REFERENCES [derived].[dim_maternity_date]([date_id]);

    ALTER TABLE [derived].[fact_maternity_infant_visit] ADD CONSTRAINT FK_fact_maternity_infant_pregnancy FOREIGN KEY ([pregnancy_id]) REFERENCES [derived].[dim_pregnancy]([pregnancy_id]);

    ALTER TABLE [derived].[fact_maternity_infant_visit] ADD CONSTRAINT FK_FK_fact_maternity_infant_visit_dim_facility FOREIGN KEY ([facility_id]) REFERENCES [derived].[dim_facility]([facility_id])

    ALTER TABLE [derived].[fact_maternity_infant_visit] ADD CONSTRAINT FK_FK_fact_maternity_infant_visit_dim_infant FOREIGN KEY ([infant_id]) REFERENCES [derived].[dim_infant]([infant_id])

    ALTER TABLE [derived].[fact_maternity_infant_visit] ADD CONSTRAINT FK_fact_maternity_infant_dim_date_encounter_date FOREIGN KEY ([encounter_date_id]) REFERENCES [derived].[dim_maternity_date]([date_id]);
-- $END