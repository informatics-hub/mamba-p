USE [staging_ptracker_test]
GO

DROP TABLE IF exists [derived].[fact_visual_access];

-- $BEGIN

    CREATE TABLE [derived].[fact_visual_access](
        [visual_access_id] [UNIQUEIDENTIFIER] NOT NULL,
        [username] [nvarchar](255) NULL,
        [visual] [nvarchar](255) NULL,
        [visual_type] [nvarchar](255) NULL,
        [visual_id] [uniqueidentifier] NULL,
        [visual_dashboard] [int] NULL,
        [visual_report] [int] NULL,
        [access_date] [date] NULL,
        [access_date_id] [uniqueidentifier] NULL,
        [status] [nvarchar](50) NULL,
        [dashboard_status_success] [int] NULL,
        [dashboard_status_error] [int] NULL,
        [report_status_success] [int] NULL,
        [report_status_error] [int] NULL,
    );

    ALTER TABLE [derived].[fact_visual_access] ADD CONSTRAINT [PK_fact_visual_access_derived] PRIMARY KEY ([visual_access_id]);

    ALTER TABLE [derived].[fact_visual_access] ADD CONSTRAINT [FK_fact_visual_access_dim_visual_access_date_derived] FOREIGN KEY([access_date_id]) REFERENCES [derived].[dim_visual_access_date] ([date_id]);

    ALTER TABLE [derived].[fact_visual_access] ADD CONSTRAINT [FK_fact_visual_access_dim_visual_derived] FOREIGN KEY([visual_id]) REFERENCES [derived].[dim_visual] ([visual_id]);


-- $END