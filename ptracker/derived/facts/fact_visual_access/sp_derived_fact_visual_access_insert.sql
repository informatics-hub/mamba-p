USE [staging_ptracker_test]
GO

TRUNCATE TABLE [derived].[fact_visual_access]

-- $BEGIN
    INSERT INTO [derived].[fact_visual_access](
        [visual_access_id],
        [username],
        [visual],
        [visual_id],
        [visual_dashboard],
        [visual_report],
        [visual_type],
        [access_date],
        [access_date_id],
        [status],
        [dashboard_status_success],
        [dashboard_status_error],
        [report_status_success],
        [report_status_error]
    )

    SELECT
        [visual_access_id],
        [username],
        SUBSTRING(visual,charindex('PTRACKER',visual) + LEN('PTRACKER'), LEN(visual)),
        visual_id,
        CASE WHEN 
            visual LIKE '%dashboard%' 
        THEN 
            1
        END,
        CASE WHEN 
            visual NOT LIKE '%dashboard%' 
        THEN 
            1
        END,
        CASE WHEN 
            visual LIKE '%dashboard%' 
        THEN 
            'Dashboard'
        ELSE
            'Report'
        END,
        access_date,
        date_id,
        CASE WHEN 
            [Status] = 'rsSuccess'
        THEN
            'Success'
        WHEN 
            [Status] = 'rsInternalError'
        THEN
            'Error'
        END,
        CASE WHEN 
            [Status] = 'rsSuccess' AND visual LIKE '%dashboard%' 
        THEN 
            1
        END,
        CASE WHEN 
            [Status] = 'rsInternalError' AND visual LIKE '%dashboard%' 
        THEN 
            1
        END,
        CASE WHEN 
            [Status] = 'rsSuccess' AND visual NOT LIKE '%dashboard%' 
        THEN 
            1
        END,
        CASE WHEN 
            [Status] = 'rsInternalError' AND visual NOT LIKE '%dashboard%' 
        THEN 
            1
        END
    FROM
        [base].[fact_visual_access] va
    INNER JOIN
        [derived].[dim_visual_access_date] d
    ON
        va.access_date = d.date_yyyymmdd
    INNER JOIN
        [derived].[dim_visual] v
    ON
        va.visual = v.visual_name
    WHERE 
        username !='NETWORK SERVICE'
;


-- $END

SELECT TOP 400 *
FROM [derived].[fact_visual_access]
