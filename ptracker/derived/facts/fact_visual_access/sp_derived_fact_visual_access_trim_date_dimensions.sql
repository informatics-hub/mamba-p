USE staging_ptracker_test;
GO

--Count date before trimming

    SELECT COUNT(*) count_dim_visual_access_date_before_trim
    FROM derived.dim_visual_access_date;
    

-- $BEGIN

    DELETE a
    FROM derived.dim_visual_access_date a
    WHERE 
        NOT EXISTS
            (
                SELECT 1
                FROM derived.fact_visual_access b
                WHERE a.date_id=b.access_date_id
            )

-- $END

--Count date after trimming

    SELECT COUNT(*) count_dim_visual_access_date_after_trim
    FROM derived.dim_visual_access_date;

