USE [staging_ptracker_test]
GO
-- $BEGIN

    EXEC sp_derived_fact_visual_access_create;
    EXEC sp_derived_fact_visual_access_insert;
    EXEC sp_derived_fact_visual_access_trim_date_dimensions;
-- $END