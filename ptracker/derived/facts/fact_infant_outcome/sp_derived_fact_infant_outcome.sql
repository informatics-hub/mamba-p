USE [staging_ptracker_test]
GO
-- $BEGIN

    EXEC sp_derived_fact_infant_outcome_create;
    EXEC sp_derived_fact_infant_outcome_insert;
    EXEC sp_derived_fact_infant_outcome_update;
    EXEC sp_derived_fact_infant_outcome_trim_facility_dimensions;

-- $END