USE [staging_ptracker_test]
GO

-- $BEGIN

    UPDATE 
        i_outcome
    SET 
        [pnc_date_id] = pd.date_id
    FROM 
        [derived].[fact_infant_outcome] i_outcome
    LEFT JOIN
        [derived].[dim_pnc_date] pd
    ON
        pd.date_yyyymmdd = i_outcome.pnc_date

    UPDATE 
        i_outcome
    SET 
        [last_visit_date] = ipv.encounter_date,
        [last_visit_date_id] = ipv.encounter_date_id,
        [last_visit_age_in_weeks] = DATEDIFF(WEEK, i_outcome.date_of_birth, ipv.encounter_date) ,
        [last_visit_age_group_id] = ag.infant_age_group_id
    FROM 
        [derived].[fact_infant_outcome] i_outcome
    INNER JOIN
        [derived].fact_infant_postnatal_visit ipv 
    ON
        ipv.infant_postnatal_visit_id=i_outcome._infant_pnc_last_visit_id
    LEFT JOIN
        [derived].[dim_infant_age_group] ag
    ON
        ag.age_in_weeks=DATEDIFF(WEEK, i_outcome.date_of_birth, ipv.encounter_date)
    
    UPDATE 
        i_outcome
    SET 
        [is_newly_registered] =
            CASE WHEN 
                DATEDIFF(DAY, date_of_birth, pnc_date) <= 2 
            THEN
                'At Birth'
            WHEN 
                DATEDIFF(DAY, date_of_birth, pnc_date) > 2 AND  DATEDIFF(WEEK, date_of_birth, pnc_date) <= 8
            THEN
                'After Birth and <= 8 weeks'
            WHEN 
                DATEDIFF(WEEK, date_of_birth, pnc_date) > 8 AND  DATEDIFF(MONTH, date_of_birth, pnc_date) <= 8
            THEN
                '9 weeks - 8 months'
            WHEN 
                DATEDIFF(MONTH, date_of_birth, pnc_date) > 8 AND  DATEDIFF(MONTH, date_of_birth, pnc_date) <= 12
            THEN
                '9 months - 12 months'
            WHEN 
                DATEDIFF(MONTH, date_of_birth, pnc_date) >= 12
            THEN
                '> 12 months'
            END,
        [is_newly_registered_order] = 
            CASE WHEN 
                DATEDIFF(DAY, date_of_birth, pnc_date) <= 2 
            THEN
                1
            WHEN 
                DATEDIFF(DAY, date_of_birth, pnc_date) > 2 AND  DATEDIFF(WEEK, date_of_birth, pnc_date) <= 8
            THEN
                2
            WHEN 
                DATEDIFF(WEEK, date_of_birth, pnc_date) > 8 AND  DATEDIFF(MONTH, date_of_birth, pnc_date) <= 8
            THEN
                3
            WHEN 
                DATEDIFF(MONTH, date_of_birth, pnc_date) > 8 AND  DATEDIFF(MONTH, date_of_birth, pnc_date) <= 12
            THEN
                4
            WHEN 
                DATEDIFF(MONTH, date_of_birth, pnc_date) >= 12
            THEN
                5
            END
    FROM 
        [derived].[fact_infant_outcome] i_outcome
    ;

    UPDATE 
        i_outcome
    SET 
        [hiv_test_status_within_48_hours] = 
            CASE WHEN 
                ip_test.test_period_days <= 2 
            THEN
                ip_test.pcr_result
            END,        
        [hiv_test_status_within_48_hours_date] = 
            CASE WHEN 
                ip_test.test_period_days <= 2 
            THEN
                ip_test.result_date
            END,
        [hiv_test_status_within_48_hours_date_id] = 
            CASE WHEN 
                ip_test.test_period_days <= 2 
            THEN
                ip_test.result_date_id
            END,
        [is_tested_within_48_hours] = 
            CASE WHEN 
                ip_test.test_period_days <= 2 
            THEN
                1
            ELSE
                0
            END,
        [hiv_test_status_at_6_to_8_weeks] = 
            CASE WHEN 
                ip_test.test_period_weeks >= 6 AND  ip_test.test_period_weeks <= 8 
            THEN
                ip_test.pcr_result
            END,
        [hiv_test_status_at_6_to_8_weeks_date] = 
            CASE WHEN 
                ip_test.test_period_weeks >= 6 AND  ip_test.test_period_weeks <= 8 
            THEN
                ip_test.result_date
            END,
        [hiv_test_status_at_6_to_8_weeks_date_id] = 
            CASE WHEN 
                ip_test.test_period_weeks >= 6 AND  ip_test.test_period_weeks <= 8 
            THEN
                ip_test.result_date_id
            END,
        [is_tested_at_6_to_8_weeks] = 
            CASE WHEN 
                ip_test.test_period_weeks >= 6 AND  ip_test.test_period_weeks <= 8 
            THEN
                1
            ELSE
                0
            END,
        [hiv_test_status_at_9_weeks_to_12_months] = 
            CASE WHEN 
                ip_test.test_period_weeks >= 9 AND ip_test.test_period_months <= 12
            THEN
                ip_test.pcr_result
            END,
        [hiv_test_status_at_9_weeks_to_12_months_date] = 
            CASE WHEN 
                ip_test.test_period_weeks >= 9 AND ip_test.test_period_months <= 12
            THEN
                ip_test.result_date
            END,
        [hiv_test_status_at_9_weeks_to_12_months_date_id] = 
            CASE WHEN 
                ip_test.test_period_weeks >= 9 AND ip_test.test_period_months <= 12
            THEN
                ip_test.result_date_id
            END,
        [is_tested_at_9_weeks_to_12_months] = 
            CASE WHEN 
                ip_test.test_period_weeks >= 9 AND ip_test.test_period_months <= 12
            THEN
                1
            ELSE
                0
            END,
        [hiv_test_status_over_12_months] = 
            CASE WHEN 
                ip_test.test_period_months > 12
            THEN
                ip_test.pcr_result
            END,
        [hiv_test_status_over_12_months_date] = 
            CASE WHEN 
                ip_test.test_period_months > 12
            THEN
                ip_test.result_date
            END,
        [hiv_test_status_over_12_months_date_id] = 
            CASE WHEN 
                ip_test.test_period_months > 12
            THEN
                ip_test.result_date_id
            END,
        [is_tested_over_12_months] = 
            CASE WHEN 
                ip_test.test_period_months > 12
            THEN
                1
            ELSE
                0
            END,
        [is_newly_registered_testing_period] = 
            CASE WHEN 
                ip_test.test_period_days <= 2 
            THEN
                'At Birth'
            WHEN 
                ip_test.test_period_days > 2 AND  ip_test.test_period_weeks <= 8
            THEN
                'After Birth and <= 8 weeks'
            WHEN 
                ip_test.test_period_weeks > 8 AND  ip_test.test_period_months <= 8
            THEN
                '9 weeks - 8 months'
            WHEN 
                ip_test.test_period_months > 8 AND  ip_test.test_period_months <= 12
            THEN
                '9 months - 12 months'
            WHEN 
                ip_test.test_period_months >= 12
            THEN
                '> 12 months'
            END,
        [is_ever_tested] = 
             CASE WHEN 
                ip_test.infant_id IS NOT NULL
            THEN
                1
            ELSE
                0
            END,
        [is_test_result_missing] = 
            CASE WHEN 
                ip_test.pcr_result IS NULL AND is_in_pnc = 1 
            THEN
                1
            WHEN
                ip_test.pcr_result IS NOT NULL AND is_in_pnc = 1 
            THEN
                0
            END
    FROM 
        [derived].[fact_infant_outcome] i_outcome
    LEFT JOIN
        (
            SELECT 
                *
            FROM
            (
                SELECT 
                i.infant_id, 
                pcr_result, 
                result_date, 
                date_of_birth, 
                DATEDIFF(DAY,date_of_birth, result_date ) AS test_period_days, 
                DATEDIFF(WEEK,date_of_birth, result_date ) AS test_period_weeks, 
                DATEDIFF(MONTH,date_of_birth, result_date ) AS test_period_months, 
                result_date_id,
                ROW_NUMBER() OVER (PARTITION BY i.infant_id ORDER BY result_date ASC) AS row_number
            FROM 
                [derived].[fact_pcr_result] a
            INNER JOIN 
                derived.dim_infant i 
            ON 
                a.infant_id=i.infant_id
            ) a
            WHERE a.row_number = 1
        ) ip_test
    ON 
        ip_test.infant_id = i_outcome.infant_id  
    ;

    UPDATE 
        i_outcome
    SET 
        [is_mother_hiv_positive] =
            CASE WHEN 
                av.is_hiv_positive = 1 OR mv.is_hiv_positive = 1 OR mpv.is_hiv_positive = 1
            THEN
                1
            ELSE
                0
            END,
        [is_mother_hiv_positive_at_maternity] = 
            CASE WHEN 
                mv.is_hiv_positive = 1 AND i_outcome.is_in_maternity = 1
            THEN
                1
            ELSE
                0
            END
    FROM 
        [derived].[fact_infant_outcome] i_outcome 
    LEFT JOIN
        [derived].[fact_antenatal_visit] av
    ON 
        i_outcome.mother_client_id=av.mother_id AND i_outcome.pregnancy_id=av.pregnancy_id
    LEFT JOIN
        [derived].[fact_maternity_visit] mv 
    ON 
        i_outcome.mother_client_id=mv.mother_id AND i_outcome.pregnancy_id=mv.pregnancy_id
    LEFT JOIN
        [derived].[fact_mother_postnatal_visit] mpv
    ON 
        i_outcome.mother_client_id=mpv.mother_id AND i_outcome.pregnancy_id=mpv.pregnancy_id
    ;

    UPDATE 
        i_outcome
    SET 
        is_prophylaxis_received_at_birth =
            CASE WHEN 
                pro_arv_mat.infant_id IS NOT NULL AND is_mother_hiv_positive_at_maternity = 1
            THEN
                1
            ELSE
                0
            END,
        is_arv_prophylaxis_received = 
            CASE WHEN
                pro_arv_post.infant_id IS NOT NULL OR pro_arv_mat.infant_id IS NOT NULL
            THEN
                1
            ELSE
                0
            END,
        is_ctx_prophylaxis_received = 
            CASE WHEN
                 pro_ctx.infant_id IS NOT NULL
            THEN
                1
            ELSE
                0
            END,
        is_hiv_exposed = 
            CASE WHEN
                 exposed.infant_id IS NOT NULL
            THEN
                1
            ELSE
                0
            END
    FROM 
        [derived].[fact_infant_outcome] i_outcome
    LEFT JOIN
        (
            SELECT DISTINCT infant_id
            FROM [derived].fact_infant_postnatal_visit
            WHERE is_prophylaxis_arv = 1
        ) pro_arv_post
    ON 
        i_outcome.infant_id = pro_arv_post.infant_id 
    LEFT JOIN
        (
            SELECT DISTINCT infant_id
            FROM [derived].fact_maternity_infant_visit
            WHERE is_arv_type_nvp = 1 OR is_arv_type_nvp_plus_azt = 1

        ) pro_arv_mat
    ON 
        i_outcome.infant_id = pro_arv_mat.infant_id 
    LEFT JOIN
        (
            SELECT DISTINCT infant_id
            FROM [derived].fact_infant_postnatal_visit
            WHERE  is_prophylaxis_started_ctx = 1
        ) pro_ctx
    ON 
        i_outcome.infant_id = pro_ctx.infant_id 
    LEFT JOIN
        (
            SELECT DISTINCT infant_id
            FROM [derived].fact_infant_postnatal_visit
            WHERE is_hiv_exposed = 1
        ) exposed
    ON 
        i_outcome.infant_id = exposed.infant_id 
    ;

     UPDATE 
        i_outcome
    SET 
        is_arv_type_nvp_at_birth = 
            CASE WHEN 
                mi.is_arv_type_nvp = 1
            THEN
                1
            ELSE
                0
            END,
        is_arv_type_nvp_plus_azt_at_birth = 
            CASE WHEN 
                mi.is_arv_type_nvp_plus_azt = 1
            THEN
                1
            ELSE
                0
            END,
        is_prophylaxis_refused_at_birth = 
            CASE WHEN
                mi.received_arv = 'REFUSAL OF TREATMENT BY PATIENT'
            THEN
                1
            ELSE
                0
            END,
        is_prophylaxis_stocked_out_at_birth = 
            CASE WHEN
                mi.received_arv = 'Not started due to stockout of ART'
            THEN
                1
            ELSE
                0
            END,
        is_prophylaxis_unknown_at_birth = 
            CASE WHEN
                mi.received_arv IS NULL
            THEN
                1
            ELSE
                0
            END,
        is_exclusive_breastfeeding_at_birth = mi.is_exclusive_breastfeeding,
        is_replacement_feeding_at_birth = mi.is_replacement_feeding,
        is_unknown_feeding_at_birth = 
            CASE WHEN
                ((mi.is_exclusive_breastfeeding IS NULL AND mi.is_replacement_feeding IS NULL) OR (mi.is_exclusive_breastfeeding = 0 AND mi.is_replacement_feeding = 0))
            THEN
                1
            ELSE
                0
            END,
        is_unknown_arv_type_at_birth = 
            CASE WHEN
                ((mi.is_arv_type_nvp = 0 AND mi.is_arv_type_nvp_plus_azt = 0) OR (mi.is_arv_type_nvp IS NULL AND mi.is_arv_type_nvp_plus_azt IS NULL))
            THEN
                1
            ELSE
                0
            END
    FROM 
        [derived].[fact_infant_outcome] i_outcome
    LEFT JOIN
        [derived].fact_maternity_infant_visit mi
    ON
        i_outcome.infant_id = mi.infant_id
    WHERE 
        is_mother_hiv_positive_at_maternity = 1
    ; 

    UPDATE 
        i_outcome
    SET 
        is_ever_tested_by_12_months = 
            CASE WHEN
                tested.infant_id IS NOT NULL
            THEN
                1
            ELSE
                0
            END,
        is_ever_hiv_test_result_positive = 
            CASE WHEN
                pos.infant_id IS NOT NULL
            THEN
                1
            ELSE
                0
            END
    FROM 
        [derived].[fact_infant_outcome] i_outcome
    LEFT JOIN
        (
            SELECT DISTINCT ipv.infant_id
            FROM [derived].fact_infant_postnatal_visit ipv
            INNER JOIN [derived].dim_infant i ON ipv.infant_id = i.infant_id
            WHERE hiv_test_status = 'Tested for HIV during this visit' AND DATEDIFF(MONTH, i.date_of_birth, ipv.encounter_date) <= 12
        ) tested
    ON 
        i_outcome.infant_id = tested.infant_id 
    LEFT JOIN
        (
            SELECT DISTINCT ipv.infant_id
            FROM [derived].fact_infant_postnatal_visit ipv
            INNER JOIN derived.dim_infant i ON ipv.infant_id=i.infant_id
            WHERE (dna_pcr_test_result = 'Positive' OR rapid_test_result = 'Positive') AND DATEDIFF(MONTH, date_of_birth, encounter_date)<=12
        ) pos
    ON 
        i_outcome.infant_id = pos.infant_id 
    ;

    UPDATE 
        i_outcome
    SET 
        [final_status] = fn.outcome_status,
        [transfer_in_facility] = fn.transfer_in_from,
        [transfer_in_date] = fn.transfer_in_date,
        [transfer_in_date_id] = fn.transfer_in_date_id,
        [transfered_out_facility] = fn.transfer_out_to,
        [transfered_out_to_date] = fn.transfer_out_to_date,
        [transfered_out_date_id] = fn.transfer_out_to_date_id,
        [death_date] = fn.death_date,
        [death_date_id] = fn.death_date_id
    FROM 
        [derived].[fact_infant_outcome] i_outcome
    LEFT JOIN
        (
            SELECT
                infant_postnatal_visit_id,
                encounter_date,
                infant_id, 
                outcome_status, 
                transfer_in_date, 
                transfer_in_from, 
                transfer_in_date_id, 
                transfer_out_to, 
                transfer_out_to_date, 
                transfer_out_to_date_id, 
                death_date, 
                death_date_id 
            FROM 
                [derived].fact_infant_postnatal_visit
        ) fn
    ON
        fn.infant_id = i_outcome.infant_id AND fn.infant_postnatal_visit_id = i_outcome._infant_pnc_last_visit_id
    WHERE
        DATEDIFF(MONTH, date_of_birth,fn.encounter_date )<=18

    
    UPDATE 
        i_outcome
    SET
        [is_final_status_still_in_care] = 
            CASE WHEN
                final_status = 'Still in Care'
            THEN
                1
            ELSE 
                0
            END,
        [is_final_status_transfer_in_to_art_clinic] = 
            CASE WHEN
                final_status = 'Transfer in' AND pos.infant_id IS NOT NULL
            THEN
                1
            ELSE 
                0
            END,
        [is_final_status_transfered_out] =
            CASE WHEN
                final_status = 'Transferred out'
            THEN
                1
            ELSE 
                0
            END,
        [is_final_status_dead] =
            CASE WHEN
                final_status = 'Dead'
            THEN
                1
            ELSE 
                0
            END,
        [is_final_status_confirmed_negative] = 
            CASE WHEN
                final_status = 'HIV negative infant discharged from PMTCT'
            THEN
                1
            ELSE 
                0
            END,
        [is_final_status_lost_to_follow_up] =
            CASE WHEN
                final_status = 'Lost to followup'
            THEN
                1
            ELSE 
                0
            END
    FROM 
        [derived].[fact_infant_outcome] i_outcome
    LEFT JOIN
        (
            SELECT DISTINCT ipv.infant_id
            FROM [derived].fact_infant_postnatal_visit ipv
            INNER JOIN derived.dim_infant i ON ipv.infant_id=i.infant_id
            WHERE (dna_pcr_test_result = 'Positive' AND DATEDIFF(MONTH, date_of_birth, encounter_date)<=17) OR (rapid_test_result = 'Positive' AND DATEDIFF(MONTH, date_of_birth, encounter_date)>=18)
        ) pos
    ON 
        i_outcome.infant_id = pos.infant_id 
    

    UPDATE 
        i_outcome
    SET 
        [is_art_initiation_missing] = 
            CASE WHEN
                is_final_status_transfer_in_to_art_clinic = 0 AND is_ever_hiv_test_result_positive = 1
            THEN
                1
            ELSE 
                0
            END
    FROM 
        [derived].[fact_infant_outcome] i_outcome

    UPDATE 
        i_outcome
    SET 
        [is_final_status_present] = 
            CASE WHEN
                status_by_18_months.infant_id IS NOT NULL
            THEN
                1
            ELSE 
                0
            END
    FROM 
        [derived].[fact_infant_outcome] i_outcome
    LEFT JOIN
        (
            SELECT DISTINCT ipv.infant_id
            FROM [derived].fact_infant_postnatal_visit ipv
            INNER JOIN [derived].dim_infant i ON ipv.infant_id = i.infant_id
            WHERE outcome_status IS NOT NULL AND outcome_status != 'Still in Care' AND DATEDIFF(MONTH, i.date_of_birth, ipv.encounter_date) <= 18
        ) status_by_18_months
    ON 
        i_outcome.infant_id = status_by_18_months.infant_id 

-- $END
SELECT TOP 400 *
FROM [derived].[fact_infant_outcome]