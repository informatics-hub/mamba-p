USE [staging_ptracker_test]
GO

TRUNCATE TABLE [derived].[fact_infant_outcome];

-- $BEGIN
    WITH min_mat AS 
    (
        SELECT 
            encounter_date, 
            infant_id,
            facility_id,
            ROW_NUMBER() OVER (PARTITION BY infant_id ORDER BY encounter_date ASC) AS row_number
        FROM 
            [derived].[fact_maternity_infant_visit] mp
    ), 
    min_pnc AS 
    (
        SELECT 
            encounter_date, 
            ip.infant_id,
            CASE WHEN 
                ip.facility_id IS NOT NULL
            THEN
                ip.facility_id
            ELSE 
                i.facility_id
            END AS facility_id,
            ROW_NUMBER() OVER (PARTITION BY ip.infant_id ORDER BY encounter_date ASC) AS row_number
        FROM 
            [derived].[fact_infant_postnatal_visit] ip
        INNER JOIN 
            [derived].[dim_infant] i
        ON 
            i.infant_id=ip.infant_id
    ),
    max_pnc AS 
    (
        SELECT 
            infant_postnatal_visit_id,
            encounter_date, 
            infant_id,
            facility_id,
            ROW_NUMBER() OVER (PARTITION BY infant_id ORDER BY encounter_date DESC) AS row_number
        FROM 
            [derived].[fact_infant_postnatal_visit] ip
    )

    INSERT INTO [derived].[fact_infant_outcome](
        [infant_outcome_id],
        [infant_id],
        [pregnancy_id],
        [ptracker_identifier], 
        [sex],
        [openmrs_patient_identifier],
        [mother_client_id],
        [date_created],
        [date_created_id],
        [district],
        [region],
        [facility_id],
        [date_of_birth],
        [date_of_birth_estimated],
        [maternity_date],
        [maternity_facility_id],
        [pnc_date],
        [pnc_facility_id],
        [is_in_maternity],
        [is_in_pnc],
        [is_in_maternity_and_pnc],
        [_infant_pnc_last_visit_id]
    )
    
    SELECT DISTINCT
        NEWID(),
        i.infant_id,
        p.pregnancy_id,
        i.ptracker_identifier,
        i.sex,
        i.openmrs_patient_identifier,
        p.mother_id,
        i.patient_date_created,
        d_create.date_id,
        i.district,
        i.region,
        i.facility_id,
        i.date_of_birth,
        i.date_of_birth_estimated,
        min_mat.encounter_date,
        min_mat.facility_id,
        min_pnc.encounter_date,
        min_pnc.facility_id,
        CASE WHEN 
            min_mat.encounter_date IS NOT NULL
        THEN
            1
        ELSE
            0
        END,
        CASE WHEN 
            min_pnc.encounter_date IS NOT NULL
        THEN
            1
        ELSE
            0
        END,
        CASE WHEN 
            min_mat.encounter_date IS NOT NULL AND min_pnc.encounter_date IS NOT NULL
        THEN
            1
        ELSE
            0
        END,
        max_pnc.infant_postnatal_visit_id
    FROM 
        [derived].[dim_infant] i
    LEFT JOIN
        [derived].[dim_pregnancy] p 
    ON 
        i.pregnancy_id=p.pregnancy_id
    LEFT JOIN
        [derived].[dim_date] d_create 
    ON 
        d_create.date_yyyymmdd=i.patient_date_created
    LEFT JOIN
        min_mat 
    ON 
        min_mat.infant_id=i.infant_id AND min_mat.row_number=1
    LEFT JOIN
        min_pnc 
    ON 
        min_pnc.infant_id=i.infant_id AND min_pnc.row_number=1
    LEFT JOIN
        max_pnc 
    ON 
        max_pnc.infant_id=i.infant_id AND max_pnc.row_number=1

    
    
-- $END

SELECT TOP 400 *
FROM [derived].[fact_infant_outcome]


