USE [staging_ptracker_test]
GO

DROP TABLE IF exists [derived].[fact_infant_outcome];

-- $BEGIN

    CREATE TABLE [derived].[fact_infant_outcome](
        [infant_outcome_id] [uniqueidentifier] NOT NULL,
        [infant_id] [uniqueidentifier] NULL,
        [pregnancy_id] [uniqueidentifier] NULL,
        [ptracker_identifier] [nvarchar] (255) NULL, 
        [sex] [nvarchar] (255) NULL,
        [openmrs_patient_identifier] [int] NULL,
        [mother_client_id] [uniqueidentifier] NULL,
        [date_created] [date] NULL,
        [date_created_id] [uniqueidentifier] NULL,
        [district] [nvarchar] (255) NULL,
        [region] [nvarchar] (255) NULL,
        [facility_id] [uniqueidentifier] NULL,
        [date_of_birth] [date] NULL,
        [date_of_birth_estimated] [int] NULL,
        [maternity_date] [date] NULL,
        [pnc_date] [date] NULL,
        [maternity_date_id] [uniqueidentifier] NULL,
        [pnc_date_id] [uniqueidentifier] NULL,
        [maternity_facility_id] [uniqueidentifier] NULL,
        [is_in_maternity] [tinyint] NULL,
        [is_in_pnc] [tinyint] NULL,
        [is_in_maternity_and_pnc] [tinyint] NULL,
        [is_mother_hiv_positive] [tinyint] NULL,
        [is_mother_hiv_positive_at_maternity] [tinyint] NULL,
        [is_prophylaxis_received_at_birth] [tinyint] NULL,
        [is_prophylaxis_refused_at_birth] [tinyint] NULL,
        [is_prophylaxis_stocked_out_at_birth] [tinyint] NULL,
        [is_prophylaxis_unknown_at_birth] [tinyint] NULL,
        [is_arv_type_nvp_at_birth] [tinyint] NULL,
        [is_arv_type_nvp_plus_azt_at_birth] [tinyint] NULL,
        [is_unknown_arv_type_at_birth] [tinyint] NULL,
        [is_exclusive_breastfeeding_at_birth] [int] NULL,
        [is_replacement_feeding_at_birth] [int] NULL,
        [is_unknown_feeding_at_birth] [int] NULL,
        [pnc_facility_id] [uniqueidentifier]  NULL,
        [hiv_test_status_within_48_hours] [nvarchar] (255) NULL,
        [hiv_test_status_within_48_hours_date] [date] NULL,
        [hiv_test_status_within_48_hours_date_id] [uniqueidentifier] NULL,
        [is_tested_within_48_hours] [tinyint] NULL,
        [hiv_test_status_at_6_to_8_weeks] [nvarchar] (255) NULL,
        [hiv_test_status_at_6_to_8_weeks_date] [date] NULL,
        [hiv_test_status_at_6_to_8_weeks_date_id] [uniqueidentifier] NULL,
        [is_tested_at_6_to_8_weeks] [tinyint] NULL,
        [hiv_test_status_at_9_weeks_to_12_months] [nvarchar] (255) NULL,
        [hiv_test_status_at_9_weeks_to_12_months_date] [date] NULL,
        [hiv_test_status_at_9_weeks_to_12_months_date_id] [uniqueidentifier] NULL,
        [is_tested_at_9_weeks_to_12_months] [tinyint] NULL,
        [hiv_test_status_over_12_months] [nvarchar] (255) NULL,
        [hiv_test_status_over_12_months_date] [date] NULL,
        [hiv_test_status_over_12_months_date_id] [uniqueidentifier] NULL,
        [is_tested_over_12_months] [tinyint] NULL,
        [is_newly_registered] [nvarchar] (255) NULL,
        [is_newly_registered_testing_period] [nvarchar] (255) NULL,
        [is_newly_registered_order] [tinyint] NULL,
        [is_ever_tested] [tinyint] NULL,
        [is_test_result_missing] [tinyint] NULL,
        [is_art_initiation_missing] [tinyint] NULL,
        [is_arv_prophylaxis_received] [tinyint] NULL,
        [is_ever_tested_by_12_months] [tinyint] NULL,
        [is_ever_hiv_test_result_positive] [tinyint] NULL,
        [is_hiv_exposed] [tinyint] NULL,
        [is_ctx_prophylaxis_received] [tinyint] NULL,
        [final_status] [nvarchar] (255) NULL,
        [is_final_status_present] [tinyint] NULL,
        [is_final_status_still_in_care]  [tinyint] NULL,
        [is_final_status_transfer_in_to_art_clinic] [tinyint] NULL,
        [transfer_in_facility] [nvarchar] (255) NULL,
        [transfer_in_date] [date] NULL,
        [transfer_in_date_id] [uniqueidentifier] NULL,
        [is_final_status_transfered_out] [tinyint] NULL,
        [transfered_out_facility] [nvarchar] (255) NULL,
        [transfered_out_to_date] [date] NULL,
        [transfered_out_date_id] [uniqueidentifier] NULL,
        [death_date] [date] NULL,
        [death_date_id] [uniqueidentifier] NULL,
        [is_final_status_dead]  [tinyint] NULL,
        [is_final_status_confirmed_negative] [tinyint] NULL,
        [is_final_status_lost_to_follow_up] [tinyint] NULL,
        [last_visit_date] [date] NULL,
        [last_visit_date_id] [uniqueidentifier] NULL,
        [last_visit_age_in_weeks] [int] NULL,
        [last_visit_age_group_id] [uniqueidentifier] NULL,
        [_infant_pnc_last_visit_id] [uniqueidentifier] NULL
    );

    ALTER TABLE [derived].[fact_infant_outcome] ADD CONSTRAINT [PK_fact_infant_outcome_derived] PRIMARY KEY ([infant_outcome_id]);

    ALTER TABLE [derived].[fact_infant_outcome] ADD CONSTRAINT FK_fact_infant_outcome_dim_infant_derived FOREIGN KEY ([infant_id]) REFERENCES [derived].[dim_infant]([infant_id]);

    ALTER TABLE [derived].[fact_infant_outcome] ADD CONSTRAINT FK_fact_infant_outcome_dim_pregnancy_derived FOREIGN KEY ([pregnancy_id]) REFERENCES [derived].[dim_pregnancy]([pregnancy_id]);

    ALTER TABLE [derived].[fact_infant_outcome] ADD CONSTRAINT FK_fact_infant_outcome_dim_mother_derived FOREIGN KEY ([mother_client_id]) REFERENCES [derived].[dim_mother]([mother_id]);

    ALTER TABLE [derived].[fact_infant_outcome] ADD CONSTRAINT FK_fact_infant_outcome_dim_date_derived FOREIGN KEY ([date_created_id]) REFERENCES [derived].[dim_date]([date_id]);
    
    ALTER TABLE [derived].[fact_infant_outcome] ADD CONSTRAINT FK_fact_infant_outcome_dim_facility_derived FOREIGN KEY ([facility_id]) REFERENCES [derived].[dim_facility]([facility_id]);

    ALTER TABLE [derived].[fact_infant_outcome] ADD CONSTRAINT FK_fact_infant_outcome_dim_maternity_facility_derived FOREIGN KEY ([maternity_facility_id]) REFERENCES [derived].[dim_maternity_facility]([facility_id]);

    ALTER TABLE [derived].[fact_infant_outcome] ADD CONSTRAINT FK_fact_infant_outcome_dim_infant_pnc_facility_derived FOREIGN KEY ([pnc_facility_id]) REFERENCES [derived].[dim_infant_pnc_facility]([facility_id]);

    ALTER TABLE [derived].[fact_infant_outcome] ADD CONSTRAINT FK_fact_infant_outcome_dim_pnc_date_48_hours_derived FOREIGN KEY ([hiv_test_status_within_48_hours_date_id]) REFERENCES [derived].[dim_pnc_date]([date_id]);

    ALTER TABLE [derived].[fact_infant_outcome] ADD CONSTRAINT FK_fact_infant_outcome_dim_pnc_date_6_weeks_derived FOREIGN KEY ([hiv_test_status_at_6_to_8_weeks_date_id]) REFERENCES [derived].[dim_pnc_date]([date_id]);

    ALTER TABLE [derived].[fact_infant_outcome] ADD CONSTRAINT FK_fact_infant_outcome_dim_pnc_date_9_months_derived FOREIGN KEY ([hiv_test_status_at_9_weeks_to_12_months_date_id]) REFERENCES [derived].[dim_pnc_date]([date_id]);

    ALTER TABLE [derived].[fact_infant_outcome] ADD CONSTRAINT FK_fact_infant_outcome_dim_pnc_date_18_months_derived FOREIGN KEY ([hiv_test_status_over_12_months_date_id]) REFERENCES [derived].[dim_pnc_date]([date_id]);

    ALTER TABLE [derived].[fact_infant_outcome] ADD CONSTRAINT FK_fact_infant_outcome_dim_pnc_date_transfer_in_derived FOREIGN KEY ([transfer_in_date_id]) REFERENCES [derived].[dim_pnc_date]([date_id]);

    ALTER TABLE [derived].[fact_infant_outcome] ADD CONSTRAINT FK_fact_infant_outcome_dim_pnc_date_transfered_out_derived FOREIGN KEY ([transfered_out_date_id]) REFERENCES [derived].[dim_pnc_date]([date_id]);

    ALTER TABLE [derived].[fact_infant_outcome] ADD CONSTRAINT FK_fact_infant_outcome_dim_pnc_date_death_derived FOREIGN KEY ([death_date_id]) REFERENCES [derived].[dim_pnc_date]([date_id]);

    ALTER TABLE [derived].[fact_infant_outcome] ADD CONSTRAINT FK_fact_infant_outcome_dim_mat_date_derived FOREIGN KEY ([maternity_date_id]) REFERENCES [derived].[dim_maternity_date]([date_id]);

    ALTER TABLE [derived].[fact_infant_outcome] ADD CONSTRAINT FK_fact_infant_outcome_dim_pnc_date_last_visit_derived FOREIGN KEY ([last_visit_date_id]) REFERENCES [derived].[dim_pnc_date]([date_id]);

    ALTER TABLE [derived].[fact_infant_outcome] ADD CONSTRAINT FK_fact_infant_outcome_dim_infant_age_group_derived FOREIGN KEY ([last_visit_age_group_id]) REFERENCES [derived].[dim_infant_age_group]([infant_age_group_id]);

    ALTER TABLE [derived].[fact_infant_outcome] ADD CONSTRAINT FK_fact_infant_outcome_dim_pnc_date_derived FOREIGN KEY ([pnc_date_id]) REFERENCES [derived].[dim_pnc_date]([date_id]);
    
-- $END