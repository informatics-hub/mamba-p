USE staging_ptracker_test;
GO

--Count facilities before trimming

    SELECT COUNT(*) count_dim_infant_pnc_facility_before_trim
    FROM derived.dim_infant_pnc_facility;

-- $BEGIN

    DELETE a
    FROM derived.dim_infant_pnc_facility a
    WHERE 
        NOT EXISTS
            (
                SELECT 1
                FROM derived.fact_infant_outcome b
                WHERE a.facility_id=b.pnc_facility_id
            )


-- $END

--Count facilities after trimming

    SELECT COUNT(*) count_dim_infant_pnc_facility_after_trim
    FROM derived.dim_infant_pnc_facility;

