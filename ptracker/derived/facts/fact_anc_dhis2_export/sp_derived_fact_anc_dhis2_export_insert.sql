USE [staging_ptracker_test]
GO

TRUNCATE TABLE [derived].[fact_anc_dhis2_export]

-- $BEGIN

    INSERT INTO [derived].[fact_anc_dhis2_export](
        [data_element],
        [period],
        [org_unit_uid], 
        [COCUID],
        [AOCUID],
        [datavalue]
    )
    
    SELECT
        dhis2_identifier,
        CAST(FORMAT(indicator_date,'yyyy') AS VARCHAR) + CAST(FORMAT(indicator_date,'MM') AS VARCHAR) AS [period],
        af.dhis_facility_orgunit_id,
        '',
        '',
        fi.reported_value
    FROM 
        [derived].[fact_facility_indicator] fi
    INNER JOIN
        [derived].[dim_indicator] i
    ON 
        fi.indicator_id = i.indicator_id
    INNER JOIN
        derived.dim_anc_facility af
    ON
        fi.facility_id = af.facility_id
    WHERE 
        i.report_type = 'ANC Monthly Report'
    AND 
        i.dhis2_identifier IS NOT NULL
    AND
        dhis_facility_orgunit_id IS NOT NULL

-- $END;
SELECT TOP 400 *
FROM [derived].[fact_anc_dhis2_export]