USE [staging_ptracker_test]
GO
-- $BEGIN

    EXEC sp_derived_fact_anc_dhis2_export_create;
    EXEC sp_derived_fact_anc_dhis2_export_insert;

-- $END