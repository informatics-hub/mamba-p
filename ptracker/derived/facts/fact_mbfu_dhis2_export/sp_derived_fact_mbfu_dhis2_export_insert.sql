USE [staging_ptracker_test]
GO

TRUNCATE TABLE [derived].[fact_mbfu_dhis2_export]

-- $BEGIN

    INSERT INTO [derived].[fact_mbfu_dhis2_export](
        [data_element],
        [period],
        [org_unit_uid], 
        [COCUID],
        [AOCUID],
        [datavalue]
    )
    
    SELECT
        dhis2_identifier,
        CAST(FORMAT(indicator_date,'yyyy') AS VARCHAR) + CAST(FORMAT(indicator_date,'MM') AS VARCHAR) AS [period],
        pf.dhis_facility_orgunit_id,
        '',
        '',
        fi.reported_value
    FROM 
        [derived].[fact_facility_indicator] fi
    INNER JOIN
        [derived].[dim_indicator] i
    ON 
        fi.indicator_id = i.indicator_id
    INNER JOIN
        derived.dim_infant_pnc_facility pf
    ON
        fi.facility_id = pf.facility_id
    WHERE 
        i.report_type = 'MBFU Monthly Report'
    AND
        dhis_facility_orgunit_id IS NOT NULL

-- $END;
SELECT TOP 400 *
FROM [derived].[fact_mbfu_dhis2_export]