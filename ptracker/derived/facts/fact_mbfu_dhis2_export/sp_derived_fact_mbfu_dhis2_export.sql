USE [staging_ptracker_test]
GO
-- $BEGIN

    EXEC sp_derived_fact_mbfu_dhis2_export_create;
    EXEC sp_derived_fact_mbfu_dhis2_export_insert;

-- $END