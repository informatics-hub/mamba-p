USE [ptracker_db_analysis]
GO

SELECT
    given_name, 
    middle_name, 
    family_name,
    dc.gender,
    dc.date_of_birth,
    pt_num.identifier as ptracker_id,
    bc.art_number,
    'Namibia' as 'country',
    [address],
    [value] AS 'contact_phone_number',
    dc.region,
    encounter_date AS last_visit_date,
    facility_name AS last_visit_facility,
    encounter_type AS last_visit_type
FROM 
(
    SELECT
        mother_id AS client_id,
        'Female' AS gender,
        openmrs_patient_identifier,
        date_of_birth,
        region
    FROM derived.dim_mother 
    UNION
    SELECT
        infant_id AS client_id,
        sex AS gender,
        openmrs_patient_identifier,
        date_of_birth,
        region
    FROM derived.dim_infant 
) dc
LEFT JOIN
    base.dim_client bc
ON
    dc.client_id = bc.client_id

LEFT JOIN
(
    SELECT
        *
    FROM
    (
        SELECT 
            encounter_date,
            facility_id,
            client_id,
            encounter_type,
            ROW_NUMBER() OVER (PARTITION BY client_id ORDER BY encounter_date DESC) AS row_number
        FROM
            base.dim_encounter e
    )a
    WHERE row_number = 1       
) AS e
ON
    e.client_id=dc.client_id
LEFT JOIN
    base.dim_facility f
ON
    e.facility_id=f.facility_id
INNER JOIN 
    lake_ptracker.dbo.person p 
ON 
    dc.openmrs_patient_identifier = p.person_id
INNER JOIN
    lake_ptracker.dbo.person_name pn
ON
    pn.person_id=p.person_id and pn.voided=0
LEFT JOIN 
(
    SELECT
        *
    FROM
    (
        SELECT 
            patient_id, 
            identifier,
            ROW_NUMBER() OVER (PARTITION BY patient_id ORDER BY date_created DESC) AS row_number
        FROM
            lake_ptracker.dbo.patient_identifier
        WHERE
            identifier_type = 5 AND voided = 0
    )a
    WHERE row_number = 1        
)pt_num
ON 
    pt_num.patient_id = dc.openmrs_patient_identifier
LEFT JOIN 
(
    SELECT
        *
    FROM
    (
        SELECT 
            person_id, 
            [value],
            ROW_NUMBER() OVER (PARTITION BY person_id ORDER BY date_created DESC) AS row_number
        FROM
            lake_ptracker.dbo.person_attribute
        WHERE
            person_attribute_type_id=8 and voided=0
    )a
    WHERE row_number = 1        
)phone
ON 
    p.person_id = phone.person_id