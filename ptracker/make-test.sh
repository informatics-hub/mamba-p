base_contents=$(cat base/make-test.sh)
eval "${base_contents}"

derived_contents=$(cat derived/make-test.sh)
eval "${derived_contents}"

mkdir -p build
cat base/build/create_stored_procedures.sql derived/build/create_stored_procedures.sql > build/create_stored_procedures.sql

cat <<EOF >build/execute_stored_procedures.sql
EXEC staging_ptracker_test.base.sp_data_processing
go
EXEC staging_ptracker_test.derived.sp_data_processing
go
EOF