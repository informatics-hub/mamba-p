USE [ptracker_db_analysis_test]
GO
-- $BEGIN

    EXEC sp_mask_pii_create;
    EXEC sp_mask_pii_insert;
    EXEC sp_mask_pii_update;

-- $END