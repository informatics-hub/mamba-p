USE [lake_ptracker]
GO

-- $BEGIN

    DECLARE @table_name VARCHAR (255)
    DECLARE @column_name VARCHAR (255)
    DECLARE @where VARCHAR (255)
    DECLARE @query VARCHAR (255)

    DECLARE @totalrows INT =    (
                                    SELECT count(*) 
                                    FROM [dbo].[masking_object]
                                )
    DECLARE @currentrow INT = 1

    WHILE @currentrow <=  @totalrows  
    BEGIN 
        SET @table_name =   (
                                SELECT table_name 
                                FROM [dbo].[masking_object] 
                                WHERE mask_id = @currentrow
                            )
        SET @column_name =  (
                                SELECT column_name 
                                FROM [dbo].[masking_object] 
                                WHERE mask_id = @currentrow
                            )
        SET @where =        (
                                SELECT where_condition 
                                FROM [dbo].[masking_object] 
                                WHERE mask_id = @currentrow
                            )

        BEGIN
            IF @where IS NULL 
                SET @query = 'UPDATE ' + @table_name + ' SET ' + @column_name + ' = '''''
            ELSE
                SET @query = 'UPDATE ' + @table_name + ' SET ' + @column_name + ' = '''' WHERE ' + @where
        END

        PRINT @query
        EXECUTE (@query)

        SET @currentrow = @currentrow + 1
    END  

-- $END

