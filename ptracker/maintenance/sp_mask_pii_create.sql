USE [lake_ptracker]
GO

DROP TABLE IF EXISTS [dbo].[masking_object];

-- $BEGIN

    CREATE TABLE [dbo].[masking_object](
        [mask_id] INT IDENTITY(1,1) NOT NULL,
        [table_name] VARCHAR(255) NOT NULL,
        [column_name] VARCHAR(255) NOT NULL,
        [where_condition] VARCHAR(255) NULL
    );

    ALTER TABLE [dbo].[masking_object] ADD CONSTRAINT PK_masking_object_dbo PRIMARY KEY ([mask_id]);

    
-- $END

