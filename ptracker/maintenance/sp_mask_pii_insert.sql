USE [lake_ptracker]
GO

TRUNCATE TABLE [dbo].[masking_object];

-- $BEGIN

    INSERT INTO [dbo].[masking_object](
        [table_name],
        [column_name],
        [where_condition]
    ) 
    VALUES
    (
        'dbo.person_name',
        'given_name',
        NULL
    ),
    (
        'dbo.person_name',
        'middle_name',
        NULL
    ),
    (
        'dbo.person_name',
        'family_name',
        NULL
    ),
    (
        'dbo.person_name',
        'family_name2',
        NULL
    ),
    (
        'dbo.person_attribute',
        'value',
        'person_attribute_type_id IN (4,5,8,13,14)'
    )

    ;

-- $END

