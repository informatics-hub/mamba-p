USE [staging_ptracker_test]
GO

TRUNCATE TABLE [base].[dim_date]

-- $BEGIN
    
    SET NOCOUNT ON;

    SET DATEFORMAT dmy    

    DECLARE @RowNum INT = 1
	DECLARE @LastDayOfMon CHAR(1);

    DECLARE @BeginDate DATE;    
	DECLARE @EndDate DATE;  

    SET @BeginDate = '01/01/1900';
    SET @EndDate = '31/12/2030';

    DECLARE @DateCounter DATE; 

	SET @DateCounter = @BeginDate;

	WHILE @DateCounter <= @EndDate

    BEGIN
        
        IF EOMONTH(@DateCounter) = CAST(@DateCounter AS DATE)
            begin
            SET @LastDayOfMon = 'Y';
            end
        ELSE
            begin
            SET @LastDayOfMon = 'N';
            end

        INSERT INTO [base].[dim_date](
            [date_id],
            [date_yyyymmdd],
            [date_slash_yyyymd],
            [date_slash_mdyyyy],
            [date_slash_dmyyyy],
            [day_number_of_week],
            [day_name_of_week],
            [day_of_month],
            [day_of_year],
            [weekday_weekend],
            [week_number_of_year],
            [month_name],
            [month_number_of_year],
            [year],
            [last_day_of_month],
            [datim_annual_period],
            [datim_semiannual_period],
            [datim_quarter_period],
            [quarter_period],
            [semiannual_period],
            [pepfar_annual_period],
            [pepfar_quarter_period] 
        )
        VALUES(
            NEWID(),
            @DateCounter,
            FORMAT (@DateCounter, 'yyyy/M/d'),
            FORMAT (@DateCounter, 'M/d/yyyy'),
            FORMAT (@DateCounter, 'd/M/yyyy'),
            DATEPART(DW,@DateCounter),
            DATENAME(WEEKDAY, @DateCounter),
            DATENAME(DAY, @DateCounter),
            DATENAME(DAYOFYEAR, @DateCounter),
            CASE DATENAME(WEEKDAY, @DateCounter)
                WHEN 'Saturday' THEN 'Weekend'
                WHEN 'Sunday' THEN 'Weekend'
                ELSE 'Weekday'
            END,
            DATEPART(WEEK, @DateCounter),
            DATENAME(MONTH, @DateCounter),
            DATEPART(MONTH, @DateCounter),
            DATEPART(YEAR, @DateCounter),
            CASE 
                WHEN EOMONTH(@DateCounter) = @DateCounter 
                THEN 'Yes' 
                ELSE 'No' 
            END,
            CASE 
                WHEN 
                    DATEPART(MONTH, @DateCounter) IN (1, 2, 3) 
                THEN 
                    'FY' + CAST((DATEPART(YY, @DateCounter) - 1) AS VARCHAR(4))
                ELSE
                    'FY' + CAST(DATEPART(YY, @DateCounter) AS VARCHAR(4))
            END,
            CASE 
                WHEN 
                    DATEPART(MONTH, @DateCounter) IN (4, 5, 6, 7, 8, 9) 
                THEN 
                    CAST(DATEPART(YY, @DateCounter) AS VARCHAR(4)) + '-SA1' 
                WHEN 
                    DATEPART(MONTH, @DateCounter) IN (10, 11, 12) 
                THEN 
                    CAST(DATEPART(YY, @DateCounter) AS VARCHAR(4)) + '-SA2' 
                WHEN 
                    DATEPART(MONTH, @DateCounter) IN (1, 2, 3) 
                THEN 
                    CAST((DATEPART(YY, @DateCounter) - 1) AS VARCHAR(4)) + '-SA2' 
                
            END,
            CASE 
                WHEN 
                    DATEPART(MONTH, @DateCounter) IN (4, 5, 6) 
                THEN 
                    CAST(DATEPART(YY, @DateCounter) AS VARCHAR(4)) + '-Q1' 
                WHEN 
                    DATEPART(MONTH, @DateCounter) IN (7, 8, 9) 
                THEN 
                    CAST(DATEPART(YY, @DateCounter) AS VARCHAR(4)) + '-Q2' 
                WHEN 
                    DATEPART(MONTH, @DateCounter) IN (10, 11, 12) 
                THEN 
                    CAST(DATEPART(YY, @DateCounter) AS VARCHAR(4)) + '-Q3' 
                WHEN 
                    DATEPART(MONTH, @DateCounter) IN (1, 2, 3) 
                THEN 
                    CAST((DATEPART(YY, @DateCounter) - 1) AS VARCHAR(4)) + '-Q4' 
            END,
            CASE 
                WHEN 
                    DATEPART(MONTH, @DateCounter) IN (1, 2, 3) 
                THEN 
                    CAST(DATEPART(YY, @DateCounter) AS VARCHAR(4)) + '-Q1' 
                WHEN 
                    DATEPART(MONTH, @DateCounter) IN (4, 5, 6) 
                THEN 
                    CAST(DATEPART(YY, @DateCounter) AS VARCHAR(4)) + '-Q2' 
                WHEN 
                    DATEPART(MONTH, @DateCounter) IN (7, 8, 9) 
                THEN 
                    CAST(DATEPART(YY, @DateCounter) AS VARCHAR(4)) + '-Q3' 
                WHEN 
                    DATEPART(MONTH, @DateCounter) IN (10, 11, 12) 
                THEN 
                    CAST((DATEPART(YY, @DateCounter)) AS VARCHAR(4)) + '-Q4' 
            END,
            CASE 
                WHEN 
                    DATEPART(MONTH, @DateCounter) IN (1, 2, 3, 4, 5, 6) 
                THEN 
                    CAST(DATEPART(YY, @DateCounter) AS VARCHAR(4)) + '-SA1' 
                WHEN 
                    DATEPART(MONTH, @DateCounter) IN (7, 8, 9, 10, 11, 12) 
                THEN 
                    CAST(DATEPART(YY, @DateCounter) AS VARCHAR(4)) + '-SA2' 
            END,
            CASE 
                WHEN 
                    DATEPART(MONTH, @DateCounter) IN (10, 11, 12) 
                THEN 
                    'FY' + CAST((DATEPART(YY, @DateCounter)+ 1) AS VARCHAR(4))
                ELSE
                    'FY' + CAST(DATEPART(YY, @DateCounter) AS VARCHAR(4))
            END,
            CASE 
                WHEN 
                    DATEPART(MONTH, @DateCounter) IN (10, 11, 12) 
                THEN 
                    CAST((DATEPART(YY, @DateCounter) + 1) AS VARCHAR(4)) + 'Q1' 
                WHEN 
                    DATEPART(MONTH, @DateCounter) IN (1, 2, 3) 
                THEN 
                    CAST(DATEPART(YY, @DateCounter) AS VARCHAR(4)) + 'Q2' 
                WHEN 
                    DATEPART(MONTH, @DateCounter) IN (4, 5, 6) 
                THEN 
                    CAST(DATEPART(YY, @DateCounter) AS VARCHAR(4)) + 'Q3' 
                WHEN 
                    DATEPART(MONTH, @DateCounter) IN (7, 8, 9) 
                THEN 
                    CAST(DATEPART(YY, @DateCounter) AS VARCHAR(4)) + 'Q4' 
            END
        );

        SET @RowNum = @RowNum + 1
            
        SET @DateCounter = DATEADD(DAY, 1, @DateCounter);
    END
-- $END

SELECT 
    COUNT(*) as total_dates
FROM 
    [base].[dim_date]


SELECT 
    TOP 400 * 
FROM 
    [base].[dim_date]


