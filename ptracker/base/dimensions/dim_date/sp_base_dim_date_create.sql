USE [staging_ptracker_test]
GO

DROP TABLE IF exists [base].[dim_date];

-- $BEGIN

    CREATE TABLE [base].[dim_date](
        [date_id] UNIQUEIDENTIFIER NOT NULL,
        [date_yyyymmdd] [date] NULL,
        [date_slash_yyyymd] [nvarchar] (255) NOT NULL,
        [date_slash_mdyyyy] [nvarchar] (255) NOT NULL,
        [date_slash_dmyyyy] [nvarchar] (255) NOT NULL,
        [day_number_of_week] [tinyint] NOT NULL,
        [day_name_of_week] [nvarchar] (255) NOT NULL,
        [day_of_month] [tinyint] NOT NULL,
        [day_of_year] [smallint] NOT NULL,
        [weekday_weekend] [nvarchar] (255) NOT NULL,
        [week_number_of_year] [tinyint] NOT NULL,
        [month_name] [nvarchar] (255) NOT NULL,
        [month_number_of_year] [tinyint] NOT NULL,
        [year] [int] NOT NULL,
        [last_day_of_month] [nvarchar] (255) NOT NULL,
        [datim_annual_period] [nvarchar] (255) NOT NULL,
        [datim_semiannual_period] [nvarchar] (255) NOT NULL,
        [datim_quarter_period] [nvarchar] (255) NOT NULL,
        [quarter_period] [nvarchar] (255) NOT NULL,
        [semiannual_period] [nvarchar] (255) NOT NULL,
        [pepfar_annual_period] [nvarchar](255) NULL,
        [pepfar_quarter_period] [nvarchar] (255) NULL
    );

    ALTER TABLE [base].[dim_date] ADD CONSTRAINT [PK_dim_date_base] PRIMARY KEY ([date_id]);

-- $END