USE [staging_ptracker_test]
GO

DROP TABLE IF EXISTS [base].[dim_indicator];

-- $BEGIN

    CREATE TABLE [base].[dim_indicator](
        [indicator_id] UNIQUEIDENTIFIER NOT NULL,
        [indicator_identifier] [nvarchar](255) NULL,
        [indicator_name] [nvarchar](255) NULL,
        [report_type] [nvarchar](255) NULL,
        [dhis2_identifier] [nvarchar](255) NULL,
        [code] [nvarchar](255) NULL,
        [description] [text] NULL
    );

    ALTER TABLE [base].[dim_indicator] ADD CONSTRAINT PK_dim_indicator_base PRIMARY KEY (indicator_id);

-- $END