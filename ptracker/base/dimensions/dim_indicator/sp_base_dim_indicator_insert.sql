USE [staging_ptracker_test]
GO

TRUNCATE TABLE [base].[dim_indicator];

-- $BEGIN

    INSERT INTO [base].[dim_indicator]
    (
        [indicator_id],
        [indicator_identifier],
        [indicator_name],
        [report_type],
        [dhis2_identifier],
        [code],
        [description]
    )

    SELECT 
        NEWID(),
        [data_id],
        [data_name],
        [data_type],
        [dhis2_identifier],
        [data_code],
        [data_description]
    FROM 
        staging_ptracker_external_sources.dbo.indicators

    ;

-- $END


SELECT 
    TOP 400 * 
FROM 
    [base].[dim_indicator]
