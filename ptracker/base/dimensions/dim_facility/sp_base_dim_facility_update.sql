USE [staging_ptracker_test]
GO

-- $BEGIN
    DELETE FROM 
        [base].[dim_facility]
    WHERE
        openmrs_identifier IN ('gDDck3ziQN5','AOfv8v0OQEo')
    ;
-- $END

SELECT 
    TOP 400 * 
FROM 
    [base].[dim_facility]
