USE [staging_ptracker_test]
GO

TRUNCATE TABLE [base].[dim_facility];

-- $BEGIN

    INSERT INTO [base].[dim_facility]
    (
        [facility_id],
        [openmrs_location_identifier],
        [facility_name],
        [district_name],
        [region_name],
        [latitude],
        [longitude],
        [country],
        [mfl_code],
        [openmrs_identifier],
        [dhis_facility_orgunit_id],
        [dhis_maternity_orgunit_id]
    )

    SELECT 
        NEWID(),
        f.openmrs_identifier,
        f.[Facility Name],
        f.[District Name],
        f.[Region Name],
        ef.Latitude,
        ef.Longitude,
        'Namibia' AS country,
        f.[Mfl code],
        lu.ptracker_uuid,
        f.[DHIS Facility Org Unit ID],
        lu.orgunit_uid_maternity
    FROM 
        [staging_ptracker_external_sources].[dbo].[revised_facility_list] f
    INNER JOIN
        staging_ptracker_external_sources.dbo.facilities ef
    ON
        ef.Facility_Code = f.[MFL Code]
    LEFT JOIN
        [lake_ptracker].[dbo].[location_uids] lu
    ON
        f.[DHIS Facility Org Unit ID] = lu.orgunit_uid_facility

-- $END
SELECT 
    COUNT(*) as total_facilities
FROM 
    [base].[dim_facility]

SELECT 
    COUNT(*) AS total,
    [openmrs_location_identifier]
FROM
    [base].[dim_facility]
GROUP BY 
    [openmrs_location_identifier]
HAVING COUNT(*) > 1

SELECT 
    TOP 400 * 
FROM 
    [base].[dim_facility]
