USE [staging_ptracker_test]
GO

-- $BEGIN

    EXEC sp_base_dim_facility_create;
    EXEC sp_base_dim_facility_insert;
    EXEC sp_base_dim_facility_update;

-- $END