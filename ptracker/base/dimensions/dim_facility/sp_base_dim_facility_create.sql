USE [staging_ptracker_test]
GO

DROP TABLE IF EXISTS [base].[dim_facility];

-- $BEGIN

    CREATE TABLE [base].[dim_facility](
        [facility_id] UNIQUEIDENTIFIER NOT NULL,
        [openmrs_location_identifier] [int] NULL,
        [facility_name] [nvarchar](255) NOT NULL,
        [district_name] [nvarchar](255) NULL,
        [region_name] [nvarchar](255) NULL,
        [latitude] [float] NULL,
        [longitude] [float] NULL,
        [country] [nvarchar](255) NULL,
        [mfl_code] [int] NULL,
        [openmrs_identifier] [nvarchar](255) NULL,
        [dhis_facility_orgunit_id] [nvarchar] (255) NULL,
        [dhis_maternity_orgunit_id] [nvarchar] (255) NULL
    );

    ALTER TABLE [base].[dim_facility] ADD CONSTRAINT PK_dim_facility_base PRIMARY KEY (facility_id);

-- $END