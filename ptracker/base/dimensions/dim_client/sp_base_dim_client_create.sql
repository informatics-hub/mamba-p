USE [staging_ptracker_test]
GO

DROP TABLE IF exists [base].[dim_client];

-- $BEGIN

    CREATE TABLE [base].[dim_client](
        [client_id] [UNIQUEIDENTIFIER] NOT NULL,
        [openmrs_patient_identifier] [int] NOT NULL,
        [mother_client_id] [UNIQUEIDENTIFIER] NULL,
        [address] [nvarchar] (255) NULL,
        [first_name] [nvarchar] (255) NULL,
        [last_name] [nvarchar] (255) NULL,
        [phone] [nvarchar] (255) NULL,
        [sex] [nvarchar] (255) NOT NULL,
        [date_of_birth] [date] NULL,
        [date_of_birth_estimated] [tinyint] NULL,
        [patient_date_created] [date] NOT NULL,
        [patient_voided] [tinyint] NOT NULL,
        [district] [nvarchar] (255) NULL,
        [location] [nvarchar] (255) NULL,
        [region] [nvarchar] (255) NULL,
        [facility_id] [UNIQUEIDENTIFIER] NULL,
        [art_number] [nvarchar] (255) NULL,
        [has_multiple_art_numbers] [tinyint] NOT NULL,
    );

    ALTER TABLE [base].[dim_client] ADD CONSTRAINT [PK_dim_client_base] PRIMARY KEY ([client_id]);

    ALTER TABLE [base].[dim_client] ADD CONSTRAINT FK_dim_client_facility_base FOREIGN KEY ([facility_id]) REFERENCES [base].[dim_facility]([facility_id]);

-- $END