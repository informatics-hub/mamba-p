USE [staging_ptracker_test]
GO
-- $BEGIN

    EXEC sp_base_dim_client_create;
    EXEC sp_base_dim_client_insert;
    EXEC sp_base_dim_client_update;

-- $END