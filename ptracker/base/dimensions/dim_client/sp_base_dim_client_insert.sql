USE [staging_ptracker_test]
GO

TRUNCATE TABLE [base].[dim_client]

-- $BEGIN

    INSERT INTO [base].[dim_client](
        [client_id],
        [openmrs_patient_identifier],
        [address],
        [first_name],
        [last_name],
        [phone],
        [sex],
        [date_of_birth],
        [date_of_birth_estimated],
        [patient_date_created],
        [patient_voided],
        [district],
        [location],
        [region],
        [facility_id],
        [art_number],
        [has_multiple_art_numbers]
    )
    
    SELECT
        NEWID(),
        pt.patient_id,
        CASE WHEN 
            pad.address1 IS NOT NULL AND  pad.address2 IS NOT NULL
        THEN 
            CONCAT(pad.address1, ', ', pad.address2)
        WHEN 
            pad.address1 IS NOT NULL AND  pad.address2 IS NULL
        THEN
            pad.address1 
        WHEN 
            pad.address1 IS NULL AND  pad.address2 IS NOT NULL
        THEN
            pad.address2
        WHEN 
            pad.address1 IS NULL AND  pad.address2 IS NULL
        THEN
            NULL
        END AS [address],
        given_name,
        family_name,
        p_phone.value,
        p.gender,
        p.birthdate,
        p.birthdate_estimated,
        pt.date_created AS patient_date_created,
        p.voided AS patient_voided,
        pad.county_district,
        pad.address1,
        f.region_name,
        CASE WHEN 
            f.facility_name = 'Omatjette Clinic'
        THEN 
            fo.facility_id
        ELSE
            f.facility_id
        END,
        CASE WHEN
            multiple_art_num.patient_id IS NULL
        THEN
            art_num.identifier
        ELSE 
            NULL
        END,
        CASE WHEN
            multiple_art_num.patient_id IS NOT NULL
        THEN
            1
        ELSE
            0
        END
    FROM
        lake_ptracker.dbo.patient pt
    INNER JOIN 
        lake_ptracker.dbo.person p 
    ON 
        pt.patient_id = p.person_id AND pt.voided=0
    INNER JOIN
        lake_ptracker.dbo.person_name pn
    ON
        p.person_id = pn.person_id AND pn.voided=0
    LEFT JOIN
        lake_ptracker.dbo.person_attribute p_phone
    ON
        p.person_id = p_phone.person_id AND p_phone.voided=0 AND person_attribute_type_id=8
    LEFT JOIN 
    (
        SELECT
            *
        FROM
        (
            SELECT 
                patient_id, 
                identifier,
                ROW_NUMBER() OVER (PARTITION BY patient_id ORDER BY date_created DESC) AS row_number
            FROM
                lake_ptracker.dbo.patient_identifier
            WHERE
                identifier_type = 4 AND voided = 0
        )a
        WHERE row_number = 1        
    )art_num
    ON 
        art_num.patient_id = pt.patient_id
    LEFT JOIN 
    (
        SELECT 
            patient_id, 
            COUNT(identifier) AS ct
        FROM
            (
                SELECT DISTINCT 
                    patient_id, identifier
                FROM 
                    lake_ptracker.dbo.patient_identifier
                WHERE
                    identifier_type = 4 AND voided = 0
            ) a        
        GROUP BY 
            patient_id
        HAVING 
            COUNT(identifier) > 1   
    )multiple_art_num
    ON 
        multiple_art_num.patient_id = pt.patient_id
    LEFT JOIN 
        lake_ptracker.dbo.patient_identifier pi_location 
    ON 
        pt.patient_id = pi_location.patient_id AND pi_location.preferred=1 AND pi_location.voided=0
    LEFT JOIN
        base.dim_facility f
    ON
        f.openmrs_location_identifier = pi_location.location_id
    LEFT JOIN 
        lake_ptracker.dbo.person_address pad 
    ON 
        pt.patient_id = pad.person_id AND pad.voided=0 AND pad.preferred=1
    CROSS JOIN 
    (
        SELECT *
        FROM base.dim_facility
        WHERE facility_name = 'Omatjete Clinic'
    ) fo
    
-- $END

SELECT 
    COUNT(*) as total_clients
FROM 
    [base].[dim_client]

 -- Test that we don't have duplicate openmrs patient ids
SELECT 
    COUNT(*) AS total,
    [openmrs_patient_identifier]
FROM
    [base].[dim_client]
GROUP BY 
    [openmrs_patient_identifier]
HAVING COUNT(*) > 1

SELECT 
    TOP 400 * 
FROM 
    [base].[dim_client]