USE [staging_ptracker_test]
GO

-- $BEGIN
    UPDATE cl
    SET 
        cl.mother_client_id = 
            (
                SELECT 
                    client_id
                FROM 
                    [base].[dim_client] cl
                WHERE
                    cl.openmrs_patient_identifier = r.person_a

            )
    FROM 
        [base].[dim_client] cl
    LEFT JOIN 
        lake_ptracker.dbo.relationship r 
    ON 
        cl.openmrs_patient_identifier = r.person_b 
    WHERE 
        r.voided=0
    ;
    
-- $END

SELECT 
    TOP 400 * 
FROM 
    [base].[dim_client]