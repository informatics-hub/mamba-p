USE [staging_ptracker_test]
GO

DROP TABLE IF EXISTS [base].[dim_encounter];

-- $BEGIN

    CREATE TABLE [base].[dim_encounter]
    (
        [encounter_id] [UNIQUEIDENTIFIER] NOT NULL,
        [openmrs_encounter_identifier] [int] NOT NULL,
        [encounter_type] [nvarchar](255) NOT NULL,
        [creator] [nvarchar](255) NOT NULL,
        [provider] [nvarchar](255) NOT NULL,
        [encounter_date] [date] NULL,
        [encounter_date_id] [UNIQUEIDENTIFIER] NULL,
        [date_created] [date] NOT NULL,
        [date_created_id] [UNIQUEIDENTIFIER] NOT NULL,
        [facility_id] [UNIQUEIDENTIFIER] NULL,
        [client_id] [UNIQUEIDENTIFIER] NULL
    );

    ALTER TABLE [base].[dim_encounter] ADD CONSTRAINT PK_dim_encounter_base PRIMARY KEY (encounter_id);

    ALTER TABLE [base].[dim_encounter] ADD CONSTRAINT FK_dim_encounter_dim_date_encounter_base FOREIGN KEY ([encounter_date_id]) REFERENCES [base].[dim_date]([date_id]);

    ALTER TABLE [base].[dim_encounter] ADD CONSTRAINT FK_dim_encounter_dim_date_created_base FOREIGN KEY ([date_created_id]) REFERENCES [base].[dim_date]([date_id]);

    ALTER TABLE [base].[dim_encounter] ADD CONSTRAINT FK_dim_encounter_dim_facility_base FOREIGN KEY ([facility_id]) REFERENCES [base].[dim_facility]([facility_id]);

    ALTER TABLE [base].[dim_encounter] ADD CONSTRAINT FK_dim_encounter_dim_client_base FOREIGN KEY ([client_id]) REFERENCES [base].[dim_client]([client_id]);


-- $END