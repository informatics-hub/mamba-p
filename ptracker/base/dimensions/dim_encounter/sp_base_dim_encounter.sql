USE [staging_ptracker_test]
GO

-- $BEGIN

    EXEC sp_base_dim_encounter_create;
    EXEC sp_base_dim_encounter_insert;

-- $END