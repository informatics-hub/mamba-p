USE [staging_ptracker_test]
GO

TRUNCATE TABLE [base].[dim_encounter]

-- $BEGIN

    INSERT INTO [base].[dim_encounter] 
    (
        [encounter_id],
        [openmrs_encounter_identifier],
        [encounter_type],
        [creator],
        [provider],
        [encounter_date],
        [encounter_date_id],
        [date_created],
        [date_created_id],
        [facility_id],
        [client_id]
    )

    SELECT
        NEWID(),
        e.encounter_id,
        et.name,
        CONCAT(p.given_name, ' ' ,p.family_name) AS [creator],
        CONCAT(epr_name.given_name, ' ' ,epr_name.family_name) AS [provider],
        CAST(e.encounter_datetime AS DATE),
        de.date_id,
        CAST(e.date_created AS DATE),
        dc.date_id,
        f.facility_id,
        cl.client_id
    FROM 
       [lake_ptracker].[dbo].[encounter] e
    LEFT JOIN 
        [lake_ptracker].[dbo].[encounter_type] et 
    ON 
        et.encounter_type_id = e.encounter_type
    LEFT JOIN
        [lake_ptracker].[dbo].[users] u
    ON
        u.user_id = e.creator
    LEFT JOIN
        [lake_ptracker].[dbo].[person_name] p
    ON
        p.person_id = u.person_id AND p.voided=0
    LEFT JOIN
        [lake_ptracker].[dbo].[encounter_provider] epr
    ON
        epr.encounter_id = e.encounter_id AND epr.voided=0
    LEFT JOIN
        [lake_ptracker].[dbo].[provider] pr
    ON
        pr.provider_id = epr.provider_id
    LEFT JOIN
        [lake_ptracker].[dbo].[person_name] epr_name
    ON
        epr_name.person_id = pr.person_id AND epr_name.voided=0
    LEFT JOIN 
        base.dim_date dc 
    ON 
        dc.date_yyyymmdd = CAST(e.date_created AS DATE)
    LEFT JOIN 
        base.dim_date de 
    ON 
        de.date_yyyymmdd = CAST(e.encounter_datetime AS DATE) 
    INNER JOIN 
        base.dim_client cl 
    ON 
        e.patient_id = cl.openmrs_patient_identifier 
    LEFT JOIN 
        base.dim_facility f 
    ON 
        e.location_id = f.openmrs_location_identifier
    WHERE e.voided = 0



-- $END
SELECT 
    COUNT(*) as total_encounters
FROM 
    [base].[dim_encounter]

SELECT 
    TOP 400 * 
FROM 
    [base].[dim_encounter]