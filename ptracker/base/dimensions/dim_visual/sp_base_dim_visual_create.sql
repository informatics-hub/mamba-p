USE [staging_ptracker_test]
GO

DROP TABLE IF EXISTS [base].[dim_visual];

-- $BEGIN

    CREATE TABLE [base].[dim_visual](
        [visual_id] UNIQUEIDENTIFIER NOT NULL, 
        [visual_name] [nvarchar](255) NULL,
        [path] [nvarchar](255) NULL
    );

    ALTER TABLE [base].[dim_visual] ADD CONSTRAINT PK_dim_visual_base PRIMARY KEY (visual_id);

-- $END