USE [staging_ptracker_test]
GO

TRUNCATE TABLE [base].[dim_visual];

-- $BEGIN

    INSERT INTO [base].[dim_visual]
    (
        [visual_id],
        [visual_name],
        [path]
    )

    SELECT 
        NEWID(),
        [name],
        [path]
    FROM 
        [ReportServer].[dbo].[Catalog]
    WHERE 
        [name] LIKE '%PTracker%'
    AND
        [path] NOT LIKE '%Staging%'
    AND
		[path] !='/PTracker'
    ;

-- $END


SELECT 
    TOP 400 * 
FROM 
    [base].[dim_visual]
