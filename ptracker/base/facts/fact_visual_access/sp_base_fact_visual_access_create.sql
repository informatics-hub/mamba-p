USE [staging_ptracker_test]
GO

DROP TABLE IF exists [base].[fact_visual_access];

-- $BEGIN

    CREATE TABLE [base].[fact_visual_access](
        [visual_access_id] [UNIQUEIDENTIFIER] NOT NULL,
        [username] [nvarchar](255) NULL,
        [visual] [nvarchar](255) NULL,
        [access_date] [date] NULL,
        [status] [nvarchar](50) NULL
    );

    ALTER TABLE [base].[fact_visual_access] ADD CONSTRAINT [PK_fact_visual_access_base] PRIMARY KEY ([visual_access_id]);

-- $END