USE [staging_ptracker_test]
GO

TRUNCATE TABLE [base].[fact_visual_access]

-- $BEGIN
    INSERT INTO [base].[fact_visual_access](
        [visual_access_id],
        [username],
        [visual],
        [access_date],
        [status]
    )

    SELECT
        NEWID(),
        [username],
        [visual_name],
        access_date,
        [Status]
    FROM
    (
        
        SELECT DISTINCT
            RIGHT(UserName,LEN(UserName)-CHARINDEX('\',UserName)) COLLATE DATABASE_DEFAULT AS username,
            [visual_name],
            CAST(TimeStart AS Date) AS access_date,
            [Status]
        FROM
            ReportServer.dbo.ExecutionLog2
        LEFT JOIN
            [base].[dim_visual]
        ON
            [Path] = ReportPath COLLATE SQL_Latin1_General_CP1_CI_AS
        
        WHERE
            visual_name like 'Ptracker%'
    ) a
   
;


-- $END

SELECT TOP 400 *
FROM [base].[fact_visual_access]
