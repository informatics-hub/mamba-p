USE [staging_ptracker_test]
GO

TRUNCATE TABLE [base].[fact_maternity_infant_visit]

-- $BEGIN

    INSERT INTO [base].[fact_maternity_infant_visit](
        [maternity_infant_visit_id],
        [encounter_id],
        [status],
        [sex],
        [dob],
        [breastfeeding],
        [date_of_death],
        [still_birth],
        [received_arv],
        [reason_refused_arv],
        [infant_number]
    )
    
    SELECT
        NEWID(),
        e.encounter_id,
        [status],
        sex,
        dob,
        breastfeeding,
        date_of_death,
        still_birth,
        received_arv,
        reason_refused_arv,
        ptracker_identifier AS infant_number
    FROM
        base.z_form_maternity_infant_visit_encounter ie 
    INNER JOIN
        base.dim_encounter e 
    ON 
        ie.openmrs_encounter_identifier = e.openmrs_encounter_identifier
    WHERE 
        ie._unique_row_number=1
    
    
-- $END

SELECT 
    COUNT(*) as total_encounters
FROM 
    [base].[fact_maternity_infant_visit]

SELECT 
    TOP 400 * 
FROM 
    [base].[fact_maternity_infant_visit]
