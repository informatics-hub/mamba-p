USE [staging_ptracker_test]
GO

DROP TABLE IF EXISTS [base].[fact_maternity_infant_visit];

-- $BEGIN

    CREATE TABLE [base].[fact_maternity_infant_visit](
        [maternity_infant_visit_id] [UNIQUEIDENTIFIER] NOT NULL,
        [encounter_id] [uniqueidentifier] NOT NULL,
        [status] [nvarchar] (255) NULL,
        [sex] [nvarchar] (255) NULL,
        [dob] [date] NULL,
        [breastfeeding] [nvarchar] (255) NULL,
        [date_of_death] [date] NULL,
        [still_birth] [nvarchar] (255) NULL,
        [received_arv] [nvarchar] (255) NULL,
        [reason_refused_arv] [nvarchar] (max) NULL,
        [infant_number] [nvarchar] (255) NULL
    );

    ALTER TABLE [base].[fact_maternity_infant_visit] ADD CONSTRAINT [PK_fact_maternity_infant_visit_base] PRIMARY KEY ([maternity_infant_visit_id]);

    ALTER TABLE [base].[fact_maternity_infant_visit] ADD CONSTRAINT FK_fact_maternity_infant_visit_fact_encounter_base FOREIGN KEY ([encounter_id]) REFERENCES [base].[dim_encounter]([encounter_id]);

-- $END