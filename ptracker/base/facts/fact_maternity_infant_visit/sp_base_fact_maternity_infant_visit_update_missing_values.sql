USE [staging_ptracker_test]
GO

-- $BEGIN
    UPDATE mi
    SET 
        mi.date_of_death = 
            CASE WHEN 
                en.date_of_death_missing = 'Missing Death date' 
            THEN 
                NULL
            ELSE
                mi.date_of_death
            END,
        mi.reason_refused_arv = 
            CASE WHEN 
                en.missing_reason_for_refusing_arv = 'Missing Reason for refusing infant ARV prophylaxis' 
            THEN 
                NULL 
            ELSE
                mi.reason_refused_arv
            END
    FROM 
        [base].[fact_maternity_infant_visit] mi
    INNER JOIN
        [base].[dim_encounter] de 
    ON
        de.encounter_id = mi.encounter_id
    LEFT JOIN 
        base.z_form_maternity_infant_visit_encounter en 
    ON 
        en.openmrs_encounter_identifier = de.openmrs_encounter_identifier
    ;

    UPDATE mi
    SET 
        mi.[status] = 
            CASE WHEN 
                mi.[status] = 'Missing' 
            THEN 
                NULL
            ELSE
                mi.[status]
            END,
        mi.breastfeeding = 
            CASE WHEN 
                mi.breastfeeding = 'Missing' 
            THEN 
                NULL
            ELSE
                mi.breastfeeding
            END,
        mi.still_birth = 
            CASE WHEN 
                mi.still_birth = 'Missing' 
            THEN 
                NULL
            ELSE
                mi.still_birth
            END,
        mi.received_arv = 
            CASE WHEN 
                mi.received_arv = 'Missing' 
            THEN 
                NULL
            ELSE
                mi.received_arv
            END
    FROM 
        [base].[fact_maternity_infant_visit] mi
    
-- $END

SELECT 
    TOP 400 * 
FROM 
    [base].[fact_maternity_infant_visit]