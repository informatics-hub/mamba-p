USE [staging_ptracker_test]
GO
-- $BEGIN

    EXEC sp_base_fact_maternity_infant_visit_create;
    EXEC sp_base_fact_maternity_infant_visit_insert;
    EXEC sp_base_fact_maternity_infant_visit_update_missing_values;

-- $END