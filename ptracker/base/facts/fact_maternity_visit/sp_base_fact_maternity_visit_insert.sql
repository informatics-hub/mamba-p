USE [staging_ptracker_test]
GO

TRUNCATE TABLE [base].[fact_maternity_visit]

-- $BEGIN

    INSERT INTO [base].[fact_maternity_visit](
        [maternity_visit_id],
        [encounter_id],
        [ptracker_identifier],
        [anc_booked],
        [anc_hiv_test_status],
        [mother_status],
        [hiv_test_status],
        [hiv_test_result],
        [hiv_re_test_at_36_weeks],
        [art_initiation],
        [reason_for_refusing_art],
        [art_start_date],
        [recent_viral_load_test_done],
        [viral_load_test_date],
        [viral_load_test_result],
        [viral_load_copies],
        [discharge_date],
        [birth_count]
    )
    
    SELECT
        NEWID(),
        encounter_id,
        ptracker_identifier,
        anc_booked,
        anc_hiv_test_status,
        mother_status,
        hiv_test_status,
        hiv_test_result,
        hiv_re_test_at_36_weeks,
        art_initiation,
        reason_for_refusing_art,
        art_start_date,
        recent_viral_load_test_done,
        viral_load_test_date,
        viral_load_test_result,
        viral_load_copies,
        discharge_date,
        birth_count

    FROM 
        base.z_form_maternity_visit_encounter me
    INNER JOIN
        base.dim_encounter e 
    ON 
        me.openmrs_encounter_identifier = e.openmrs_encounter_identifier
    WHERE
        me._has_duplicate_encounter = 0 AND me._unique_row_number=1 AND me._has_duplicate_ptracker_identifier = 0

    
-- $END
SELECT 
    COUNT(*) as total_encounters
FROM 
    [base].[fact_maternity_visit]

SELECT 
    TOP 400 * 
FROM 
    [base].[fact_maternity_visit]

