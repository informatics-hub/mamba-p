USE [staging_ptracker_test]
GO

-- $BEGIN
    UPDATE fm
    SET 
        fm.art_start_date = 
            CASE WHEN 
                en.missing_art_start_date = 'Missing ART start date' 
            THEN 
                NULL
            ELSE
                fm.art_start_date
            END,
        fm.reason_for_refusing_art = 
            CASE WHEN 
                en.missing_refused_art = 'Missing Reason for refusing ART initiation' 
            THEN 
                NULL 
            ELSE
                fm.reason_for_refusing_art
            END,
        fm.viral_load_copies = 
            CASE WHEN 
                en.missing_viral_load_copies = 'Missing Date of last normal menstrual period' 
            THEN 
                NULL 
            ELSE
                fm.viral_load_copies
            END,
        fm.viral_load_test_date = 
            CASE WHEN 
                en.missing_viral_load_test_date = 'Missing Viral load test date' 
            THEN 
                NULL
            ELSE
                fm.viral_load_test_date 
            END
    FROM 
        [base].[fact_maternity_visit] fm
    INNER JOIN
        [base].[dim_encounter] de 
    ON
        de.encounter_id = fm.encounter_id
    LEFT JOIN 
        base.z_form_maternity_visit_encounter en 
    ON 
        en.openmrs_encounter_identifier = de.openmrs_encounter_identifier
    ;

    UPDATE fm
    SET 
        fm.mother_status = 
            CASE WHEN 
                fm.mother_status = 'Missing' 
            THEN 
                NULL
            ELSE
                fm.mother_status
            END,
        fm.anc_booked = 
            CASE WHEN 
                fm.anc_booked = 'Missing' 
            THEN 
                NULL
            ELSE
                fm.anc_booked
            END,
        fm.anc_hiv_test_status = 
            CASE WHEN 
                fm.anc_hiv_test_status = 'Missing' 
            THEN 
                NULL
            ELSE
                fm.anc_hiv_test_status
            END,
        fm.hiv_test_status = 
            CASE WHEN 
                fm.hiv_test_status = 'Missing' 
            THEN 
                NULL
            ELSE
                fm.hiv_test_status
            END,
        fm.hiv_test_result = 
            CASE WHEN 
                fm.hiv_test_result = 'Missing' 
            THEN 
                NULL
            ELSE
                fm.hiv_test_result
            END,
        fm.hiv_re_test_at_36_weeks = 
            CASE WHEN 
                fm.hiv_re_test_at_36_weeks = 'Missing' 
            THEN 
                NULL
            ELSE
                fm.hiv_re_test_at_36_weeks
            END,
        fm.art_initiation = 
            CASE WHEN 
                fm.art_initiation = 'Missing' 
            THEN 
                NULL
            ELSE
                fm.art_initiation
            END,
        fm.recent_viral_load_test_done = 
            CASE WHEN 
                fm.recent_viral_load_test_done = 'Missing' 
            THEN 
                NULL
            ELSE
                fm.recent_viral_load_test_done
            END,
        fm.viral_load_test_result = 
            CASE WHEN 
                fm.viral_load_test_result = 'Missing' 
            THEN 
                NULL
            ELSE
                fm.viral_load_test_result
            END
    FROM 
        [base].[fact_maternity_visit] fm

-- $END

SELECT 
    TOP 400 * 
FROM 
    [base].[fact_maternity_visit]