USE [staging_ptracker_test]
GO

DROP TABLE IF exists [base].[fact_maternity_visit];

-- $BEGIN

    CREATE TABLE [base].[fact_maternity_visit](
        [maternity_visit_id] [UNIQUEIDENTIFIER] NOT NULL,
        [encounter_id] [uniqueidentifier] NOT NULL,
        [ptracker_identifier] [nvarchar] (255) NULL,
        [anc_booked] [nvarchar] (255) NULL,
        [anc_hiv_test_status] [nvarchar] (255) NULL,
        [mother_status] [nvarchar] (255) NULL,
        [hiv_test_status] [nvarchar] (255) NULL,
        [hiv_test_result] [nvarchar] (255) NULL,
        [hiv_re_test_at_36_weeks] [nvarchar] (255) NULL,
        [art_initiation] [nvarchar] (255) NULL,
        [reason_for_refusing_art] [nvarchar] (max) NULL,
        [art_start_date] [date] NULL,
        [recent_viral_load_test_done] [nvarchar] (255) NULL,
        [viral_load_test_date] [date] NULL,
        [viral_load_test_result] [nvarchar] (255) NULL,
        [viral_load_copies] [float] NULL,
        [discharge_date] [date] NULL,
        [birth_count] [int] NULL
    );

    ALTER TABLE [base].[fact_maternity_visit] ADD CONSTRAINT [PK_fact_maternity_visit_base] PRIMARY KEY ([maternity_visit_id]);

    ALTER TABLE [base].[fact_maternity_visit] ADD CONSTRAINT FK_fact_maternity_visit_fact_encounter_base FOREIGN KEY ([encounter_id]) REFERENCES [base].[dim_encounter]([encounter_id]);


-- $END