USE [staging_ptracker_test]
GO

-- $BEGIN
    UPDATE fip
    SET 
        fip.other_feeding_method = 
            CASE WHEN 
                en.missing_other_feeding_method = 'Missing Other infant feeding method' 
            THEN 
                NULL
            ELSE
                fip.other_feeding_method
            END,
        fip.transfer_in_date = 
            CASE WHEN 
                en.missing_transfer_in_date = 'Missing Transfer in date' 
            THEN 
                NULL 
            ELSE
                fip.transfer_in_date
            END,
        fip.transfer_out_to_date = 
            CASE WHEN 
                en.missing_transfer_out_to_date = 'Missing Transfer out date' 
            THEN 
                NULL 
            ELSE
                fip.transfer_out_to_date
            END,
        
        fip.next_visit_date = 
            CASE WHEN 
                en.missing_next_visit_date = 'Missing Next visit date' 
            THEN 
                NULL
            ELSE
                fip.next_visit_date 
            END
    FROM 
        [base].[fact_infant_postnatal_visit] fip
    INNER JOIN
        [base].[dim_encounter] de 
    ON
        de.encounter_id = fip.encounter_id
    LEFT JOIN 
        base.z_form_infant_postnatal_visit_encounter en 
    ON 
        en.openmrs_encounter_identifier = de.openmrs_encounter_identifier
    ;

    UPDATE fip
    SET 
        fip.hiv_exposure = 
            CASE WHEN 
                fip.hiv_exposure = 'Missing' 
            THEN 
                NULL
            ELSE
                fip.hiv_exposure
            END,
        fip.arv_prophylaxis_status = 
            CASE WHEN 
                fip.arv_prophylaxis_status = 'Missing' 
            THEN 
                NULL
            ELSE
                fip.arv_prophylaxis_status
            END,
        fip.arv_prophylaxis_adherence = 
            CASE WHEN 
                fip.arv_prophylaxis_adherence = 'Missing' 
            THEN 
                NULL
            ELSE
                fip.arv_prophylaxis_adherence
            END,
        fip.ctx_prophylaxis_status = 
            CASE WHEN 
                fip.ctx_prophylaxis_status = 'Missing' 
            THEN 
                NULL
            ELSE
                fip.ctx_prophylaxis_status
            END,
        fip.ctx_prophylaxis_adherence = 
            CASE WHEN 
                fip.ctx_prophylaxis_adherence = 'Missing' 
            THEN 
                NULL
            ELSE
                fip.ctx_prophylaxis_adherence
            END,
        fip.hiv_test_status = 
            CASE WHEN 
                fip.hiv_test_status = 'Missing' 
            THEN 
                NULL
            ELSE
                fip.hiv_test_status
            END,
        fip.hiv_test_type = 
            CASE WHEN 
                fip.hiv_test_type = 'Missing' 
            THEN 
                NULL
            ELSE
                fip.hiv_test_type
            END,
        fip.dna_pcr_test_result = 
            CASE WHEN 
                fip.dna_pcr_test_result = 'Missing' 
            THEN 
                NULL
            ELSE
                fip.dna_pcr_test_result
            END,
        fip.rapid_test_result = 
            CASE WHEN 
                fip.rapid_test_result = 'Missing' 
            THEN 
                NULL
            ELSE
                fip.rapid_test_result
            END,
        fip.confirmatory_test_done = 
            CASE WHEN 
                fip.confirmatory_test_done = 'Missing' 
            THEN 
                NULL
            ELSE
                fip.confirmatory_test_done
            END,
        fip.final_test_result = 
            CASE WHEN 
                fip.final_test_result = 'Missing' 
            THEN 
                NULL
            ELSE
                fip.final_test_result
            END,
        fip.linked_to_art = 
            CASE WHEN 
                fip.linked_to_art = 'Missing' 
            THEN 
                NULL
            ELSE
                fip.linked_to_art
            END,
        fip.breastfeeding_status = 
            CASE WHEN 
                fip.breastfeeding_status = 'Missing' 
            THEN 
                NULL
            ELSE
                fip.breastfeeding_status
            END,
        fip.outcome_status = 
            CASE WHEN 
                fip.outcome_status = 'Missing' 
            THEN 
                NULL
            ELSE
                fip.outcome_status
            END
    FROM 
        [base].[fact_infant_postnatal_visit] fip
    
-- $END

SELECT 
    TOP 400 * 
FROM 
    [base].[fact_infant_postnatal_visit]