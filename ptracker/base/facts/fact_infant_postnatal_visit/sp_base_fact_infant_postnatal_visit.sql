USE [staging_ptracker_test]
GO
-- $BEGIN

    EXEC sp_base_fact_infant_postnatal_visit_create;
    EXEC sp_base_fact_infant_postnatal_visit_insert;
    EXEC sp_base_fact_infant_postnatal_visit_update_missing_values;

-- $END