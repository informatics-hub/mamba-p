USE [staging_ptracker_test]
GO

DROP TABLE IF exists [base].[fact_infant_postnatal_visit];

-- $BEGIN

    CREATE TABLE [base].[fact_infant_postnatal_visit](
        [infant_postnatal_visit_id] [UNIQUEIDENTIFIER] NOT NULL,
        [encounter_id] [uniqueidentifier] NOT NULL,
        [ptracker_identifier] [nvarchar] (255) NULL,
        [hiv_exposure] [nvarchar] (255) NULL,
        [arv_prophylaxis_status] [nvarchar] (255) NULL,
        [arv_prophylaxis_adherence] [nvarchar] (255) NULL,
        [ctx_prophylaxis_status] [nvarchar] (255) NULL,
        [ctx_prophylaxis_adherence] [nvarchar] (255) NULL,
        [hiv_test_status] [nvarchar] (255) NULL,
        [hiv_test_type] [nvarchar] (255) NULL,
        [dna_pcr_test_result] [nvarchar] (255) NULL,
        [rapid_test_result] [nvarchar] (255) NULL,
        [confirmatory_test_done] [nvarchar] (255) NULL,
        [final_test_result] [nvarchar] (255) NULL,
        [linked_to_art] [nvarchar] (255) NULL,
        [breastfeeding_status] [nvarchar] (255) NULL,
        [other_feeding_method] [nvarchar] (255) NULL,
        [outcome_status] [nvarchar] (255) NULL,
        [transfer_in_from] [nvarchar] (255)  NULL,
        [transfer_in_date] [date] NULL,
        [transfer_out_to] [nvarchar] (max) NULL,
        [transfer_out_to_date] [date] NULL,
        [event_date] [date] NULL,
        [death_date] [date] NULL,
        [next_visit_date] [date] NULL
    );

    ALTER TABLE [base].[fact_infant_postnatal_visit] ADD CONSTRAINT [PK_fact_infant_postnatal_visit_base] PRIMARY KEY ([infant_postnatal_visit_id]);

    ALTER TABLE [base].[fact_infant_postnatal_visit] ADD CONSTRAINT FK_fact_infant_postnatal_visit_fact_encounter_base FOREIGN KEY ([encounter_id]) REFERENCES [base].[dim_encounter]([encounter_id]);

    
-- $END