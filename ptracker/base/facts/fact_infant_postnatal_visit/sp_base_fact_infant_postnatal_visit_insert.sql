USE [staging_ptracker_test]
GO

TRUNCATE TABLE [base].[fact_infant_postnatal_visit]

-- $BEGIN

    INSERT INTO [base].[fact_infant_postnatal_visit](
        [infant_postnatal_visit_id],
        [encounter_id],
        [ptracker_identifier],
        [hiv_exposure],
        [arv_prophylaxis_status],
        [arv_prophylaxis_adherence],
        [ctx_prophylaxis_status],
        [ctx_prophylaxis_adherence],
        [hiv_test_status],
        [hiv_test_type],
        [dna_pcr_test_result],
        [rapid_test_result],
        [confirmatory_test_done],
        [final_test_result],
        [linked_to_art],
        [breastfeeding_status],
        [other_feeding_method],
        [outcome_status],
        [transfer_in_from],
        [transfer_in_date],
        [transfer_out_to],
        [transfer_out_to_date],
        [event_date],
        [death_date],
        [next_visit_date]
    )
    
    SELECT
        NEWID(),
        encounter_id,
        ptracker_identifier,
        hiv_exposure,
        arv_prophylaxis_status,
        arv_prophylaxis_adherence,
        ctx_prophylaxis_status,
        ctx_prophylaxis_adherence,
        hiv_test_status,
        hiv_test_type,
        dna_pcr_test_result,
        rapid_test_result,
        confirmatory_test_done,
        final_test_result,
        linked_to_art,
        breastfeeding_status,
        other_feeding_method,
        outcome_status,
        transfer_in_from,
        transfer_in_date,
        transfer_out_to,
        transfer_out_to_date,
        event_date,
        death_date,
        next_visit_date
    FROM
        base.z_form_infant_postnatal_visit_encounter a
    INNER JOIN
        base.dim_encounter e 
    ON 
        a.openmrs_encounter_identifier = e.openmrs_encounter_identifier
    WHERE 
        a._has_duplicate_encounter = 0 AND a._unique_row_number=1

-- $END
SELECT 
    COUNT(*) as total_encounters
FROM 
    [base].[fact_infant_postnatal_visit]

SELECT 
    TOP 400 * 
FROM 
    [base].[fact_infant_postnatal_visit]


