USE [staging_ptracker_test]
GO

-- $BEGIN
    UPDATE fmp
    SET 
        fmp.reason_for_refusing_art = 
            CASE WHEN 
                en.missing_reason_for_refusing_art = 'Missing Reason for refusing ART initiation' 
            THEN 
                NULL
            ELSE
                fmp.reason_for_refusing_art
            END,
        fmp.art_start_date = 
            CASE WHEN 
                en.missing_art_start_date = 'Missing ART start date' 
            THEN 
                NULL 
            ELSE
                fmp.art_start_date
            END,
        fmp.viral_load_test_date = 
            CASE WHEN 
                en.missing_viral_load_test_date = 'Missing Viral load test date' 
            THEN 
                NULL 
            ELSE
                fmp.viral_load_test_date
            END,
        fmp.viral_load_copies = 
            CASE WHEN 
                en.missing_viral_load_copies = 'Missing Viral load copies' 
            THEN 
                NULL
            ELSE
                fmp.viral_load_copies 
            END,
        fmp.next_visit_date = 
            CASE WHEN 
                en.missing_next_visit_date = 'Missing Next visit date' 
            THEN 
                NULL
            ELSE
                fmp.next_visit_date 
            END
    FROM 
        [base].[fact_mother_postnatal_visit] fmp
    INNER JOIN
        [base].[dim_encounter] de 
    ON
        de.encounter_id = fmp.encounter_id
    LEFT JOIN 
        base.z_form_mother_postnatal_visit_encounter en 
    ON 
        en.openmrs_encounter_identifier = de.openmrs_encounter_identifier
    ;

    UPDATE fmp
    SET 
        fmp.hiv_test_status = 
            CASE WHEN 
                fmp.hiv_test_status = 'Missing' 
            THEN 
                NULL
            ELSE
                fmp.hiv_test_status
            END,
        fmp.hiv_test_results = 
            CASE WHEN 
                fmp.hiv_test_results = 'Missing' 
            THEN 
                NULL
            ELSE
                fmp.hiv_test_results
            END,
        fmp.art_initiation = 
            CASE WHEN 
                fmp.art_initiation = 'Missing' 
            THEN 
                NULL
            ELSE
                fmp.art_initiation
            END,
        fmp.recent_viral_load_test_done = 
            CASE WHEN 
                fmp.recent_viral_load_test_done = 'Missing' 
            THEN 
                NULL
            ELSE
                fmp.recent_viral_load_test_done
            END,
        fmp.viral_load_test_result = 
            CASE WHEN 
                fmp.viral_load_test_result = 'Missing' 
            THEN 
                NULL
            ELSE
                fmp.viral_load_test_result
            END,
        fmp.next_appointment_facility = 
            CASE WHEN 
                fmp.next_appointment_facility = 'Missing' 
            THEN 
                NULL
            ELSE
                fmp.next_appointment_facility
            END
    FROM 
        [base].[fact_mother_postnatal_visit] fmp

    
    
    
-- $END

SELECT 
    TOP 400 * 
FROM 
    [base].[fact_mother_postnatal_visit]