USE [staging_ptracker_test]
GO

DROP TABLE IF exists [base].[fact_mother_postnatal_visit];

-- $BEGIN

    CREATE TABLE [base].[fact_mother_postnatal_visit](
        [mother_postnatal_visit_id] [UNIQUEIDENTIFIER] NOT NULL,
        [encounter_id] [uniqueidentifier] NOT NULL,
        [ptracker_identifier] [nvarchar] (255) NULL,
        [hiv_test_status] [nvarchar] (255) NULL,
        [hiv_test_results] [nvarchar] (255) NULL,
        [art_initiation] [nvarchar] (255) NULL,
        [reason_for_refusing_art] [nvarchar] (max) NULL,
        [art_start_date] [date] NULL,
        [recent_viral_load_test_done] [nvarchar] (255) NULL,
        [viral_load_test_date] [date] NULL,
        [viral_load_test_result] [nvarchar] (255) NULL,
        [viral_load_copies] [float] NULL,
        [next_visit_date] [date]  NULL,
        [next_appointment_facility] [nvarchar] (255) NULL,
        [transfer_out_to] [nvarchar] (max) NULL,
        [transfer_out_date] [date] NULL
    );

    ALTER TABLE [base].[fact_mother_postnatal_visit] ADD CONSTRAINT [PK_fact_mother_postnatal_visit_base] PRIMARY KEY ([mother_postnatal_visit_id]);

    ALTER TABLE [base].[fact_mother_postnatal_visit] ADD CONSTRAINT FK_fact_mother_postnatal_visit_fact_encounter_base FOREIGN KEY ([encounter_id]) REFERENCES [base].[dim_encounter]([encounter_id])


-- $END