USE [staging_ptracker_test]
GO

TRUNCATE TABLE [base].[fact_mother_postnatal_visit]

-- $BEGIN

    INSERT INTO [base].[fact_mother_postnatal_visit](
        [mother_postnatal_visit_id],
        [encounter_id],
        [ptracker_identifier],
        [hiv_test_status],
        [hiv_test_results],
        [art_initiation],
        [reason_for_refusing_art],
        [art_start_date],
        [recent_viral_load_test_done],
        [viral_load_test_date],
        [viral_load_test_result],
        [viral_load_copies],
        [next_visit_date],
        [next_appointment_facility],
        [transfer_out_to],
        [transfer_out_date]
    )
    
    SELECT
        NEWID(),
        encounter_id,
        ptracker_identifier,
        hiv_test_status,
        hiv_test_results,
        art_initiation,
        reason_for_refusing_art,
        art_start_date,
        recent_viral_load_test_done,
        viral_load_test_date,
        viral_load_test_result,
        viral_load_copies,
        next_visit_date,
        next_appointment_facility,
        transfer_out_to,
        transfer_out_date
    FROM 
        base.z_form_mother_postnatal_visit_encounter a
    INNER JOIN
        base.dim_encounter e 
    ON 
        a.openmrs_encounter_identifier = e.openmrs_encounter_identifier
    WHERE 
        a._has_duplicate_encounter = 0 AND a._unique_row_number=1 AND a._has_duplicate_ptracker_identifier = 0
        
-- $END
SELECT 
    COUNT(*) as total_encounters
FROM 
    [base].[fact_mother_postnatal_visit]

SELECT 
    TOP 400 * 
FROM 
    [base].[fact_mother_postnatal_visit]

