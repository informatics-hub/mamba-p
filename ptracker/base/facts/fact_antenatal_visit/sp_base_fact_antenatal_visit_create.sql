USE [staging_ptracker_test]
GO

DROP TABLE IF exists [base].[fact_antenatal_visit];

-- $BEGIN

    CREATE TABLE [base].[fact_antenatal_visit](
        [antenatal_visit_id] [uniqueidentifier] NOT NULL,
        [encounter_id] [uniqueidentifier] NOT NULL,
        [ptracker_identifier] [nvarchar] (255) NULL,
        [visit_type] [nvarchar] (255) NULL,
        [gravida] [tinyint] NULL,
        [para] [tinyint] NULL,
        [lnmp] [date] NULL,
        [edd] [date] NULL,
        [hiv_test_status] [nvarchar] (255) NULL,
        [hiv_test_results] [nvarchar] (255) NULL,
        [partner_hiv_test_done] [nvarchar] (255) NULL,
        [partner_hiv_test_date] [date] NULL,
        [partner_hiv_test_status] [nvarchar] (255) NULL,
        [art_initiation] [nvarchar] (255) NULL,
        [reason_for_refusing_art] [nvarchar] (max)  NULL,
        [art_start_date] [date] NULL,
        [recent_viral_load_test_done] [nvarchar] (255) NULL,
        [viral_load_test_date] [date] NULL,
        [viral_load_test_result] [nvarchar] (255) NULL,
        [viral_load_copies] [float] NULL,
        [next_visit_date] [date] NULL,
        [facility_of_next_appointment] [nvarchar] (255) NULL
    );

    ALTER TABLE [base].[fact_antenatal_visit] ADD CONSTRAINT [PK_fact_antenatal_visit_base] PRIMARY KEY ([antenatal_visit_id]);

    ALTER TABLE [base].[fact_antenatal_visit] ADD CONSTRAINT FK_fact_antenatal_visit_dim_encounter_base FOREIGN KEY ([encounter_id]) REFERENCES [base].[dim_encounter]([encounter_id]);

-- $END