USE [staging_ptracker_test]
GO

-- $BEGIN
    UPDATE fa
    SET 
        fa.gravida = 
            CASE WHEN 
                en.missing_gravida = 'Missing Gravida' 
            THEN 
                NULL
            ELSE
                fa.gravida
            END,
        fa.para = 
            CASE WHEN 
                en.missing_para = 'Missing Para' 
            THEN 
                NULL 
            ELSE
                fa.para
            END,
        fa.lnmp = 
            CASE WHEN 
                en.missing_lnmp = 'Missing Date of last normal menstrual period' 
            THEN 
                NULL 
            ELSE
                fa.lnmp
            END,
        fa.edd = 
            CASE WHEN 
                en.edd_missing = 'Missing Expected date of delivery' 
            THEN 
                NULL
            ELSE
                fa.edd 
            END,
        fa.partner_hiv_test_date = 
            CASE WHEN 
                en.partner_hiv_test_date_missing = 'Missing Partner Testing Date' 
            THEN 
                NULL
            ELSE
                fa.partner_hiv_test_date
            END,
        fa.reason_for_refusing_art = 
            CASE WHEN 
                en.missing_refused_art = 'Missing Reason for refusing ART initiation' 
            THEN 
                NULL
            ELSE
                fa.reason_for_refusing_art 
            END,
        fa.art_start_date = 
            CASE WHEN 
                en.missing_art_start_date = 'Missing ART start date' 
            THEN 
                NULL
            ELSE
                fa.art_start_date 
            END,
        fa.viral_load_test_date = 
            CASE WHEN 
                en.missing_viral_load_test_date = 'Missing Viral load test date' 
            THEN 
                NULL
            ELSE
                fa.viral_load_test_date 
            END,
        fa.viral_load_copies = 
            CASE WHEN 
                en.missing_viral_load_copies = 'Missing Viral load copies' 
            THEN 
                NULL 
            ELSE
                fa.viral_load_copies
            END,
        fa.next_visit_date = 
            CASE WHEN 
                en.missing_next_visit_date = 'Missing Next visit date' 
            THEN 
                NULL
            ELSE
                fa.next_visit_date 
            END

    FROM 
        [base].[fact_antenatal_visit] fa
    INNER JOIN
        [base].[dim_encounter] de 
    ON
        de.encounter_id = fa.encounter_id
    LEFT JOIN 
        base.z_form_antenatal_visit_encounter en
    ON 
        en.openmrs_encounter_identifier = de.openmrs_encounter_identifier
    ;

    UPDATE fa
    SET 
        fa.hiv_test_status = 
            CASE WHEN 
                fa.hiv_test_status = 'Missing' 
            THEN 
                NULL
            ELSE
                fa.hiv_test_status
            END,
        fa.partner_hiv_test_done = 
            CASE WHEN 
                fa.partner_hiv_test_done = 'Missing' 
            THEN 
                NULL
            ELSE
                fa.partner_hiv_test_done
            END,
        fa.hiv_test_results = 
            CASE WHEN 
                fa.hiv_test_results = 'Missing' 
            THEN 
                NULL
            ELSE
                fa.hiv_test_results
            END,
        fa.partner_hiv_test_status = 
            CASE WHEN 
                fa.partner_hiv_test_status = 'Missing' 
            THEN 
                NULL
            ELSE
                fa.partner_hiv_test_status
            END,
        fa.art_initiation = 
            CASE WHEN 
                fa.art_initiation = 'Missing' 
            THEN 
                NULL
            ELSE
                fa.art_initiation
            END,
        fa.recent_viral_load_test_done = 
            CASE WHEN 
                fa.recent_viral_load_test_done = 'Missing' 
            THEN 
                NULL
            ELSE
                fa.recent_viral_load_test_done
            END,
        fa.viral_load_test_result = 
            CASE WHEN 
                fa.viral_load_test_result = 'Missing' 
            THEN 
                NULL
            ELSE
                fa.viral_load_test_result
            END,
        fa.facility_of_next_appointment = 
            CASE WHEN 
                fa.facility_of_next_appointment = 'Missing' 
            THEN 
                NULL
            ELSE
                fa.facility_of_next_appointment
            END
    FROM 
        [base].[fact_antenatal_visit] fa
    
-- $END

SELECT 
    TOP 400 * 
FROM 
    [base].[fact_antenatal_visit]

