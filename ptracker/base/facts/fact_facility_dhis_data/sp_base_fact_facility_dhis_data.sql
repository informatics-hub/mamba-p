USE [staging_ptracker_test]
GO
-- $BEGIN

    EXEC sp_base_fact_facility_dhis_data_create;
    EXEC sp_base_fact_facility_dhis_data_insert;
    EXEC sp_base_fact_facility_dhis_data_update_facility;

-- $END