USE [staging_ptracker_test]
GO

TRUNCATE TABLE [base].[fact_facility_dhis_data];

-- $BEGIN

    WITH dhis2_data AS 
    (
        SELECT
			NEWID() as facility_dhis_data_id,
            case when ou.hierarchylevel = 5 then oup.uid else ou.uid end as organisation_unit_identifier,
            case when ou.hierarchylevel = 5 then oup.name else ou.name end as organisation_unit_name,
            case when ou.hierarchylevel = 5 then oup.code else ou.code end as organisation_unit_code,
            case when ou.hierarchylevel = 5 then oup.description else ou.[description] end  as organisation_unit_description,
            de.uid as data_identifier,
            de.name as data_name,
            de.code as data_code,
            de.[description] as data_description,
            sum ( TRY_CAST (dv.[value] as int)) as data_value,
            FORMAT(CONVERT(DATE, p.enddate), 'MMMM yyyy')   as data_period
        FROM 
            [lake_dhis2_core].[dbo].[dataset] ds
        LEFT JOIN 
            [lake_dhis2_core].[dbo].[datasetelement] dse 
        ON 
            ds.[datasetid] = dse.[datasetid]
        LEFT JOIN 
            [lake_dhis2_core].[dbo].[dataelement] de 
        ON 
            dse.[dataelementid] = de.[dataelementid]
        LEFT JOIN 
            [lake_dhis2_core].[dbo].[datavalue] dv 
        ON 
            de.[dataelementid] = dv.[dataelementid]
        LEFT JOIN 
            [lake_dhis2_core].[dbo].[period] p 
        ON 
            dv.[periodid] = p.[periodid]
        LEFT JOIN 
            [lake_dhis2_core].[dbo].[organisationunit] ou  
        ON 
            ou.organisationunitid = dv.sourceid
		 LEFT JOIN 
            [lake_dhis2_core].[dbo].[organisationunit] oup  
        ON 
            oup.organisationunitid = ou.parentid
        WHERE ds.name = 'PMTCT Antenatal Clinic (ANC) Monthly Summary Form' and p.startdate>'2016-12-31'
		group by  de.uid, de.name,de.code, de.[description], ou.hierarchylevel , ou.uid, oup.uid, ou.name, oup.name, ou.code, oup.code, ou.[description], oup.[description],p.enddate

        UNION

        SELECT
			NEWID() as  facility_dhis_data_id,
            case when ou.hierarchylevel = 5 then oup.uid else ou.uid end as organisation_unit_identifier,
            case when ou.hierarchylevel = 5 then oup.name else ou.name end as organisation_unit_name,
            case when ou.hierarchylevel = 5 then oup.code else ou.code end as organisation_unit_code,
            case when ou.hierarchylevel = 5 then oup.description else ou.[description] end  as organisation_unit_description,
            de.uid as data_identifier,
            de.name as data_name,
            de.code as data_code,
            de.[description] as data_description,
            sum ( TRY_CAST (dv.[value] as int)) as data_value,
            FORMAT(CONVERT(DATE, p.enddate), 'MMMM yyyy')   as data_period
        FROM 
            [lake_dhis2_core].[dbo].[dataset] ds
        LEFT JOIN 
            [lake_dhis2_core].[dbo].[datasetelement] dse 
        ON 
            ds.[datasetid] = dse.[datasetid]
        LEFT JOIN 
            [lake_dhis2_core].[dbo].[dataelement] de 
        ON 
            dse.[dataelementid] = de.[dataelementid]
        LEFT JOIN 
            [lake_dhis2_core].[dbo].[datavalue] dv 
        ON 
            de.[dataelementid] = dv.[dataelementid]
        LEFT JOIN 
            [lake_dhis2_core].[dbo].[period] p 
        ON 
            dv.[periodid] = p.[periodid]
        LEFT JOIN 
            [lake_dhis2_core].[dbo].[periodtype] pt 
        ON 
            p.[periodtypeid] = pt.[periodtypeid]
        LEFT JOIN 
            [lake_dhis2_core].[dbo].[organisationunit] ou  
        ON 
            ou.organisationunitid = dv.sourceid
		 LEFT JOIN 
            [lake_dhis2_core].[dbo].[organisationunit] oup  
        ON 
            oup.organisationunitid = ou.parentid
        WHERE ds.name = 'Mother Baby Follow Up Care Monthly Summary Form' and p.startdate>'2016-12-31'
		group by  de.uid, de.name,de.code, de.[description], ou.hierarchylevel , ou.uid, oup.uid, ou.name, oup.name, ou.code, oup.code, ou.[description], oup.[description],p.enddate

        UNION

        SELECT
			NEWID() as  facility_dhis_data_id,
            case when ou.hierarchylevel = 5 then oup.uid else ou.uid end as organisation_unit_identifier,
            case when ou.hierarchylevel = 5 then oup.name else ou.name end as organisation_unit_name,
            case when ou.hierarchylevel = 5 then oup.code else ou.code end as organisation_unit_code,
            case when ou.hierarchylevel = 5 then oup.description else ou.[description] end  as organisation_unit_description,
            de.uid as data_identifier,
            de.name as data_name,
            de.code as data_code,
            de.[description] as data_description,
            sum ( TRY_CAST (dv.[value] as int)) as data_value,
            FORMAT(CONVERT(DATE, p.enddate), 'MMMM yyyy')   as data_period
        FROM 
        [lake_dhis2_core].[dbo].[dataset] ds
        LEFT JOIN 
            [lake_dhis2_core].[dbo].[datasetelement] dse 
        ON 
            ds.[datasetid] = dse.[datasetid]
        LEFT JOIN 
            [lake_dhis2_core].[dbo].[dataelement] de 
        ON 
            dse.[dataelementid] = de.[dataelementid]
        LEFT JOIN 
            [lake_dhis2_core].[dbo].[datavalue] dv 
        ON 
            de.[dataelementid] = dv.[dataelementid]
        LEFT JOIN 
            [lake_dhis2_core].[dbo].[period] p 
        ON 
            dv.[periodid] = p.[periodid]
        LEFT JOIN 
            [lake_dhis2_core].[dbo].[organisationunit] ou  
        ON 
            ou.organisationunitid = dv.sourceid
		 LEFT JOIN 
            [lake_dhis2_core].[dbo].[organisationunit] oup  
        ON 
            oup.organisationunitid = ou.parentid
        WHERE  ds.name = 'PMTCT MATERNITY (L and D) MONTHLY SUMMARY FORM'  and p.startdate>'2016-12-31'
		group by  de.uid, de.name,de.code, de.[description], ou.hierarchylevel , ou.uid, oup.uid, ou.name, oup.name, ou.code, oup.code, ou.[description], oup.[description],p.enddate

        UNION

		select 
			NEWID() as  facility_dhis_data_id,
			organisation_unit_identifier,organisation_unit_name, organisation_unit_code, organisation_unit_description,
			data_identifier, data_name, data_code, data_description, SUM(data_value) as data_value, data_period
		from 
		(
		SELECT
			
            case when ou.hierarchylevel = 5 then oup.uid else ou.uid end as organisation_unit_identifier,
            case when ou.hierarchylevel = 5 then oup.name else ou.name end as organisation_unit_name,
            case when ou.hierarchylevel = 5 then oup.code else ou.code end as organisation_unit_code,
            case when ou.hierarchylevel = 5 then oup.description else ou.[description] end  as organisation_unit_description,
            'Qp5RYXqZDi8' as data_identifier,
            'Total Deliveries' as data_name,
            null as data_code,
            'DEF: The total number of deliveries in the hospital including normal, forceps, vacuum, caesarean section.' as data_description,
            TRY_CAST (dv.[value] as int) as data_value,
            FORMAT(CONVERT(DATE, p.enddate), 'MMMM yyyy')   as data_period
        FROM  
            [lake_dhis2_core].[dbo].[dataelement] de
        LEFT JOIN 
            [lake_dhis2_core].[dbo].[datavalue] dv
        ON 
            de.[dataelementid] = dv.[dataelementid]
        LEFT JOIN 
            [lake_dhis2_core].[dbo].[period] p 
        ON 
            dv.[periodid] = p.[periodid]
        LEFT JOIN 
            [lake_dhis2_core].[dbo].[periodtype] pt 
        ON 
            p.[periodtypeid] = pt.[periodtypeid]
        LEFT JOIN 
            [lake_dhis2_core].[dbo].[organisationunit] ou  
        ON 
            ou.organisationunitid = dv.sourceid
		 LEFT JOIN 
            [lake_dhis2_core].[dbo].[organisationunit] oup  
        ON 
            oup.organisationunitid = ou.parentid
        WHERE de.uid in ('HoGghV97081','S0Dexm9Odx4','d8sHuPzRG9c','nuJM0ZKCBGw','KrfSGJxuY1G','XIxO80yZ9TR')  and p.startdate>'2016-12-31'
		) a
		GROUP By organisation_unit_identifier,organisation_unit_name, organisation_unit_code, organisation_unit_description,data_identifier, data_name, data_code, data_description, data_period



    )

    INSERT INTO [base].[fact_facility_dhis_data]
    (
        [facility_dhis_data_id],
        [indicator_id],
        [facility_id],
        [organisation_unit_identifier],
        [organisation_unit_name],
        [organisation_unit_code],
        [organisation_unit_description],
        [data_identifier],
        [data_name],
        [data_code],
        [data_description],
        [data_value],
        [data_period]
    )
    SELECT 
		d.facility_dhis_data_id,
		i.indicator_id,
		f.facility_id,
		d.organisation_unit_identifier,
		d.organisation_unit_name, 
		d.organisation_unit_code, 
		d.organisation_unit_description,
		d.data_identifier, 
		d.data_name, 
		d.data_code, 
		d.data_description, 
		d.data_value, 
		d.data_period
    FROM 
        dhis2_data d
	LEFT JOIN 
        [base].[dim_facility] f
    ON 
        f.dhis_facility_orgunit_id = d.organisation_unit_identifier
	 LEFT JOIN 
        [base].[dim_indicator] i
    ON 
        i.indicator_identifier = d.data_identifier
    

-- $END

SELECT TOP 200 *
FROM [base].[fact_facility_dhis_data]