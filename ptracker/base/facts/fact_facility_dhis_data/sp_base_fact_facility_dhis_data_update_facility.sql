USE [staging_ptracker_test]
GO

-- $BEGIN
    UPDATE fhd
    SET 
        fhd.facility_id = fo.facility_id,
        [organisation_unit_identifier] = fo.openmrs_identifier,
        [organisation_unit_name] = fo.facility_name,
        [organisation_unit_code] = fo.mfl_code 
    FROM 
        [base].[fact_facility_dhis_data] fhd
    CROSS JOIN 
    (
        SELECT *
        FROM base.dim_facility
        WHERE facility_name = 'Omatjete Clinic'
    ) fo
    WHERE
        fhd.organisation_unit_name = 'Omatjette Clinic'
    ;
-- $END

SELECT 
    TOP 400 * 
FROM 
    [base].[fact_facility_dhis_data]
