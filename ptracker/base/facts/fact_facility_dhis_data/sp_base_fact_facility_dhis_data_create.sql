USE [staging_ptracker_test]
GO

DROP TABLE IF exists [base].[fact_facility_dhis_data];

-- $BEGIN

    CREATE TABLE [base].[fact_facility_dhis_data](
        [facility_dhis_data_id] [UNIQUEIDENTIFIER] NOT NULL,
        [indicator_id] [UNIQUEIDENTIFIER]  NULL,
        [facility_id] [UNIQUEIDENTIFIER] NULL,
        [organisation_unit_identifier] [nvarchar] (255) NULL,
        [organisation_unit_name] [nvarchar] (255) NULL,
        [organisation_unit_code] [int] NULL,
        [organisation_unit_description] [text] NULL,
        [data_identifier] [nvarchar] (255) NULL,
        [data_name] [nvarchar] (255) NULL,
        [data_code] [nvarchar] (255) NULL,
        [data_description] [text] NULL,
        [data_value] [int] NULL,
        [data_period] [nvarchar] (255) NULL
    );

    ALTER TABLE [base].[fact_facility_dhis_data] ADD CONSTRAINT [PK_fact_facility_dhis_data_base] PRIMARY KEY ([facility_dhis_data_id]);

    ALTER TABLE [base].[fact_facility_dhis_data] ADD CONSTRAINT FK_fact_facility_dhis_data_dim_facility_base FOREIGN KEY ([facility_id]) REFERENCES [base].[dim_facility]([facility_id]);

    ALTER TABLE [base].[fact_facility_dhis_data] ADD CONSTRAINT FK_fact_facility_dhis_data_dim_indicator_base FOREIGN KEY ([indicator_id]) REFERENCES [base].[dim_indicator]([indicator_id]);

-- $END