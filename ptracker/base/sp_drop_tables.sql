USE [staging_ptracker_test];
GO

-- $BEGIN
    DROP TABLE IF EXISTS base.fact_antenatal_visit;
    DROP TABLE IF EXISTS base.fact_infant_postnatal_visit;
    DROP TABLE IF EXISTS base.fact_maternity_infant_visit;
    DROP TABLE IF EXISTS base.fact_maternity_visit;
    DROP TABLE IF EXISTS base.fact_mother_postnatal_visit;

    DROP TABLE IF EXISTS base.dim_encounter;
    DROP TABLE IF EXISTS base.dim_client;
    DROP TABLE IF EXISTS base.dim_facility;
    DROP TABLE IF EXISTS base.dim_date;
    
    DROP TABLE IF EXISTS base.z_form_antenatal_visit_encounter;
    DROP TABLE IF EXISTS base.z_form_antenatal_visit_encounter_all;
    DROP TABLE IF EXISTS base.z_form_infant_postnatal_visit_encounter;
    DROP TABLE IF EXISTS base.z_form_infant_postnatal_visit_encounter_all;
    DROP TABLE IF EXISTS base.z_form_maternity_infant_visit_encounter;
    DROP TABLE IF EXISTS base.z_form_maternity_visit_encounter;
    DROP TABLE IF EXISTS base.z_form_maternity_visit_encounter_all;
    DROP TABLE IF EXISTS base.z_form_mother_postnatal_visit_encounter;
    DROP TABLE IF EXISTS base.z_form_mother_postnatal_visit_encounter_all;

-- $END