USE [staging_ptracker_test]
GO

-- Drop antenatal visit table
DROP TABLE IF EXISTS base.z_form_antenatal_visit_encounter;

-- Drop antenatal visit staging table
DROP TABLE IF EXISTS base.z_form_antenatal_visit_encounter_all;

-- $BEGIN

    -- Populate antenatal visit staging table
    EXEC base.sp_form_visit_generic_encounter_all 'Antenatal','z_form_antenatal_visit_encounter_all'

    -- Populate antenatal visit table
    SELECT
        CASE WHEN  
            PTrackerID IS NULL
        THEN
            1
        ELSE
            ROW_NUMBER() 
            OVER( 
            PARTITION BY 
                PTrackerID,
                CAST(e.encounter_datetime AS DATE),
                [Date of last menstrual period],
                [Estimated date of delivery],
                [Facility of next appointment]
            ORDER BY
                PTrackerID  ASC
            ) 
        END AS _unique_row_number,
        encounter_id_cod AS openmrs_encounter_identifier,
        patient_id_cod AS openmrs_patient_identifier,
        CASE WHEN 
            PTrackerID IS NULL
        THEN
            p_identifier.identifier
        ELSE
            PTrackerID
        END AS ptracker_identifier,
        [Visit type] AS visit_type,
        Gravida AS gravida,
        [Missing Gravida] AS missing_gravida,
        Parity AS para,
        [Missing Para] AS missing_para,
        [Date of last menstrual period] AS lnmp,
        [Missing Date of last normal menstrual period] AS missing_lnmp,
        [Estimated date of delivery] AS edd,
        [Missing Expected date of delivery] AS edd_missing,
        [HIV test performed] AS hiv_test_status,
        CASE WHEN 
            [Result of HIV test] = 'HIV Positive'
        THEN
            'Positive'
        WHEN 
            [Result of HIV test] = 'NEGATIVE'
        THEN
            'Negative'
        WHEN 
            [Result of HIV test] = 'Unknown'
        THEN
            'Unknown'
        WHEN 
            [Result of HIV test] = 'Missing'
        THEN
            'Missing'
        ELSE
            NULL
        END AS hiv_test_results,
        [Partner HIV tested] AS partner_hiv_test_done,
        [Date Partner Tested] AS partner_hiv_test_date,
        [Missing Partner Testing Date] AS partner_hiv_test_date_missing,
        CASE WHEN 
            [PARTNER'S HIV STATUS] = 'HIV Positive'
        THEN
            'Positive'
        WHEN 
            [PARTNER'S HIV STATUS] = 'NEGATIVE'
        THEN
            'Negative'
        WHEN 
            [PARTNER'S HIV STATUS] = 'Unknown'
        THEN
            'Unknown'
        WHEN 
            [PARTNER'S HIV STATUS] = 'Missing'
        THEN
            'Missing'
        ELSE
            NULL
        END AS partner_hiv_test_status,
        [ART Initiation Status] AS art_initiation,
        [Antiretroviral treatment comment] AS reason_for_refusing_art,
        [Missing Reason for refusing ART initiation] AS missing_refused_art,
        [ART start date] AS art_start_date,
        [Missing ART start date] AS missing_art_start_date,
        [Viral Load Test Done] AS recent_viral_load_test_done,
        [Date of reported HIV viral load] AS viral_load_test_date,
        [Missing Viral load test date] AS missing_viral_load_test_date,
        [Viral Load Results] AS viral_load_test_result,
        [VIRAL LOAD] AS viral_load_copies,
        [Missing Viral load copies] AS missing_viral_load_copies,
        [RETURN VISIT DATE] AS next_visit_date,
        [Missing Next visit date] AS missing_next_visit_date,
        [Facility of next appointment] AS facility_of_next_appointment,
        0 AS _has_duplicate_encounter,
        0 AS _has_duplicate_ptracker_identifier
    INTO 
        base.z_form_antenatal_visit_encounter
    FROM 
        base.z_form_antenatal_visit_encounter_all a
    INNER JOIN 
        [lake_ptracker].[dbo].[encounter] e 
    ON 
        a.encounter_id_cod=e.encounter_id
    LEFT JOIN
        lake_ptracker.dbo.patient_identifier p_identifier 
    ON 
        a.patient_id_cod = p_identifier.patient_id AND p_identifier.identifier_type= 5 AND p_identifier.voided=0

    -- Flag duplicate antenatal visits
    EXEC base.sp_form_visit_generic_flag_duplicate_encounter 'Visit type','Antenatal','z_form_antenatal_visit_encounter'

    -- Flag duplicate ptracker ids in the antenatal visits
    EXEC base.sp_form_visit_generic_flag_duplicate_ptracker_identifier 'z_form_antenatal_visit_encounter'

-- $END

SELECT 
    TOP 400 * 
FROM 
    base.z_form_antenatal_visit_encounter
