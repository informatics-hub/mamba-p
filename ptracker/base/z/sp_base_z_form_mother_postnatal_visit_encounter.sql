USE [staging_ptracker_test]
GO

-- Drop mother postnatal visit table
DROP TABLE IF EXISTS base.z_form_mother_postnatal_visit_encounter;

-- Drop mother postnatal visit staging table
DROP TABLE IF EXISTS base.z_form_mother_postnatal_visit_encounter_all;

-- $BEGIN

    -- Populate mother postnatal visit staging table
    EXEC base.sp_form_visit_generic_encounter_all 'Mother Postnatal','z_form_mother_postnatal_visit_encounter_all'

    SELECT 
        patient_id_cod AS openmrs_patient_identifier,
        CASE WHEN  
            PTrackerID IS NULL
        THEN
            1
        ELSE
            ROW_NUMBER() 
            OVER( 
            PARTITION BY 
                PTrackerID,
                CAST(e.encounter_datetime AS DATE),
                [Facility of next appointment],
                [HIV test performed],
                [RETURN VISIT DATE]
            ORDER BY
                PTrackerID  ASC
            ) 
        END AS _unique_row_number,
        encounter_id_cod AS openmrs_encounter_identifier,
        CASE WHEN 
            PTrackerID IS NULL
        THEN
            p_identifier.identifier
        ELSE
            PTrackerID
        END AS ptracker_identifier,
        [HIV test performed] AS hiv_test_status,
        CASE WHEN 
            [Result of HIV test] = 'HIV Positive'
        THEN
            'Positive'
        WHEN 
            [Result of HIV test] = 'NEGATIVE'
        THEN
            'Negative'
        WHEN 
            [Result of HIV test] = 'Unknown'
        THEN
            'Unknown'
        WHEN 
            [Result of HIV test] = 'Missing'
        THEN
            'Missing'
        ELSE
            NULL
        END AS hiv_test_results,
        [ART Initiation Status] AS art_initiation,
        [Antiretroviral treatment comment] AS reason_for_refusing_art,
        [Missing Reason for refusing ART initiation] AS missing_reason_for_refusing_art,
        [ART start date] AS art_start_date,
        [Missing ART start date] AS missing_art_start_date,
        [Viral Load Test Done] AS recent_viral_load_test_done,
        [Date of reported HIV viral load] AS viral_load_test_date,
        [Missing Viral load test date] AS missing_viral_load_test_date,
        [Viral Load Results] AS viral_load_test_result,
        [VIRAL LOAD] AS viral_load_copies,
        [Missing Viral load copies] AS missing_viral_load_copies,
        [RETURN VISIT DATE] AS next_visit_date,
        [Missing Next visit date] AS missing_next_visit_date,
        [Facility of next appointment] AS next_appointment_facility, 
        [Transferred out to] AS transfer_out_to,
        [Date transferred out] AS transfer_out_date,
        0 AS _has_duplicate_encounter,
        0 AS _has_duplicate_ptracker_identifier
    INTO
        base.z_form_mother_postnatal_visit_encounter
    FROM 
        base.z_form_mother_postnatal_visit_encounter_all a
    INNER JOIN 
        [lake_ptracker].[dbo].[encounter] e 
    ON 
        a.encounter_id_cod=e.encounter_id
    LEFT JOIN
        lake_ptracker.dbo.patient_identifier p_identifier 
    ON 
        a.patient_id_cod = p_identifier.patient_id AND p_identifier.identifier_type= 5 AND p_identifier.voided=0

    -- Flag duplicate mother postnatal visits
    EXEC base.sp_form_visit_generic_flag_duplicate_encounter 'Facility of next appointment','Mother Postnatal','z_form_mother_postnatal_visit_encounter'
    
    -- Flag duplicate mother ptracker ids in postnatal visits
    EXEC base.sp_form_visit_generic_flag_duplicate_ptracker_identifier 'z_form_mother_postnatal_visit_encounter'
    
-- $END

SELECT 
    TOP 400 * 
FROM 
    base.z_form_mother_postnatal_visit_encounter
    