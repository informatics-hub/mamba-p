USE [staging_ptracker_test]
GO

CREATE OR ALTER PROCEDURE base.sp_form_visit_generic_flag_duplicate_ptracker_identifier(@table  AS NVARCHAR(MAX)) 
AS
BEGIN
 
    DECLARE @sqlCommand  AS NVARCHAR(MAX);

    /*
        Determine if the ptracker id is a duplicate
    */

    SET @sqlCommand = 
                'UPDATE 
                   base.'  + @table +
                ' SET 
                    _has_duplicate_ptracker_identifier=1 
                FROM 
                    base.' + @table + ' a
                INNER JOIN
                (
                    SELECT 
                        ptracker_identifier, COUNT(*) AS ct
                    FROM 
                    (
                        SELECT DISTINCT 
                            openmrs_patient_identifier, ptracker_identifier
                        FROM 
                            base.' + @table + ' 
                        WHERE ptracker_identifier IS NOT NULL
                    ) a
                    GROUP BY ptracker_identifier
                    HAVING COUNT(*) > 1
                ) b
                ON
                    a.ptracker_identifier = b.ptracker_identifier
                '

    EXECUTE(@sqlCommand);

END