USE [staging_ptracker_test]
GO

-- Drop infant maternal infant visit  table
DROP TABLE IF EXISTS base.z_form_maternity_infant_visit_encounter;

-- Drop infant maternal visit table
DROP TABLE IF EXISTS base.z_form_maternity_visit_encounter;

-- Drop infant maternal visit staging table
DROP TABLE IF EXISTS base.z_form_maternity_visit_encounter_all;

-- $BEGIN

    -- Populate infant maternal visit staging table
    EXEC base.sp_form_visit_generic_encounter_all 'Labor and Delivery','z_form_maternity_visit_encounter_all'

    -- Populate infant maternal visit table
    SELECT 
        ROW_NUMBER() 
        OVER( 
        PARTITION BY 
            [Infant's medical record number]
        ORDER BY
            [Infant's medical record number]  ASC
        ) AS _unique_row_number,
        encounter_id_cod AS openmrs_encounter_identifier,
        [Qualitative birth outcome] AS [status],
        [CHILD GENDER] AS sex,
        [Infant's date of birth] AS dob,
        [Infant feeding method] AS breastfeeding,
        [DATE OF DEATH] AS date_of_death,
        [Missing Death date] AS date_of_death_missing,
        [STILLBIRTH] AS still_birth,
        [ARV Prophylaxis Status] AS received_arv,
        [Antiretroviral treatment comment] AS reason_refused_arv,
        [Missing Reason for refusing infant ARV prophylaxis] AS missing_reason_for_refusing_arv,
        [Infant's medical record number] AS ptracker_identifier
    INTO
        base.z_form_maternity_infant_visit_encounter    
    FROM 
        base.z_form_maternity_visit_encounter_all a
    INNER JOIN 
        [lake_ptracker].[dbo].[encounter] e 
    ON 
        a.encounter_id_cod=e.encounter_id
        
    -- Populate maternal visit table
    SELECT 
        CASE WHEN  
            PTrackerID IS NULL
        THEN
            1
        ELSE
            ROW_NUMBER() 
            OVER( 
            PARTITION BY 
                PTrackerID,
                CAST(e.encounter_datetime AS DATE),
                [ANTENATAL CARD PRESENT],
                [MOTHER'S HEALTH STATUS],
                [NUMBER OF BIRTHS FROM CURRENT PREGNANCY],
                CAST([Discharge Date] AS DATE)
            ORDER BY
                PTrackerID  ASC
            ) 
        END AS _unique_row_number,
        encounter_id_cod AS openmrs_encounter_identifier,
        patient_id_cod AS openmrs_patient_identifier,
        CASE WHEN 
            PTrackerID IS NULL
        THEN
            p_identifier.identifier
        ELSE
            PTrackerID
        END AS ptracker_identifier,
        [ANTENATAL CARD PRESENT] AS anc_booked,
        [MOTHER'S HEALTH STATUS] AS mother_status,
        [HIV test performed] AS hiv_test_status,
        CASE WHEN 
            [Result of HIV test] = 'HIV Positive'
        THEN
            'Positive'
        WHEN 
            [Result of HIV test] = 'NEGATIVE'
        THEN
            'Negative'
        WHEN 
            [Result of HIV test] = 'Unknown'
        THEN
            'Unknown'
        WHEN 
            [Result of HIV test] = 'Missing'
        THEN
            'Missing'
        ELSE
            NULL
        END AS hiv_test_result,
        CASE WHEN 
            [reason for declining HIV test] = 'HIV Positive'
        THEN
            'Positive'
        WHEN 
            [reason for declining HIV test] = 'NEGATIVE'
        THEN
            'Negative'
        WHEN 
            [reason for declining HIV test] = 'Not tested for HIV during this visit'
        THEN
            'Not tested for HIV during this visit'
        WHEN 
            [reason for declining HIV test] = 'Unknown'
        THEN
            'Unknown'
        WHEN 
            [reason for declining HIV test] = 'Missing'
        THEN
            'Missing'
        ELSE
            NULL
        END AS hiv_re_test_at_36_weeks,
        [ART Initiation Status] AS art_initiation,
        [Antiretroviral treatment comment] AS reason_for_refusing_art,
        [Missing Reason for refusing ART initiation] AS missing_refused_art,
        [ART start date] AS art_start_date,
        [Missing ART start date] AS missing_art_start_date,
        [Viral Load Test Done] AS recent_viral_load_test_done,
        [Date of reported HIV viral load] AS viral_load_test_date,
        [Missing Viral load test date] AS missing_viral_load_test_date,
        [Viral Load Results] AS viral_load_test_result,
        [VIRAL LOAD] AS viral_load_copies,
        [Missing Viral load copies] AS missing_viral_load_copies,
        CAST([Discharge Date] AS DATE) AS discharge_date,
        [NUMBER OF BIRTHS FROM CURRENT PREGNANCY] AS birth_count,
        [ANC HIV Status First Visit] AS anc_hiv_test_status,
        0 AS _has_duplicate_encounter,
        0 AS _has_duplicate_ptracker_identifier
    INTO
        base.z_form_maternity_visit_encounter 
    FROM 
        base.z_form_maternity_visit_encounter_all a
    INNER JOIN 
        [lake_ptracker].[dbo].[encounter] e 
    ON 
        a.encounter_id_cod=e.encounter_id
    LEFT JOIN
        lake_ptracker.dbo.patient_identifier p_identifier 
    ON 
        a.patient_id_cod = p_identifier.patient_id AND p_identifier.identifier_type= 5 AND p_identifier.voided=0
    

    -- Flag duplicate maternal visits
    EXEC base.sp_form_visit_generic_flag_duplicate_encounter 'ANTENATAL CARD PRESENT','Labor and Delivery', 'z_form_maternity_visit_encounter'

    -- Flag duplicate ptracker ids in the maternal visits
    EXEC base.sp_form_visit_generic_flag_duplicate_ptracker_identifier  'z_form_maternity_visit_encounter'

 
-- $END

SELECT 
    TOP 400 * 
FROM 
    base.z_form_maternity_infant_visit_encounter

SELECT 
    TOP 400 * 
FROM 
    base.z_form_maternity_visit_encounter
