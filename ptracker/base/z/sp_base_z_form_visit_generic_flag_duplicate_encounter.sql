USE [staging_ptracker_test]
GO

CREATE OR ALTER PROCEDURE base.sp_form_visit_generic_flag_duplicate_encounter(@field AS NVARCHAR(255),@visit AS NVARCHAR(255), @table  AS NVARCHAR(MAX)) 
AS
BEGIN
 
    DECLARE @sqlCommand  AS NVARCHAR(MAX);

    /*
        Get columns dynamically for each data type
    */

    SET @sqlCommand = 
                'UPDATE 
                   base.'  + @table +
                ' SET 
                    _has_duplicate_encounter=1 
                FROM 
                    base.' + @table + ' a
                INNER JOIN
                (
                    SELECT
                        a.encounter_id
                    FROM 
                    (
                        SELECT 
                            e.encounter_id,
                            cn_coded.name AS field
                        FROM 
                            lake_ptracker.dbo.encounter e
                            INNER JOIN lake_ptracker.dbo.encounter_type et ON e.encounter_type = et.encounter_type_id AND et.name=''' + @visit + '''
                            INNER JOIN lake_ptracker.dbo.obs o ON e.encounter_id = o.encounter_id
                            left JOIN lake_ptracker.dbo.concept_name cn ON cn.concept_id = o.concept_id AND concept_name_type = ''FULLY_SPECIFIED'' AND locale = ''en''
                            left JOIN lake_ptracker.dbo.concept_name cn_coded ON o.value_coded = cn_coded.concept_id AND cn_coded.concept_name_type = ''FULLY_SPECIFIED'' AND cn_coded.locale = ''en''
                        WHERE 
                            e.voided=0 AND o.voided=0 AND cn.name=''' + @field + '''
                    ) a
                    WHERE field IS NOT NULL
                    GROUP BY 
                        encounter_id
                    HAVING 
                        COUNT(field) > 1
                ) b
                ON
                    a.openmrs_encounter_identifier = b.encounter_id
                '

    EXECUTE(@sqlCommand);

END