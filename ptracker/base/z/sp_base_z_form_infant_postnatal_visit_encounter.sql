USE [staging_ptracker_test]
GO

-- Drop infant postnatal visit staging table
DROP TABLE IF EXISTS base.z_form_infant_postnatal_visit_encounter;

-- Drop infant postnatal visit staging table
DROP TABLE IF EXISTS base.z_form_infant_postnatal_visit_encounter_all;

-- $BEGIN

    -- Populate infant postnatal visit staging table
    EXEC base.sp_form_visit_generic_encounter_all 'Infant Postnatal','z_form_infant_postnatal_visit_encounter_all'

    -- Populate infant postnatal visit table
    SELECT 
        ROW_NUMBER() 
        OVER( 
        PARTITION BY 
            e.patient_id,
            CAST(e.encounter_datetime AS DATE),
            [HIV Exposure Status],
            [ARV Prophylaxis Status],
            [CTX Prophylaxis Status],
            [RETURN VISIT DATE]
        ORDER BY
            pid.identifier  ASC
        ) AS _unique_row_number,
        encounter_id_cod AS openmrs_encounter_identifier,
        pid.identifier AS ptracker_identifier,
        [HIV Exposure Status] AS hiv_exposure,
        [ARV Prophylaxis Status] AS arv_prophylaxis_status,
        [ARV Adherence] AS arv_prophylaxis_adherence,
        [CTX Prophylaxis Status] AS ctx_prophylaxis_status,
        [Cotrimoxazole adherence] AS ctx_prophylaxis_adherence,
        [HIV test performed] AS hiv_test_status,
        [Infant HIV Test] AS hiv_test_type,
        CASE WHEN 
            [Child HIV DNA/PCR test result] = 'POSITIVE'
        THEN
            'Positive'
        WHEN 
            [Child HIV DNA/PCR test result] = 'NEGATIVE'
        THEN
            'Negative'
        WHEN 
            [Child HIV DNA/PCR test result] = 'Results Pending'
        THEN
            'Results Pending'
        WHEN 
            [Child HIV DNA/PCR test result] = 'Missing'
        THEN
            'Missing'
        ELSE
            NULL
        END AS dna_pcr_test_result,
        CASE WHEN 
            [Rapid HIV antibody test result at 18 months of age] = 'POSITIVE'
        THEN
            'Positive'
        WHEN 
            [Rapid HIV antibody test result at 18 months of age] = 'NEGATIVE'
        THEN
            'Negative'
        WHEN 
            [Rapid HIV antibody test result at 18 months of age] = 'Missing'
        THEN
            'Missing'
        ELSE
            NULL
        END AS rapid_test_result,
        [Was a confirmatory test performed on this vist] AS confirmatory_test_done,
        CASE WHEN 
            [Child HIV antibody test result] = 'POSITIVE'
        THEN
            'Positive'
        WHEN 
            [Child HIV antibody test result] = 'NEGATIVE'
        THEN
            'Negative'
        WHEN 
            [Child HIV antibody test result] = 'Results Pending'
        THEN
            'Results Pending'
        ELSE
            NULL
        END AS final_test_result,
        [Linked to ART] AS linked_to_art,
        [Infant feeding method] AS breastfeeding_status,
        [other infant feeding method type (text)] AS other_feeding_method,
        [Missing Other infant feeding method] AS missing_other_feeding_method,
        [Patient outcome status] AS outcome_status,
        [Transferred from location] AS transfer_in_from,
        [Transfer in date] AS transfer_in_date,
        [Missing Transfer in date] AS missing_transfer_in_date,
        [Transferred out to] AS transfer_out_to,
        [Date transferred out] AS transfer_out_to_date,
        [Missing Transfer out date] AS missing_transfer_out_to_date,
        [Date of event] AS event_date,
        [DATE OF DEATH] AS death_date,
        [RETURN VISIT DATE] AS next_visit_date,
        [Missing Next visit date] AS missing_next_visit_date,
        0 AS _has_duplicate_encounter
    INTO 
        base.z_form_infant_postnatal_visit_encounter
    FROM 
        base.z_form_infant_postnatal_visit_encounter_all a
    INNER JOIN 
        [lake_ptracker].[dbo].[encounter] e 
    ON 
        a.encounter_id_cod=e.encounter_id
    INNER JOIN 
        lake_ptracker.dbo.patient pt 
    ON 
        e.patient_id=pt.patient_id
    LEFT JOIN 
        lake_ptracker.dbo.patient_identifier pid 
    ON 
        pt.patient_id = pid.patient_id AND pid.voided=0 AND pid.identifier_type=5

    -- Flag duplicate infant postnatal visits
    EXEC base.sp_form_visit_generic_flag_duplicate_encounter 'HIV Exposure Status','Infant Postnatal', 'z_form_infant_postnatal_visit_encounter'

-- $END

SELECT 
     top 400 * 
FROM 
    base.z_form_infant_postnatal_visit_encounter    