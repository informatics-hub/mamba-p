USE [staging_ptracker_test]
GO


CREATE OR ALTER PROCEDURE base.sp_form_visit_generic_encounter_all(@visit AS NVARCHAR(255), @table  AS NVARCHAR(MAX)) 
AS
BEGIN 

    DECLARE @cols_date AS NVARCHAR(MAX),
        @cols_num AS NVARCHAR(MAX),
        @cols_date_time AS NVARCHAR(MAX),
        @cols_text AS NVARCHAR(MAX),
        @cols_coded AS NVARCHAR(MAX),
        @cols_boolean AS NVARCHAR(MAX),
        @query  AS NVARCHAR(MAX)

    /*
        Get columns dynamically for each data type
    */

    EXEC base.sp_form_visit_generic_columns 'Date',@visit, @cols_date OUTPUT;

    EXEC base.sp_form_visit_generic_columns 'Datetime',@visit, @cols_date_time OUTPUT;

    EXEC base.sp_form_visit_generic_columns 'Text',@visit, @cols_text OUTPUT;

    EXEC base.sp_form_visit_generic_columns 'Coded',@visit, @cols_coded OUTPUT;

    EXEC base.sp_form_visit_generic_columns 'Boolean',@visit, @cols_boolean OUTPUT;

    EXEC base.sp_form_visit_generic_columns 'Numeric',@visit, @cols_num OUTPUT;

    /*
        Generate dynamic query to load patient visits
    */
    SET @query = '
    ;WITH pivot_number AS
    (
        SELECT
        *
        FROM
        (
            SELECT DISTINCT 
                e.patient_id AS patient_id_num, 
                e.encounter_id AS encounter_id_num, 
                cn.name,
                CASE WHEN EXISTS 
                            (
                                SELECT 1
                                FROM lake_ptracker.dbo.concept cn 
                                INNER JOIN lake_ptracker.dbo.concept_datatype d ON cn.datatype_id=d.concept_datatype_id
                                WHERE  d.name=''Numeric''  AND cn.concept_id = o.concept_id
                            )
                THEN 
                    o.value_numeric
                ELSE
                    NULL
                END AS v_num
            FROM
                lake_ptracker.dbo.encounter e
                INNER JOIN lake_ptracker.dbo.encounter_type et ON e.encounter_type = et.encounter_type_id AND et.name='''+@visit+'''
                INNER JOIN lake_ptracker.dbo.obs o ON e.encounter_id = o.encounter_id
                LEFT JOIN lake_ptracker.dbo.concept_name cn ON o.concept_id=cn.concept_id AND cn.voided=0 AND cn.locale=''en'' AND cn.locale_preferred=1
            WHERE 
                o.voided=0 AND e.voided=0
        ) AS P

        PIVOT
        (
        MAX(v_num) FOR [name] IN (' + @cols_num + ')
        ) AS pvt_num
    ),pivot_text AS
    (
        SELECT
        *
        FROM
        (
            SELECT DISTINCT 
                e.patient_id AS patient_id_txt, 
                e.encounter_id AS encounter_id_txt, 
                cn.name,
                CASE WHEN EXISTS 
                            (
                                SELECT 1
                                FROM lake_ptracker.dbo.concept cn 
                                INNER JOIN lake_ptracker.dbo.concept_datatype d ON cn.datatype_id=d.concept_datatype_id
                                WHERE  d.name=''Text''  AND cn.concept_id = o.concept_id
                            )
                THEN 
                    o.value_text
                ELSE
                    NULL
                END AS v_num
            FROM
                lake_ptracker.dbo.encounter e
                INNER JOIN lake_ptracker.dbo.encounter_type et ON e.encounter_type = et.encounter_type_id AND et.name='''+@visit+'''
                INNER JOIN lake_ptracker.dbo.obs o ON e.encounter_id = o.encounter_id
                LEFT JOIN lake_ptracker.dbo.concept_name cn ON o.concept_id=cn.concept_id AND cn.voided=0 AND cn.locale=''en'' AND cn.locale_preferred=1
            WHERE 
                o.voided=0 AND e.voided=0
        ) AS P

        PIVOT
        (
        MAX(v_num) FOR [name] IN (' + @cols_text + ')
        ) AS pvt_num
    ),pivot_coded AS
    (
        SELECT
        *
        FROM
        (
            SELECT DISTINCT 
                e.patient_id AS patient_id_cod, 
                e.encounter_id AS encounter_id_cod, 
                cn.name,
                CASE WHEN EXISTS 
                            (
                                SELECT 1
                                FROM lake_ptracker.dbo.concept cn 
                                INNER JOIN lake_ptracker.dbo.concept_datatype d ON cn.datatype_id=d.concept_datatype_id
                                WHERE  d.name=''Coded''  AND cn.concept_id = o.concept_id
                            )
                THEN 
                    (SELECT [NAME] FROM lake_ptracker.dbo.concept_name WHERE concept_id = o.value_coded AND concept_name_type = ''FULLY_SPECIFIED'' AND locale = ''en'')
                ELSE
                    NULL
                END AS v_coded
            FROM
                lake_ptracker.dbo.encounter e
                INNER JOIN lake_ptracker.dbo.encounter_type et ON e.encounter_type = et.encounter_type_id AND et.name='''+@visit+'''
                INNER JOIN lake_ptracker.dbo.obs o ON e.encounter_id = o.encounter_id
                LEFT JOIN lake_ptracker.dbo.concept_name cn ON o.concept_id=cn.concept_id AND cn.voided=0 AND cn.locale=''en'' AND cn.locale_preferred=1
            WHERE 
                o.voided=0 AND e.voided=0
        ) AS P

        PIVOT
        (
        MAX(v_coded) FOR [name] IN (' + @cols_coded + ')
        ) AS pvt_coded
    ),pivot_boolean AS
    (
        SELECT
        *
        FROM
        (
            SELECT DISTINCT 
                e.patient_id AS patient_id_bol, 
                e.encounter_id AS encounter_id_bol, 
                cn.name,
                CASE WHEN EXISTS 
                            (
                                SELECT 1
                                FROM lake_ptracker.dbo.concept cn 
                                INNER JOIN lake_ptracker.dbo.concept_datatype d ON cn.datatype_id=d.concept_datatype_id
                                WHERE  d.name=''Boolean''  AND cn.concept_id = o.concept_id
                            ) AND o.value_coded = 1
                THEN 
                    (SELECT NAME FROM lake_ptracker.dbo.concept_name WHERE concept_id = o.concept_id AND concept_name_type = ''FULLY_SPECIFIED'' AND locale = ''en'')
                ELSE
                    NULL
                END AS v_boolean
            FROM
                lake_ptracker.dbo.encounter e
                INNER JOIN lake_ptracker.dbo.encounter_type et ON e.encounter_type = et.encounter_type_id AND et.name='''+@visit+'''
                INNER JOIN lake_ptracker.dbo.obs o ON e.encounter_id = o.encounter_id
                LEFT JOIN lake_ptracker.dbo.concept_name cn ON o.concept_id=cn.concept_id AND cn.voided=0 AND cn.locale=''en'' AND cn.locale_preferred=1
            WHERE 
                o.voided=0 AND e.voided=0
        ) AS P

        PIVOT
        (
        MAX(v_boolean) FOR [name] IN (' + @cols_boolean + ')
        ) AS pvt_bool
    ),pivot_datetime AS
    (
        SELECT
        *
        FROM
        (
            SELECT DISTINCT 
                e.patient_id AS patient_id_dat_tim, 
                e.encounter_id AS encounter_id_dat_tim, 
                cn.name,
                CASE WHEN EXISTS 
                            (
                                SELECT 1
                                FROM lake_ptracker.dbo.concept cn 
                                INNER JOIN lake_ptracker.dbo.concept_datatype d ON cn.datatype_id=d.concept_datatype_id
                                WHERE  d.name=''Datetime''  AND cn.concept_id = o.concept_id
                            ) 
                THEN 
                    o.value_datetime
                ELSE
                    NULL
                END AS v_date_time
            FROM
                lake_ptracker.dbo.encounter e
                INNER JOIN lake_ptracker.dbo.encounter_type et ON e.encounter_type = et.encounter_type_id AND et.name='''+@visit+'''
                INNER JOIN lake_ptracker.dbo.obs o ON e.encounter_id = o.encounter_id
                LEFT JOIN lake_ptracker.dbo.concept_name cn ON o.concept_id=cn.concept_id AND cn.voided=0 AND cn.locale=''en'' AND cn.locale_preferred=1
            WHERE 
                o.voided=0 AND e.voided=0
        ) AS P

        PIVOT
        (
        MAX(v_date_time) FOR [name] IN (' + @cols_date_time + ')
        ) AS pvt_bool
    ),pivot_date AS
    (
    SELECT
        *
        FROM
        (
            SELECT DISTINCT 
                e.patient_id AS patient_id_dat, 
                e.encounter_id AS encounter_id_dat, 
                cn.name,
                CASE WHEN EXISTS 
                            (
                                SELECT 1
                                FROM lake_ptracker.dbo.concept cn 
                                INNER JOIN lake_ptracker.dbo.concept_datatype d ON cn.datatype_id=d.concept_datatype_id
                                WHERE  d.name=''Date'' AND cn.concept_id = o.concept_id
                            )
                THEN 
                    o.value_datetime
                ELSE
                    NULL
                END AS v_date
            FROM
                lake_ptracker.dbo.encounter e
                INNER JOIN lake_ptracker.dbo.encounter_type et ON e.encounter_type = et.encounter_type_id AND et.name='''+@visit+'''
                INNER JOIN lake_ptracker.dbo.obs o ON e.encounter_id = o.encounter_id
                LEFT JOIN lake_ptracker.dbo.concept_name cn ON o.concept_id=cn.concept_id AND cn.voided=0 AND cn.locale=''en'' AND cn.locale_preferred=1
            WHERE 
                o.voided=0 AND e.voided=0
        ) AS P

        PIVOT
        (
        MAX(v_date) FOR [name] IN (' + @cols_date + ')
        ) AS pvt_date
    )
    SELECT 
        cd.*,nm.*,dt.*,tx.*,bl.*,dtime.*
    INTO 
        base.'+@table+'
    FROM pivot_coded cd
    LEFT JOIN pivot_datetime dtime ON cd.patient_id_cod = dtime.patient_id_dat_tim AND cd.encounter_id_cod = dtime.encounter_id_dat_tim
    LEFT JOIN pivot_date dt ON cd.patient_id_cod = dt.patient_id_dat AND cd.encounter_id_cod = dt.encounter_id_dat
    LEFT JOIN pivot_text tx ON cd.patient_id_cod = tx.patient_id_txt AND cd.encounter_id_cod = tx.encounter_id_txt
    LEFT JOIN pivot_number nm ON nm.patient_id_num = cd.patient_id_cod AND nm.encounter_id_num = cd.encounter_id_cod
    LEFT JOIN pivot_boolean bl ON cd.patient_id_cod = bl.patient_id_bol AND cd.encounter_id_cod = bl.encounter_id_bol
    '
    EXECUTE(@query);

END