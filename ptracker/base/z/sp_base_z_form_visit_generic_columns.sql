USE [staging_ptracker_test]
GO

 CREATE OR ALTER PROCEDURE base.sp_form_visit_generic_columns(@data_type AS NVARCHAR(255),@visit AS NVARCHAR(255),@cols AS NVARCHAR(MAX) OUTPUT) 
 AS
 BEGIN   
        /*
                Get columns dynamically for each data type
        */
        SELECT @cols = COALESCE(STUFF((SELECT DISTINCT ',' + QUOTENAME(cn.name) 
                                FROM lake_ptracker.dbo.encounter e 
                                INNER JOIN lake_ptracker.dbo.encounter_type et ON e.encounter_type = et.encounter_type_id AND et.name=@visit
                                INNER JOIN lake_ptracker.dbo.obs o ON e.encounter_id = o.encounter_id
                                INNER JOIN lake_ptracker.dbo.concept_name cn ON o.concept_id=cn.concept_id AND cn.voided=0 AND cn.locale='en' AND cn.locale_preferred=1
                                INNER JOIN lake_ptracker.dbo.concept c on c.concept_id=o.concept_id
                                INNER JOIN lake_ptracker.dbo.concept_datatype d ON c.datatype_id=d.concept_datatype_id
                                WHERE  d.name=@data_type
                        FOR XML PATH(''), TYPE
                        ).value('.', 'NVARCHAR(MAX)') 
                ,1,1,''),  CONCAT('[NULL',@data_type,']') )

        RETURN
END
