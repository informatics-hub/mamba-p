USE [staging_ptracker_test]
GO

-- $BEGIN


    PRINT 'Starting execution of ptracker base objects'

    --PRINT ' Dropping tables '
    PRINT 'dbo.sp_xf_system_drop_all_foreign_keys in base'
    EXEC dbo.sp_xf_system_drop_all_foreign_keys_in_schema 'base';
    
    PRINT 'dbo.sp_xf_system_drop_all_tables in base'
    EXEC dbo.sp_xf_system_drop_all_tables_in_schema 'base';

    -- dimensions
    EXEC base.sp_dim_date;
    EXEC base.sp_dim_facility;
    EXEC base.sp_dim_client;
    EXEC base.sp_dim_encounter;
    EXEC base.sp_dim_indicator;
    EXEC base.sp_dim_visual;

    -- facts
    -- z tables
    EXEC base.sp_z_form_antenatal_visit_encounter;
    EXEC base.sp_z_form_maternity_visit_encounter;
    EXEC base.sp_z_form_infant_postnatal_visit_encounter;
    EXEC base.sp_z_form_mother_postnatal_visit_encounter;

    --fact tables
    EXEC base.sp_fact_antenatal_visit;
    EXEC base.sp_fact_infant_postnatal_visit;
    EXEC base.sp_fact_maternity_infant_visit;
    EXEC base.sp_fact_maternity_visit;
    EXEC base.sp_fact_mother_postnatal_visit;
    EXEC base.sp_fact_facility_dhis_data;
    EXEC base.sp_fact_visual_access;

    PRINT 'Completed execution of ptracker base objects'

-- $END