/*
Version: 	1.0
Author:		Ezekiel K. Chiteri
Date:		20 Mar 2018

Change log:
Date:		20 Mar 2018
Change log:
Date: 03 April 2018
View prefix name change
*/ 

DROP VIEW
IF
	EXISTS ds_repeat_visit;
CREATE VIEW ds_repeat_visit AS 
SELECT  encounter_id, patient_id, encounter_datetime, ptracker_id, visit_id, DuplicateSR_No
FROM
        (
            select  encounter_id, patient_id,
										encounter_datetime,
                    ptracker_id,
                    visit_id,
                    @sum := if(@ptr_id = ptracker_id and @pt_id !=  ptracker_id, @sum ,0) + 1 DuplicateSR_No,
                    @ptr_id := ptracker_id,
                    @pt_id := patient_id,
										@encdate := encounter_datetime
            from    ds_revised_encounter_mch,
                    (select @ptr_id := '', @sum := 0, @pt_id := '') vars
										-- group by visit_id
										where encounter_datetime is not null
            order   by  ptracker_id
        ) s
														GROUP BY visit_id
ORDER   BY ptracker_id