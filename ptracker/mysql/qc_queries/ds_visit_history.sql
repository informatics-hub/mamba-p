/*
Version: 	1.0
Author:		Ezekiel K. Chiteri
Date:		08 Feb 2018

Change log:
Date: 03 April 2018
View prefix name change
*/ 
CREATE VIEW ds_visit_history AS SELECT
patient_id,
ptracker_id,
given_name,
middle_name,
family_name,
gender,
visit_type_code AS current_visit_type_code,
encounter_datetime AS current_encounter_datetime,
( SELECT visit_type_code FROM vw_encounter_mch e2 WHERE e2.visit_type_code < e1.visit_type_code ORDER BY visit_type_code DESC LIMIT 1 ) AS previous_visit_type_code,
( SELECT encounter_datetime FROM vw_encounter_mch e2 WHERE e2.visit_type_code < e1.visit_type_code ORDER BY visit_type_code DESC LIMIT 1 ) AS previous_encounter_datetime,
( SELECT visit_type_code FROM vw_encounter_mch e3 WHERE e3.visit_type_code > e1.visit_type_code ORDER BY visit_type_code ASC LIMIT 1 ) AS next_visit_type_code,
( SELECT encounter_datetime FROM vw_encounter_mch e3 WHERE e3.visit_type_code > e1.visit_type_code ORDER BY visit_type_code ASC LIMIT 1 ) AS next_encounter_datetime
FROM
	vw_encounter_mch e1 
WHERE
	visit_uuid IN ( '2549af50-75c8-4aeb-87ca-4bb2cef6c69a', '269bcc7f-04f8-4ddc-883d-7a3a0d569aad', '2678423c-0523-4d76-b0da-18177b439eed' ) 
GROUP BY
patient_id