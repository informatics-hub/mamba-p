/*
Version: 	1.0
Author:		Ezekiel K. Chiteri
Date:		12 Sept 2017

Change log:
12 Sept 2017
Creation

Change log:
Author:		Echiteri
Date:		1 April 2020
Added: Added filter parameters
*/ 
SELECT
	region,
	district,
	encounter_facility,
	encounter_datetime,
	ptracker_id,
	hiv_test_status,
	recent_viral_load,
	viral_load_test_date,
	viral_load_results
FROM
mds_encounter_mch
					WHERE $X{IN, encounter_facility, encounter_facility} 
					AND
					(
			          (encounter_datetime BETWEEN $P{FromDate} AND $P{ToDate})
			          OR
			          ( ($P{FromDate} = '' OR $P{FromDate} IS NULL) or ($P{ToDate} = '' OR $P{ToDate} IS NULL) )
			       )
			       AND $X{IN, visit_type, visit_type}
			       AND recent_viral_load = "Yes"
					AND viral_load_test_date IS NULL
			       AND voided = 0 
GROUP BY
	ptracker_id;