/*
Version: 	1.0
Author:		Asen Mwandemele
Date:		01 Oct 2020
Description: Count all records recored by Ptracker data clerks


SELECT * FROM users
SELECT * FROM provider 
SELECT * FROM encounter_provider 
SELECT * FROM encounter


*/ 


DROP TABLE IF EXISTS `ds_data_clerks_records_count_by_month`;

CREATE TABLE IF NOT EXISTS ds_data_clerks_records_count_by_month AS 
SELECT 
	u.user_id,
	u.person_id,
	pr.provider_id,
	u.date_created,
    (SELECT 
           region 
        FROM 
            ds_data_clerks_list 
        WHERE 
            user_id = u.user_id
    )  as region,
    (SELECT 
           district 
        FROM 
            ds_data_clerks_list 
        WHERE 
            user_id = u.user_id
    )  as district,
    (SELECT 
           duty_station 
        FROM 
            ds_data_clerks_list 
        WHERE 
            user_id = u.user_id
    )  as duty_station,
    pn.given_name,
    pn.family_name,
	u.username,
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'all',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            YEAR(encounter.date_created)='2020'                    
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS ytd,
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            MONTH(encounter.date_created)='1'
        AND
            YEAR(encounter.date_created)='2020'                          
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS Jan,
	(
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            MONTH(encounter.date_created)='2'                    
        AND
            YEAR(encounter.date_created)='2020'
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS Feb,
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            MONTH(encounter.date_created)='3' 
        AND
            YEAR(encounter.date_created)='2020'                       
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS Mar,
	(
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            MONTH(encounter.date_created)='4'                    
        AND
            YEAR(encounter.date_created)='2020'
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS Apr,
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            MONTH(encounter.date_created)='5'                    
        AND
            YEAR(encounter.date_created)='2020'
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS May,
	(
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            MONTH(encounter.date_created)='6'                    
        AND
            YEAR(encounter.date_created)='2020'
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS Jun,
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            MONTH(encounter.date_created)='7'                    
        AND
            YEAR(encounter.date_created)='2020'
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS Jul,
	(
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            MONTH(encounter.date_created)='8'                    
        AND
            YEAR(encounter.date_created)='2020'
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS Aug,
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            MONTH(encounter.date_created)='9'                    
        AND
            YEAR(encounter.date_created)='2020'
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS Sep,
	(
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            MONTH(encounter.date_created)='10'                    
        AND
            YEAR(encounter.date_created)='2020'
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS Oct,
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            MONTH(encounter.date_created)='11'                    
        AND
            YEAR(encounter.date_created)='2020'
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS Nov,
	(
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            MONTH(encounter.date_created)='12'                    
        AND
            YEAR(encounter.date_created)='2020'
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Dec',
    u.retired   
FROM 
	users u
LEFT JOIN 
	provider pr
ON 
	u.person_id = pr.person_id
LEFT JOIN 
	person_name pn
ON
    u.person_id	= pn.person_id
WHERE 
    pr.provider_id IS NOT NULL
AND 
    u.user_id IN (
                    SELECT 
                        user_id 
                    FROM 
                        ds_data_clerks_list 
                    WHERE 
                    user_id IS NOT NULL
    )    