SELECT
	region,
	district,
	facility,
	COUNT(IF(hiv_test_status = 'Previously known positive', encounter_id, NULL )) AS total_kp,
	COUNT(IF(art_number IS NULL AND hiv_test_status = 'Previously known positive', encounter_id, NULL )) AS missing_art_number,
	COUNT(IF(art_start_date IS NULL AND hiv_test_status = 'Previously known positive', encounter_id, NULL )) AS missing_art_start_date,
	COUNT(IF(recent_viral_load = 'Yes', encounter_id, NULL )) AS total_recent_viral_load_done,
	COUNT(IF(recent_viral_load = 'Yes' AND viral_load_test_date IS NULL, encounter_id, NULL )) AS missing_viral_load_date,
	COUNT(IF(recent_viral_load = 'Yes' AND viral_load_results IS NULL , encounter_id, NULL )) AS missing_viral_load_result,
	COUNT(IF(anc_visit_type = 'New ANC Visit', encounter_id, NULL )) AS total_new_pregnancies,
	-- COUNT(IF(anc_visit_type = 'New ANC Visit' AND (hiv_test_status IS NULL OR hiv_test_result) IS NULL , encounter_id, NULL )) AS missing_hiv_stauts_and_results_first_anc,
	COUNT(IF(anc_visit_type = 'New ANC Visit' AND gravida IS NULL , encounter_id, NULL )) AS missing_gravida,
	COUNT(IF(anc_visit_type = 'New ANC Visit' AND para IS NULL , encounter_id, NULL )) AS missing_para,
	COUNT(IF(visit_uuid = '269bcc7f-04f8-4ddc-883d-7a3a0d569aad', encounter_id, NULL )) AS total_mother_pnc,
	COUNT(IF( patient_id NOT IN (SELECT mother_id FROM mds_encounter_mch WHERE visit_uuid = 'af1f1b24-d2e8-4282-b308-0bf79b365584' AND mother_id IS NOT NULL) AND visit_uuid = '269bcc7f-04f8-4ddc-883d-7a3a0d569aad', encounter_id, NULL )) AS mother_pnc_not_linked_to_infants,
	COUNT(IF(visit_uuid = 'af1f1b24-d2e8-4282-b308-0bf79b365584', encounter_id, NULL )) AS total_infant_pnc,
	COUNT(IF(visit_uuid = 'af1f1b24-d2e8-4282-b308-0bf79b365584' AND hiv_test_status = 'Tested for HIV during this visit' AND ROUND(DATEDIFF(encounter_datetime, birthdate)/7)<=6, encounter_id, NULL )) AS tested_infant_at_six_weeks,
	COUNT(IF(visit_uuid = 'af1f1b24-d2e8-4282-b308-0bf79b365584' AND hiv_test_status = 'Tested for HIV during this visit' AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43) BETWEEN 2 AND 9, encounter_id, NULL )) AS tested_infant_at_nine_months,
	COUNT(IF(visit_uuid = 'af1f1b24-d2e8-4282-b308-0bf79b365584' AND hiv_test_status = 'Tested for HIV during this visit' AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43) BETWEEN 9 AND 18, encounter_id, NULL )) AS tested_infant_at_eighteen_months
FROM
mds_encounter_mch
WHERE visit_uuid IN ('269bcc7f-04f8-4ddc-883d-7a3a0d569aad','af1f1b24-d2e8-4282-b308-0bf79b365584' )
	GROUP BY
facility;

/* select hiv_test_status, hiv_test_result, mother_id, ptracker_id, visit_type from mds_encounter_mch where hiv_test_result is null and hiv_test_status = 'Tested for HIV during this visit' */
SELECT
	region,
	district,
	facility,
	COUNT(IF(visit_uuid = '269bcc7f-04f8-4ddc-883d-7a3a0d569aad', encounter_id, NULL )) AS total_mother_pnc,
	COUNT(IF( patient_id NOT IN (SELECT mother_id FROM mds_encounter_mch WHERE visit_uuid = 'af1f1b24-d2e8-4282-b308-0bf79b365584' AND mother_id IS NOT NULL) AND visit_uuid = '269bcc7f-04f8-4ddc-883d-7a3a0d569aad', encounter_id, NULL )) AS mother_pnc_not_linked_to_infants,
	COUNT(IF(visit_uuid = 'af1f1b24-d2e8-4282-b308-0bf79b365584', encounter_id, NULL )) AS total_infant_pnc,
	COUNT(IF(visit_uuid = 'af1f1b24-d2e8-4282-b308-0bf79b365584' AND hiv_test_status = 'Tested for HIV during this visit' AND ROUND(DATEDIFF(encounter_datetime, birthdate)/7)<=6, encounter_id, NULL )) AS tested_infant_at_six_weeks,
	COUNT(IF(visit_uuid = 'af1f1b24-d2e8-4282-b308-0bf79b365584' AND hiv_test_status = 'Tested for HIV during this visit' AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43) BETWEEN 2 AND 9, encounter_id, NULL )) AS tested_infant_at_nine_months,
	COUNT(IF(visit_uuid = 'af1f1b24-d2e8-4282-b308-0bf79b365584' AND hiv_test_status = 'Tested for HIV during this visit' AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43) BETWEEN 9 AND 18, encounter_id, NULL )) AS tested_infant_at_eighteen_months
FROM
mds_encounter_mch
			       AND voided = 0 
			       AND visit_uuid IN ('2549af50-75c8-4aeb-87ca-4bb2cef6c69a', '2678423c-0523-4d76-b0da-18177b439eed', '269bcc7f-04f8-4ddc-883d-7a3a0d569aad' )
GROUP BY
	facility;
	
	-- for data quality check and inform on facility for supervision prioritization -- 
	SELECT
	region,
	district,
	facility,
	COUNT(DISTINCT(IF(hiv_test_status = 'Previously known positive', encounter_uuid, NULL))) AS total_kp,
	COUNT(DISTINCT(IF(art_number IS NULL AND hiv_test_status = 'Previously known positive', encounter_uuid, NULL))) AS missing_art_number,
	COUNT(DISTINCT(IF(art_start_date IS NULL AND hiv_test_status = 'Previously known positive', encounter_uuid, NULL))) AS missing_art_start_date,
	COUNT(DISTINCT(IF(recent_viral_load = 'Yes', encounter_uuid, NULL))) AS total_recent_viral_load_done,
	COUNT(DISTINCT(IF(recent_viral_load = 'Yes' AND viral_load_test_date IS NULL, encounter_uuid, NULL))) AS missing_viral_load_date,
	COUNT(DISTINCT(IF(recent_viral_load = 'Yes' AND viral_load_results IS NULL , encounter_uuid, NULL))) AS missing_viral_load_result,
	COUNT(DISTINCT(IF(anc_visit_type = 'New ANC Visit', encounter_uuid, NULL))) AS total_new_pregnancies,
	COUNT(DISTINCT(IF(anc_visit_type = 'New ANC Visit' AND gravida IS NULL , encounter_uuid, NULL))) AS missing_gravida,
	COUNT(DISTINCT(IF(anc_visit_type = 'New ANC Visit' AND para IS NULL , encounter_uuid, NULL))) AS missing_para,
	COUNT(DISTINCT(IF(anc_visit_type = 'New ANC Visit' AND hiv_test_status = 'Missing', encounter_uuid, NULL))) AS missing_hiv_status_for_first_anc,
	COUNT(DISTINCT(IF(anc_visit_type = 'New ANC Visit' AND hiv_test_result = 'Missing', encounter_uuid, NULL))) AS missing_hiv_result_for_first_anc
FROM
mds_encounter_mch
					WHERE  voided = 0 
					AND encounter_datetime BETWEEN '2018-06-01' AND '2018-08-31'
			       AND visit_uuid IN ('2549af50-75c8-4aeb-87ca-4bb2cef6c69a', '2678423c-0523-4d76-b0da-18177b439eed', '269bcc7f-04f8-4ddc-883d-7a3a0d569aad' )
GROUP BY
	facility
	ORDER BY
missing_art_number DESC, missing_art_start_date DESC;