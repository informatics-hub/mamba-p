
SELECT
ptracker_id AS PtrackerID,
encounter_facility AS Facility,
mfl_code AS Faciliy_Code,
district AS District,
encounter_datetime AS Visit_Date,
DATE_FORMAT(encounter_datetime,'%M') AS Visit_Month,
YEAR(CURDATE()) - YEAR(birthdate) AS Age,
visit_type AS Modality,
hiv_test_result as HIV_result,
IF 
	(encounter_facility IN 
		('Engela Clinic',
		'Engela District Hospital',
		'Hamukoto Wakapa Clinic',
		'Odibo Health Centre',
		'Ohangwena Clinic',
		'Okatope Clinic',
		'Omungwelume Clinic',
		'Ondobe Clinic',
		'Ongenga Clinic',
		'Ongha Health Centre',
		'Bukalo Health Centre',
		'Katima District Hospital',
		'Katima Mulilo Clinic',
		'Mafuta Clinic',
		'Mavuluma Clinic',
		'Ngweze Clinic',
		'Sangwali Health Centre',
		'Sesheke Clinic',
		'Sibbinda Health Centre',
		'Okankolo Health Centre',
		'Olukonda Clinic',
		'Oshigambo Clinic',
		'Onandjokwe District Hospital',
		'Onyaanya Health Centre',
		'Eluwa Clinic',
		'Okatana Health Centre',
		'Ondangwa Health Centre',
		'Ongwediva Health Centre',
		'Oshakati District Hospital',
		'Oshakati Health Centre',
		'Ou Nick Health Centre',
		'Donkerhoek Clinic',
		'Hakahana Clinic',
		'Katutura District Hospital',
		'Katutura Health Centre',
		'Khomasdal Health Centre',
		'Maxuilili Clinic',
		'Okuryangava Clinic',
		'Otjomuise Clinic',
		'Robert Mugabe Clinic',
		'Wanaheda Clinic',
		'Windhoek Central Hospital'),
	'YES', 'NO')  
AS Recency_Facility
FROM
mds_encounter_mch
WHERE
encounter_facility IS NOT NULL
AND
hiv_test_result IS NOT NULL
GROUP BY patient_id;