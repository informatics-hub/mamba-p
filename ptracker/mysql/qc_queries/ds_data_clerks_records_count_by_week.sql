/*
Version: 	1.0
Author:		Asen Mwandemele
Date:		06 Oct 2020
Description: Count all records recored by Ptracker data clerks and arrange by week

*/ 


DROP TABLE IF EXISTS `ds_data_clerks_records_count_by_week`;

CREATE TABLE IF NOT EXISTS ds_data_clerks_records_count_by_week AS 
SELECT 
	u.user_id,
	u.person_id,
	pr.provider_id,
	u.date_created,
    (SELECT 
           region 
        FROM 
            ds_data_clerks_list 
        WHERE 
            user_id = u.user_id
    )  as region,
    (SELECT 
           district 
        FROM 
            ds_data_clerks_list 
        WHERE 
            user_id = u.user_id
    )  as district,
    (SELECT 
           duty_station 
        FROM 
            ds_data_clerks_list 
        WHERE 
            user_id = u.user_id
    )  as duty_station,
    pn.given_name,
    pn.family_name,
	u.username,
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                      ) 
    ) AS 'all',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            YEAR(encounter.date_created)='2020'                                               
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                      ) 
    ) AS 'ytd', 
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created) = WEEK(CURDATE())                           
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Current Week',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='1'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 1',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='2'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 2',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='3'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 3',
       (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='4'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 4',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='5'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 5',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='6'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 6',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='7'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 7',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='8'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 8',
       (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='9'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 9',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='10'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 10', 
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='11'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 11',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='12'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 12',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='13'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 13',
       (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='14'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 14',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='15'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 15',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='16'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 16',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='17'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 17',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='18'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 18',
       (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='19'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 19',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='20'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 20',                              
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='21'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 21',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='22'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 22',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='23'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 23',
       (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='24'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 24',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='25'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 25',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='26'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 26',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='27'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 27',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='28'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 28',
       (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='29'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 29',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='30'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 30', 
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='31'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 31',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='32'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 32',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='33'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 33',
       (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='34'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 34',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='35'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 35',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='36'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 36',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='37'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 37',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='38'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 38',
       (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='39'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 39',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='40'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 40',                              
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='41'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 41',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='42'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 42',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='43'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 43',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='44'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 44',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='45'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 45',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='46'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 46',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='47'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 47',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='48'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 48',
       (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='49'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 49',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='50'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 50', 
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='51'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 51', 
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            WEEK(encounter.date_created)='52'                     
        AND
            YEAR(encounter.date_created)='2020'                            
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'Week 52',        
    u.retired   
FROM 
	users u
LEFT JOIN 
	provider pr
ON 
	u.person_id = pr.person_id
LEFT JOIN 
	person_name pn
ON
    u.person_id	= pn.person_id
WHERE 
    pr.provider_id IS NOT NULL
AND 
    u.user_id IN (
                    SELECT 
                        user_id 
                    FROM 
                        ds_data_clerks_list 
                    WHERE 
                    user_id IS NOT NULL
    )     