/*
Version: 	1.0
Author:		Ezekiel K. Chiteri
Date:		12 Sept 2017

Change log:
Author:		Echiteri
Date:		1 April 2020
Added: Creation
*/ 

-- Duplicate  ptracker_id ---
	SELECT
	region,
	district,
	encounter_facility,
	patient_id,
	ptracker_id,
	NULL AS given_name,
	NULL AS middle_name,
	NULL AS family_name,
	NULL AS gender,
	NULL AS birthdate,
	NULL AS gravida,
	NULL AS para,
	NULL AS art_number,
	NULL AS art_number_count,
	COUNT( ptracker_id ) ptracker_id_count,
	NULL AS demographic_count,
	patient_identifier_voided
FROM
	mds_encounter_mch 
	WHERE patient_identifier_voided = 0
GROUP BY
	ptracker_id
HAVING
	 ptracker_id_count > 1 
AND $X{IN, encounter_facility, encounter_facility}
   AND COUNT(DISTINCT patient_id) > 1
  
	UNION
	-- Duplicate ART number --
SELECT
	region,
	district,
	encounter_facility,
	patient_id,
	ptracker_id,
	given_name,
	middle_name,
	family_name,
	NULL AS gender,
	NULL AS birthdate,
	NULL AS gravida,
	NULL AS para,
	art_number,
	COUNT( art_number ) art_number_count,
	NULL AS ptracker_id_count,
	NULL AS demographic_count,
	patient_identifier_voided
FROM
	mds_encounter_mch
WHERE patient_identifier_voided = 0
GROUP BY
	art_number
HAVING
	art_number_count > 1
	AND $X{IN, encounter_facility, encounter_facility}
    AND COUNT(DISTINCT patient_id) > 1
	
UNION 
	-- Duplicates by Name, DOB, Gravida & Para --
SELECT
	region,
	district,
	encounter_facility,
	patient_id,
	ptracker_id,
	given_name,
	middle_name,
	family_name,
	gender,
	birthdate,
	gravida,
	para,
	NULL AS art_number,
	NULL AS art_number_count,
	NULL AS ptracker_id_count,
	COUNT(patient_id) AS demographic_count,
	patient_identifier_voided
FROM
	mds_encounter_mch 
	WHERE patient_identifier_voided = 0
GROUP BY
	ptracker_id
HAVING
	COUNT(given_name) > 1 
	AND COUNT(middle_name)  > 1 
	AND COUNT(family_name)  > 1 
	AND COUNT(gender)  > 1 
	AND COUNT(birthdate)  > 1 
	AND (gravida)  > 1 
	AND COUNT(para)  > 1
	AND $X{IN, encounter_facility, encounter_facility} 
	AND COUNT(DISTINCT patient_id) > 1