/*
Version: 	1.0
Author:		Asen Mwandemele
Date:		06 Oct 2020
Description: A table listing all data clerks and their details (provided by Dr Agabu September 2020)

*/ 


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table ds_data_clerks_list
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ds_data_clerks_list`;

CREATE TABLE `ds_data_clerks_list` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `username` text,
  `given_name` text,
  `family_name` text,
  `region` text,
  `district` text,
  `duty_station` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ds_data_clerks_list` WRITE;
/*!40000 ALTER TABLE `ds_data_clerks_list` DISABLE KEYS */;

INSERT INTO `ds_data_clerks_list` (`id`, `user_id`, `username`, `given_name`, `family_name`, `region`, `district`, `duty_station`)
VALUES
	(1,1923,'mkakonya','Mekulilo','Kakonya','Oshana','Oshakati','Oshakati Intermediate Hospital'),
	(2,1918,'tshimooshili','Tobias','Shimooshili','Oshikoto','Omuthiya','Omuthiya clinic'),
	(3,1921,'jkanjaa','Jackson','Kanjaa','Otjozondjupa','Grootfontein','Grootfontein District Hospital'),
	(4,1927,'rsimasiku','Rosalia','Simasiku','Hardap','Mariental','Mariental District Hospital'),
	(5,1926,'sshalimba','Shalimba','Shalimba','Otjozondjupa','Otjiwarongo','Otjiwarongo district Hospital'),
	(6,1924,'famesho','Foibe','Amesho','Erongo','Swakopmund','Swakopmund District Hospital'),
	(7,1111,'vjoseph','Victoria','Josef','Karas','Luderitz','Luderitz District Hospital'),
	(8,1919,'tmuma','Tuwilika','Muma','Ohangwena','Eenhana','Eenhana District Hospital'),
	(9,1401,'tantonius','Tuhafeni','Antonius','Kavango','Rundu','Rundu Intermediate hospital'),
	(10,1922,'singo','Selma','Ingo','Kavango','Nyangana','Nyangana District Hospital'),
	(11,1935,'sdaniel2','Secilie','Daniel','Khomas','Windhoek','Windhoek Central Hospital'),
	(12,1402,'rkaisala','Rosina','Kaisala','Khomas','Katutura','Katutura Intermediate Hospital'),
	(13,1920,'gpeelo','George','Peelo','Kunene','Opuwo','Opuwo District Hospital'),
	(14,1112,'nkafe','Ndapewa','Kafe','Khomas','Windhoek','Okuryangava clinic'),
	(15,1561,'snanghanda','Setson','Nanghanda','Ohangwena','Engela','Engela district Hospital'),
	(16,1925,'rnambahu','Rauha','Nambahu','Zambezi','Katima Mulilo','Katima Mulilo District Hospital'),
	(17,1110,'vmwanyangapo','Veronica','Mwanyangapo','Oshana','Ondangwa','Ondangwa Health Centre');

/*!40000 ALTER TABLE `ds_data_clerks_list` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
