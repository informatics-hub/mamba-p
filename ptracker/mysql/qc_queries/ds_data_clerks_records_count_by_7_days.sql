/*
Version: 	1.0
Author:		Asen Mwandemele
Date:		01 Oct 2020
Description: Count all records recored by Ptracker data clerks by 7 days

*/ 


DROP TABLE IF EXISTS `ds_data_clerks_records_count_by_7_days`;

CREATE TABLE IF NOT EXISTS ds_data_clerks_records_count_by_7_days AS 
SELECT 
	u.user_id,
	u.person_id,
	pr.provider_id,
	u.date_created,
    (SELECT 
           region 
        FROM 
            ds_data_clerks_list 
        WHERE 
            user_id = u.user_id
    )  as region,
    (SELECT 
           district 
        FROM 
            ds_data_clerks_list 
        WHERE 
            user_id = u.user_id
    )  as district,
    (SELECT 
           duty_station 
        FROM 
            ds_data_clerks_list 
        WHERE 
            user_id = u.user_id
    )  as duty_station,
    pn.given_name,
    pn.family_name,
	u.username,
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS 'all',
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            YEAR(encounter.date_created)='2020'                    
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS ytd,
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            encounter.date_created= CURDATE()                      
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS Today,
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            DATE_FORMAT(encounter.date_created , "%M %d %Y") = DATE_FORMAT(CURDATE() - INTERVAL 1 DAY, "%M %d %Y")                        
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS Yesterday,
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            DATE_FORMAT(encounter.date_created , "%M %d %Y") = DATE_FORMAT(CURDATE() - INTERVAL 2 DAY, "%M %d %Y")                        
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS '2 days ago', 
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            DATE_FORMAT(encounter.date_created , "%M %d %Y") = DATE_FORMAT(CURDATE() - INTERVAL 3 DAY, "%M %d %Y")                        
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS '3 days ago', 
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            DATE_FORMAT(encounter.date_created , "%M %d %Y") = DATE_FORMAT(CURDATE() - INTERVAL 4 DAY, "%M %d %Y")                        
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS '4 days ago', 
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            DATE_FORMAT(encounter.date_created , "%M %d %Y") = DATE_FORMAT(CURDATE() - INTERVAL 5 DAY, "%M %d %Y")                        
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS '5 days ago', 
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            DATE_FORMAT(encounter.date_created , "%M %d %Y") = DATE_FORMAT(CURDATE() - INTERVAL 6 DAY, "%M %d %Y")                        
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS '6 days ago', 
    (
        SELECT 
            count(*) 
        FROM 
            encounter 
        WHERE 
            DATE_FORMAT(encounter.date_created , "%M %d %Y") = DATE_FORMAT(CURDATE() - INTERVAL 7 DAY, "%M %d %Y")                        
        AND   
            encounter.encounter_id IN (
                                        SELECT 
                                            encounter_id 
                                        FROM 
                                            encounter_provider 
                                        WHERE 
                                            provider_id = pr.provider_id
                                        ) 
    ) AS '7 days ago',                     
    u.retired   
FROM 
	users u
LEFT JOIN 
	provider pr
ON 
	u.person_id = pr.person_id
LEFT JOIN 
	person_name pn
ON
    u.person_id	= pn.person_id
WHERE 
    pr.provider_id IS NOT NULL
AND 
    u.user_id IN (
                    SELECT 
                        user_id 
                    FROM 
                        ds_data_clerks_list 
                    WHERE 
                    user_id IS NOT NULL
    )    