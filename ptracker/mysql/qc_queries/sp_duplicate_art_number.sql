
/*
Version: 	1.0
Author:		Ezekiel K. Chiteri
Date:		12 Sept 2017

Change log:
12 Sept 2017
Creation
*/
SELECT
	patient_id,
	art_number,
	COUNT( art_number ) artnumber
FROM
	mds_encounter_mch
GROUP BY
	patient_id
HAVING
	artnumber > 1
	AND art_number IS NOT NULL
	AND patient_id <> patient_id