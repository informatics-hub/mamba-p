/*
 Duplicate report
*/

-- Duplicate ART Number ---
SELECT
	patient_id,
	ptracker_id,
	NULL AS given_name,
	NULL AS middle_name,
	NULL AS family_name,
	NULL AS gender,
	NULL AS birthdate,
	NULL AS gravida,
	NULL AS para,
	art_number,
	COUNT( art_number ) art_number_count,
	NULL AS ptracker_id_count,
	NULL AS demographic_count
FROM
	mds_encounter_mch
GROUP BY
	patient_id
HAVING
	art_number_count > 1
	-- AND art_number IS NOT NULL
	AND COUNT(DISTINCT ptracker_id) > 1
	
	UNION
	
	-- Duplicate  ptracker_id ---
	SELECT
	patient_id,
	ptracker_id,
	NULL AS given_name,
	NULL AS middle_name,
	NULL AS family_name,
	NULL AS gender,
	NULL AS birthdate,
	NULL AS gravida,
	NULL AS para,
	art_number,
	NULL AS art_number_count,
	COUNT( ptracker_id ) ptracker_id_count,
	NULL AS demographic_count
FROM
	mds_encounter_mch 
GROUP BY
	patient_id
HAVING
	ptracker_id_count > 1 
	AND COUNT(DISTINCT ptracker_id) > 1
	
	UNION 
	
	-- Duplicates by Name, DOB, Gravida & Para --
	SELECT
	patient_id,
	ptracker_id,
	given_name,
	middle_name,
	family_name,
	gender,
	birthdate,
	gravida,
	para,
	art_number,
	NULL AS art_number_count,
	NULL AS ptracker_id_count,
	COUNT(patient_id) AS demographic_count 
FROM
	mds_encounter_mch 
GROUP BY
	patient_id 
HAVING
	COUNT(given_name) > 1 AND
	COUNT(middle_name)  > 1 AND
	COUNT(family_name)  > 1 AND
	COUNT(gender)  > 1 AND
	COUNT(birthdate)  > 1 AND
	COUNT(gravida)  > 1 AND
	COUNT(para)  > 1 AND
	COUNT(DISTINCT ptracker_id) > 1
	
	Select * FROM mds_encounter_mch
GROUP BY birthdate
Having COUNT(birthdate) > 1 AND COUNT(distinct patient_id) > 1
order by birthdate

