
/*
Version: 	1.0
Author:		Ezekiel K. Chiteri
Date:		12 Sept 2017

Change log:
12 Sept 2017
Creation
*/
SELECT
	patient_id,
	ptracker_id,
	COUNT( ptracker_id ) ID 
FROM
	mds_encounter_mch 
GROUP BY
	patient_id 
HAVING
	ID > 1 
	AND patient_id <> patient_id