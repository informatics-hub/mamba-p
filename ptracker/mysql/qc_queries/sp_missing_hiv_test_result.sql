/*
Version: 	1.0
Author:		Ezekiel K. Chiteri
Date:		12 Sept 2017

Change log:
12 Sept 2017
Creation
*/ 
SELECT
patient_id,
given_name,
family_name,
(
	CASE
			visit_uuid 
			WHEN '2549af50-75c8-4aeb-87ca-4bb2cef6c69a' THEN
			'ANC' 
			WHEN '2678423c-0523-4d76-b0da-18177b439eed' THEN
			'Labor and Delivery' 
			WHEN '269bcc7f-04f8-4ddc-883d-7a3a0d569aad' THEN
			'Mother PNC' 
			WHEN 'af1f1b24-d2e8-4282-b308-0bf79b365584' THEN
			'Infant PNC' ELSE '' 
		END 
		) AS visit_type,
		facility,
		hiv_test_status,
		hiv_test_result 
	FROM
		mds_encounter_mch 
	GROUP BY
		patient_id 
	HAVING
	hiv_test_status = 'Tested for HIV during this visit'
	AND hiv_test_result IS NULL
ORDER BY
	facility