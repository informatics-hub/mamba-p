-- Next appointment
SELECT
       ptracker_id,
       given_name,
       family_name,
       encounter_datetime,
       next_visit_date,
       (ROUND(DATEDIFF(next_visit_date, current_date))) AS days,
       DATE_ADD(NOW(), INTERVAL 0 DAY) AS first_day,
       DATE_ADD(NOW(), INTERVAL 5 DAY) AS last_day
FROM
     mds_encounter_mch
WHERE
    next_visit_date BETWEEN DATE_ADD(NOW(), INTERVAL 0 DAY) AND DATE_ADD(NOW(), INTERVAL  DAY)
AND
    (ROUND(DATEDIFF(next_visit_date, current_date))) <= 0;

-- Next appointment (IMPROVED)
SELECT
    ptracker_id,
    given_name,
    family_name,
    encounter_datetime,
    next_visit_date,
    (ROUND(DATEDIFF(next_visit_date, current_date))) AS days,
    DATE_ADD(NOW(), INTERVAL 0 DAY) AS first_day,
    DATE_ADD(NOW(), INTERVAL 5 DAY) AS last_day
FROM
    mds_encounter_mch
WHERE
    next_visit_date BETWEEN NOW() AND '2020-08-24';

-- Missed appointment this week
SELECT
    DISTINCT ptracker_id,
    given_name,
    family_name,
    encounter_datetime,
    next_visit_date,
    (ROUND(DATEDIFF(next_visit_date, current_date))) AS days,
    DATE_ADD(NOW(), INTERVAL 0 DAY) AS first_day,
    DATE_ADD(NOW(), INTERVAL 5 DAY) AS last_day
FROM
    mds_encounter_mch
WHERE
    (ROUND(DATEDIFF(next_visit_date, current_date))) < 0
AND
    next_visit_date BETWEEN '2020-08-19' AND NOW()
ORDER BY encounter_datetime DESC;
-- Without missed appointment
SELECT
    DISTINCT ptracker_id,
    given_name,
    family_name,
    encounter_datetime,
    next_visit_date,
    (ROUND(DATEDIFF(next_visit_date, encounter_datetime))) AS days
FROM
    mds_encounter_mch
WHERE
      encounter_datetime IS NOT NULL
AND
    (encounter_datetime > next_visit_date OR next_visit_date IS NULL);

-- Transfer to facility
SELECT
    DISTINCT ptracker_id,
     given_name,
     family_name,
    transfer_out_date,
    transfer_status,
    transfer_out,
    transfer_out_to,
    transfer_from
FROM
    mds_encounter_mch
WHERE
    transfer_out_to IS NOT NULL
  AND
    (encounter_datetime > next_visit_date OR next_visit_date IS NULL);