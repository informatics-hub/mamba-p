/*
Version: 	1.0
Author:		Ezekiel K. Chiteri
Date:		12 Sept 2017

Change log:
12 Sept 2017
Creation
*/ 
SELECT
	location_id,
	region,
	district,
	facility,
	COUNT(*) AS total,
	COUNT( IF ( ( DATEDIFF( date_created, encounter_datetime ) / 7 <= 1 ), encounter_id, NULL ) ) AS timely,
	COUNT( IF ( ( DATEDIFF( date_created, encounter_datetime ) / 7 > 1 ), encounter_id, NULL ) ) AS late,
	-- concat(100 * @timely/@total, '%') as percentage_timely,
	date_created AS entry_date,
	encounter_datetime AS visit_date 
FROM
vw_all_encounters
	GROUP BY
location_id;