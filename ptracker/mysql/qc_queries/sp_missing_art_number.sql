/*
Version: 	1.0
Author:		Ezekiel K. Chiteri
Date:		12 Sept 2017

Change log:
12 Sept 2017
Creation

Change log:
Author:		Echiteri
Date:		1 April 2020
Added: Added filter parameters
*/
SELECT
DATE_FORMAT(encounter_datetime, '%d %M, %Y') AS encounter_date,
encounter_datetime,
	ptracker_id,
	visit_uuid,
	visit_type,
	region,
	district,
	encounter_facility,
	hiv_test_status,
	art_initiation,
	art_number
	FROM
		mds_encounter_mch
		WHERE $X{IN, encounter_facility, encounter_facility} 
					AND
					(
			          (encounter_datetime BETWEEN $P{FromDate} AND $P{ToDate})
			          OR
			          ( ($P{FromDate} = '' OR $P{FromDate} IS NULL) or ($P{ToDate} = '' OR $P{ToDate} IS NULL) )
			       )
			       AND $X{IN, visit_type, visit_type}
			       AND art_number IS NULL 
		AND art_initiation IN ('Already on ART before current pregnancy', 'Started on ART in ANC current pregnancy', 'Started on ART in PNC','Started on ART during Labour and Delivery' 
			-- OR art_start_date IS NOT NULL 
		)
		AND voided = 0
	GROUP BY patient_id
	ORDER BY encounter_date