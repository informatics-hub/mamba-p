CREATE DEFINER=`root`@`localhost` 
PROCEDURE `mds_update_dhis2_integration_staging`() NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER 
BEGIN # update DHIS 2 integration staging tables 
	TRUNCATE TABLE mds_pmtct_anc_dhis2_transpose; 
	INSERT INTO mds_pmtct_anc_dhis2_transpose SELECT * FROM ds_pmtct_anc_dhis2_transpose; 
	
	TRUNCATE TABLE mds_pmtct_mbfu_dhis2_transpose; 
	INSERT INTO mds_pmtct_mbfu_dhis2_transpose SELECT * FROM ds_pmtct_mbfu_dhis2_transpose; 
	
	TRUNCATE TABLE mds_pmtct_maternity_dhis2_transpose; 
	INSERT INTO mds_pmtct_maternity_dhis2_transpose SELECT * FROM ds_pmtct_maternity_dhis2_transpose; 
	
END
