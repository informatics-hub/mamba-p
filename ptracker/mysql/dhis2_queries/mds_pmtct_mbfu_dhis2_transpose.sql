/*
Version: 	1.0
Author:		Ezekiel K. Chiteri
Date:		18 February 2019
Create staging table for mds_pmtct_mbfu_dhis2_transpose from ds_pmtct_mbfu_dhis2_three_periods

Date:		19 February 2019
Change log: Filter NULL org_unit_uid

Date:		22 February 2019
Change log: Reorder the columns to data_element, period, org_unit_uid, datavalue

Change log:
Date:		13 Mar 2019
Description:
Revised the dataelementuids to the newly mapped uids
Added categoryoptioncombuuids (COCUID)
Added attributeoptioncombouid (AOCUID)
*/
DROP TABLE
IF
	EXISTS mds_pmtct_mbfu_dhis2_transpose;
CREATE TABLE mds_pmtct_mbfu_dhis2_transpose AS 
SELECT 'PClxnQWLMfo' data_element, period, org_unit_uid, '' COCUID, '' AOCUID, PClxnQWLMfo datavalue FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'PClxnQWLMfo', period, org_unit_uid, 'dU4joeXwjAo', '', dU4joeXwjAo FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'PClxnQWLMfo', period, org_unit_uid, 'wDzLI7v6u26', '', wDzLI7v6u26 FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'PClxnQWLMfo', period, org_unit_uid, 'pkzdQyPrZje', '', pkzdQyPrZje FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'PClxnQWLMfo', period, org_unit_uid, 'vJ0QRvtPy2y', '', vJ0QRvtPy2y FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'mMxDjrGMkQh', period, org_unit_uid, '', '', mMxDjrGMkQh FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'clc7mBt3q0O', period, org_unit_uid, '', '', clc7mBt3q0O FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'clc7mBt3q0O', period, org_unit_uid, 'uuV5vR0vX2A', '', uuV5vR0vX2A FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'clc7mBt3q0O', period, org_unit_uid, 'vNKOGMEKz7l', '', vNKOGMEKz7l FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'clc7mBt3q0O', period, org_unit_uid, 'YgY5g8Ifk4Q', '', YgY5g8Ifk4Q FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'clc7mBt3q0O', period, org_unit_uid, 'IyQNFdUBair', '', IyQNFdUBair FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'HT1Jq3Tvjt2', period, org_unit_uid, '', '', HT1Jq3Tvjt2 FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'HT1Jq3Tvjt2', period, org_unit_uid, 'qv4Af3LornN', '', qv4Af3LornN FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'HT1Jq3Tvjt2', period, org_unit_uid, 'Sfo4a8rJpbl', '', Sfo4a8rJpbl FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'HT1Jq3Tvjt2', period, org_unit_uid, 'qm0oDbJh00p', '', qm0oDbJh00p FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'rODiD9zi0WP', period, org_unit_uid, '', '', rODiD9zi0WP FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'rODiD9zi0WP', period, org_unit_uid, 'qxaoOGjLv94', '', qxaoOGjLv94 FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'rODiD9zi0WP', period, org_unit_uid, 'seNpDhm5VSV', '', seNpDhm5VSV FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'pMkEzPC5GSh', period, org_unit_uid, '', '', pMkEzPC5GSh FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'pMkEzPC5GSh', period, org_unit_uid, 'RN1UyKFyH9b', '', RN1UyKFyH9b FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'pMkEzPC5GSh', period, org_unit_uid, 'ThzSezs8egm', '', ThzSezs8egm FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'pMkEzPC5GSh', period, org_unit_uid, 'ZXkqOebz7K7', '', ZXkqOebz7K7 FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'pMkEzPC5GSh', period, org_unit_uid, 'ud3NA1vVUl9', '', ud3NA1vVUl9 FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'htGck98WfWb', period, org_unit_uid, '', '', htGck98WfWb FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'L6d5YE42bSy', period, org_unit_uid, '', '', L6d5YE42bSy FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'kLPkxBFS4E5', period, org_unit_uid, '', '', kLPkxBFS4E5 FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'w1LbgLoOl8Q', period, org_unit_uid, '', '',  w1LbgLoOl8Q FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'MJzMNeYvJ1E', period, org_unit_uid, '', '',  MJzMNeYvJ1E FROM ds_pmtct_mbfu_dhis2_three_periods WHERE org_unit_uid <> ''
ORDER BY period ASC;
