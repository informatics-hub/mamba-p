/*
Version: 	1.0
Author:		Ezekiel K. Chiteri
Date:		14 Feb 2019
Description: Create a ds_pmtct_anc_dhis2 report view

Change log:
Date:		19 April 2019
Description:
- Modified period to dymaically get the value at runtime
- Added the period filter on the where clause based on the the current month
- Ability to filter by encounter current date, previous one month and previous two months.
*/
DROP VIEW IF EXISTS ds_pmtct_anc_dhis2;
CREATE VIEW ds_pmtct_anc_dhis2 AS
SELECT 
				region,
				district,
				facility,
				org_unit_uid, 
				DATE_FORMAT(encounter_datetime ,'%Y%m') as period,
				DATE_FORMAT(NOW() ,'%Y-%m-01') AS current_date_month,
				COUNT(DISTINCT(patient_id)) AS XX0VWMX37pK /*Ptracker: total_anc*/, 
				COUNT(DISTINCT(if(hiv_test_status = 'Previously known positive', patient_id, NULL ))) AS cq6wJbe937t /*hiv_kp*/,
				COUNT(DISTINCT(if(hiv_test_status <> 'Previously known positive', patient_id, NULL ))) AS NJqlznXnUVP /*Ptracker: pre_counseled*/,
				COUNT(DISTINCT(if(hiv_test_status = 'Tested for HIV during this visit' ,patient_id, NULL ))) AS mSYZcWpFuPa /*PTracker: hiv_tested*/,
				COUNT(DISTINCT(if(hiv_test_status = 'Not tested for HIV during this visit' AND anc_visit_type = 'New ANC Visit',patient_id, NULL ))) AS yq4MnSbbVf2 /*Ptracker: hiv_not_tested*/,
				COUNT(DISTINCT(if(hiv_test_result = 'HIV Positive' ,patient_id, NULL ))) AS HYaH8kchO73 /*Ptracker: new_pos*/,
				COUNT(DISTINCT(if(hiv_test_result = 'HIV Positive' ,patient_id, NULL ))) AS inpYDEb8Yzi /*Ptracker: post_counseled_pos*/,
				COUNT(DISTINCT(if(hiv_test_result = 'Negative' ,patient_id, NULL ))) AS S1o3QZ1BeZH /*Ptracker: new_neg*/,
				COUNT(DISTINCT(if(hiv_test_result = 'Negative' ,patient_id, NULL ))) AS S4IL4ti1tUA /*Ptracker: post_counseled_neg*/,
				COUNT(DISTINCT(if((hiv_test_status = 'Tested for HIV during this visit' AND (hiv_test_result IS NULL OR hiv_test_result = "Missing" OR hiv_test_result = "Unknown")), patient_id, NULL ))) AS qi2rTZFxfgI /*PTracker: unknown_lost*/,
				COUNT(DISTINCT(if(recent_viral_load = 'Yes' AND (viral_load_results IS NOT NULL OR viral_load_results <> 'Results Pending') AND anc_visit_type = 'New ANC Visit' ,patient_id, NULL ))) AS eTvAQrz3mjG /*Ptracker: viral_load*/,
				COUNT(DISTINCT(if((viral_load_results = 'Not Detected' OR viral_load_copies < 40) AND anc_visit_type = 'New ANC Visit' ,patient_id, NULL ))) AS YroT7szontI /*Ptracker: vl_not_detected*/,
				COUNT(DISTINCT(if(art_initiation = 'Started on ART in ANC current pregnancy' AND anc_visit_type = 'New ANC Visit' ,patient_id, NULL ))) AS ysnacwDrzjU /*Ptracker: started_art*/,
				COUNT(DISTINCT(if((art_initiation IS NULL AND art_start_date IS NOT NULL) ,if((art_start_date < encounter_datetime OR DATEDIFF(encounter_datetime, art_start_date) / 7 > 2), patient_id, NULL), if(art_initiation = 'Already on ART before current pregnancy' ,patient_id, NULL )))) AS hxta9maIFSB /*Ptracker: already_on_art*/,
				COUNT(DISTINCT(if(art_initiation = 'Refused ART' OR art_initiation = 'Not started due to stockout of ART',patient_id, NULL ))) AS QfOR3z2RdhP /*Ptracker: not_started_on_art*/,
				COUNT(DISTINCT(if(art_initiation = 'Refused ART' ,patient_id, NULL ))) AS jQgqywlptQl /*Ptracker: refused_art*/,
				COUNT(DISTINCT(if(art_initiation = 'Not started due to stockout of ART' ,patient_id, NULL ))) AS kwbcflQPEyx /*Ptracker: art_stockout*/
FROM mds_encounter_mch
WHERE visit_uuid in ('2549af50-75c8-4aeb-87ca-4bb2cef6c69a')
AND anc_visit_type = 'New ANC Visit'
AND voided = 0
-- AND encounter_datetime BETWEEN DATE_ADD(DATE_ADD(LAST_DAY(NOW()), INTERVAL 1 DAY), INTERVAL - 1 MONTH) AND LAST_DAY(NOW()) -- current month
AND encounter_datetime BETWEEN DATE_ADD(DATE_ADD(LAST_DAY(NOW()), INTERVAL 1 DAY), INTERVAL - 2 MONTH) AND LAST_DAY(DATE_ADD(NOW(), INTERVAL - 1 MONTH)) -- previous one month
-- AND encounter_datetime BETWEEN DATE_ADD(DATE_ADD(LAST_DAY(NOW()), INTERVAL 1 DAY), INTERVAL - 3 MONTH) AND LAST_DAY(DATE_ADD(NOW(), INTERVAL - 2 MONTH)) -- two months old
GROUP BY facility
ORDER BY period DESC;