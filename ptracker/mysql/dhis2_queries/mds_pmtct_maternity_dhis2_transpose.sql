/*
Version: 	1.0
Author:		Ezekiel K. Chiteri
Date:		18 February 2019
Create staging table for mds_pmtct_maternity_dhis2_transpose from ds_pmtct_maternity_dhis2_three_periods

Date:		19 February 2019
Change log: Filter NULL org_unit_uid

Date:		22 February 2019
Change log: Reorder the columns to data_element, period, org_unit_uid, datavalue

Change log:
Date:		12 Mar 2019
Description:
Revised the dataelementuids to the newly mapped uids
Added categoryoptioncombuuids (COCUID)

Change log:
Date:		13 Mar 2019
Description:
Added attributeoptioncombouid (AOCUID)
*/
DROP TABLE
IF
	EXISTS mds_pmtct_maternity_dhis2_transpose;
CREATE TABLE mds_pmtct_maternity_dhis2_transpose AS 
SELECT 'ESSlGDK7kwu' data_element, period, org_unit_uid, '' COCUID, '' AOCUID, ESSlGDK7kwu datavalue FROM ds_pmtct_maternity_dhis2_three_periods WHERE org_unit_uid IS NOT NULL  AND TRIM(org_unit_uid) <> ''
UNION
SELECT 'qsTs0L1oFTz', period, org_unit_uid, '', '' , qsTs0L1oFTz FROM ds_pmtct_maternity_dhis2_three_periods WHERE org_unit_uid IS NOT NULL AND TRIM(org_unit_uid) <> ''
UNION
SELECT 'JCHvmsEi07x', period, org_unit_uid, '', '' , JCHvmsEi07x FROM ds_pmtct_maternity_dhis2_three_periods WHERE org_unit_uid IS NOT NULL AND TRIM(org_unit_uid) <> ''
UNION
SELECT 'fMsyIj2ZcwN', period, org_unit_uid, '', '' ,  fMsyIj2ZcwN FROM ds_pmtct_maternity_dhis2_three_periods WHERE org_unit_uid IS NOT NULL AND TRIM(org_unit_uid) <> ''
UNION
SELECT 'KToDvpUiwsM', period, org_unit_uid, '', '' , KToDvpUiwsM FROM ds_pmtct_maternity_dhis2_three_periods WHERE org_unit_uid IS NOT NULL AND TRIM(org_unit_uid) <> ''
UNION
SELECT 'gHGIS9Mo9te', period, org_unit_uid, '', '' , gHGIS9Mo9te FROM ds_pmtct_maternity_dhis2_three_periods WHERE org_unit_uid IS NOT NULL AND TRIM(org_unit_uid) <> ''
UNION
SELECT 'ZBqvmZXmKQh', period, org_unit_uid, '', '' , ZBqvmZXmKQh FROM ds_pmtct_maternity_dhis2_three_periods WHERE org_unit_uid IS NOT NULL AND TRIM(org_unit_uid) <> ''
UNION
SELECT 'NprBzmM2v3q', period, org_unit_uid, '', '' , NprBzmM2v3q FROM ds_pmtct_maternity_dhis2_three_periods WHERE org_unit_uid IS NOT NULL AND TRIM(org_unit_uid) <> ''
UNION
SELECT 'JwjbNtSdbyc', period, org_unit_uid, '', '' , JwjbNtSdbyc FROM ds_pmtct_maternity_dhis2_three_periods WHERE org_unit_uid IS NOT NULL AND TRIM(org_unit_uid) <> ''
UNION
SELECT 'ILqnl25PSDT', period, org_unit_uid, '', '' , ILqnl25PSDT FROM ds_pmtct_maternity_dhis2_three_periods WHERE org_unit_uid IS NOT NULL AND TRIM(org_unit_uid) <> ''
UNION
SELECT 'rsQy75sINh0', period, org_unit_uid, '', '' , rsQy75sINh0 FROM ds_pmtct_maternity_dhis2_three_periods WHERE org_unit_uid IS NOT NULL AND TRIM(org_unit_uid) <> ''
UNION
SELECT 'VpBmMqoI3hr', period, org_unit_uid, '', '' , VpBmMqoI3hr FROM ds_pmtct_maternity_dhis2_three_periods WHERE org_unit_uid IS NOT NULL AND TRIM(org_unit_uid) <> ''
UNION
SELECT 'NI7lZYNU9MR', period, org_unit_uid, '', '' , NI7lZYNU9MR FROM ds_pmtct_maternity_dhis2_three_periods WHERE org_unit_uid IS NOT NULL AND TRIM(org_unit_uid) <> ''
UNION
SELECT 'yNjgqhDt1Oc', period, org_unit_uid, '', '' ,  yNjgqhDt1Oc FROM ds_pmtct_maternity_dhis2_three_periods WHERE org_unit_uid IS NOT NULL AND TRIM(org_unit_uid) <> ''
UNION
SELECT 'r7dvgRhGmc6', period, org_unit_uid, '', '' ,  r7dvgRhGmc6 FROM ds_pmtct_maternity_dhis2_three_periods WHERE org_unit_uid IS NOT NULL AND TRIM(org_unit_uid) <> ''
UNION
SELECT 'WS1SeZwZ9f6', period, org_unit_uid, '', '' ,  WS1SeZwZ9f6 FROM ds_pmtct_maternity_dhis2_three_periods WHERE org_unit_uid IS NOT NULL AND TRIM(org_unit_uid) <> ''
UNION
SELECT 'UYvwaXzgmP9', period, org_unit_uid, '', '' ,  UYvwaXzgmP9 FROM ds_pmtct_maternity_dhis2_three_periods WHERE org_unit_uid IS NOT NULL AND TRIM(org_unit_uid) <> ''
UNION
SELECT 'NmhbUMZklkI', period, org_unit_uid, '', '' ,  NmhbUMZklkI FROM ds_pmtct_maternity_dhis2_three_periods WHERE org_unit_uid IS NOT NULL AND TRIM(org_unit_uid) <> ''
ORDER BY period ASC
