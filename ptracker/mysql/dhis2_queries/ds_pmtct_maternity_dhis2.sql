/*
Version: 	1.0
Author:		Ezekiel K. Chiteri
Date:		14 Feb 2019
Description: Create a ds_pmtct_maternity_dhis2 report view

Change log:
Date:		19 April 2019
Description:
- Modified period to dymaically get the value at runtime
- Added the period filter on the where clause based on the the current month
- Ability to filter by encounter current date, previous one month and previous two months.

Change log:
Date:	4 Apr 2019
Description: Replaced org_unit_uid with org_unit_uid_maternity
*/
DROP VIEW
IF
	EXISTS ds_pmtct_maternity_dhis2;
	CREATE VIEW ds_pmtct_maternity_dhis2 AS
SELECT 
					region,
					district,
					facility,
					-- org_unit_uid_maternity,
					if(org_unit_uid_maternity = '',  org_unit_uid, org_unit_uid_maternity) AS org_unit_uid,
					DATE_FORMAT(encounter_datetime ,'%Y%m') as period,
					DATE_FORMAT(NOW() ,'%Y-%m-01') AS current_date_month,
					COUNT(DISTINCT patient_id) AS ESSlGDK7kwu /*Ptracker: total_delivery*/, -- MAT 01
					-- Not relevant for this exercise: COUNT(DISTINCT (if(patient_id IN (SELECT distinct patient_id FROM mds_encounter_mch WHERE (hiv_test_status = 'Previously known Positive' OR hiv_test_result IN ('HIV Positive', 'Unknown') OR anc_booked = 'Yes') AND visit_type = 'Antenatal'), patient_id, NULL ))) AS add_to_mat02, -- These are patient who came to L&D with previous known HIV status at ANC in PTracker. Don't count Negatives because they will be tested at L&D
					-- Not relevant for this exercise: COUNT(DISTINCT (if(patient_id IN (SELECT distinct patient_id FROM mds_encounter_mch WHERE (hiv_test_status = 'Previously known Positive' OR hiv_test_result IN ('HIV Positive', 'Unknown') OR anc_booked = 'Yes') AND visit_type = 'Antenatal'), patient_id, NULL ))) AS add_to_mat02, -- These are patient who came to L&D with previous known HIV status at ANC in PTracker. Don't count Negatives because they will be tested at L&D
					COUNT(DISTINCT(if(hiv_test_status = 'Previously known positive' OR hiv_re_test_36wks = 'NEGATIVE' OR hiv_re_test_36wks = 'HIV Positive', patient_id, NULL ))) AS qsTs0L1oFTz /*Ptracker: known_hiv_status*/, -- MAT 02 Those that came to L&D and their test status is KP, were negative at ANC hence re-tested at L&D 
					COUNT(DISTINCT(if(patient_id IN (SELECT distinct patient_id FROM mds_encounter_mch WHERE (hiv_test_status = 'Previously known Positive' OR hiv_test_result ='HIV Positive') AND visit_type = 'Antenatal'), patient_id, NULL ))) AS JCHvmsEi07x /*Ptracker: known_hiv_pos_status*/, -- MAT 02.1
					COUNT(DISTINCT(if(hiv_re_test_36wks = 'HIV Positive' OR hiv_re_test_36wks = 'NEGATIVE', patient_id, NULL ))) AS fMsyIj2ZcwN /*retest_36_wks*/, -- MAT 03
					COUNT(DISTINCT(if(hiv_re_test_36wks = 'HIV Positive', patient_id, NULL ))) AS KToDvpUiwsM /*pos_retest_36_wks*/, -- MAT 03.1
					COUNT(DISTINCT(if(hiv_test_status = 'Not tested for HIV during this visit'
							OR hiv_test_status = 'Missing'
							OR (anc_booked = 'No' AND hiv_test_status = 'Not tested for HIV during this visit') 
							OR (anc_booked = 'Missing' AND hiv_test_status = 'Not tested for HIV during this visit') 
							OR hiv_test_result = 'Unknown', patient_id, NULL ))) AS gHGIS9Mo9te /*Ptracker: unknown_hiv_status*/, -- MAT 04
					COUNT(DISTINCT(if(hiv_test_result = 'HIV Positive' OR hiv_test_result = 'NEGATIVE', patient_id, NULL ))) AS ZBqvmZXmKQh /*Ptracker: newly_hiv_tested*/, -- MAT 05
					COUNT(DISTINCT(if(hiv_test_result = 'HIV Positive',  patient_id, NULL ))) AS NprBzmM2v3q /*Ptracker: newly_hiv_tested_pos*/, -- MAT 05.1 Consider using hiv_test_status
					-- Not relevant for this exercise: COUNT(DISTINCT(if(hiv_test_result = 'Negative', patient_id, NULL ))) AS newly_hiv_tested_neg, -- for CHECK-SUM
					-- Not relevant for this exercise: COUNT(DISTINCT(if(hiv_test_status = 'Previously known positive' OR hiv_test_result = 'HIV Positive' OR hiv_re_test_36wks = 'HIV Positive', patient_id, NULL ))) AS all_hiv_pos, -- MAT 06
					COUNT(DISTINCT(if(art_initiation = 'Already on ART before current pregnancy', patient_id, NULL ))) AS  JwjbNtSdbyc /*already_on_art*/, -- MAT 07
					COUNT(DISTINCT(if(patient_id IN 
								(SELECT distinct patient_id FROM mds_encounter_mch WHERE 
									(art_initiation = 'Started on ART in ANC current pregnancy') 
									/* Getting clients whose ANC encounter is current to this pregnancy */
									AND ROUND(DATEDIFF(encounter_datetime, art_start_date)/7) >= 4 
									AND visit_type = 'Antenatal')
									/* The next OR is supposed to capture those started on ART in L&D */
									OR (art_initiation = 'Started on ART in ANC current pregnancy' AND ROUND(DATEDIFF(encounter_datetime, art_start_date)/7) >= 4), patient_id, NULL)
								)) AS ILqnl25PSDT /*PTracker: art_4wks_plus*/, -- MAT 08.1 Sum for MAT 08 started on art at anc
					COUNT(DISTINCT(if(patient_id IN 
								(SELECT distinct patient_id FROM mds_encounter_mch WHERE 
									(art_initiation = 'Started on ART in ANC current pregnancy') 
									/* Getting clients whose ANC encounter is current to this pregnancy */
									AND ROUND(DATEDIFF(encounter_datetime, art_start_date)/7) <= 4 
									AND visit_type = 'Antenatal')
									/* The next OR is supposed to capture those started on ART in L&D */
									OR (art_initiation = 'Started on ART in ANC current pregnancy' AND ROUND(DATEDIFF(encounter_datetime, art_start_date)/7) <= 4), patient_id, NULL)
								)) AS rsQy75sINh0 /*art_4wks_less*/, -- MAT 08.2 Sum for MAT 08 started on art at anc
					COUNT(DISTINCT(if(patient_id IN 
								(SELECT distinct patient_id FROM mds_encounter_mch WHERE 
									(art_initiation = 'Started on ART in ANC current pregnancy') 
									/* Getting clients whose ANC encounter is current to this pregnancy */
									AND visit_type = 'Antenatal')
									/* The next OR is supposed to capture those started on ART in L&D */
									OR (art_initiation = 'Started on ART in ANC current pregnancy'), patient_id, NULL)
								)) AS VpBmMqoI3hr /*art_initiated*/, -- MAT 08 sum art_4wks_plus and art_4wks_less
					COUNT(DISTINCT(if(art_initiation = 'Started on ART during Labour and Delivery', patient_id, NULL ))) AS NI7lZYNU9MR /*Ptracker: art_initiated_at_delivery*/,  -- MAT 09
					COUNT(DISTINCT(if(art_initiation = 'Refused ART', patient_id, NULL ))) AS yNjgqhDt1Oc /*Ptracker: refused_art*/,  -- MAT 10
					COUNT(DISTINCT(if(infant_recieved_arv = 'Infant received NVP + AZT prophylaxis up to 6', patient_id, NULL ))) AS r7dvgRhGmc6 /*infant_recieved_arv*/,  -- MAT 11
					COUNT(DISTINCT(if(infant_recieved_arv = 'Received ARV Prophylaxis', patient_id, NULL ))) AS WS1SeZwZ9f6 /*infant_recieved_arv_nvp*/,  -- MAT 12. PTracker is not collecting this
					COUNT(DISTINCT(if(breastfeeding = 'BREASTFED EXCLUSIVELY', patient_id, NULL ))) AS UYvwaXzgmP9 /*PTracker: excl_bf*/,  -- MAT 13
					COUNT(DISTINCT(if(breastfeeding = 'Replacement feeding', patient_id, NULL ))) AS NmhbUMZklkI /*PTracker: replacement_bf*/  -- MAT 14
			FROM mds_encounter_mch
WHERE visit_uuid in ('2678423c-0523-4d76-b0da-18177b439eed')
-- AND encounter_datetime BETWEEN DATE_ADD(DATE_ADD(LAST_DAY(NOW()), INTERVAL 1 DAY), INTERVAL - 1 MONTH) AND LAST_DAY(NOW()) -- current month
AND encounter_datetime BETWEEN DATE_ADD(DATE_ADD(LAST_DAY(NOW()), INTERVAL 1 DAY), INTERVAL - 2 MONTH) AND LAST_DAY(DATE_ADD(NOW(), INTERVAL - 1 MONTH)) -- previous one month
-- AND encounter_datetime BETWEEN DATE_ADD(DATE_ADD(LAST_DAY(NOW()), INTERVAL 1 DAY), INTERVAL - 3 MONTH) AND LAST_DAY(DATE_ADD(NOW(), INTERVAL - 2 MONTH)) -- previous two months
AND voided = 0
GROUP BY facility;