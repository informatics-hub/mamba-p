/*
Version: 	1.0
Author:		Ezekiel K. Chiteri
Date:		14 Feb 2019
Description: Create a ds_pmtct_mbfu_dhis2 report view

Change log:
Date:		19 Feb 2019
Description:
- Modified period to dymaically get the value at runtime
- Added the period filter on the where clause based on the the current month
- Ability to filter by encounter current date, previous one month and previous two months.

Change log:
Date:		12 Mar 2019
Description:
Revised the dataelemenetuids including the categoryoptioncombosuids for age disagreegations. 
For uniquness, stored the used the categoryoptioncombosuid as datalementuids 
but added comment with the right dataelementuids e.g. [coc = dU4joeXwjAo] [euid = PClxnQWLMfo]

Change log:
Date:		10 July 2020
Description:
1. Changed uuid to patient_id in the indicators w1LbgLoOl8Q and MJzMNeYvJ1E
2. Corrected WHERE clause to use 'NVP Prophylaxis daily up to 6 weeks' from 'ARV Prophylaxis daily up to 6 weeks'
*/
DROP VIEW
IF
	EXISTS ds_pmtct_mbfu_dhis2;
	CREATE VIEW ds_pmtct_mbfu_dhis2 AS
SELECT 
				region,
				district,
				facility,
				encounter_datetime,
				'null' AS org_unit_uid, 
				DATE_FORMAT(encounter_datetime ,'%Y%m') as period,
				DATE_FORMAT(NOW() ,'%Y-%m-01') AS current_date_month,
				-- Not relevant for this exercise: COUNT(DISTINCT(IF(visit_uuid = 'af1f1b24-d2e8-4282-b308-0bf79b365584' , encounter_uuid, NULL))) AS total_mbfu, -- newly registered for the month
				COUNT(DISTINCT(IF(hiv_exposure = 'Currently Exposed', encounter_uuid, NULL ))) AS PClxnQWLMfo /*PTracker: exposed*/, -- 1. exposed first visit
				COUNT(DISTINCT(IF(hiv_exposure = 'Currently Exposed' AND ROUND(DATEDIFF(encounter_datetime, birthdate)/7)  BETWEEN 0 AND 8, encounter_uuid, NULL ))) AS /* [coc = dU4joeXwjAo] [euid = PClxnQWLMfo] */ dU4joeXwjAo /*PTracker: exposed_0_8wks */, -- exposed 0_8 wks.
				COUNT(DISTINCT(IF(hiv_exposure = 'Currently Exposed' AND (ROUND(DATEDIFF(encounter_datetime, birthdate)/7) >= 9 AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43) <= 8), encounter_uuid, NULL ))) AS /* [coc = wDzLI7v6u26] [euid = PClxnQWLMfo] */ wDzLI7v6u26 /*Ptracker: exposed_9wks_8mth*/, -- exposed 9wks to 8months.
				COUNT(DISTINCT(IF(hiv_exposure = 'Currently Exposed' AND (ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43)  BETWEEN 9 AND 12), encounter_uuid, NULL ))) AS /* [coc = pkzdQyPrZje] [euid = PClxnQWLMfo] */ pkzdQyPrZje /*Ptracker: exposed_9_12mth*/, -- exposed 9 to 12months.
				COUNT(DISTINCT(IF(hiv_exposure = 'Currently Exposed' AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43)  > 12, encounter_uuid, NULL ))) AS /* [coc = vJ0QRvtPy2y] [euid = PClxnQWLMfo] */ vJ0QRvtPy2y /*PTracker: exposed_greater_than_12mth*/ , -- exposed more than 12months
				
				COUNT(DISTINCT(IF(hiv_exposure IS NULL AND visit_uuid = 'af1f1b24-d2e8-4282-b308-0bf79b365584', encounter_uuid, NULL ))) AS mMxDjrGMkQh /*Ptracker:repeat_visit*/, -- 2. exposed repeat visit already
				
				COUNT(DISTINCT(IF(hiv_test_type = 'DNA PCR' AND ROUND(DATEDIFF(encounter_datetime, birthdate)/7)  >=  6, encounter_uuid, NULL ))) AS clc7mBt3q0O /*PTracker: first_hiv_tested_total*/, -- 3. HIV exposed children tested for HIV - 1st test
				COUNT(DISTINCT(IF(hiv_test_type = 'DNA PCR'  AND (ROUND(DATEDIFF(encounter_datetime, birthdate)/7)  BETWEEN 6 AND 8), encounter_uuid, NULL ))) AS /* [coc = uuV5vR0vX2A] [euid = clc7mBt3q0O] */ uuV5vR0vX2A /*PTracker: six_eight_wks_first_hiv_tested*/, -- 6 to 8weeks first hiv test
				COUNT(DISTINCT(IF(hiv_test_type = 'DNA PCR'  AND (ROUND(DATEDIFF(encounter_datetime, birthdate)/7)  >= 9 AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43)  <= 8), encounter_uuid, NULL ))) AS /* [coc = vNKOGMEKz7l] [euid = clc7mBt3q0O] */ vNKOGMEKz7l /*Ptracker: nine_wks_eight_months_first_hiv_tested*/, -- 6weeks to 8months first hiv test
				COUNT(DISTINCT(IF(hiv_test_type = 'DNA PCR'  AND (ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43) BETWEEN 9 AND 12), encounter_uuid, NULL ))) AS /* [coc = YgY5g8Ifk4Q] [euid = clc7mBt3q0O] */ YgY5g8Ifk4Q /*PTracker: nine_twelve_months_first_hiv_tested*/, -- 9 to 12 months first hiv test
				COUNT(DISTINCT(IF(hiv_test_type = 'DNA PCR'  AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43) > 12, encounter_uuid, NULL ))) AS /* [coc = IyQNFdUBair] [euid = clc7mBt3q0O]*/ IyQNFdUBair /*PTracker: twelve_more_months_first_hiv_tested */,
				
				COUNT(DISTINCT(IF(hiv_test_type = 'Rapid Test' AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43)  >=  9, encounter_uuid, NULL ))) AS HT1Jq3Tvjt2 /*PTracker: second_hiv_tested_total*/, -- 4. HIV exposed children tested for HIV - repeat test
				COUNT(DISTINCT(IF(hiv_test_type = 'Rapid Test'  AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43)  <  9, encounter_uuid, NULL ))) AS /*[coc = qv4Af3LornN] [euid = HT1Jq3Tvjt2]*/ qv4Af3LornN /*Ptracker: nine_less_months_second_hiv_tested*/, -- less than 9months second hiv test
				COUNT(DISTINCT(IF(hiv_test_type = 'Rapid Test'  AND (ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43)  BETWEEN 9 AND 12), encounter_uuid, NULL ))) AS /*[coc = Sfo4a8rJpbl] [euid = HT1Jq3Tvjt2]*/ Sfo4a8rJpbl /*Ptracker: nine_twelve_months_second_hiv_tested*/, -- 9 to 12 months second hiv test
				COUNT(DISTINCT(IF(hiv_test_type = 'Rapid Test'  AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43)  > 12, encounter_uuid, NULL ))) AS /*[coc = qm0oDbJh00p] [euid = HT1Jq3Tvjt2]*/ qm0oDbJh00p /*Ptracker: twelve_more_months_second_hiv_tested*/, -- more than 12months second hiv test
				
				COUNT(DISTINCT(IF(confirmatory_test_done = 'Yes' /*AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43)  >=  9*/, encounter_uuid, NULL ))) AS rODiD9zi0WP /*PTracker: third_hiv_tested_total*/, -- 5. Confirmatory/3rd test done
				COUNT(DISTINCT(IF(confirmatory_test_done = 'Yes'  AND (ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43) BETWEEN 9 AND 17), encounter_uuid, NULL ))) AS /*[coc = qxaoOGjLv94] [euid = rODiD9zi0WP]*/  qxaoOGjLv94 /*PTracker: nine_seventeen_months_third_hiv_tested*/, --  9 to 17 months Confirmatory/3rd test done
				COUNT(DISTINCT(IF(confirmatory_test_done = 'Yes'  AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43)  >= 18, encounter_uuid, NULL ))) AS /*[coc = seNpDhm5VSV] [euid = rODiD9zi0WP]*/ seNpDhm5VSV /*PTracker: eighteen_more_months_third_hiv_tested*/, --  more than 18 months Confirmatory/3rd test done
				
				COUNT(DISTINCT(IF(final_test_result = 'Positive' /* AND ROUND(DATEDIFF(encounter_datetime, birthdate)/7)  >=  6*/, encounter_uuid, NULL ))) AS pMkEzPC5GSh /*PTracker: hiv_pos_total*/, -- 6. children diagnosed (confirmed) HIV positive
				COUNT(DISTINCT(IF(final_test_result = 'Positive'  AND (ROUND(DATEDIFF(encounter_datetime, birthdate)/7)  >=  6 AND ROUND(DATEDIFF(encounter_datetime, birthdate)/7)  <= 8), encounter_uuid, NULL ))) AS /*[coc = RN1UyKFyH9b] [euid = pMkEzPC5GSh]*/ RN1UyKFyH9b /*Ptracker: six_eight_wks_hiv_pos*/, -- 6 to 8 weeks hiv test
				COUNT(DISTINCT(IF(final_test_result = 'Positive'  AND (ROUND(DATEDIFF(encounter_datetime, birthdate)/7)  >=  9 AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43)  <= 8), encounter_uuid, NULL ))) AS /*[coc = ThzSezs8egm] [euid = pMkEzPC5GSh]*/ ThzSezs8egm /*PTracker: nine_wks_eight_months_hiv_pos*/, -- 9weeks to 8months hiv test
				COUNT(DISTINCT(IF(final_test_result = 'Positive'  AND (ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43)  BETWEEN 9 AND 12), encounter_uuid, NULL ))) AS /*[coc = ZXkqOebz7K7] [euid = pMkEzPC5GSh]*/ ZXkqOebz7K7 /*Ptracker: nine_twelve_months_hiv_pos*/, -- 12 months and more hiv test
				COUNT(DISTINCT(IF(final_test_result = 'Positive'  AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43) > 12, encounter_uuid, NULL ))) /*[coc = ud3NA1vVUl9] [euid = pMkEzPC5GSh]*/ AS ud3NA1vVUl9 /*PTracker: twelve_more_months_hiv_pos*/,
				
				COUNT(DISTINCT(IF(art_link = 'Yes'  AND final_test_result = 'Positive',encounter_uuid, NULL ))) AS htGck98WfWb /*Ptracker: art_linked*/, -- 7. art linked
				COUNT(DISTINCT(IF(transfer_status = 'HIV negative infant discharged from PMTCT' ,encounter_uuid, NULL ))) AS L6d5YE42bSy /*Ptracker: hiv_neg*/, -- 8. negative final test result
				COUNT(DISTINCT(IF(arv_prophylaxis_status = 'Received ARV prophylaxis' AND (ROUND(DATEDIFF(encounter_datetime, birthdate)/7)  >=  6 AND ROUND(DATEDIFF(encounter_datetime, birthdate)/7)  <= 8) ,encounter_uuid, NULL ))) AS kLPkxBFS4E5 /*Ptracker: receive_arv*/, -- 9. arv prophylaxis status
				COUNT(DISTINCT(IF( mother_id IN (SELECT patient_id FROM mds_encounter_mch WHERE visit_uuid = '2678423c-0523-4d76-b0da-18177b439eed' AND infant_recieved_arv = 'ARV Prophylaxis daily up to 6 weeks'), encounter_uuid, NULL ))) AS w1LbgLoOl8Q /*Ptracker: receive_ctx */, -- 10. ctx prophylaxis status {the infant who received ARV prophylaxis from Labor and Delivery}
				COUNT(DISTINCT(IF(hiv_exposure = 'Currently Exposed' AND mother_id IS NOT NULL AND mother_id IN (SELECT patient_id FROM mds_encounter_mch WHERE visit_uuid = '269bcc7f-04f8-4ddc-883d-7a3a0d569aad' AND infant_breastfeeding = 'BREASTFED EXCLUSIVELY') AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43) <= 6, encounter_uuid, NULL))) AS MJzMNeYvJ1E /*Ptracker: breastfeeding */ -- 11. infant breastfeeding
				FROM mds_encounter_mch
				WHERE visit_uuid in ('af1f1b24-d2e8-4282-b308-0bf79b365584')
			  AND voided = 0
				-- AND encounter_datetime BETWEEN DATE_ADD(DATE_ADD(LAST_DAY(NOW()), INTERVAL 1 DAY), INTERVAL - 1 MONTH) AND LAST_DAY(NOW()) -- current month	
				AND encounter_datetime BETWEEN DATE_ADD(DATE_ADD(LAST_DAY(NOW()), INTERVAL 1 DAY), INTERVAL - 2 MONTH) AND LAST_DAY(DATE_ADD(NOW(), INTERVAL - 1 MONTH)) -- previous one months	
				-- AND encounter_datetime BETWEEN DATE_ADD(DATE_ADD(LAST_DAY(NOW()), INTERVAL 1 DAY), INTERVAL - 3 MONTH) AND LAST_DAY(DATE_ADD(NOW(), INTERVAL - 2 MONTH)) -- previous two months	
GROUP BY facility;