/*
Version: 	1.0
Author:		Ezekiel K. Chiteri
Date:		18 February 2019
Create staging table for mds_pmtct_anc_dhis2_transpose from ds_pmtct_anc_dhis2_three_periods

Date:		19 February 2019
Change log: Filter NULL org_unit_uid

Date:		22 February 2019
Change log: Reorder the columns to data_element, period, org_unit_uid, datavalue

Change log:
Date:		12 Mar 2019
Description:
Revised the dataelementuids to the newly mapped uids
Added categoryoptioncombuuids (COCUID)

Change log:
Date:		13 Mar 2019
Description:
Added attributeoptioncombouid (AOCUID)
*/
DROP TABLE
IF
	EXISTS mds_pmtct_anc_dhis2_transpose;
CREATE TABLE mds_pmtct_anc_dhis2_transpose AS 
SELECT 'XX0VWMX37pK' data_element, period, org_unit_uid, '' COCUID, '' AOCUID, XX0VWMX37pK datavalue FROM ds_pmtct_anc_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'cq6wJbe937t', period, org_unit_uid, '', '', cq6wJbe937t FROM ds_pmtct_anc_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'NJqlznXnUVP', period, org_unit_uid, '', '', NJqlznXnUVP FROM ds_pmtct_anc_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'mSYZcWpFuPa', period, org_unit_uid, '', '',  mSYZcWpFuPa FROM ds_pmtct_anc_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'yq4MnSbbVf2', period, org_unit_uid, '', '', yq4MnSbbVf2 FROM ds_pmtct_anc_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'HYaH8kchO73', period, org_unit_uid, '', '', HYaH8kchO73 FROM ds_pmtct_anc_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'inpYDEb8Yzi', period, org_unit_uid, '', '', inpYDEb8Yzi FROM ds_pmtct_anc_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'S1o3QZ1BeZH', period, org_unit_uid, '', '', S1o3QZ1BeZH FROM ds_pmtct_anc_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'S4IL4ti1tUA', period, org_unit_uid, '', '', S4IL4ti1tUA FROM ds_pmtct_anc_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'qi2rTZFxfgI', period, org_unit_uid, '', '', qi2rTZFxfgI FROM ds_pmtct_anc_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'eTvAQrz3mjG', period, org_unit_uid, '', '', eTvAQrz3mjG FROM ds_pmtct_anc_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'YroT7szontI', period, org_unit_uid, '', '', YroT7szontI FROM ds_pmtct_anc_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'ysnacwDrzjU', period, org_unit_uid, '', '', ysnacwDrzjU FROM ds_pmtct_anc_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'hxta9maIFSB', period, org_unit_uid, '', '',  hxta9maIFSB FROM ds_pmtct_anc_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'QfOR3z2RdhP', period, org_unit_uid, '', '',  QfOR3z2RdhP FROM ds_pmtct_anc_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'jQgqywlptQl', period, org_unit_uid, '', '',  jQgqywlptQl FROM ds_pmtct_anc_dhis2_three_periods WHERE org_unit_uid <> ''
UNION
SELECT 'kwbcflQPEyx', period, org_unit_uid, '', '',  kwbcflQPEyx FROM ds_pmtct_anc_dhis2_three_periods WHERE org_unit_uid <> ''
ORDER BY period ASC