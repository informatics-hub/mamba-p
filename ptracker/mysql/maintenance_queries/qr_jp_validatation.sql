-- ANC 03.2 --
select 
ptracker_id,
district,
facility, 
hiv_test_status, 
visit_type, 
encounter_datetime, 
date_created  
from vw_encounter_mch 
where facility = 'Ongha clinic' 
and hiv_test_status = 'Not tested for HIV during this visit' 
and visit_type = 'Antenatal'
AND encounter_datetime BETWEEN '2017-11-01 00:00:00' AND '2017-11-31 00:00:00'

-- ANC 09.1 --
SELECT 
ptracker_id,
region,
district,
facility,
viral_load_results, 
viral_load_copies,
anc_visit_type
FROM vw_encounter_mch
WHERE facility = 'Ongha clinic'
and visit_type = 'Antenatal'
and (viral_load_results = 'Not Detected' AND viral_load_copies < 40)
and anc_visit_type = 'New ANC Visit'
AND encounter_datetime BETWEEN '2017-11-01 00:00:00' AND '2017-11-31 00:00:00'

-- ANC 11 -- no type of art inititation for this facility in this period 
SELECT 
ptracker_id,
region,
district,
facility,
art_initiation, 
viral_load_copies,
anc_visit_type
FROM vw_encounter_mch
WHERE facility = 'Ongha clinic'
and visit_type = 'Antenatal'
-- and art_initiation = 'Already on ART before current pregnancy'
-- and anc_visit_type = 'New ANC Visit'
AND encounter_datetime BETWEEN '2017-11-01 00:00:00' AND '2017-11-31 00:00:00'

-- ANC 12 --

SELECT 
region,
district,
facility,
art_initiation,
art_start_date,
hiv_test_result,
encounter_datetime,
-- COUNT(if((hiv_test_result = 'HIV Positive' AND art_initiation IS NULL) ,patient_id, NULL )) AS not_started_on_art,
-- COUNT(if((art_initiation IS NULL AND art_start_date IS NOT NULL) ,if((art_start_date < encounter_datetime), patient_id, NULL), NULL)) AS already_on_art,
if((art_initiation IS NULL AND art_start_date IS NOT NULL) ,if((art_start_date < encounter_datetime OR DATEDIFF(encounter_datetime, art_start_date) / 7 > 2), patient_id, NULL), if(art_initiation = 'Already on ART before current pregnancy' ,patient_id, NULL )) AS already_on_art
-- COUNT(if((art_initiation IS NULL AND art_start_date IS NOT NULL) ,if((art_start_date < encounter_datetime AND anc_visit_type <> 'New ANC Visit'), patient_id, NULL), if((hiv_test_result = 'HIV Positive' AND anc_visit_type <> 'New ANC Visit') ,patient_id, NULL ))) AS not_started_on_art,
FROM vw_encounter_mch
WHERE facility = 'Otjomuise clinic'
and visit_type = 'Antenatal'
-- and art_initiation = 'Already on ART before current pregnancy'
-- and anc_visit_type = 'New ANC Visit'
AND encounter_datetime BETWEEN '2017-11-01 00:00:00' AND '2017-11-31 00:00:00'
-- ANC 13 --

SELECT 
ptracker_id,
region,
district,
facility,
art_initiation, 
hiv_test_result
FROM vw_encounter_mch
WHERE facility = 'Otjomuise clinic'
and visit_type = 'Antenatal'
-- and art_initiation = 'Already on ART before current pregnancy'
-- and anc_visit_type = 'New ANC Visit'
AND encounter_datetime BETWEEN '2017-11-01 00:00:00' AND '2017-11-31 00:00:00'

-- L&D MAT 01 --
SELECT 
ptracker_id,
region,
district,
facility,
patient_id, -- MAT 01
FROM vw_encounter_mch
WHERE facility = 'Mupini Health centre'
and visit_type = 'Labor and Delivery' 
and encounter_datetime BETWEEN '2017-11-01 00:00:00' AND '2017-11-31 00:00:00'

-- MAT 04 ---
SELECT 
ptracker_id,
region,
district,
facility,
patient_id, 
hiv_test_status
FROM vw_encounter_mch
WHERE facility = 'Mupini Health centre'
and visit_type = 'Labor and Delivery' 
and encounter_datetime BETWEEN '2017-11-01 00:00:00' AND '2017-11-31 00:00:00'

-- MAT 05 ---
SELECT 
ptracker_id,
region,
district,
facility,
patient_id, 
hiv_test_status,
hiv_test_result
FROM vw_encounter_mch
WHERE facility = 'Mupini Health centre'
and visit_type = 'Labor and Delivery' 
and encounter_datetime BETWEEN '2017-11-01 00:00:00' AND '2017-11-31 00:00:00'
-- MAT 11 ---
SELECT 
ptracker_id,
region,
district,
facility,
patient_id, 
infant_recieved_arv
FROM vw_encounter_mch
WHERE facility = 'Mupini Health centre'
and visit_type = 'Labor and Delivery' 
and encounter_datetime BETWEEN '2017-11-01 00:00:00' AND '2017-11-31 00:00:00'

-- MAT 13 ---

SELECT 
ptracker_id,
region,
district,
facility,
patient_id, 
breastfeeding
FROM vw_encounter_mch
WHERE facility = 'Mupini Health centre'
and visit_type = 'Labor and Delivery' 
and encounter_datetime BETWEEN '2017-11-01 00:00:00' AND '2017-11-31 00:00:00'

-- tst --- 
select if((hiv_test_result = 'HIV Positive') ,1, NULL ) AS test_status from vw_encounter_mch