
/*
Version: 	1.0
Author:		Ezekiel K. Chiteri
Date:		12 Sept 2017

Change log:
13 Sept 2017
Creation
*/
SELECT
	visit_uuid,
	district,
	facility,
	patient_id,
	family_name,
	gender,
	count( encounter_uuid ) AS Followup 
FROM
	vw_all_encounters 
GROUP BY
	patient_id 
HAVING
	Followup > 1 
	AND visit_uuid IN ( '2549af50-75c8-4aeb-87ca-4bb2cef6c69a' ) 
ORDER BY
	facility