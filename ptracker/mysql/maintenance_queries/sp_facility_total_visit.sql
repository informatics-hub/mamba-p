/*
Version: 	1.0
Author:		Ezekiel K. Chiteri
Date:		12 Sept 2017

Change log:
12 Sept 2017
Creation
*/ 
SELECT
	location_id,
	region,
	district,
	facility,
	COUNT( IF ( visit_uuid = '2549af50-75c8-4aeb-87ca-4bb2cef6c69a', visit_uuid, NULL ) ) AS ANC,
	COUNT( IF ( visit_uuid = '2678423c-0523-4d76-b0da-18177b439eed', visit_uuid, NULL ) ) AS LnD,
	COUNT( IF ( visit_uuid = '269bcc7f-04f8-4ddc-883d-7a3a0d569aad', visit_uuid, NULL ) ) AS MotherPNC,
	COUNT( IF ( visit_uuid = 'af1f1b24-d2e8-4282-b308-0bf79b365584', visit_uuid, NULL ) ) AS InfantPNC,
	COUNT( visit_uuid ) AS Totals 
FROM
	vw_all_encounters 
GROUP BY
	location_id;