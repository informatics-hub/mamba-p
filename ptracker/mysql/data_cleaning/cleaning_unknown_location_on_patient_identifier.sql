-- STEP 1: [CREATE TEMPORARY TABLE TO HOLD CORRECT LOCATION ID] --

DROP TABLE IF EXISTS temp_patient_identifier_user_location;
CREATE TABLE IF NOT EXISTS temp_patient_identifier_user_location (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT NOT NULL,
    user_name VARCHAR(255) NOT NULL,
    person_id INT NOT NULL,
    given_name VARCHAR(255) NOT NULL,
    family_name VARCHAR(255) NOT NULL,
    location_id INT NOT NULL
)  ENGINE=INNODB;

-- STEP 2: [INSERT INTO THE TEMPORARY TABLE] --

INSERT INTO temp_patient_identifier_user_location (
		user_id,
    user_name,
    person_id,
    given_name,
    family_name,
    location_id
)
SELECT 
	s.user_id, 
	s.username, 
	s.person_id, 
	p.given_name, 
	p.family_name, 
	NULL AS location_id 
FROM users s
JOIN person_name p ON s.person_id = p.person_id
WHERE s.user_id IN (SELECT DISTINCT creator FROM patient_identifier WHERE location_id=1);

-- STEP 3: [MANUALLY UPDATE THE TEMPORARY TABLE]---

-- Find the ptracker_id with 'Unknown' location created by the user_id = 150 in question to get the first five digit of for mfl_code
SELECT * FROM patient_identifier WHERE identifier_type = 5 AND patient_id IN (SELECT patient_id FROM patient_identifier WHERE creator = 150 AND location_id = 1);
-- Get the facility name using the mfl_code obtained from the first five digit of ptracker_id above
SELECT name FROM location_uids WHERE mfl_code = 12214;
-- Using the facility name above get the location_id for the patien
SELECT location_id, name FROM location WHERE `name` = 'Maxuilili Clinic';
-- Update the location id obtained above to the temp_patient_identifier_user_location on corresponding user record for user_id =150
UPDATE temp_patient_identifier_user_location SET location_id = 488 WHERE user_id = 8;

-- STEP 4:  [Verify the 'Unknown location' to be corrected on the patient_identifier table from from the temporary table] --
SELECT p.identifier, p.identifier_type, p.creator, t.user_id, p.location_id AS patient_identifier_location_id, t.location_id AS temp_location_id  FROM patient_identifier p
JOIN temp_patient_identifier_user_location t ON p.creator = t.user_id
WHERE p.location_id = 1 
AND t.location_id <> 0;

/* 
copy the resulting identifiers below to verify
10G1KL
124UNA
12FD1F
138HJK
12TNWN
*/

-- STEP 5:  [Update the 'Unknown location' with the corrected location from the temp_patient_identifier_user_location table] --
UPDATE patient_identifier e
JOIN temp_patient_identifier_user_location t ON e.creator = t.user_id
SET e.location_id =  t.location_id 
WHERE e.location_id = 1
AND t.location_id <> 0;

-- STEP 6:  [Confirm the UPDATE was successful] --
SELECT * FROM patient_identifier WHERE identifier IN ('10G1KL',
'124UNA',
'12FD1F',
'138HJK',
'12TNWN')

