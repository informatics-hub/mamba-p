/*
PTracker facility based comprehensive report 
*/ 

-- Week in numbers
SELECT
	COUNT( DISTINCT ( IF ( visit_uuid = '2549af50-75c8-4aeb-87ca-4bb2cef6c69a' AND anc_visit_type = 'New ANC Visit', encounter_uuid, NULL ) ) ) AS FirstANC,
	COUNT( DISTINCT ( IF ( visit_uuid = '2549af50-75c8-4aeb-87ca-4bb2cef6c69a' AND anc_visit_type = 'Return ANC Visit', encounter_uuid, NULL ) ) ) AS FollowupANC,
	COUNT( DISTINCT ( IF ( visit_uuid = '2678423c-0523-4d76-b0da-18177b439eed', encounter_uuid, NULL ) ) ) AS Delivery,
	COUNT( DISTINCT ( IF ( visit_uuid = '269bcc7f-04f8-4ddc-883d-7a3a0d569aad', encounter_uuid, NULL ) ) ) AS MotherPNC,
	COUNT( DISTINCT ( IF ( visit_uuid = 'af1f1b24-d2e8-4282-b308-0bf79b365584', encounter_uuid, NULL ) ) ) AS InfantPNC,
	COUNT( DISTINCT ( encounter_uuid ) ) AS TotalsVisits 
FROM
	mds_encounter_mch 
WHERE
	voided = 0 
	AND (
		( encounter_datetime BETWEEN $P { FromDate } AND $P { ToDate } ) 
		OR (
			( $P { FromDate } = '' OR $P { FromDate } IS NULL ) 
			OR ( $P { ToDate } = '' OR $P { ToDate } IS NULL ) 
		) 
	)

-- PTracker Timeliness of data received  per facility
SELECT
	region,
	district,
	facility,
	COUNT( DISTINCT encounter_id ) AS total,
	COUNT( DISTINCT ( IF ( ( DATEDIFF( date_created, encounter_datetime ) / 7 <= 1 ), encounter_uuid, NULL ) ) ) AS timely,
	COUNT( DISTINCT ( IF ( ( DATEDIFF( date_created, encounter_datetime ) / 7 > 1 ), encounter_uuid, NULL ) ) ) AS late 
FROM
	mds_encounter_mch 
WHERE
	(
		( encounter_datetime BETWEEN $P { FromDate } AND $P { ToDate } ) 
		OR (
			( $P { FromDate } = '' OR $P { FromDate } IS NULL ) 
			OR ( $P { ToDate } = '' OR $P { ToDate } IS NULL ) 
		) 
	) 
	AND voided = 0 
	AND facility = $P { facility } 
GROUP BY
	facility 
	
-- Testing coverage at ANC
SELECT
	COUNT( DISTINCT ( IF ( visit_uuid = '2549af50-75c8-4aeb-87ca-4bb2cef6c69a' AND anc_visit_type = 'New ANC Visit', encounter_uuid, NULL ) ) ) AS FirstANC,
	COUNT( DISTINCT ( IF ( visit_uuid = '2549af50-75c8-4aeb-87ca-4bb2cef6c69a' AND hiv_test_status = 'Tested for HIV during this visit' AND anc_visit_type = 'New ANC Visit', patient_id, NULL ) ) ) AS hiv_tested,
	COUNT(
		DISTINCT (
		IF
			( visit_uuid = '2549af50-75c8-4aeb-87ca-4bb2cef6c69a' AND ( hiv_test_status = 'Not tested for HIV during this visit' OR hiv_test_status = 'Missing' OR hiv_test_status IS NULL OR hiv_test_status = 'Previously known positive' ) AND anc_visit_type = 'New ANC Visit', patient_id, NULL ) 
		) 
	) AS hiv_not_tested,
	COUNT( DISTINCT ( IF ( visit_uuid = '2549af50-75c8-4aeb-87ca-4bb2cef6c69a' AND hiv_test_status = 'Previously known positive' AND anc_visit_type = 'New ANC Visit', patient_id, NULL ) ) ) AS hiv_kp,
	COUNT( DISTINCT ( IF ( visit_uuid = '2549af50-75c8-4aeb-87ca-4bb2cef6c69a' AND hiv_test_result = 'HIV Positive' AND anc_visit_type = 'New ANC Visit', patient_id, NULL ) ) ) AS new_pos,
	COUNT( DISTINCT ( IF ( visit_uuid = '2549af50-75c8-4aeb-87ca-4bb2cef6c69a' AND hiv_test_result = 'Missing' AND anc_visit_type = 'New ANC Visit', patient_id, NULL ) ) ) AS missing_hiv_test_result,
	COUNT( DISTINCT ( IF ( visit_uuid = '2549af50-75c8-4aeb-87ca-4bb2cef6c69a' AND hiv_test_result = 'Unknown' AND anc_visit_type = 'New ANC Visit', patient_id, NULL ) ) ) AS unknown_hiv_test_result 
FROM
	mds_encounter_mch 
WHERE
	voided = 0 
	AND (
		( encounter_datetime BETWEEN $P { FromDate } AND $P { ToDate } ) 
		OR (
			( $P { FromDate } = '' OR $P { FromDate } IS NULL ) 
			OR ( $P { ToDate } = '' OR $P { ToDate } IS NULL ) 
		) 
	) 
	AND facility = $P { facility}
	