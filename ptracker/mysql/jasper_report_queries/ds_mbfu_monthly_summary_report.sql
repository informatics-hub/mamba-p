/*
Version: 	1.0
Author:		Ezekiel K. Chiteri
Date:		03 Oct 2017

Change log:
Date:		07 Nov 2017

Change log:
Date:		10 May 2018
Corrected age deaggregation for 2 hiv on infants
Corrected age deaggregation for 3 hiv on infants
Fixed bugs found on value fileds
ARV given to infant should be from L&D on indicator nine.

Change log:
Date:		08 Feb 2019
Replaced count variable from 'patient_id' to encounter_uuid to fix a single distinct counting of infant who falls in two different age disaggregations
Correct comments to point to the right indicators on the summary report

Change log:
Date:		06 March 2020
Update:
1. changed facility to encounter facility.
2. refactored indicator 8. negative final test result to get final_test_result = "NEGATIVE"

*/

SELECT 
				region,
				district,
				encounter_facility,
				encounter_datetime,
				org_unit_uid,
				'201710' AS period,
				DATE_FORMAT(NOW() ,'%Y-%m-01') AS current_date_month,
				COUNT(DISTINCT(IF(visit_uuid = 'af1f1b24-d2e8-4282-b308-0bf79b365584' , encounter_uuid, NULL))) AS total_mbfu, -- newly registered for the month
				COUNT(DISTINCT(IF(hiv_exposure = 'Currently Exposed', encounter_uuid, NULL ))) AS exposed, -- 1. exposed first visit
				COUNT(DISTINCT(IF(hiv_exposure = 'Currently Exposed' AND ROUND(DATEDIFF(encounter_datetime, birthdate)/7) <= 8, encounter_uuid, NULL ))) AS exposed_0_8wks, -- exposed 0_8 wks
				COUNT(DISTINCT(IF(hiv_exposure = 'Currently Exposed' AND (ROUND(DATEDIFF(encounter_datetime, birthdate)/7) >= 9 AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43) <= 8), encounter_uuid, NULL ))) AS exposed_9wks_8mth, -- exposed 9wks to 8months
				COUNT(DISTINCT(IF(hiv_exposure = 'Currently Exposed' AND (ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43)  BETWEEN 9 AND 12), encounter_uuid, NULL ))) AS exposed_9_12mth, -- exposed 9 to 12months
				COUNT(DISTINCT(IF(hiv_exposure = 'Currently Exposed' AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43)  > 12, encounter_uuid, NULL ))) AS exposed_greater_than_12mth, -- exposed more than 12months
				
				COUNT(DISTINCT(IF(hiv_exposure IS NULL AND visit_uuid = 'af1f1b24-d2e8-4282-b308-0bf79b365584', encounter_uuid, NULL ))) AS repeat_visit, -- 2. exposed repeat visit already
				
				COUNT(DISTINCT(IF(hiv_test_type = 'DNA PCR' AND ROUND(DATEDIFF(encounter_datetime, birthdate)/7)  >=  6, encounter_uuid, NULL ))) AS first_hiv_tested_total, -- 3. HIV exposed children tested for HIV - 1st test
				COUNT(DISTINCT(IF(hiv_test_type = 'DNA PCR'  AND (ROUND(DATEDIFF(encounter_datetime, birthdate)/7) <= 8), encounter_uuid, NULL ))) AS six_eight_wks_first_hiv_tested, -- 6 to 8weeks first hiv test
				COUNT(DISTINCT(IF(hiv_test_type = 'DNA PCR'  AND (ROUND(DATEDIFF(encounter_datetime, birthdate)/7)  >= 9 AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43)  <= 8), encounter_uuid, NULL ))) AS nine_wks_eight_months_first_hiv_tested, -- 6weeks to 8months first hiv test
				COUNT(DISTINCT(IF(hiv_test_type = 'DNA PCR'  AND (ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43) BETWEEN 9 AND 12), encounter_uuid, NULL ))) AS nine_twelve_months_first_hiv_tested, -- 9 to 12 months first hiv test
				COUNT(DISTINCT(IF(hiv_test_type = 'DNA PCR'  AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43) > 12, encounter_uuid, NULL ))) AS twelve_more_months_first_hiv_tested,
				
				COUNT(DISTINCT(IF(hiv_test_type = 'Rapid Test' /* AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43)  >=  9*/, encounter_uuid, NULL ))) AS second_hiv_tested_total, -- 4. HIV exposed children tested for HIV - repeat test
				COUNT(DISTINCT(IF(hiv_test_type = 'Rapid Test'  AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43)  <  9, encounter_uuid, NULL ))) AS nine_less_months_second_hiv_tested, -- less than 9months second hiv test
				COUNT(DISTINCT(IF(hiv_test_type = 'Rapid Test'  AND (ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43)  BETWEEN 9 AND 12), encounter_uuid, NULL ))) AS nine_twelve_months_second_hiv_tested, -- 9 to 12 months second hiv test
				COUNT(DISTINCT(IF(hiv_test_type = 'Rapid Test'  AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43)  > 12, encounter_uuid, NULL ))) AS twelve_more_months_second_hiv_tested, -- more than 12months second hiv test
				
				COUNT(DISTINCT(IF(confirmatory_test_done = 'Yes' /*AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43)  >=  9*/, encounter_uuid, NULL ))) AS third_hiv_tested_total, -- 5. Confirmatory/3rd test done
				COUNT(DISTINCT(IF(confirmatory_test_done = 'Yes'  AND (ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43) BETWEEN 9 AND 17), encounter_uuid, NULL ))) AS nine_seventeen_months_third_hiv_tested,
				COUNT(DISTINCT(IF(confirmatory_test_done = 'Yes'  AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43)  >= 18, encounter_uuid, NULL ))) AS eighteen_more_months_third_hiv_tested,
				
				COUNT(DISTINCT(IF(final_test_result = 'Positive' /* AND ROUND(DATEDIFF(encounter_datetime, birthdate)/7)  >=  6*/, encounter_uuid, NULL ))) AS hiv_pos_total, -- 6. children diagnosed (confirmed) HIV positive
				COUNT(DISTINCT(IF(final_test_result = 'Positive'  AND (ROUND(DATEDIFF(encounter_datetime, birthdate)/7)  >=  6 AND ROUND(DATEDIFF(encounter_datetime, birthdate)/7)  <= 8), encounter_uuid, NULL ))) AS six_eight_wks_hiv_pos, -- 6 to 8 weeks hiv test
				COUNT(DISTINCT(IF(final_test_result = 'Positive'  AND (ROUND(DATEDIFF(encounter_datetime, birthdate)/7)  >=  9 AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43)  <= 8), encounter_uuid, NULL ))) AS nine_wks_eight_months_hiv_pos, -- 9weeks to 8months hiv test
				COUNT(DISTINCT(IF(final_test_result = 'Positive'  AND (ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43)  BETWEEN 9 AND 12), encounter_uuid, NULL ))) AS nine_twelve_months_hiv_pos, -- 12 months and more hiv test
				COUNT(DISTINCT(IF(final_test_result = 'Positive'  AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43) > 12, encounter_uuid, NULL ))) AS twelve_more_months_hiv_pos,
				
				COUNT(DISTINCT(IF(art_link = 'Yes'  AND final_test_result = 'Positive',encounter_uuid, NULL ))) AS art_linked, -- 7. art linked
				COUNT(DISTINCT(IF(final_test_result = 'Negative', encounter_uuid, NULL ))) AS hiv_neg, -- 8. negative final test result
				COUNT(DISTINCT(IF(arv_prophylaxis_status = 'Received ARV prophylaxis' AND (ROUND(DATEDIFF(encounter_datetime, birthdate)/7)  >=  6 AND ROUND(DATEDIFF(encounter_datetime, birthdate)/7)  <= 8) ,encounter_uuid, NULL ))) AS receive_arv, -- 9. arv prophylaxis status
				COUNT(DISTINCT(IF( mother_id IN (SELECT encounter_uuid FROM mds_encounter_mch WHERE visit_uuid = '2678423c-0523-4d76-b0da-18177b439eed' AND infant_recieved_arv = 'ARV Prophylaxis daily up to 6 weeks'), encounter_uuid, NULL ))) AS receive_ctx, -- 10. ctx prophylaxis status {the infant who received ARV prophylaxis from Labor and Delivery}
				COUNT(DISTINCT(IF(hiv_exposure = 'Currently Exposed' AND mother_id IS NOT NULL AND mother_id IN (SELECT encounter_uuid FROM mds_encounter_mch WHERE visit_uuid = '269bcc7f-04f8-4ddc-883d-7a3a0d569aad' AND infant_breastfeeding = 'BREASTFED EXCLUSIVELY') AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30.43) <= 6, encounter_uuid, NULL))) AS breastfeeding -- 11. infant breastfeeding
				FROM mds_encounter_mch
				WHERE visit_uuid in ('af1f1b24-d2e8-4282-b308-0bf79b365584')
			  AND voided = 0
				AND facility = 'Ndama Clinic'
				AND  encounter_datetime BETWEEN  '2017-10-01 00:00:00' AND '2017-10-28 00:00:00'				