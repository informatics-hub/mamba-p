SELECT 
					COUNT(DISTINCT(IF((arv_prophylaxis_status = 'ARV Prophylaxis daily up to 6 weeks' OR arv_prophylaxis_status = 'Received ARV Prophylaxis'), patient_id, NULL))) AS Number,
					'Infant ARV prophylaxis at delivery' AS caption,
					'TOTAL' AS AGE_GROUP
FROM vw_revised_encounter_mch
WHERE visit_uuid in ('2678423c-0523-4d76-b0da-18177b439eed')
					AND voided = 0
UNION
SELECT 
					COUNT(DISTINCT(IF((ctx_prophylaxis_status = 'Received CTX prophylaxis'), patient_id, NULL))) AS Number,
					'Infant CTX prophylaxis' AS caption,
					'TOTAL' AS AGE_GROUP
FROM vw_revised_encounter_mch
WHERE visit_uuid in ('af1f1b24-d2e8-4282-b308-0bf79b365584')
					AND voided = 0
UNION
SELECT 
					COUNT(DISTINCT(IF((hiv_test_status = 'Tested for HIV during this visit'), patient_id, NULL))) AS Number,
					'Infant HIV tested' AS caption,
					'TOTAL' AS AGE_GROUP
FROM vw_revised_encounter_mch
WHERE visit_uuid in ('af1f1b24-d2e8-4282-b308-0bf79b365584')
					AND voided = 0
					UNION
SELECT 
					COUNT(DISTINCT(IF((dnapcr_test_result = 'Positive' OR dnapcr_test_result = 'Negative' OR rapid_test_result = 'Positive' OR rapid_test_result = 'Negative'), patient_id, NULL))) AS Number,
					'Recieved HIV results' AS caption,
					'TOTAL' AS AGE_GROUP
FROM vw_revised_encounter_mch
WHERE visit_uuid in ('af1f1b24-d2e8-4282-b308-0bf79b365584')
					AND voided = 0
					UNION
SELECT 
					COUNT(DISTINCT(IF((dnapcr_test_result = 'Positive' OR rapid_test_result = 'Positive'), patient_id, NULL))) AS Number,
					'HIV Positive Infants' AS caption,
					'TOTAL' AS AGE_GROUP
FROM vw_revised_encounter_mch
WHERE visit_uuid in ('af1f1b24-d2e8-4282-b308-0bf79b365584')
					AND voided = 0
UNION
SELECT 
					COUNT(DISTINCT(IF((art_link = 'Yes'), patient_id, NULL))) AS Number,
					'Infant on ART' AS caption,
					'TOTAL' AS AGE_GROUP
FROM vw_revised_encounter_mch
WHERE visit_uuid in ('af1f1b24-d2e8-4282-b308-0bf79b365584')
					AND voided = 0
					