/*
Version: 	1.0
Author:		Ezekiel K. Chiteri
Date:		10 Nov 2017

Change log:
Date:		10 Nov 2017
Creation
Date:		11 May 2018
Update: added voided clause
				changed indicator 04, 08.1 and 08.2
Update: Added Infant received NVP + AZT prophylaxis up to 6 indicator on MAT 12

Change log:
Date:		20 Feb 2020
Updated: Revised after program review of the summary report

Change log:
Date:		6 March 2020
Added: Added anc_hiv_test_status to MAT 2.1
				
*/

SELECT 
	region,
	district,
	encounter_facility,
	encounter_datetime,
	COUNT(DISTINCT patient_id) AS total_delivery, -- MAT 01
	COUNT(DISTINCT (if(patient_id IN (SELECT distinct patient_id FROM mds_encounter_mch WHERE visit_uuid = '2549af50-75c8-4aeb-87ca-4bb2cef6c69a' AND (hiv_test_status = 'Previously known Positive' OR hiv_test_result IN ('HIV Positive', 'Unknown') OR anc_booked = 'Yes')), patient_id, NULL ))) AS add_to_mat02, -- These are patient who came to L&D with previous known HIV status at ANC in PTracker. Don't count Negatives because they will be tested at L&D
	COUNT(DISTINCT(if(hiv_test_status = 'Previously known positive' OR hiv_re_test_36wks = 'NEGATIVE' OR hiv_re_test_36wks = 'HIV Positive' OR anc_hiv_test_status = 'NEGATIVE', patient_id, NULL ))) AS known_hiv_status, -- MAT 02 Those that came to L&D and their test status is KP, were negative at ANC hence re-tested at L&D 
	COUNT(DISTINCT(if(patient_id IN (SELECT distinct patient_id FROM mds_encounter_mch WHERE visit_uuid = '2549af50-75c8-4aeb-87ca-4bb2cef6c69a' AND (hiv_test_status = 'Previously known Positive' OR hiv_test_result = 'HIV Positive')) /* AND anc_hiv_test_status = 'Previously Known Positive' */, patient_id, NULL ))) AS known_hiv_pos_status, -- MAT 02.1
	COUNT(DISTINCT(if(hiv_re_test_36wks = 'HIV Positive' OR hiv_re_test_36wks = 'NEGATIVE', patient_id, NULL ))) AS retest_36_wks, -- MAT 03
	COUNT(DISTINCT(if(hiv_re_test_36wks = 'HIV Positive', patient_id, NULL ))) AS pos_retest_36_wks, -- MAT 03.1
	COUNT(DISTINCT(if(hiv_test_status <> 'Previously Known Positive' AND (hiv_test_result IS NULL OR hiv_test_result = 'Unknown' OR  hiv_test_result = 'Missing') AND hiv_re_test_36wks IN ('Missing', 'Not tested for HIV during this visit') , patient_id, NULL ))) AS unknown_hiv_status, -- MAT 04
	COUNT(DISTINCT(if(hiv_test_status = 'Tested for HIV during this visit', patient_id, NULL ))) AS newly_hiv_tested, -- MAT 05
	COUNT(DISTINCT(if(hiv_test_result = 'HIV Positive',  patient_id, NULL ))) AS newly_hiv_tested_pos, -- MAT 05.1 Consider using hiv_test_status
	COUNT(DISTINCT(if(hiv_test_result = 'Negative', patient_id, NULL ))) AS newly_hiv_tested_neg, -- for CHECK-SUM
	COUNT(DISTINCT(if(hiv_test_status = 'Previously known positive' OR hiv_test_result = 'HIV Positive' OR hiv_re_test_36wks = 'HIV Positive', patient_id, NULL ))) AS all_hiv_pos, -- MAT 06
	COUNT(DISTINCT(if(art_initiation = 'Already on ART before current pregnancy', patient_id, NULL ))) AS already_on_art, -- MAT 07
	COUNT(DISTINCT(if(patient_id IN 
		(SELECT distinct patient_id FROM mds_encounter_mch WHERE 
			(art_initiation = 'Started on ART in ANC current pregnancy') 
			/* Getting clients whose ANC encounter is current to this pregnancy */
			AND ROUND(DATEDIFF(encounter_datetime, art_start_date)/7) >= 4 
			AND visit_uuid = '2549af50-75c8-4aeb-87ca-4bb2cef6c69a')
			/* The next OR is supposed to capture those started on ART in L&D */
			OR (art_initiation = 'Started on ART in ANC current pregnancy' AND ROUND(DATEDIFF(encounter_datetime, art_start_date)/7) >= 4), patient_id, NULL)
		)) AS art_4wks_plus, -- MAT 08.1 Sum for MAT 08 started on art at anc
	COUNT(DISTINCT(if(patient_id IN 
		(SELECT distinct patient_id FROM mds_encounter_mch WHERE 
			(art_initiation = 'Started on ART in ANC current pregnancy') 
			/* Getting clients whose ANC encounter is current to this pregnancy */
			AND ROUND(DATEDIFF(encounter_datetime, art_start_date)/7) <= 4 
			AND visit_type = 'Antenatal')
			/* The next OR is supposed to capture those started on ART in L&D */
			OR (art_initiation = 'Started on ART in ANC current pregnancy' AND ROUND(DATEDIFF(encounter_datetime, art_start_date)/7) <= 4), patient_id, NULL)
		)) AS art_4wks_less, -- MAT 08.2 Sum for MAT 08 started on art at anc
	COUNT(DISTINCT(if(art_initiation = 'Already on ART before current pregnancy' OR art_initiation = 'Started on ART in ANC current pregnancy', patient_id, NULL ))) AS art_initiated, -- MAT 08 sum art_4wks_plus and art_4wks_less
	COUNT(DISTINCT(if(art_initiation = 'Started on ART in ANC current pregnancy', patient_id, NULL ))) AS art_initiated_at_delivery,  -- MAT 09
	COUNT(DISTINCT(if(art_initiation = 'Refused ART', patient_id, NULL ))) AS refused_art,  -- MAT 10
	COUNT(DISTINCT(if(infant_recieved_arv = 'Received ARV Prophylaxis', patient_id, NULL ))) AS infant_recieved_arv,  -- MAT 11
	COUNT(DISTINCT(if(infant_recieved_arv = 'Infant received NVP + AZT prophylaxis up to 6 weeks', patient_id, NULL ))) AS infant_recieved_arv_nvp,  -- MAT 12. PTracker is not collecting this
	COUNT(DISTINCT(if(breastfeeding = 'BREASTFED EXCLUSIVELY', patient_id, NULL ))) AS excl_bf,  -- MAT 13
	COUNT(DISTINCT(if(breastfeeding = 'Replacement feeding', patient_id, NULL ))) AS replacement_bf  -- MAT 14
	FROM mds_encounter_mch
WHERE visit_uuid in ('2678423c-0523-4d76-b0da-18177b439eed')
AND encounter_datetime BETWEEN '2017-11-01 00:00:00' AND '2017-11-30 00:00:00'
AND voided = 0
-- AND facility = 'Ndama Clinic'
GROUP BY facility;