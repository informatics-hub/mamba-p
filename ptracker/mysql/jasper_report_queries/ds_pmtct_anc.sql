/*
Version: 	1.0
Author:		Ezekiel K. Chiteri
Date:		12 Feb 2019
Description: Create a ds_pmtct_anc report view

Change log:
Date:		19 Feb 2019
Description: Removed null on org_unit_uid column
*/
DROP VIEW
IF
	EXISTS ds_pmtct_anc;
CREATE VIEW ds_pmtct_anc AS
SELECT 
					region,
					district,
					facility,
					org_unit_uid, 
					'201711' as period,
					DATE_FORMAT(NOW() ,'%Y-%m-01') AS current_date_month,
					COUNT(DISTINCT(patient_id)) AS total_anc, 
					COUNT(DISTINCT(if(hiv_test_status = 'Previously known positive', patient_id, NULL ))) AS hiv_kp,
					COUNT(DISTINCT(if(hiv_test_status <> 'Previously known positive', patient_id, NULL ))) AS pre_counseled,
					COUNT(DISTINCT(if(hiv_test_status = 'Tested for HIV during this visit' ,patient_id, NULL ))) AS hiv_tested,
					COUNT(DISTINCT(if(hiv_test_status = 'Not tested for HIV during this visit' AND anc_visit_type = 'New ANC Visit',patient_id, NULL ))) AS hiv_not_tested,
					COUNT(DISTINCT(if(hiv_test_result = 'HIV Positive' ,patient_id, NULL ))) AS new_pos,
					COUNT(DISTINCT(if(hiv_test_result = 'HIV Positive' ,patient_id, NULL ))) AS post_counseled_pos,
					COUNT(DISTINCT(if(hiv_test_result = 'Negative' ,patient_id, NULL ))) AS new_neg,
					COUNT(DISTINCT(if(hiv_test_result = 'Negative' ,patient_id, NULL ))) AS post_counseled_neg,
					COUNT(DISTINCT(if((hiv_test_status = 'Tested for HIV during this visit' AND (hiv_test_result IS NULL OR hiv_test_result = "Missing" OR hiv_test_result = "Unknown")), patient_id, NULL ))) AS unknown_lost,
					COUNT(DISTINCT(if(recent_viral_load = 'Yes' AND (viral_load_results IS NOT NULL OR viral_load_results <> 'Results Pending') AND anc_visit_type = 'New ANC Visit' ,patient_id, NULL ))) AS viral_load,
					COUNT(DISTINCT(if((viral_load_results = 'Not Detected' OR viral_load_copies < 40) AND anc_visit_type = 'New ANC Visit' ,patient_id, NULL ))) AS vl_not_detected,
					COUNT(DISTINCT(if(art_initiation = 'Started on ART in ANC current pregnancy' AND anc_visit_type = 'New ANC Visit' ,patient_id, NULL ))) AS started_art,
					COUNT(DISTINCT(if((art_initiation IS NULL AND art_start_date IS NOT NULL) ,if((art_start_date < encounter_datetime OR DATEDIFF(encounter_datetime, art_start_date) / 7 > 2), patient_id, NULL), if(art_initiation = 'Already on ART before current pregnancy' ,patient_id, NULL )))) AS already_on_art,
					COUNT(DISTINCT(if(art_initiation = 'Refused ART' OR art_initiation = 'Not started due to stockout of ART',patient_id, NULL ))) AS not_started_on_art,
					COUNT(DISTINCT(if(art_initiation = 'Refused ART' ,patient_id, NULL ))) AS refused_art,
					COUNT(DISTINCT(if(art_initiation = 'Not started due to stockout of ART' ,patient_id, NULL ))) AS art_stockout
FROM mds_encounter_mch
WHERE visit_uuid in ('2549af50-75c8-4aeb-87ca-4bb2cef6c69a')
AND anc_visit_type = 'New ANC Visit'
					AND voided = 0
AND encounter_datetime BETWEEN '2017-11-01 00:00:00' AND '2017-11-31 00:00:00'
GROUP BY facility;