-- Total
SELECT 
					COUNT(DISTINCT(patient_id )) AS Number,
					'No. Of Infants' AS caption,
					'TOTAL' AS AGE_GROUP
FROM vw_revised_encounter_mch
WHERE visit_uuid in ('af1f1b24-d2e8-4282-b308-0bf79b365584')
					AND voided = 0
UNION
SELECT 
					COUNT(DISTINCT(IF(hiv_test_status = 'Tested for HIV during this visit', patient_id, NULL))) AS Number,
					'No. Of Infants tested on registration date' AS caption,
					'TOTAL' AS AGE_GROUP
FROM vw_revised_encounter_mch
WHERE visit_uuid in ('af1f1b24-d2e8-4282-b308-0bf79b365584')
					AND voided = 0
UNION
SELECT 
					ROUND(COUNT(DISTINCT(IF(hiv_test_status = 'Tested for HIV during this visit', patient_id, NULL)))/COUNT(DISTINCT(patient_id)), 2)*100
					AS Number,
					'Percentage of Infant tested' AS caption,
					'TOTAL' AS AGE_GROUP
FROM vw_revised_encounter_mch
WHERE visit_uuid in ('af1f1b24-d2e8-4282-b308-0bf79b365584')
					AND voided = 0
UNION
-- age group less than six weeks
SELECT 
					COUNT(DISTINCT(patient_id )) AS Number,
					'No. Of Infants' AS caption,
					'Less than 6 Weeks' AS AGE_GROUP
FROM vw_revised_encounter_mch
WHERE visit_uuid in ('af1f1b24-d2e8-4282-b308-0bf79b365584')
AND ROUND(DATEDIFF(encounter_datetime, birthdate)/7) < 6
					AND voided = 0
UNION
SELECT 
					COUNT(DISTINCT(IF(hiv_test_status = 'Tested for HIV during this visit', patient_id, NULL))) AS Number,
					'No. Of Infants tested on registration date' AS caption,
					'Less than 6 Weeks' AS AGE_GROUP
FROM vw_revised_encounter_mch
WHERE visit_uuid in ('af1f1b24-d2e8-4282-b308-0bf79b365584')
AND ROUND(DATEDIFF(encounter_datetime, birthdate)/7) < 6
					AND voided = 0
UNION
SELECT 
					ROUND(COUNT(DISTINCT(IF(hiv_test_status = 'Tested for HIV during this visit', patient_id, NULL)))/COUNT(DISTINCT(patient_id)), 2)*100
					AS Number,
					'Percentage of Infant tested' AS caption,
					'Less than 6 Weeks' AS AGE_GROUP
FROM vw_revised_encounter_mch
WHERE visit_uuid in ('af1f1b24-d2e8-4282-b308-0bf79b365584')
AND ROUND(DATEDIFF(encounter_datetime, birthdate)/7) < 6
					AND voided = 0
UNION
-- age group 6 TO 8 WEEKS
SELECT 
					COUNT(DISTINCT(patient_id )) AS Number,
					'No. Of Infants' AS caption,
					'6 to 8 Weeks' AS AGE_GROUP
FROM vw_revised_encounter_mch
WHERE visit_uuid in ('af1f1b24-d2e8-4282-b308-0bf79b365584')
AND ROUND(DATEDIFF(encounter_datetime, birthdate)/7) BETWEEN 6 AND 8
					AND voided = 0
UNION
SELECT 
					COUNT(DISTINCT(IF(hiv_test_status = 'Tested for HIV during this visit', patient_id, NULL))) AS Number,
					'No. Of Infants tested on registration date' AS caption,
					'6 to 8 Weeks' AS AGE_GROUP
FROM vw_revised_encounter_mch
WHERE visit_uuid in ('af1f1b24-d2e8-4282-b308-0bf79b365584')
AND ROUND(DATEDIFF(encounter_datetime, birthdate)/7) BETWEEN 6 AND 8
					AND voided = 0
UNION
SELECT 
					ROUND(COUNT(DISTINCT(IF(hiv_test_status = 'Tested for HIV during this visit', patient_id, NULL)))/COUNT(DISTINCT(patient_id)), 2)*100
					AS Number,
					'Percentage of Infant tested' AS caption,
					'6 to 8 Weeks' AS AGE_GROUP
FROM vw_revised_encounter_mch
WHERE visit_uuid in ('af1f1b24-d2e8-4282-b308-0bf79b365584')
AND ROUND(DATEDIFF(encounter_datetime, birthdate)/7) BETWEEN 6 AND 8
					AND voided = 0
UNION
-- age group 9 WEEKS TO 8 MONTHS
SELECT 
					COUNT(DISTINCT(patient_id )) AS Number,
					'No. Of Infants' AS caption,
					'9 Weeks - 8 Months' AS AGE_GROUP
FROM vw_revised_encounter_mch
WHERE visit_uuid in ('af1f1b24-d2e8-4282-b308-0bf79b365584')
AND ROUND(DATEDIFF(encounter_datetime, birthdate)/7) >= 9 AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30) < 8
					AND voided = 0
UNION
SELECT 
					COUNT(DISTINCT(IF(hiv_test_status = 'Tested for HIV during this visit', patient_id, NULL))) AS Number,
					'No. Of Infants tested on registration date' AS caption,
					'9 Weeks - 8 Months' AS AGE_GROUP
FROM vw_revised_encounter_mch
WHERE visit_uuid in ('af1f1b24-d2e8-4282-b308-0bf79b365584')
AND ROUND(DATEDIFF(encounter_datetime, birthdate)/7) >= 9 AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30) < 8
					AND voided = 0
UNION
SELECT 
					ROUND(COUNT(DISTINCT(IF(hiv_test_status = 'Tested for HIV during this visit', patient_id, NULL)))/COUNT(DISTINCT(patient_id)), 2)*100
					AS Number,
					'Percentage of Infant tested' AS caption,
					'9 Weeks - 8 Months' AS AGE_GROUP
FROM vw_revised_encounter_mch
WHERE visit_uuid in ('af1f1b24-d2e8-4282-b308-0bf79b365584')
AND ROUND(DATEDIFF(encounter_datetime, birthdate)/7) >= 9 AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30) < 8
					AND voided = 0
UNION
-- age group 9 WEEKS TO 8 MONTHS
SELECT 
					COUNT(DISTINCT(patient_id )) AS Number,
					'No. Of Infants' AS caption,
					'9 - 17 Months' AS AGE_GROUP
FROM vw_revised_encounter_mch
WHERE visit_uuid in ('af1f1b24-d2e8-4282-b308-0bf79b365584')
AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30) BETWEEN 9 AND 17
					AND voided = 0
UNION
SELECT 
					COUNT(DISTINCT(IF(hiv_test_status = 'Tested for HIV during this visit', patient_id, NULL))) AS Number,
					'No. Of Infants tested on registration date' AS caption,
					'9 - 17 Months' AS AGE_GROUP
FROM vw_revised_encounter_mch
WHERE visit_uuid in ('af1f1b24-d2e8-4282-b308-0bf79b365584')
AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30) BETWEEN 9 AND 17
					AND voided = 0
UNION
SELECT 
					ROUND(COUNT(DISTINCT(IF(hiv_test_status = 'Tested for HIV during this visit', patient_id, NULL)))/COUNT(DISTINCT(patient_id)), 2)*100
					AS Number,
					'Percentage of Infant tested' AS caption,
					'9 - 17 Months' AS AGE_GROUP
FROM vw_revised_encounter_mch
WHERE visit_uuid in ('af1f1b24-d2e8-4282-b308-0bf79b365584')
AND ROUND(DATEDIFF(encounter_datetime, birthdate)/30) BETWEEN 9 AND 17
					AND voided = 0