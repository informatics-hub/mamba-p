/*
Version: 	1.0
Author:		Ezekiel K. Chiteri
Date:		09 Oct 2017

Change log:
Date:		21 Nov 2017
Change log:
Date:		15 Jan 2018
Change log:
Date:		31 Jan 2018
Change log:
Comment: Revised to include all obs by grouping by both encounter_id and obs_id. Objective is to include infant born to mothers at L&D
Date:		21 Feb 2018
Change log:
Date:		22 Feb 2018
Comment: Revised to include all patients with or withouth encounters
Change log:
Date:		26 Feb 2018
Comment: Revised to capture migration of ART initiation from 160117 to 164915
Change log:
Date:		1 Mar 2018
Comment: Restored client names
Change log:
Date: 03 April 2018
Comment: View prefix name change
Change log:
Date: 12 April 2018
Comment: Add, HIV negative infant discharged from PMTCT inidicator
Change log:
Date: 28 May 2018
Comment: Add,"ARV refusal reason" as column
Change log:
Date: 12 July 2018
Added: creator and date_created
Change log:
Date: 23 August 2018
Added: columns (missing_refused_art, missing_art_start_date, missing_viral_load_test_date, missing_viral_load_copies,
missing_next_visit_date,
missing_death_date,
missing_transfer_out_date,
missing_transfer_in_date,
missing_infant_hiv_negative_outcome_date,
missing_other_breastfeeding)

Change log:
Date:		15 Feb 2019
Added org_unit_uid

Change log:
Date: 4 April 2019
Added: org_unit_uid_maternity

Change log:
Date: 13 June 2019
Modified: orgunit_uid to orgunit_uid_facility
Modified: lu.uuid to lu.ptracker_uuid

Change log:
Author:		Herobiam Heita
Date:		19 September 2019
Modified: Echiteri
Date: 25 Sept 2019
Added: added encounter location_id
Added: partner_hiv_test_done
Added: partner_hiv_test_result
Added: partner_hiv_test_date
Added: missing_partner_hiv_test_date

--
Change log:
Author:		Echiteri
Date:		17 October 2019
Added: mfl_code

Change log:
Author:		Echiteri
Date:		3 March 2020
Added: anc_hiv_test_status

--
Change log:
Author:		Echiteri
Date:		31st March 2020
Added: patient_identifier_voided

Change log:
Author:		Echiteri
Date:		21st August 2020
Modified: Modified transfer_out_to to capture the facility name

Change log:
Date: 26 August 2020
Added: Contact as phone number from person_attribute
*/ 

DROP VIEW
IF
	EXISTS ds_revised_encounter_mch;
CREATE VIEW ds_revised_encounter_mch AS 
SELECT
vp.patient_id,
vp.ptracker_id,
vp.mother_id,
vp.given_name,
vp.middle_name,
vp.family_name,
vp.contact,
vp.gender,
vp.birthdate,
vp.dead,
vp.patient_creator,
vp.patient_date_created,
vp.patient_identifier_voided,
et.uuid AS visit_uuid,
(SELECT name FROM encounter_type WHERE uuid = et.uuid ) AS visit_type,
(CASE (SELECT name FROM encounter_type WHERE uuid = et.uuid )	WHEN 'Antenatal' THEN 1 WHEN 'Labor and Delivery' THEN 2 WHEN 'Mother Postnatal' THEN 3 end) AS visit_type_code,
l.parent_location,
( SELECT parent_location FROM location WHERE location_id = l.parent_location ) AS district_parent_location,
( SELECT NAME FROM location WHERE location_id = district_parent_location ) AS region,
( SELECT NAME FROM location WHERE location_id = l.parent_location ) AS district,
( select name from address_hierarchy_entry where address_hierarchy_entry_id = (SELECT parent_id FROM address_hierarchy_entry where name = district)) AS country,
l.NAME AS facility,
lu.orgunit_uid_facility AS org_unit_uid,
lu.orgunit_uid_maternity AS org_unit_uid_maternity,
lu.mfl_code,
e.uuid AS encounter_uuid,
e.visit_id,
e.encounter_datetime,
e.date_created,
e.encounter_id,
e.creator,
(SELECT name FROM location WHERE location_id = (SELECT parent_location FROM location WHERE location_id = (SELECT parent_location FROM location WHERE location_id = e.location_id))) AS encounter_region,
(SELECT name FROM location where location_id = e.location_id) as encounter_facility,
MAX(
	IF
		( o.concept_id = 164181, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS anc_visit_type,
	MAX( IF ( o.concept_id = 5624, o.value_numeric, NULL ) ) AS gravida,
	MAX( IF ( o.concept_id = 164949 AND o.value_coded = 1, (SELECT NAME FROM concept_name WHERE concept_id = 164949 AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en'), NULL ) ) AS missing_gravida,
	MAX( IF ( o.concept_id = 1053, o.value_numeric, NULL ) ) AS para,
	MAX( IF ( o.concept_id = 164933 AND o.value_coded = 1, (SELECT NAME FROM concept_name WHERE concept_id = 164933 AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en'), NULL ) ) AS missing_para,
	MAX( IF ( o.concept_id = 1427, o.value_datetime, NULL ) )  AS lmp,
	MAX( IF ( o.concept_id = 164934 AND o.value_coded = 1, (SELECT NAME FROM concept_name WHERE concept_id = 164934 AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en'), NULL ) ) AS missing_lmp,
  MAX( IF ( o.concept_id = 5596, o.value_datetime, NULL ) ) AS edd,
	MAX( IF ( o.concept_id = 164935 AND o.value_coded = 1, (SELECT NAME FROM concept_name WHERE concept_id = 164935 AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en'), NULL ) ) AS missing_edd,
	MAX(
	IF
		( o.concept_id = 164959, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS anc_hiv_test_status,
	MAX(
	IF
		( o.concept_id = 164401, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS hiv_test_status,
	MAX(
	IF
		( o.concept_id = 159803, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS hiv_re_test_36wks,
	MAX(
	IF
		( o.concept_id = 159427, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS hiv_test_result,
	MAX(
	IF
		( o.concept_id = 161557, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS partner_hiv_test_done,
	MAX(
	IF
		( o.concept_id = 1436, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS partner_hiv_test_result,
	MAX(
	IF
		( o.concept_id = 164957, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS partner_hiv_test_date,
	MAX(
	IF
		( o.concept_id = 164958, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS partner_hiv_test_date_missing,
	MAX(
	IF
		( o.concept_id = 164915, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS art_initiation,
	vp.art_number,
	/* MAX( IF ( vp.identifier_type = 4, vp.identifier, NULL ) ) AS art_number, */
	MAX( IF ( o.concept_id = 163322, o.value_text, NULL ) ) AS refused_art,
	MAX( IF ( o.concept_id = 164937 AND o.value_coded = 1, (SELECT NAME FROM concept_name WHERE concept_id = 164937 AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en'), NULL ) ) AS missing_refused_art,
	MAX( IF ( o.concept_id = 159599, o.value_datetime, NULL ) ) art_start_date,
	MAX( IF ( o.concept_id = 164938 AND o.value_coded = 1, (SELECT NAME FROM concept_name WHERE concept_id = 164938 AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en'), NULL ) ) AS missing_art_start_date,
	MAX(
	IF
		( o.concept_id = 163310, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS recent_viral_load,
	MAX( IF ( o.concept_id = 163281, o.value_datetime, NULL ) ) AS viral_load_test_date,
	MAX( IF ( o.concept_id = 164939 AND o.value_coded = 1, (SELECT NAME FROM concept_name WHERE concept_id = 164939 AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en'), NULL ) ) AS missing_viral_load_test_date,
	MAX(
	IF
		( o.concept_id = 1305, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS viral_load_results,
	MAX( IF ( o.concept_id = 856, o.value_numeric, NULL ) ) AS viral_load_copies,
	MAX( IF ( o.concept_id = 164940 AND o.value_coded = 1, (SELECT NAME FROM concept_name WHERE concept_id = 164940 AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en'), NULL ) ) AS missing_viral_load_copies,
	MAX( IF ( o.concept_id = 163719, o.value_text, NULL ) ) AS other_breastfeeding,
	MAX( IF ( o.concept_id = 164945 AND o.value_coded = 1, (SELECT NAME FROM concept_name WHERE concept_id = 164945 AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en'), NULL ) ) AS missing_other_breastfeeding,
	MAX(
	IF
		( o.concept_id = 1719, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS anc_booked,
	MAX(
	IF
		( o.concept_id = 1856, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS mother_status,
	MAX( IF ( o.concept_id = 1641, o.value_datetime, NULL ) ) AS discharge_date,
	MAX( IF ( o.concept_id = 1568, o.value_numeric, NULL ) ) AS birth_count,
	MAX(
	IF
		( o.concept_id = 1587, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS infant_gender,
	MAX( IF ( o.concept_id = 164802, o.value_datetime, NULL ) )  AS infant_dob,
	MAX(
	IF
		( o.concept_id = 1151, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS infant_breastfeeding,
	MAX(
	IF
		( o.concept_id = 125872, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS still_birth,
	MAX(
	IF
		( o.concept_id = 159917, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS infant_status,
	MAX(
	IF
		( o.concept_id = 1148, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS infant_recieved_arv, 
	MAX( IF ( o.concept_id = 163322, o.value_text, NULL ) ) AS infant_reason_refused_arv,
	MAX(o.obs_group_id) AS mother_child_id,
	MAX( IF ( o.concept_id = 164803, o.value_text, NULL ) ) AS infant_number,
	MAX(
	IF
		( o.concept_id = 1151, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS breastfeeding,
	MAX(
	IF
		( o.concept_id = 1148, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS arv_prophylaxis_status,
/* 1148 used in L&D and Infant PNC with different codes*/
	MAX(
	IF
		( o.concept_id = 1658, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS arv_prophylaxis_adherence,
	MAX(
	IF
		( o.concept_id = 164928, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS ctx_prophylaxis_status,
	MAX(
	IF
		( o.concept_id = 161652, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS ctx_prophylaxis_adherence,
	MAX(
	IF
		( o.concept_id = 164929, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS hiv_test_type,
	MAX(
	IF
		( o.concept_id = 164461, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS dnapcr_test_result,
	MAX(
	IF
		( o.concept_id = 164860, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS rapid_test_result,
	MAX(
	IF
		( o.concept_id = 164930, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS confirmatory_test_done,
	MAX(
	IF
		( o.concept_id = 164460, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS final_test_result,
	MAX(
	IF
		( o.concept_id = 164931, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS art_link,
	MAX(
	IF
		( o.concept_id = 160433, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS transfer_status,
	MAX(
	IF
		( o.concept_id = 160753, o.value_datetime, NULL ) 
	) AS infant_hiv_negative_outcome_date,
	MAX( IF ( o.concept_id = 164954 AND o.value_coded = 1, (SELECT NAME FROM concept_name WHERE concept_id = 164954 AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en'), NULL ) ) AS missing_infant_hiv_negative_outcome_date,
	MAX(
	IF
		( o.concept_id = 164921, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS hiv_exposure,
	(SELECT NAME FROM location WHERE location_id = MAX( IF ( o.concept_id = 160535, o.value_text, NULL ))) AS transfer_from,
	MAX( IF ( o.concept_id = 160534, o.value_datetime, NULL ) ) AS transfer_in_date,
	MAX( IF ( o.concept_id = 164946 AND o.value_coded = 1, (SELECT NAME FROM concept_name WHERE concept_id = 164946 AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en'), NULL ) ) AS missing_transfer_in_date,
	MAX(
	IF
		( o.concept_id = 164916, ( SELECT NAME FROM concept_name WHERE concept_id = o.value_coded AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en' ), NULL ) 
	) AS transfer_out,
	MAX( IF ( o.concept_id = 160649, o.value_datetime, NULL ) ) AS transfer_out_date,
	MAX( IF ( o.concept_id = 164950 AND o.value_coded = 1, (SELECT NAME FROM concept_name WHERE concept_id = 164950 AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en'), NULL ) ) AS missing_transfer_out_date,
	(SELECT NAME FROM location WHERE location_id = MAX( IF ( o.concept_id = 159495, o.value_text, NULL ) ) ) AS transfer_out_to,
	MAX( IF ( o.concept_id = 1543, o.value_datetime, NULL ) ) AS death_date,
	MAX( IF ( o.concept_id = 164947 AND o.value_coded = 1, (SELECT NAME FROM concept_name WHERE concept_id = 164947 AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en'), NULL ) ) AS missing_death_date,
	MAX( IF ( o.concept_id = 5096, o.value_datetime, NULL ) ) AS next_visit_date,
	MAX( IF ( o.concept_id = 164941 AND o.value_coded = 1, (SELECT NAME FROM concept_name WHERE concept_id = 164941 AND concept_name_type = 'FULLY_SPECIFIED' AND locale = 'en'), NULL ) ) AS missing_next_visit_date,
	o.voided
	FROM
	ds_revised_patient vp
	LEFT JOIN encounter e ON vp.patient_id = e.patient_id
	LEFT JOIN obs o ON e.encounter_id = o.encounter_id 
	LEFT JOIN person p ON e.patient_id = p.person_id
	LEFT JOIN location l ON vp.location_id = l.location_id
	LEFT JOIN location_uids_ lu ON l.uuid = lu.ptracker_uuid -- revert back to lu.ptracker_uuid once corrected
	LEFT OUTER JOIN relationship r ON e.patient_id = r.person_b
	LEFT JOIN encounter_type et ON et.encounter_type_id = e.encounter_type 
 GROUP BY
e.encounter_id, 
vp.patient_id,
o.obs_group_id