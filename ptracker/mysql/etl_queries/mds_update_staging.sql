/*
Version: 	1.0
Author:		Ezekiel K. Chiteri
Date:		03 April 2018

Change log:
Date:		03 April 2018
Create staging table for mds_encounter_mch from vw_revised_encounter_mch
*/ 
DELIMITER $$
DROP PROCEDURE
IF
	EXISTS `mds_update_staging` $$ CREATE PROCEDURE `mds_update_staging` ( ) BEGIN
	TRUNCATE mds_encounter_mch;
	INSERT INTO mds_encounter_mch SELECT * FROM ds_revised_encounter_mch;

UPDATE mds_encounter_mch AS t1,
(SELECT breastfeeding, patient_id, encounter_datetime
FROM mds_encounter_mch WHERE breastfeeding IS NOT NULL AND visit_type = 'Mother Postnatal'
GROUP BY patient_id
) AS t2
SET t1.infant_breastfeeding = t2.breastfeeding
WHERE t1.mother_id = t2.patient_id AND t1.encounter_datetime = t2.encounter_datetime;
	
END