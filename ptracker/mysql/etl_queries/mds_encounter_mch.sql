/*
Version: 	1.0
Author:		Ezekiel K. Chiteri
Date:		03 April 2018

Change log:
Date:		03 April 2018
Create staging table for mds_encounter_mch from ds_revised_encounter_mch
*/
DROP TABLE
IF
	EXISTS mds_encounter_mch;
CREATE TABLE mds_encounter_mch AS SELECT * FROM ds_revised_encounter_mch;