/*
Version: 	1.1
Author:		Ezekiel K. Chiteri
Date:		03 Oct 2017

Change log:
Date:		03 Oct 2017
Change log:
Date:		10 Oct 2017
Change log:
Date:		15 Jan 2018
Change log:
Date:		2 Feb 2018
Change log:
Date:		21 Feb 2018
Change log:
Date:		22 Feb 2018
Change log:
Date: 03 April 2018
View prefix name change
Change log:
Date: 12 July 2018
Added: creator and date_created

Change log:
Date: 23 January 2019
Added: Added voided patient identifier

Change log:
Date: 26 August 2020
Added: Contact as phone number from person_attribute
*/

DROP VIEW
IF
	EXISTS ds_revised_patient;  
CREATE VIEW ds_revised_patient AS
SELECT
pt.patient_id,
pi.identifier_type,
pi.identifier,
MAX(IF ( pi.identifier_type = 5, pi.identifier, NULL)) AS ptracker_id,
MAX(IF ( pi.identifier_type = 4, pi.identifier, NULL)) AS art_number,
pn.given_name,
pn.middle_name,
pn.family_name,
pn.uuid,
pa.value AS contact,
p.gender,
p.birthdate,
r.person_a AS mother_id,
( CASE p.dead WHEN 0 THEN 'No' WHEN 1 THEN 'Yes' END ) AS dead,
pi.location_id,
pt.creator AS patient_creator,
pt.date_created AS patient_date_created,
pi.voided AS patient_identifier_voided
FROM
	patient pt
	INNER JOIN person p ON pt.patient_id = p.person_id
	LEFT OUTER JOIN patient_identifier pi ON pt.patient_id = pi.patient_id
	INNER JOIN person_name pn ON pt.patient_id = pn.person_id 
	LEFT OUTER JOIN relationship r ON pt.patient_id = r.person_b
    LEFT JOIN person_attribute pa ON pt.patient_id = pa.person_id
GROUP BY
pt.patient_id;